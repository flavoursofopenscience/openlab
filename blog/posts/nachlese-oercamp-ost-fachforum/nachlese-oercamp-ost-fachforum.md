<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Nachlese: OERFestival: #OERcamp17 Ost plus OER-Fachforum 2017 in Berlin

## Vier-Etappen-Tournee rund um das Thema Bildung mit freien Bildungsressourcen.

Tags:

-->

[Pfefferberg OERde17 Camp](http://www.oercamp.de/17/ost/bildergalerie/). Tilman Vogler für OERde, [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/). Berlin, 27.11.2017.

![Frontalansicht auf den Eingang zum Pfefferberg – ein altes Brauereigelände. Der Eingang besteht aus mehreren Torbögen und Säulen, eine Treppe führt hinauf in den Innenhof.](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-oercamp-ost-fachforum/pfefferberg-oerde17-camp-tilman-vogler.jpg)

Das [OERcamp Ost](http://www.oercamp.de/17/ost/) eröffnete am 27. November das Festivalgelände am Pfefferberg zur Vier-Etappen-Tournee rund um das Thema Bildung mit freien Bildungsressourcen. Nach den Camps in München (Süd), Köln (West) und Hamburg (Nord) rief nun Berlin (Ost) zum *last order 2017!* an die Barcamp-Stationen auf. Mit insgesamt 92 Workshops und Informationsrunden waren es zwei volle Programmtage mit eng getaktetem [Veranstaltungsplan](https://docs.google.com/spreadsheets/d/1Rh0_mLcce2Sz6SRJzkAtUOGnP8_dQnF0xt4H8kGH1MA/edit#gid=1912764379). Die Veranstalter, der gemeinnützige Verein ZLL 21 e.V. und die Organisation der *[Agentur J&K – Jöran und Konsorten](https://www.joeran.de)* betiteln den Höhepunkt 2017 rund um Open Educational Resources im deutschsprachigen Raum als **OER-Festival**. Im Gegensatz zu den bereits vergangenen Camps vereinte das Festival weitere Veranstaltungsformate unter einem Dach - diese beinhalteten:

-   OERcamp – das Treffen der Praktiker\*innen (27./28.11.)
-   OER-Fachforum – als Dialog der Macher*innen und Multiplikator*innen (28./29.11.)
-   OER-Award – die Auszeichnung für Open Educational Resources im deutschsprachigen Raum (mit Preisverleihung am 27.11.)
-   sowie die Präsentation des OER-Atlas – die gebündelte Bestandsaufnahme der \#OERde-Landschaft (veröffentlicht am 28.11.)

### OERcamp

![Vogelperspektive auf den Veranstaltungssaal und der großen Bühne](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-oercamp-ost-fachforum/oerde17-camp-tilman-vogler-tag-1.jpg)

[OERcamp Ost: Große Bühne](http://www.oercamp.de/17/ost/bildergalerie/). Tilman Vogler für OERde, [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/). Berlin, 27.11.2017.

Zwölf Seminarräume und drei zusätzliche Räume für den Empfang (Foyer), Kantine und Rezeption/Garderrobe bildeten das Festivalgelände, auf dem mit knapp 300 Personen die höchste Teilnehmendenzahl aller OERCamps erreicht wurde und restlos ausgebucht war.

![Statistik der vier OERcamps (Süd - West - Nord - Ost). Anmeldungen an nach Reihenfolge 97, 203, 201, 291. Abgehaltene Workshops nach Reihenfolge 21, 45, 43, 44 und der Abgehaltenen Sessions nach Reihenfolge 22, 28, 33, 48.](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-oercamp-ost-fachforum/oercamps-2017-eckdaten.png)

Einzusehen unter: [oercamp.de/2017/11/27/oercamp-2018](http://www.oercamp.de/2017/11/27/oercamp-2018/).

Die Vielfalt der Angebote am Camp zeichnete sich durch das Engagement der Community, vieler vom Bundesministerium für Bildung und Forschung (BMBF) geförderter Projekte sowie einer stetig ansteigenden Anzahl an Initiativen aus. Das [SynLLOER](https://openlab.blogs.uni-hamburg.de/)-Team und Freunde brachten sich ebenfalls aktiv ins Festivalgeschehen ein. Mit zwei Barcamp-Sessions zu den Themen *[Bildungsgenuss – Was guter Kaffee und gute Bildung gemeinsam ausmacht](http://pad.o-e-r.de/p/oercamp17-ost-b3-balkonraum)* sowie *[Bildungspolitische Herausforderungen für OER](http://pad.o-e-r.de/p/oercamp17-ost-a4-schwarzer-pfeffer)* ging es insbesondere um die Reflexion über gemachte Erfahrungen sowie eine rege Diskussion von Chancen, Potentialen und gemeinsamen Wegen zu einer basisdemokratischen Bildungspolitik mittels OER.

### OER-Fachforum

Das OER-Fachforum 2017 bot 50 Sprecher\*innen und rund 35 Stunden [Programm](https://open-educational-resources.de/wp-content/uploads/Programm_LineUp_FachforumOERde17.pdf) auf. Speziell interessant war hier die Perspektive auf internationale Projekte wie die norwegische Nasjonal digital læringsarena / Norwegian digital learning arena (NDLA) als Blick "über den Tellerrand".

> Das OER-Fachforum verfolgt das Ziel, die umfangreiche und bunte Landschaft im deutschsprachigen Raum zu OER vorzustellen und ihren Austausch zu fördern. Dafür werden zwei Kernzielgruppen eingeladen: - OER-Macher*innen, also Organisationen, Initiativen, Projekte, die bereits zu OER arbeiten (Produzent*innen, Aktivist*innen, Wissenschaftler*innen etc.) - OER-Multiplikator*innen, also Entscheider*innen in Behörden und Ministerien, in Aus- und Fortbildungsverantwortung, in Verbänden und Stiftungen, in Bildungsinstitutionen aller Bereiche.

Quelle: open-educational-resources.de: OER-Fachforum 2017 – Macher*innen treffen Entscheider*innen\*. Einzusehen [hier](https://open-educational-resources.de/veranstaltungen/17/fachforum/). Stand und zuletzt aufgerufen am 14.02.2018 18:00 Uhr.

#### Bandauftritt H5P

Die enge Korrelation aus OERCamp und Fachforum zeigte Synergie-Effekte. Bekannte und Freunde aus der OER-Community brachten ihr Wissen aus dem Produzent*innen-Alltag in das Fachforum mit ein. Mit einem **Bandauftritt** unter dem Motto: \*“We’re getting the band back together …”* und der Intention: *We are on a mission from BMBF. We have a band powerful enough to turn knowledge into OER* stellten Matthias Andrasch ([@m\_andrasch](https://twitter.com/m_andrasch)), Lisa Rosenbaum, Manfred Steger ([@manfred\_steger](https://twitter.com/manfred_steger)), Tobias Steiner ([@cmplxtv\_studies](https://twitter.com/cmplxtv_studies)) und Oliver Tacke ([@otacke](https://twitter.com/otacke)) das webbasierte Tool [H5P](https://h5p.org/) auf den OER-Prüfstand.

![Alle Vortragenden auf einer schwarz-weiss Collage mit dem Slogan: We are on a mission from BMBF. We have a band powerful enough to turn knowledge into OER.](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-oercamp-ost-fachforum/oer-mission.png)

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/80x15.png)](http://creativecommons.org/licenses/by/4.0/) H5P-Band und Intention von [Manfred Steger](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-oercamp-ost-fachforum/oer-mission.png) ist lizenziert unter einer [Creative Commons Namensnennung 4.0 International Lizenz](http://creativecommons.org/licenses/by/4.0/).

Mit der Open-Source-Software H5P lassen sich in intuitiver Weise interaktive Materialien für das Web erstellen. Die hierfür erstellte Präsentation ist mit H5P erstellt und selbstredend auch eine OER:

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=20" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/) H5P-Präsentation "Beitrag OER-Fachforum 2017 Berlin: Bandauftritt H5P" von [H5P Band](https://openlab.blogs.uni-hamburg.de/beitrag-oer-fachforum-2017-berlin-bandauftritt-h5p/) ist lizenziert unter einer [CC BY 4.0 International Lizenz](http://creativecommons.org/licenses/by/4.0/).

Nach einer kurzen Vorstellungsrunde veranschaulichten wir anhand von Praxisbeispielen aus dem Schul- und Hochschulbereich Einsatzgebiete sowie Vor- und Nachteile von H5P (Folie 9-13). Anschließend folgte eine detaillierte Ansicht beim Einsatz in Blended-Learning-Szenarien (Folie 14-15). Zum Schluss wagten wir einen kritischen Blick darauf, wo die Reise hingehen könnte und welche Probleme und Chancen sich speziell für die Erstellung von OER ergeben.

#### OER-Award

Für die Verleihung des diesjährigen OER-Awards gab es neun Kategorien, in denen je nach Bildungsbereich (01 Schule, 02 Hochschule, 03 Weiter-/Erwachsenenbildung, 04 Aus-/Berufsbildung, 05 The Great Wide Open, 06 Sonder-Award „OER über OER“, 07 Sonder-Award „Qualität für OER“, 08 Sonder-Award „Geschäftsmodelle für OER“, 09 Sonder-Award „Infrastruktur für OER“) OER-Angebote eingereicht werden konnten. Insgesamt kamen 89 Bewerbungen aus den unterschiedlichsten Fachrichtungen zusammen. Diese wurden von der Jury, welche aus Expert\*innen aus Praxis und Theorie rund um OER stammen, bewertet und am 27.11. im Rahmen einer Abendveranstaltung des OER-Festivals verliehen. Jede/r Gewinner/in bekam einen Award und eine Laudatio.

Die Gewinner sind aufgelistet unter:

-   <https://open-educational-resources.de/veranstaltungen/17/award/>

Jede Einreichung wurde zum Einen anhand eines Fragebogens vorgestellt, zum Anderen gab es immer die Möglichkeit, sich die Angebote im Netz anzuschauen.

Fazit & persönliche Eindrücke
-----------------------------

Alles in allem war das OERFestival 2017 eine hochinteressante Veranstaltung, die einen Querschnitts-Blick auf die im Themenfeld "OER" aktiven Akteure, Initiativen und Institutionen in Deutschland ermöglichte.

Reger Austausch war zu vielen, schon in früheren OERCamps diskutierten Fragen möglich und die unterschiedlichen Veranstaltungsformate förderten die kreative Aktivierung der Teilnehmenden. Oftmals wurden aktuelle Zwischenstände von von früheren OERCamps bekannten Projekten vorgestellt, und in machen Sessions blieb auch Zeit, neue Perspektiven und Herangehensweisen zu diskutieren.

![Whiteboard: Notizen des Workshops "Bessere OER mit Public Domain"](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-oercamp-ost-fachforum/cc0-workshop-lambert.jpg)

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/80x15.png)](http://creativecommons.org/licenses/by/4.0/) Whiteboard: Notizen des Workshops "Bessere OER mit Public Domain" von [Tobias Steiner](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-oercamp-ost-fachforum/cc0-workshop-lambert.jpg) ist lizenziert unter einer [Creative Commons Namensnennung 4.0 International Lizenz](http://creativecommons.org/licenses/by/4.0/).

Im Gegensatz zu früheren OERCamps ging gefühlt leider eine gewisse Beschwingtheit bzw. Leichtigkeit abhanden, da durch die hohe Anzahl von Sessions und die daraus entstehende ziemlich strenge organisatorische Taktung der Session-Planung, die durch die geografische Weitläufigkeit des Veranstaltungsgeländes verstärkt wurde, kaum Zeit blieb, Gedanken und Impulse aus einer Session auch danach spontan weiterzuführen, da gleich zum nächsten Slot geeilt werden musste.

![Lange Schlange von Session-Teilgebenden beim OERCamp Ost 2017](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-oercamp-ost-fachforum/barcamp-session-queue.jpg)

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/80x15.png)](http://creativecommons.org/licenses/by/4.0/) Lange Schlange von Session-Teilgebenden beim OERCamp Ost 2017 von [Tobias Steiner](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-oercamp-ost-fachforum/barcamp-session-queue.jpg) ist lizenziert unter einer [Creative Commons Namensnennung 4.0 International Lizenz](http://creativecommons.org/licenses/by/4.0/).

Dies hat uns direkt vor Ort dann auch dazu animiert, die Initiative zu ergreifen und die - weiter oben schon genannte - Session *[Bildungsgenuss – Was guter Kaffee und gute Bildung gemeinsam ausmacht](http://pad.o-e-r.de/p/oercamp17-ost-b3-balkonraum)* als Reaktion auf diese gefühlte Hektik und im Sinne einer zumindest temporären Entschleunigung anzubieten. Weiterführende Gedanken dazu finden sich in der [Session-Dokumentation](http://pad.o-e-r.de/p/oercamp17-ost-b3-balkonraum)

Speziell für die BMBF-geförderten Projekte der OERinfo-Linie entwickelte sich eine fast schon familiäre Atmosphäre, die einerseits vielfach geschätzt wurde, da man mit vielen schon aus anderen OERCamps bekannten Personen zusammenkam. Andererseits fehlte die Öffnung für potentiell neue Zielgruppen, durch die gerade im Kontext des Veranstaltungsortes in der Hauptstadt sowie der grundlegenden Mission der BMBF-Förderlinie der Awareness-Schaffung Rechnung tragend noch zusätzlich wertvolle neue Impulse eingebracht hätten werden können - ein Punkt, der auch in der temporären OER-Wohngemeinschaft mit anderen Teilnehmenden rege diskutiert wurde. Insbesondere eine noch weitergehende Öffnung hin zu der Zielgruppen von Lehrenden an Schulen sowie Lehramtsstudierenden scheint uns hier ein wichtiger Punkt, zu dem auch die OERCamps beitragen können.

![Nächtlicher Blick über die Dächer Berlins aus der OER-WG am Platz der Zionskirche](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-oercamp-ost-fachforum/ueber-den-daechern-berlins-oer-wg2017.jpg)

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/80x15.png)](http://creativecommons.org/licenses/by/4.0/) Nächtlicher Blick über die Dächer Berlins aus der OER-WG am Platz der Zionskirche von [Tobias Steiner](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-oercamp-ost-fachforum/ueber-den-daechern-berlins-oer-wg2017.jpg) ist lizenziert unter einer [Creative Commons Namensnennung 4.0 International Lizenz](http://creativecommons.org/licenses/by/4.0/).

### Lese-Empfehlung: weitere Berichte und Gedanken zum OERFestival17

-   [Markus Deimann, "Das Ende der Leichtigkeit: Rückblick auf das OERde-Festival"](https://markusmind.wordpress.com/2017/12/01/das-ende-der-leichtigkeit-rueckblick-auf-das-oerde17-festival/amp/)
-   [Christian Friedrich, "Mainstreaming OER: Dafür braucht es weniger zentrale OER-Events"](https://christianfriedrich.org/deutsch/mainstreaming-oer-dafur-braucht-es-weniger-zentrale-oer-events/)
-   zudem auch: [Dokumentations-Sammlung \#OERde17](https://open-educational-resources.de/veranstaltungen/17/dokumentation/)


## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Nachlese: OERFestival: #OERcamp17 Ost plus OER-Fachforum 2017 in Berlin</span> von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/?p=1245" property="cc:attributionName" rel="cc:attributionURL">Manfred Steger, Nina Rüttgens, Tobias Steiner für SynLLOER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Namensnennung 4.0 International Lizenz</a>.
