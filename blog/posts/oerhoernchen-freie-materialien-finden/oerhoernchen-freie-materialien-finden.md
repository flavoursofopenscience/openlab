<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# OERhörnchen: Freie Materialien finden

## Das OERhörnchen geht gezielt auf die Suche nach Lehr-/Lernmaterialen, welche frei veränderbar sind und wieder veröffentlicht werden dürfen - sogenannte Open Educational Resources (OER).

Tags:

-->

Das [OERhörnchen](https://oerhoernchen.de/#) ist ein offenes Projekt von [Matthias Andrasch](https://matthias-andrasch.de/impressum/).

Per obigem Link gelangt man direkt zur Seite des OERhörnchens. Alternativ kann man hier unten auch direkt eine Suchanfrage platzieren.

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=22" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

## Lizenz

<a rel="license" href="https://creativecommons.org/publicdomain/zero/1.0/"><img width="80px" alt="Icon CC0 / CC Zero" style="border-width:0" src="https://oerhoernchen.de/img/license_icons/cc0.png" /></a><br />Das Projekt "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">OERhörnchen</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://oerhoernchen.de/" property="cc:attributionName" rel="cc:attributionURL">Matthias Andrasch</a> steht unter einer <a rel="license" href="https://creativecommons.org/publicdomain/zero/1.0/">Creative Commons Zero (CC0 1.0 Universal) Lizenz</a>.
