<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Herzlich Willkommen auf den Seiten des SynLLOER Projekt-Blogs

## Das Projekt SynLLOER steht schon in den Startlöchern

Tags:

-->

Ab Januar 2017 widmet sich das Projekt _SynLLOER_ (Synergien für Lehren und Lernen durch OER) grundlegenden Fragen zu OER (Open Educational Ressources) wie:

*   Wo finde ich OER?
*   Wie kann man aus eigenen Lehrmaterialien OER machen?
*   Welche rechtlichen Probleme gibt es zu beachten, welche technischen Vorgaben?

Mit Informationsveranstaltungen an den Hamburger Hochschulen und Schulen sowie einem offenem Werkstattangebot wollen wir Lehrende (und Lernende) auf OER aufmerksam machen, aktivieren, und ihnen ermöglichen, erste Erfahrungen in der Welt der Open Educational Ressourcees sammeln zu können. In den nächsten Wochen finden Sie an dieser Stelle weitere Informationen und Details...
