<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Nachlese: #Hack4OER, OER-IT-Sommercamp 2017, Weimar – 31.7. - 2.8.2017

## Wie sollen künftige OER-Infrastrukturen in der Bildungwelt archtektonisch aussehen, damit keine monolithischen Türme die digitale Landschaft zeichnen?

Tags:

-->

Dieser Verständnis-Problematik nahmen sich beim OER-IT-Sommercamp 2017 IT-Spezialisten aus unterschiedlichen Bildungsbereichen an: [@edu-sharing](https://twitter.com/edusharing), [@moodle](https://twitter.com/moodle), [@LTI](https://twitter.com/LTI_Global), [@OERinfo](https://twitter.com/OERinfo), [@oerworldmap_de](https://twitter.com/oerworldmap_de), [@oncampusfhl](https://twitter.com/oncampusfhl), [@DBS_20](https://twitter.com/DBS_20), [@memuchoWissen](https://twitter.com/memuchoWissen), [@OER_JOINTLY](https://twitter.com/OER_JOINTLY), [@SynLLOER](https://twitter.com/SynLLOER), [@uhhewmz](https://twitter.com/uhhewmz), [u.v.m.](http://jointly.info/2017/08/03/oer-it-sommercamp-ergebnisse/)

Mit einem Appell zur OER-Kooperation begann der Hackathon. Spontane Lightning Talks regte die Kreativität der Teilnehmenden an und zeigte zudem die technologische Vielfältigkeit des Event. Der Fokus gerichtet auf **Metadaten und Harvesting-Technologien/Standards**, starteten die Teams in den Wettstreit. Thematisch bildeten sich folgende Gruppen:

*   OER-Portale
*   OER in LMS und Autorenwerkzeuge
*   OER Nutzerorientierung und Konzepte

![Das Bild zeigt eine Metaplanwand mit einem Zettel auf dem Steht OER-Portale vernetzen](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-oer-infrastrkutur/oer-portale-vernetzen.jpg) _Bildnachweis: [OER-Portale vernetzen](https://www.flickr.com/photos/fotoexperiencede/35608855434), Manfred Steger, [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)_.

Zielgerichtet überlegten sich die Teilnehmenden wie sie OER-Portale, Lernplattformen und andere OER-Werkzeuge _zusammencoden_ können. SynLLOER arbeitete in der Gruppe **OER Nutzerorientierung und Konzepte** mit. Unser gemeinsames Ziel war es, ein Manifest für OER-Infrastrukturen an normativen Anforderungen vorzudenken und zu skizzieren.

![Das Bild zeigt eine Verknüpfung Kategorien, die für eine OER-Infrastruktur berücksichtigt werden müssen wie z.B. Qualitätsmanagement, Rechtliches, technologische Standards, Organisationsmodelle uvm.](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-oer-infrastrkutur/brainstorming-oer-infrastruktur.jpg) _Bildnachweis Foto: [Brainstorming OER-Infrastruktur](https://www.flickr.com/photos/fotoexperiencede/35608853394/), Manfred Steger, [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)_.

Der Wissensaustausch über Schnittstellen, Metadaten, Formate, Organisations- sowie Betriebsstrukturen und technologische Standards auf Grundlage der [DIPF Machbarkeitsstudie](https://www.dipf.de/de/forschung/projekte/machbarkeitsstudie-zum-aufbau-und-betrieb-von-oer-infrastrukturen-in-der-bildung) Kriterien:

*   Rechtliche Sicherheit
*   moderne Bildung
*   Individualisierung und Binnendifferenzierung (Menschen und Länder) und Inklusion
*   Lehrenden-Kulturwandel
*   Transparenz
*   effiziente Nutzung von Quellen

war für einen Überblick förderlich, führte jedoch mit zunehmender Intensität und Dauer in Richtung _Status quo_. Eine allgemeingültige OER-Infrastruktur zu definieren, gleicht dem Turmbau zu Babel und begann mit der eingetreten _Sprachverwirrung_. Weder die spezifischen Anforderungen per se noch die divergenten Voraussetzungen der Einzelprojekte/Länder konnten eine Basis schaffen. Das Konzeptteam hat diese Problemstellung frühzeitig erkannt und der bekanntlichen _Zerstreuung der Architekten_ entgegengewirkt.

Vom konkreten Konzept abweichen hin zur prozessorientierten Strategie war die agile Teamentscheidung. Es entstand ein Strukturkonzept mit Ansätzen und Lösungsvorschlägen, wie eine OER-Infrastruktur aus verschiedenen Blickwinkeln durchdacht werden kann: ![Das Bild zeigt ein Strukturkonzept mit verschiedenen Bereichen für OER-Infrastrukturen.](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-oer-infrastrkutur/strukturkonzept-oer-infrastruktur.jpg) _Bildnachweis Foto: [Strukturkonzept OER Infrastruktur](https://www.flickr.com/photos/fotoexperiencede/36306721741/), Manfred Steger, [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)_. _Gemeinsame OER-Services anstatt redundanter Entwicklung_ war der Konzept-Slogan. Das nachhaltig gedachte IT-Konzept wurde von der Jury, die aus allen Teilnehmenden des Hackathon bestand, angenommen und mit dem vierten Platz belohnt:

<iframe id="iframe_container" src="https://prezi.com/embed/bk9bdp4tzk31/?bgcolor=ffffff&amp;lock_to_path=0&amp;autoplay=0&amp;autohide_ctrls=0&amp;landing_data=bHVZZmNaNDBIWnNjdEVENDRhZDFNZGNIUE43MHdLNWpsdFJLb2ZHanI5aEcwTEFwUm9RMmpsbnZOZm1HRDltSllBPT0&amp;landing_sign=HzoheYpZ5P-1EhI4Zh3nbyhkcJjpUmOVk9kOuZUKlnQ" allowfullscreen="allowfullscreen" width="550" height="400" frameborder="0"></iframe>

Sieger des diesjährigen OER-Hackathons war Thomas Wegricht (Bildungsportal Sachsen) mit einem System Plugin für OPAL – _OPAL meets edu-sharing_ (LMS-Anbindung an das OER-Repository edu-sharing).

Eine Zusammenfassung aller Ergebnisse des Events wurde vom Jointly Team bereits veröffentlicht:
[jointly.info/2017/08/03/oer-it-sommercamp-ergebnisse](http://jointly.info/2017/08/03/oer-it-sommercamp-ergebnisse/).

![Foto des Konferenzraums mit mehreren Teilnehmenden, die gespannt in ihre Laptops blicken oder gemeinsam an OER-Problematiken arbeiten.](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-oer-infrastrkutur/hack4oer-teilnehmende.jpg) _Bildnachweis: Hack4OER Teilnehmende, Manfred Steger, [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)_.

Ähnlich dem [Train-us-OER-Trainer](https://openlab.blogs.uni-hamburg.de/nachleseoerjointly17b-train-us-oer-trainer/) Event im Juni 2017 brachte die OER IT-Expertinnen- und IT-Expertenrunde wichtige Erkenntisse und offene Fragen hervor. Altbekanntes resümieren, Interdependenzen erkennen und vereint ein gemeinsames OER-Ziel verfolgen, lautet das Fazit des 3 Tage IT-Workshops.

Der ausgesähte Open-Source-Gedanke ist zu einer ausgereiften sozialen Bewegung im Internet herangewachsen, eben nicht nur bei der Software-Entwicklung. Mit Open Access und Open Educational Resources verbreitet sich der korrelierende Gedanke auch im Bildungsbereich. Die zunehmend digital gerpägte Wissensgesellschaft fordert nun auch implizit geeignete Zugänge an öffentlichen Einrichtungen. Doch wo findet die Community Platz dafür, ihre Gedanken auszutauschen, kollaborativ Projekte voranzutreiben und Ergebnisse zur Weiterentwicklung bereitzustellen? Im Bereich IT und Software gibt es darauf eine Antwort – GitHub.

Im Bildungsbereich tendiert, zumindest bei den Großanbietern wie z.B. [edX](https://www.edx.org) und [coursera](https://www.coursera.org/), der solidarische Aspekt des Teilens in Richtung Monetarierung des Wissen abzuflachen. Die monolithischen Giganten gleichen in der OER-Landschaft einer Sackgasse, die Daten aus der Community und von Bildungsträgern generieren lassen, für ihre Zwecke aufbereiten und dann schnittstellenlos enden lassen. Die entstanden OER-Services des Hackathons verhalten sich konträr – über eine zentrale Plattform lassen sich Daten wie z.B. Metadaten oder ganze Materialien im Idealfall bidirektional mit einem kohärenten System abgleichen. Ein einfaches Beispiel hierfür wäre der Austausch einer Textdatei von Moodle nach Edu-Sharing und umgekehrt. Netzwerke anstatt Hierarchie – in dieser Denkweise verbirgt sich das Potenzial einer OER-Infrastruktur für alle Beteiligten.

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /> Nachlese "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">#Hack4OER, OER-IT-Sommercamp 2017, Weimar – 31.7. - 2.8.2017</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/nachlese-hack4oer-oer-it-sommercamp-2017-weimar-31-7-2-8-2017/" property="cc:attributionName" rel="cc:attributionURL">Manfred Steger für SynLLOER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0 International Lizenz</a>.
