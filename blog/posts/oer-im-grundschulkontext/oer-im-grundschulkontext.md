<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# OER im Grundschulkontext

## H5P-Präsentation zur Thematik: Der Weg zu freien Bildungsmaterialien mit – OER im Grundschulkontext.

Tags:

-->

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=30" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a> <br /> H5P-Präsentation "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Wenn alles offen ist, sind wir dann noch ganz dicht?- Der Weg zu freien Bildungsmaterialien</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/open-educational-resources-im-schulkontext/" property="cc:attributionName" rel="cc:attributionURL">Astrid Wittenberg, Celestine Kleinesper, Christina Schwalbe, Lucas Jacobsen, Klaas Opitz und Tobias Steiner für SynLLOER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0 International Lizenz</a>.
