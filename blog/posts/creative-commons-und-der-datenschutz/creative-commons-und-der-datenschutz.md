<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Creative Commons und offene Informationsmaterialien für den Datenschutz

## Workshop zu den Themen: Offenheit, Lizenzen, und der Datenschutz

Tags:

-->

Workshop am Donnerstag, den 22.02.2018 zu OER und Creative Commons-Lizenzen als Basis einer zukünftigen Erstellung von offenen Informationsmaterialien am Unabhängigen Landeszentrum für Datenschutz Schleswig-Holstein, Kiel.

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=25" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />H5P-Präsentation "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Offenheit, Lizenzen, und der Datenschutz</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/creative-commons-und-offene-informationsmaterialien-fuer-den-datenschutz/" property="cc:attributionName" rel="cc:attributionURL">Tobias Steiner, Manfred Steger und Valentin Dander für SynLLOER & LOERSH</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0 International Lizenz</a>.
