<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Impressionen des OERInfo-Auftaktworkshops in Frankfurt a. Main

## Am 12. und 13. Dezember fand der OERInfo-Auftaktworkshop am Deutschen Institut für Internationale Pädagogische Förderung in Frankfurt am Main statt - und SynLLOER war auch vor Ort.

Tags:

-->

Fragen wie

> Wer ist wer und wer macht was? Wie sollen Informationen untereinander ausgetauscht werden und wie kann die Vernetzung gelingen?

wurden rege diskutiert. Während ein [Kurzbeitrag der Informationsstelle OER](http://open-educational-resources.de/startworkshop-der-projektpartner/#more-6336) weitere Details aufzeigt, haben wir so manche Impressionen in visueller Form festgehalten. Viel Spaß beim Ansehen der Galerie.
