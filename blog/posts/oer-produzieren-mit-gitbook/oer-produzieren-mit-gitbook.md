<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# OER produzieren und veröffentlichen mit gitbook.com

## Handlungsorientierter Leitfaden zur Entwicklung einer OER mittels Gitbook.com

Tags:

-->

Im Kontext der SynLLOER-Workshopreihe, die in Kooperation mit dem Landesinstitut für Lehrerbildung und Schulentwicklung in Hamburg konzipiert ist, entstand das OER-Produktions-Modul: _OER erstellen und verteilen._ Diese Präsentation ist ein handlungsorientierter Leitfaden zur Entwicklung einer OER mit der Auszeichnungssprache Markdown und der Plattform gitbook.com, die mithilfe des Tools H5P erstellt wurde.

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=3" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a> <br /> H5P-Präsentation "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">OER produzieren und veröffentlichen mit gitbook.com</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/workshop-praesentation-oer-produzieren-und-veroeffentlichen-mit-gitbook-com/" property="cc:attributionName" rel="cc:attributionURL">Manfred Steger für SynLLOER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0 International Lizenz</a>.
