<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# #OERCamp17 Nord: Workshop-Folien "Digitalisierung und OER in der Metropolregion Hamburg"

## Unterlagen zur Digitalisierung und OER in der Metropolregion Hamburg

Tags:

-->

Hier finden sich die Folien zum #oercamp17-Workshop "Digitalisierung und OER in der Metropolregion Hamburg", der am 24.06.2017 während des OERCamps 17 Nord in Hamburg angeboten wird.

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=6" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a> <br /> H5P-Präsentation "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Digitalisierung und OER in der Metropolregion Hamburg</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/oercamp17-nord-workshop-folien-digitalisierung-und-oer-in-der-metropolregion-hamburg/" property="cc:attributionName" rel="cc:attributionURL">Tobias Steiner, Christina Schwalbe für SynLLOER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0 International Lizenz</a>
