<!-- Intro Text

 ## Herzlich Willkommen im OER OpenLab. Hier finden Sie die wichtigsten Informationen und Öffnungszeiten.

-->
Das openLab ist ein Projekt der Hamburg Open Online University (HOOU) am Universitätskolleg Digital der Universität Hamburg und bietet Raum zur Entwicklung von offenen Bildungspraktiken sogenannten [Open Educational Resources](https://de.wikipedia.org/wiki/Open_Educational_Resources). Es richtet sich dabei an Lehrende und Studierende der Universität Hamburg. 

**Was machen wir?**

Wir bieten Individuelle Beratungen für die offene Umgestaltung und Aufwertung von vorhandenem Materialien (Powerpoint, PDF etc.) an. Dabei können wir sowohl mit rechtlichen Fragen und bei der Recherche von freien Bildungsmaterialien helfen, als auch die eigene Erstellung von freien und digitalen Lehrmaterialien begleiten.


![Grafik mit folgenden Fragen: Bedeuten OER, dass jede und jeder mit meinem Werk machen kann,was sie oder er will? OER – wofür soll dasgutsein? Woran erkenne ich OER? Wo  finde ich OER von anderen, die mir weiterhelfen können? Wo veröentliche ich meine OER?  Was brauche ich, um selbst OER zu erstellen?](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/openlab-info/oer-fragen-openlab.png)

![Das vorherige Bild darf ohne Angaben frei für jegliche Zwecke verwendet werden.](https://mirrors.creativecommons.org/presskit/buttons/80x15/svg/cc-zero.svg) _"OER-Fragen" von blum Design und Konzeption / Universitaetskolleg Hamburg ist lizenziert unter einer [CC0 Lizenz](https://creativecommons.org/publicdomain/zero/1.0/deed.de)._

Sie erhalten bei uns Einblicke in die Nutzung von aktuellen Medien und Technologien wie [Markdown](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/blob/master/markdown-introduction/script/markdown-script-introduction.md), [GitLab](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/blob/master/gitlab-introduction/script/gitlab-script-introduction.md), [WordPress](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/blob/master/wordpress-introduction/script/wordpress-script-introduction.md), [GitBook](https://www.gitbook.com/), [H5P](https://h5p.org/) und anderen digitalen Helfern, die Ihren Lehr-Lern-Alltag bereichern können. Außerdem bieten wir Ihnen die Möglichkeit Werkzeuge, wie unseren 3D-Drucker, die Drohne oder die Oculus Rift auszuprobieren und zeigen Ihnen, wie Sie diese in verschiedenen Lehr- und Lernszenarien einbringen könnten.  

## Ansprechpersonen & Zeiten

>**[Astrid](https://www.hoou.uni-hamburg.de/kontakt/team/astrid-wittenberg.html), [Celestine](https://synlloer.blogs.uni-hamburg.de/synlloer-what-we-do/) & [Sophia Zicari](https://synlloer.blogs.uni-hamburg.de/synlloer-what-we-do/)**
>
>Kontakt: [synlloer@uni-hamburg.de](mailto:synlloer@uni-hamburg.de)
>
>Öffnungszeiten: Donnerstags von 11–16 Uhr in der [Schlüterstraße 51, Raum 4018/4019](https://goo.gl/maps/X5nKUupgEVQ2)
>
> Termine außerhalb der Öffnungszeiten jederzeit möglich nach Abstimmung per [E-Mail](mailto:synlloer@uni-hamburg.de).

## Ziele der Werkstatt

<!-- Bild Glühbirne Sprechblase

![Schwarzer Hintergrund auf dem eine Sprechblase mit Kreide gezeichnet wurde und in der Sprechblase ist eine Glühbirne.](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/openlab-info/pixabay-gedanken-idee-innovation-phantasie-212397.jpg)
![Das vorherige Bild darf ohne Angaben frei für jegliche Zwecke verwendet werden.](https://mirrors.creativecommons.org/presskit/buttons/80x15/svg/cc-zero.svg) _"Thoughts" von [Tero Vesalainen](https://pixabay.com/de/gedanken-idee-innovation-phantasie-2123970/) ist lizenziert unter einer [CC0 Lizenz](https://creativecommons.org/publicdomain/zero/1.0/deed.de)._

-->

![Kleiner Robotter der einen mit Glühbirne als Kopf, der einen Stecker in die Steckdose steckt.](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/openlab-info/light-bulb-3104355.jpg)
![Das vorherige Bild darf ohne Angaben frei für jegliche Zwecke verwendet werden.](https://mirrors.creativecommons.org/presskit/buttons/80x15/svg/cc-zero.svg) _"Light-Bulb" von [ColiN00B](https://pixabay.com/de/gl%C3%BChbirne-idee-selbstst%C3%A4ndig-3104355/) ist lizenziert unter einer [CC0 Lizenz](https://creativecommons.org/publicdomain/zero/1.0/deed.de)._

*   Beratungs- und Anlaufstelle für mediale Frage- und Problemstellungen zu den Themenbereichen:
    *   Digital Publishing
    *   OER allgemein
    *   Open Access
    *   Medienproduktion und -integration
    *   Open Source
    *   Lizenzierung
      
*   Aufbau und Förderung des Toolbaukastens mit u.a. <a href="https://www.gitbook.com/@synlloer" target="_blank" rel="noopener">GitBook</a>, <a href="http://www.sumo.uni-hamburg.de/, <a href="https://www.blogs.uni-hamburg.de/" target="_blank" rel="noopener">WordPress</a>, <a href="https://h5p.org/content-types-and-applications" target="_blank" rel="noopener">WP-H5P</a>, etc.
*   Fort-/Weiterbildung zu Urheberrecht, Offenheit, Digitalisierung und verwandten Themen
* Schulungen für einzelne digitale Werkzeuge zur Erstellung von Präsentationen, Arbeitsblättern, interaktiven Videos sowie lernbegleitender Blogs

![Banner mit dem Schriftzug OPENLAB – keine weiteren Informationen auf diesem Bild](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/openlab-info/openlab-banner-mid.jpg)

## Zielgruppen der Werkstatt

*   Durch Workshops aktivierte Lehrende von Hamburger Schulen und Hochschulen
*   Projektberatung /-unterstützung ergänzend zu bestehenden Strukturen, bspw. e-Learning-Büros, für Lehrende und Studierende der Universität Hamburg

## Modus/Form (Turnus, Veranstaltungsart)

*   openLab/Offene Werkstatt mit festen Zeiten, aktuell Donnerstags von 11–16 Uhr
*   Individualisierte Workshops für Kleingruppen während der Offenen Werkstatt oder nach Terminabsprache via <a href="mailto:synlloer@uni-hamburg.de" target="_blank" rel="noopener">synlloer@uni-hamburg.de</a>
*   **Grundhaltung: Hilfe zur Selbsthilfe**

![Darstellung der Öffnungszeiten (Donnerstags 11–18 Uhr in der Schlüterstraße 51, Raum 4018/4019) und der Kontaktadresse für Fragen und Workshop-Angebote und Wünsche. E-Mail an tobias.steiner@uni-hamburg.de ](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/openlab-info/openlab-banner-bottom.jpg)

![Icon Creative Commons Namensnennung – CC BY](https://mirrors.creativecommons.org/presskit/buttons/80x15/svg/by.svg) _Der Artikel "OER OpenLab der Universität Hamburg" Manfred Steger und Tobias Steiner ist lizenziert unter einer [CC BY International 4.0 Lizenz](https://creativecommons.org/licenses/by/4.0/)._
