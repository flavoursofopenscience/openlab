<!-- Intro Text

 ## Herzlich Willkommen im OER OpenLab. Hier finden Sie die wichtigsten Informationen und Öffnungszeiten.

-->

Im OpenLab der Universität Hamburg besteht die Möglichkeit für alle Personen, freie Bildungsmaterialien sog. [Open Educational Resources](https://de.wikipedia.org/wiki/Open_Educational_Resources) (OER) kennenzulernen. Wir helfen Ihnen dabei rechtliche Fragen zu klären, freie Materialien zu finden und für die Lehre einzusetzen. Darüber hinaus unterstützen wir gerne Ihr OER-Projekt bei der Materialerstellung, dem Einsatz in der Praxis und der zielgerichteten Verbreitung.

![Grafik mit folgenden Fragen: Bedeuten OER, dass jede und jeder mit meinem Werk machen kann,was sie oder er will? OER – wofür soll dasgutsein? Woran erkenne ich OER? Wo  finde ich OER von anderen, die mir weiterhelfen können? Wo veröentliche ich meine OER?  Was brauche ich, um selbst OER zu erstellen?](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/openlab-info/oer-fragen-openlab.png)

![Das vorherige Bild darf ohne Angaben frei für jegliche Zwecke verwendet werden.](https://mirrors.creativecommons.org/presskit/buttons/80x15/svg/cc-zero.svg) _"OER-Fragen" von blum Design und Konzeption / Universitaetskolleg Hamburg ist lizenziert unter einer [CC0 Lizenz](https://creativecommons.org/publicdomain/zero/1.0/deed.de)._

Sie erhalten bei uns Einblicke in die Nutzung von aktuellen Medien und Technologien wie [Markdown](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/blob/master/markdown-introduction/script/markdown-script-introduction.md), [GitLab](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/blob/master/gitlab-introduction/script/gitlab-script-introduction.md), [WordPress](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/blob/master/wordpress-introduction/script/wordpress-script-introduction.md), [GitBook](https://www.gitbook.com/), [H5P](https://h5p.org/) und anderen digitalen Helfern, die Ihren Lehr-Lern-Alltag bereichern können. Sie können sich jederzeit bei uns melden.

## Ansprechpersonen & Zeiten

>**[Manfred Steger](https://www.universitaetskolleg.uni-hamburg.de/kontakt/leitung-und-mitarbeitende/steger-manfred.html) & [Sophia Zicari](https://synlloer.blogs.uni-hamburg.de/synlloer-what-we-do/)**
>
>Kontakt: [synlloer@uni-hamburg.de](mailto:synlloer@uni-hamburg.de)
>
>Öffnungszeiten: Donnerstags von 14–18 Uhr in der [Schlüterstraße 51, Raum 4018/4019](https://goo.gl/maps/X5nKUupgEVQ2)
>
> Termine außerhalb der Öffnungszeiten jederzeit möglich nach Abstimmung per [E-Mail](mailto:synlloer@uni-hamburg.de).

## Ziele der Werkstatt

<!-- Bild Glühbirne Sprechblase

![Schwarzer Hintergrund auf dem eine Sprechblase mit Kreide gezeichnet wurde und in der Sprechblase ist eine Glühbirne.](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/openlab-info/pixabay-gedanken-idee-innovation-phantasie-212397.jpg)
![Das vorherige Bild darf ohne Angaben frei für jegliche Zwecke verwendet werden.](https://mirrors.creativecommons.org/presskit/buttons/80x15/svg/cc-zero.svg) _"Thoughts" von [Tero Vesalainen](https://pixabay.com/de/gedanken-idee-innovation-phantasie-2123970/) ist lizenziert unter einer [CC0 Lizenz](https://creativecommons.org/publicdomain/zero/1.0/deed.de)._

-->

![Kleiner Robotter der einen mit Glühbirne als Kopf, der einen Stecker in die Steckdose steckt.](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/openlab-info/light-bulb-3104355.jpg)
![Das vorherige Bild darf ohne Angaben frei für jegliche Zwecke verwendet werden.](https://mirrors.creativecommons.org/presskit/buttons/80x15/svg/cc-zero.svg) _"Light-Bulb" von [ColiN00B](https://pixabay.com/de/gl%C3%BChbirne-idee-selbstst%C3%A4ndig-3104355/) ist lizenziert unter einer [CC0 Lizenz](https://creativecommons.org/publicdomain/zero/1.0/deed.de)._

*   Beratungs- und Anlaufstelle für mediale Frage- und Problemstellungen zu den Themenbereichen:
    *   Digital Publishing
    *   OER allgemein
    *   Open Access
    *   Medienproduktion und -integration
    *   Open Source
    *   Lizenzierung
*   Rückkanal zu Infoveranstaltungen / Workshops / Fortbildungen des SynLLOER-Projekts (siehe <a href="https://uhh.de/synlloer-events" target="_blank" rel="noopener">https://uhh.de/synlloer-events</a> )
*   Aufbau und Förderung des Toolbaukastens mit u.a. <a href="https://www.gitbook.com/@synlloer" target="_blank" rel="noopener">GitBook</a>, <a href="http://www.sumo.uni-hamburg.de/DigitaleSkripte/" target="_blank" rel="noopener">Digitale Skripte</a>, <a href="https://www.blogs.uni-hamburg.de/" target="_blank" rel="noopener">WordPress</a>, <a href="https://h5p.org/content-types-and-applications" target="_blank" rel="noopener">WP-H5P</a>, etc.

![Banner mit dem Schriftzug OPENLAB – keine weiteren Informationen auf diesem Bild](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/openlab-info/openlab-banner-mid.jpg)

## Zielgruppen der Werkstatt

*   Durch SynLLOER-Workshops aktivierte Lehrende von Hamburger Schulen und Hochschulen
*   Projektberatung /-unterstützung ergänzend zu bestehenden Strukturen, bspw. e-Learning-Büros, für Lehrende und Studierende der Universität Hamburg

## Modus/Form (Turnus, Veranstaltungsart)

*   openLab/Offene Werkstatt mit festen Zeiten, aktuell Donnerstags von 14–18 Uhr
*   Individualisierte Workshops für Kleingruppen während der Offenen Werkstatt oder nach Terminabsprache via <a href="mailto:synlloer@uni-hamburg.de" target="_blank" rel="noopener">synlloer@uni-hamburg.de</a>
*   **Grundhaltung: Hilfe zur Selbsthilfe**

![Darstellung der Öffnungszeiten (Donnerstags 14–18 Uhr in der Schlüterstraße 51, Raum 4018/4019) und der Kontaktadresse für Fragen und Workshop-Angebote und Wünsche. E-Mail an tobias.steiner@uni-hamburg.de ](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/openlab-info/openlab-banner-bottom.jpg)

![Icon Creative Commons Namensnennung – CC BY](https://mirrors.creativecommons.org/presskit/buttons/80x15/svg/by.svg) _Der Artikel "OER OpenLab der Universität Hamburg" Manfred Steger und Tobias Steiner ist lizenziert unter einer [CC BY International 4.0 Lizenz](https://creativecommons.org/licenses/by/4.0/)._
