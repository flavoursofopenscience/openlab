<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Open Educational Practices: Wenn alles offen ist, sind wir dann nicht ganz dicht?

##  Themenbereich der Open Educational Practices mit einem Fokus auf Schule

Tags:

-->

Am 07.10.2017 wurden wir (Franziska Bellinger und Tobias Steiner) durch das [LOERSH](https://www.uni-flensburg.de/medienbildung/projekte/loersh/)-Projekt an die Europa-Universität Flensburg eingeladen, um dort gemeinsam mit Valentin Dander im Kontext der Lehramtsausbildung den Themenbereich der Open Educational Practices mit einem Fokus auf Schule zu beleuchten. Der Workshop erwies sich als hochspannend, da sowohl theoretische Aspekte zu OEP, als auch pragmatische Fragen zum Einsatz von OER und den Auswirkungen einer offenen Grundhaltung im Unterricht im Bezug auf dadurch entstehende Möglichkeiten und Herausforderungen rege diskutiert werden konnten. Die Kurzbeschreibung lautete wie folgt:

> Der Input thematisiert das Ziel gelebter Offenheit im Sinne von Open Educational Practices für den Hochschul- und Schulkontext. Diese werden als Bedingung eingeführt, um sich mit der Gestaltung offener Bildungsmaterialien (OER) zu befassen, eigene Ideen zu entwickeln und passende Medienformate dazu zu recherchieren. Open Educational Practices können demnach als Bindeglied zwischen offenen Materialien - Open Educational Resources - und professionellem (medien-)didaktischen Handeln verstanden werden.

Anbei stellen wir das im Workshop eingesetzte Folienset als OER zur freien Verfügung und Weiterverwendung.

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=13" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />
H5P-Präsentation "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Open Educational Practices: Wenn alles offen ist, sind wir dann nicht ganz dicht?</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/open-educational-practices-wenn-alles-offen-ist-sind-wir-dann-nicht-ganz-dicht/" property="cc:attributionName" rel="cc:attributionURL"> Franziska Bellinger, Tobias Steiner, Universität Hamburg</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0 International Lizenz</a>.
