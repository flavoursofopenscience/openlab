<!--

# H5P und xAPI und so: Educational Data Mining, Learning Analytics und die Öffnung der Bildungswelt

## Educational Data Mining, Learning Analytics und die Öffnung der Bildungswelt

-->

*Transparenzhinweis: Ich, der Autor dieses Beitrags, bin seit Jahren Verfechter von Initiativen wie der OER-Bewegung. Ich bin inzwischen aber auch Mitarbeiter der Firma Joubel, die als Kernteam an der Entwicklung der Open-Source-Software H5P beteiligt ist. Der Beitrag ist dennoch ein privates Produkt.*

---

Educational Data Mining (EDM) und Learning Analytics (LA) gehören gerade zum heißen Scheiß in der Ed-Tech-Szene. Bei beiden geht es darum, die von Lernenden erstellten Inhalte und anfallende Metadaten festzuhalten und auszuwerten. Wozu? Etwa, um Lernfortschritte erkennen zu können. Manch eine/r träumt auch davon, zukünftige Leistungen oder Probleme vorherzusagen. Noch einen Schritt weiter gehen diejenigen, die gleich Plattformen bauen wollen, die sich den Lernenden anpassen. Während das beim EDM eher reduktionistisch und automatisiert geschieht, setzt man bei LA eher auf eine ganzheitliche Sicht sowie menschliche Urteile und Interventionen (vgl. [Baker & Inventado, 2014, S. 62](#baker_inventado-2014)).

Beiden Ansätzen ist gemein, dass sie Daten benötigen. Ein Lieferant dafür kann die Software [H5P](https://h5p.org) sein. Hinter dem Kürzel verbirgt sich ein Autorenwerkzeug, das es erlaubt, vielfältige interaktive Aufgaben für das Netz zu erstellen. Die Bandbreite reicht von Multiple-Choice-Quizzes oder Lückentexten über interaktive Videos bis zu Ausspracheübungen. Gleichzeitig bietet H5P verschiedene Möglichkeiten, um Daten zu sammeln. Je nach Szenario fallen darunter nicht lediglich die erzielten Ergebnisse, sondern auch kleinteilige Ereignisse wie das Springen innerhalb eines Videos oder das Wechseln von einer Antwortoption zur anderen.

H5P wurde speziell für die Erstellung von offenen Bildungsmaterialien (Open Educational Resources, OER) konzipiert. Daher werden bei der Entwicklung nach Möglichkeit technische Entscheidungen vermieden, die offene Inhalte weniger offen machen (vgl. [Wiley, o. J.](#wiley-oj)). Konsequent steht die Software samt Quelltext kostenfrei und offen lizenziert zur Verfügung -- und sie bietet verschiedene Wege, um der Daten Herr zu werden.

### Auf Daten direkt über PlugIns zugreifen

H5P lässt sich zwar direkt auf der zugehörigen Website verwenden, allerdings ziehen es viele Organisationen und Personen vor, die Software in ihre eigene technische Infrastruktur einzubinden. Das geschieht derzeit über PlugIns, die für [Drupal](https://drupal.org/), [moodle](https://moodle.org/) und [WordPress](https://wordpress.org/) existieren. Die eigene Plattform zeichnet dann bei vielen der möglichen Interaktionen die Eingaben und Ergebnisse der Lernenden auf. Besonders bequem ist dies, wenn moodle eingesetzt wird. Das Lernmanagement-System stellt das sogenannte „Gradebook“ zur Verfügung, in das erzielte Ergebnisse eingetragen werden. Damit stehen die gewohnten Auswertungsmöglichkeiten von moodle zur Verfügung.

Für WordPress existiert das PlugIn [H5PxAPIkatchu](https://wordpress.org/plugins/h5pxapikatchu/), das die Ergebnisse der Inhaltstypen in tabellarischer Form speichert. Es bietet allerdings bewusst keine weiteren Optionen zur Analyse an. Diese Aufgabe überlässt es lieber darauf spezialisierter Software. Die gespeicherten Daten lassen sich dazu in eine CSV-Datei exportieren und individuell auswerten. Vermutlich die geringste Hürde stellt dafür eine Tabellenkalkulation dar. Weitaus mehr Möglichkeiten hat man allerdings mit gängigen Umgebungen zur Datenanalyse, etwa eine Kombination aus [Python](https://python.org/), [Jupyter](https://jupyter.org/) und [scikit-learn](http://scikit-learn.org/) oder [R](https://www.r-project.org/).

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=27" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

Für Drupal wiederum existiert ein Plugin mit dem schlichten Namen [Quiz](https://www.drupal.org/project/quiz). Es erweitert das Content-Management-System um diverse Reportingfunktionen für H5P-Inhalte.

### Die ExperienceAPI anzapfen und ...

Detaillierter und (nach erfolgter Einrichtung) bequemer als mit den zuvor genannten Möglichkeiten wird es, wenn man die ExperienceAPI ([xAPI](https://xapi.com/)) verwendet. Dahinter verbirgt sich eine von H5P unterstützte Schnittstelle, die zum Dokumentieren von Lernerfahrungen konzipiert wurde. Es spielt dabei keine Rolle, ob diese Erfahrungen sich auf online-gestützte Aufgaben beziehen, oder ob es sich um Erfahrungen in Präsenzveranstaltungen handelt. Bei Letzteren wird das manuelle Erfassen allerdings aufwändig bis unmöglich. Man kann in diesem Fall etwa zu Sensoren oder Audio- und Videoaufzeichnungen greifen (vgl. [Ochoa & Worsley, 2016, S. 214-215](#ochoa_worsley-2016)). Ethische und datenschutzrechtliche Fragen sollten bei aller Sammelleidenschaft allerdings nicht außen vor gelassen werden (vgl. dazu etwa [Ferguson, Hoel, Scheffel & Drachsler, 2016](#ferguson-2016)).

Jeder gesammelte Datensatz folgt bei xAPI einem klaren Schema. Es umfasst stets die agierende Person(engruppe), ein Verb und ein Objekt. Einige Beispiele könnten in verbaler Form sein:

-   Schülerin A hat Multiple-Choice-Frage X beantwortet.
-   Studentengruppe B hat Video Y zu 80 Prozent angesehen.
-   Teilnehmer C hat an Seminar Z teilgenommen.

Hinzu kommen -- je nach Art des Akteurs, des Verbs und des Objektes -- zahlreiche weitere Daten. Das können Zeitpunkte sein, Bearbeitungszeiten, gegebene Antworten, erzielte Punkte, usw.

### ... die Daten an einen Learning Record Store weiterleiten

Jedes der geschnürten xAPI-Pakete lässt sich mit H5P an einen Learning Record Store der Wahl weiterleiten. Dahinter verbirgt sich eine Plattform, die zunächst einmal lediglich darauf spezialisiert ist, die mitunter großen Datenmengen zu speichern und wieder abrufbar zu machen. Je nach Modell bzw. Anbieter gibt es zusätzlich unterschiedliche Möglichkeiten zur Darstellung und Auswertung.

Für die genannten Plattformen existieren jeweils PlugIns, mit denen die gesammelten Daten über die xAPI einer passenden Gegenstelle überlassen werden können:

-   **Drupal:** [Tincan API](https://www.drupal.org/project/tincanapi)
-   **moodle:** [xAPIWrapper](https://github.com/adlnet/xAPIWrapper) und etwas Handarbeit (vgl. [Davies, 2016](#davies-2016))
-   **WordPress:** [wp-h5p-xapi](https://wordpress.org/plugins/wp-h5p-xapi/)

Bei allen drei Lösungen muss man im Wesentlichen seine Zugangsdaten zum Learning Record Store sowie dessen Adresse eintragen. Er kann dann unmittelbar mit dem Sammeln beginnen. Wie bereits angedeutet, gibt es verschiedene Angebote, auf die man zurückgreifen kann. Einige seien hier kurz erwähnt:

-   **[Grassblade](https://www.nextsoftwaresolutions.com/grassblade-lrs-experience-api/):** Kommerzielle Software, die auf einem eigenen Server installiert werden kann bzw. muss. Optional kann man sie über [Scorm Cloud](https://xapi.com/hosted-lrs/) oder [Wax LRS](http://www.saltbox.com/home.html) mit Erweiterungen und Zusatzdiensten betreiben.
-   **[Learning Locker](https://learninglocker.net/):** Kostenfreie Open-Source-Software mit zahlreichen Funktionen. Kann selbst betrieben oder als Dienst genutzt werden.
-   **[Watershed](https://www.watershedlrs.com/):** Kommerzielle Software, die gehosted kostenlos zum Sammeln von Daten genutzt werden kann. Umfangreiche Analysemöglichkeiten sind jedoch den kostenpflichtigen Zusatzangeboten vorbehalten.

### Alles unter einem Dach serviert bekommen

In Kürze wird die erste Version der Plattform [h5p.com](https://h5p.com) fertiggestellt werden. Sie wird zum einen etwas umfangreicheres Hosting von H5P-Inhalten bieten, als es kostenlos auf h5p.org möglich ist -- und möglich bleiben wird.

Über h5p.com wird es allerdings auch möglich, bisher außen vor gebliebene Lernmanagement-Systeme mit H5P zu verzahnen. Ermöglicht wird dies standardisiert über [LTI](http://www.imsglobal.org/activity/learning-tools-interoperability). Die Notwendigkeit eines PlugIns entfällt. Trotzdem lassen sich die Inhalte und Ergebnisse nahtlos an die angeschlossenen Plattformen weiterreichen und dort auswerten. Dazu zählen dann etwa Blackboard oder Canvas. Wer über kein eigenes Lernmanagement-System verfügt, muss nicht verzagen. Die Plattform h5p.com wird auch selbst Analysewerkzeuge für die erzeugten Daten anbieten. Die Anbindung an einen Learning Record Store ist ebenfalls vorgesehen.

![Anbindungsmöglichkeiten an h5p.com](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/h5p-und-xapi-und-so/lrs-h5p-lms.png)

### Mit Daten die Lehre öffnen?

Allgemein drängt sich mir beim Thema Datenerhebung die Frage auf, weshalb die Daten nicht den produzierenden Personen selbst zur Verfügung gestellt werden. LA bildet keine Ausnahme. Es geht mir nicht einmal zwingend um Transparenz als ideellen Wert, sondern vielmehr um Open Educational Practices. Zu diesen zählt es, Verantwortung für das eigene Lernen zu übernehmen -- ohne dabei allerdings komplett auf sich alleine gestellt zu sein (vgl. [Mayrberger & Hofhues, 2013, S. 63](#mayrberger_hofhues-2013)). Warum werden die Lernenden bevormundet, statt ihnen die Daten auch selbst in die Hand zu geben? Dies würde es ihnen gestatten, auch datengestützt ihren eigenen Lernprozess zu reflektieren. Eine aussagekräftige Grafik, die etwa die eigene Prokrastination visualisiert, könnte wirksamer sein als wiederkehrende Ratschläge von Lehrpersonen.

Die Lernenden könnten sich anhand der Daten sogar selbst ein Motivationssystem schneidern, das am besten zu ihnen passt. Die dafür notwendigen Werkzeuge existieren im Prinzip bereits. Sie müssten bloß noch lernen, miteinander zu sprechen. Ein Baustein könnte die Software "[If This Then That](https://ifttt.com/)" (IFTTT) sein. Sie kann von zahlreichen Diensten im Internet Daten einlesen und anhand erstellbarer Regeln Aktionen bei anderen Diensten auslösen. Auslöser wie Empfänger können aber auch Sensoren eines Smartphones oder eine Heimautomationsanlage sein.

Mit den gesammelten und verarbeiteten xAPI-Daten von H5P wären dann etwa folgende automatisch ablaufenden Szenarien denkbar:

-   Falls Kurs abgeschlossen, dann bestelle Videospiel bei BestBuy.
-   Falls fünf Tage lang nichts getan, dann twittere: "Ich brauche Motivationsideen von Euch, jetzt!"
-   Falls Lernaktivität nachts um Zwei, dann koche Kaffee.

Auch wenn dies behavioristische Maßnahmen sind, die nicht bei allen auf Anklang stoßen werden, gibt es einen Unterschied zu üblichen Lehrszenarien: Lernende entscheiden sich freiwillig dafür, sich selbst für einen zukünftigen Zeitpunkt an etwas zu binden. Auch solche Odysseus-Verträge sind nicht zwangsläufig frei von paternalistischen Einflüssen (vgl. etwa [Reamer, 1983](#reamer-1983)), aber dennoch können sie offenere Lehrszenarien stützen.

### Fazit

Die Software H5P erlaubt es auf verschiedenen Wegen, Daten über das Lernen von NutzerInnen zu erheben und auszuwerten. Die größte Flexibilität und das größte Potenzial bildet dabei der Rückgriff auf die standardisierte ExperienceAPI. Sie erfordert allerdings auch mehr Aufwand beim Einrichten und gegebenenfalls beim Betreuen der Infrastruktur. Daher eignet sich eher für Organisationen oder ambitionierte Personen. Scheut man die Mühen nicht, vergrößert man jedoch nicht nur den Datenpool für Educational Data Mining und Learning Analytics. Wenn man die Daten und passende Werkzeuge den Lernenden selbst zur Verfügung stellt, leistet man auch einen Beitrag zur Öffnung der Bildungswelt.

### Literatur

-   **Baker, R. S., & Inventado, P. S. (2014).** [Educational data mining and learning analytics](https://doi.org/10.1007/978-1-4614-3305-7_4#). In J. Larusson, & B. White (Hrsg), *Learning Analytics* (S. 61-75). Springer, New York, NY. doi: <https://doi.org/10.1007/978-1-4614-3305-7_4>
-   **Davies, J. (2016).** [Connecting H5P interactive activity in Moodle to an LRS](http://juliandavis.com/connecting-h5p-interactive-activity-in-moodle-to-an-lrs/). Abgerufen auf <http://juliandavis.com/connecting-h5p-interactive-activity-in-moodle-to-an-lrs/>
-   **Ferguson, R., Hoel, T., Scheffel, M., & Drachsler, H. (2016).** [Guest editorial: Ethics and privacy in learning analytics](http://dx.doi.org/10.18608/jla.2016.31.2). *Journal of learning analytics*, 3(1), 5-15. doi: <http://dx.doi.org/10.18608/jla.2016.31.2>
-   **Mayberger, K., & Hofhues, Sandra (2013).** [Akademische Lehre braucht mehr „Open Educational Practices“ für den Umgang mit „Open Educational Resources“ – ein Plädoyer](https://doi.org/10.3217/zfhe-8-04/07). *Zeitschrift für Hochschulentwicklung ZFHE* 8(4), 56-68. doi: <https://doi.org/10.3217/zfhe-8-04/07>
-   **Ochoa, X., & Worsley, M. (2016).** [Augmenting Learning Analytics with Multimodal Sensory Data](http://dx.doi.org/10.18608/jla.2016.32.10)​. *Journal of Learning Analytics*, 3(2), 213-219. doi: <http://dx.doi.org/10.18608/jla.2016.32.10>
-   **Reamer, F. G. (1983).** [The Concept of Paternalism in Social Work](https://doi.org/10.1086/644516). *Social Service Review* 57(2), 254-271. doi: <https://doi.org/10.1086/644516>
-   **Wiley, D. (o. J.).** [Defining the "Open" in Open Content and Open Educational Resources](https://opencontent.org/definition/). Abgerufen auf [http://opencontent.org/definition/](https://opencontent.org/definition/)

### Downloads

-   [Literaturangaben im BibTeX-Format](https://www.olivertacke.de/wp-content/uploads/2018/03/tacke_oliver-2018-mit_h5p_daten_sammeln_auswerten_und_vielleicht_die_lehre_oeffnen.bib)
-   [Abbildung im SVG-Format](https://www.olivertacke.de/wp-content/uploads/2018/03/LRS-h5p-LMS.svg)

### Hinweis

---

Der Text sowie die Abbildung stehen unter der Lizenz [CC0](https://creativecommons.org/publicdomain/zero/1.0/deed.de). Macht damit, was ihr wollt!

![Creative Commons Zero Public Domain Dedication](https://licensebuttons.net/p/zero/1.0/88x31.png)
Der Text sowie die Abbildung "H5P-API" von [Oliver Tacke](https://openlab.blogs.uni-hamburg.de/?p=1295) sind freigegeben unter einer [CC Zero 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).

---

*Transparency note: I, the author of this post, have been an advocate of initiatives such as the OER movement for many years. Meanwhile, I am also an employee of Joubel. Joubel provides the core team that’s developing the open source software H5P used for creating teaching and learning materials. However, this post is still a personal view.*

Educational Data Mining (EDM) and Learning Analytics (LA) are among the top buzzwords of the EdTech scene right now. Both are all about storing and evaluating the content and meta data that has been produced by students. Why? You can e.g. recognize progress in learning. Some people also dream about predicting future performance or problems, while others even want to build platforms that can be adjusted to the learners. Throughout that process, while EDM has a rather reductionist focus and a tendency towards automation, LA favors a holistic view and human decision making and interventions. (cmp. [Baker & Inventado, 2014, S. 62](#baker_inventado-2014)).

Both approaches share the need for data. One supplier for those could be the software [H5P](https://h5p.org). It is an authoring tool which enables you to create a wide range of interactive tasks for the web. It spans from multiple choice quizzes or clozes over interactive videos and speaking excercises for pronunciation. Additionally, H5P offers several ways to gather data. This does not only mean to record the results of tasks that students have completed, but also to make note of small experiences such as seeking to a position in a video or switching from one answer option to another.

H5P was designed for creating Open Educational Resources (OER), so the developers try to avoid making technical decisions that could impede openness (cmp. [Wiley, n. d.](#wiley-oj)). Consequently, the software is free and the source code is openly licensed — and H5P offers several ways to handle data.

### Get the data directly using plugins

You can use H5P on the software’s designated website, but many organizations and individual persons prefer to use it on their own technical infrastructure. Currently, there are plugins for [Drupal](https://drupal.org/), [Moodle](https://moodle.org/) and [WordPress](https://wordpress.org/). With those, you can record the results of many interaction types. This approach is convenient for moodle in particular, because the learning management system offers a gradebook that will be used for storing data. In consequence, you can use all the tools for analysis that moodle has in stock.

If you’re using WordPress, then you could have a look at the plugin [H5PxAPIkatchu](https://wordpress.org/plugins/h5pxapikatchu/). It allows you to store quite detailed reports in a table, but it does not offer any features for analysis. This job is left to specialized software, and you can export data into a CSV file for transfer. It can then be used in spreadsheet programs easily, but you could also go for tools that are commonly used for data analysis, e.g a combination of [Python](https://python.org/), [Jupyter](https://jupyter.org/), and [scikit-learn](http://scikit-learn.org/) or [R](https://www.r-project.org/).

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=27" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

For Drupal, there’s also a plugin to help you out. It bears the simple name [Quiz](https://www.drupal.org/project/quiz). It will add some functionality for reportingto the content management system, and it can be used for H5P content.

### Tapping the Experience API and …

While the aforementioned plugins offer basic functionaly, there’s a more versatile approach which is called Experience API ([xAPI](https://xapi.com/)). It’s a standardized interface that is supported by H5P, and it was actually specifically designed for keeping record of students’ learning experiences. Furthermore, it can handle both: online based exercises or something that happens on-site — although for the latter it can be very tedious or even impossible to register what’s going on. In that case, you could e.g. use sensors or video recordings that can be interpreted automatically (cmp. [Ochoa & Worsley, 2016, S. 214-215](#ochoa_worsley-2016)). However, you should not forget to consider ethical implications and privacy issues (cmp. e.g. [Ferguson, Hoel, Scheffel & Drachsler, 2016](#ferguson-2016)).

In xAPI, each dataset has the same basic structure. It always contains information about the actor or group of actors, a verb for describing what happened and an object that is dealt with. Some simple examples in common language are:

-   Student A answered multiple-choice question X.
-   Student group B watched 80 % of video Y.
-   Person C attended seminar Z.

In addition to these plain data, there can be plenty more depending on the verb and object, e.g. dates, time periods, options chosen, answers given, scores, etc.

### … push the data to a learning record store

When one of these xAPI statements has been created and wrapped up, H5P can forward it to a Learning Record Store (LRS) of your choice. That’s a platform which is specialized in handling the data that can become “big”. Also, it will provide you with different options for retrieval, visualization and analysis.

For Drupal, moodle and WordPress, there’s a plugin that you can use to send data to an LRS using xAPI:

-   **Drupal:** [Tincan API](https://www.drupal.org/project/tincanapi)
-   **moodle:** [xAPIWrapper](https://github.com/adlnet/xAPIWrapper) and some configuring (see [Davies, 2016](#davies-2016))
-   **WordPress:** [wp-h5p-xapi](https://wordpress.org/plugins/wp-h5p-xapi/)

Basically, all you have to do given one of these solutions, is to enter the address of the LRS and your credentials. Then the system can begin its duty right away. I already hinted at the fact that there are multiple LRS that you can choose from. For example, there is:

-   **[Grassblade](https://www.nextsoftwaresolutions.com/grassblade-lrs-experience-api/):** Commercial software, which needs to be installed on your own server or used as a service, e.g. via [Scorm Cloud](https://xapi.com/hosted-lrs/) or [Wax LRS](http://www.saltbox.com/home.html) which offer several features on top.
-   **[Learning Locker](https://learninglocker.net/):** Free open source software with numerous features. Can be installed yourself or used as a paid service.
-   **[Watershed](https://www.watershedlrs.com/):** Commercial Software, which is offered as a service only, but it’s free in the basic version. The extensive funtionality for evaluation is reserved for paying customers however.

### Getting everything served on one plate

Shortly, the first version of [h5p.com](https://h5p.com) will be completed and released. It will not only provide some more hosting capabilities than h5p.org offers for free today — and will continue to do so.

Using h5p.com, you will also be able to interleave H5P with many learning management systems that didn’t have this option before, e.g. Blackboard or Canvas. This is made possible by [LTI](http://www.imsglobal.org/activity/learning-tools-interoperability). It will no longer be necessary to use a plugin for a specific platform. Nevertheless, you will be able to seamlessly create content within your system and to retrieve and evaluate the results..

Using h5p.com can also be useful if you don’t have your own learning management system but still want to know more about how your content is used. In the future, you will be able to use some tools for analysis, too, and it is also planned to make use or Learning Record Stores.

![Connections possible with h5p.com](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/h5p-und-xapi-und-so/lrs-h5p-lms.png)

### Opening up education with data?

With all the ongoing hype around gathering data in so many fields, I wonder why they are seldomly given to those who actually produced them in the first place. Learning Analytics is no exception. I am not even arguing from a perspective of transparency as an idealist value, but from the perspective of Open Educational Practices. Among several other aspects, these embrace taking responsibility for one’s own learning process — yet without being left on one’s own (cmp. [Mayrberger & Hofhues, 2013, S. 63](#mayrberger_hofhues-2013)). So, why are students often being patronized instead of giving them access to the data that were collected about them?

Students could be empowered to also reflect upon their learning process based on data and aggregated data. For example, seeing a meaningful visualization of their own procrastination could be more effective than recurring reminders by teachers. Also, having the means to track and think about their progress themselves will better help them to become a self-reliant person than constantly being told when to do what by a teacher or even by a computer system.

Based upon the access to the flow of data, the students could even build their own motivational system that suits themselves best. The tools to achieve this are basically in front of our noses already. The only thing left to do is make them talk with each other. One linking pin could be the software "[If This Then That](https://ifttt.com/)" (IFTTT) or a clone. It can read data from numerous services on the internet and trigger many actions of other services based on rules that you can define. A simple example could be to automatically store all the tweets that you like in a Google Spreadsheet. It doesn’t stop there though. Instead of services, you could also think of sensors of a smartphone or home automation devices.

Given the xAPI data provided by H5P you could imagine automated scenarios like these that students could set up for themselves:

-   If course completed, order video game at BestBuy.
-   If not done anything within five days, tweet “I desperately need your ideas for motivation. Now!”
-   If learning activity takes place at 2 AM, make coffee.

Yes, those are behaviorist measures that not everyone is going to like, but still there’s a difference to common scenarios: the students themselves decide voluntarily to bind themselves in the future. While those ulysses pacts are not necessarily free of paternalistic or external influence (cmp. e.g. [Reamer, 1983](#reamer-1983)), they can still enable learning settings that are more open than today’s.

### Conclusion

The software H5P offers different ways to collect and evaluate data about the learning experiences of students. The most flexible and powerful option is to use the standardized Experience API. On the other hand, it still requires some effort for installing, configuration and maintenance. In consequence, it is rather suited for organizations or ambitious individuals. However, if you bother to use it, you do not only enlarge the data pool for Educational Data Mining and Learning Analytics. If you also provide students with the data and the right tools, too, you can also have a share in opening up the world of education.

### Sources

-   **Baker, R. S., & Inventado, P. S. (2014).** [Educational data mining and learning analytics](https://doi.org/10.1007/978-1-4614-3305-7_4#). In J. Larusson, & B. White (Hrsg), *Learning Analytics* (S. 61-75). Springer, New York, NY. doi: <https://doi.org/10.1007/978-1-4614-3305-7_4>
-   **Davies, J. (2016).** [Connecting H5P interactive activity in Moodle to an LRS](http://juliandavis.com/connecting-h5p-interactive-activity-in-moodle-to-an-lrs/). Abgerufen auf <http://juliandavis.com/connecting-h5p-interactive-activity-in-moodle-to-an-lrs/>
-   **Ferguson, R., Hoel, T., Scheffel, M., & Drachsler, H. (2016).** [Guest editorial: Ethics and privacy in learning analytics](http://dx.doi.org/10.18608/jla.2016.31.2). *Journal of learning analytics*, 3(1), 5-15. doi: <http://dx.doi.org/10.18608/jla.2016.31.2>
-   **Mayberger, K., & Hofhues, Sandra (2013).** [Akademische Lehre braucht mehr „Open Educational Practices“ für den Umgang mit „Open Educational Resources“ – ein Plädoyer](https://doi.org/10.3217/zfhe-8-04/07). *Zeitschrift für Hochschulentwicklung ZFHE* 8(4), 56-68. doi: <https://doi.org/10.3217/zfhe-8-04/07>
-   **Ochoa, X., & Worsley, M. (2016).** [Augmenting Learning Analytics with Multimodal Sensory Data](http://dx.doi.org/10.18608/jla.2016.32.10)​. *Journal of Learning Analytics*, 3(2), 213-219. doi: <http://dx.doi.org/10.18608/jla.2016.32.10>
-   **Reamer, F. G. (1983).** [The Concept of Paternalism in Social Work](https://doi.org/10.1086/644516). *Social Service Review* 57(2), 254-271. doi: <https://doi.org/10.1086/644516>
-   **Wiley, D. (o. J.).** [Defining the "Open" in Open Content and Open Educational Resources](https://opencontent.org/definition/). Abgerufen auf [http://opencontent.org/definition/](https://opencontent.org/definition/)

### Downloads

-   [aources in BibTeX format](https://www.olivertacke.de/wp-content/uploads/2018/03/tacke_oliver-2018-mit_h5p_daten_sammeln_auswerten_und_vielleicht_die_lehre_oeffnen.bib)
-   [illustration as SVG format](https://www.olivertacke.de/wp-content/uploads/2018/03/LRS-h5p-LMS.svg)

### Note

---

The text and the illustration are licensed under a CC0 license. Feel free to do with it whatever you want to.

![Creative Commons Zero Public Domain Dedication](https://licensebuttons.net/p/zero/1.0/88x31.png)
Text and illustration "H5P-API" by [Oliver Tacke](https://openlab.blogs.uni-hamburg.de/?p=1295) is published under a [CC Zero 1.0 Universal Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
