<p><em>Transparenzhinweis: Ich, der Autor dieses Beitrags, bin seit Jahren Verfechter von Initiativen wie der OER-Bewegung. Ich bin inzwischen aber auch Mitarbeiter der Firma Joubel, die als Kernteam an der Entwicklung der Open-Source-Software H5P beteiligt ist. Der Beitrag ist dennoch ein privates Produkt.</em></p>

<hr />

<p>Educational Data Mining (EDM) und Learning Analytics (LA) gehören gerade zum heißen Scheiß in der Ed-Tech-Szene. Bei beiden geht es darum, die von Lernenden erstellten Inhalte und anfallende Metadaten festzuhalten und auszuwerten. Wozu? Etwa, um Lernfortschritte erkennen zu können. Manch eine/r träumt auch davon, zukünftige Leistungen oder Probleme vorherzusagen. Noch einen Schritt weiter gehen diejenigen, die gleich Plattformen bauen wollen, die sich den Lernenden anpassen. Während das beim EDM eher reduktionistisch und automatisiert geschieht, setzt man bei LA eher auf eine ganzheitliche Sicht sowie menschliche Urteile und Interventionen (vgl. <a href="#baker_inventado-2014">Baker &amp; Inventado, 2014, S. 62</a>).</p>

<p>Beiden Ansätzen ist gemein, dass sie Daten benötigen. Ein Lieferant dafür kann die Software <a href="https://h5p.org">H5P</a> sein. Hinter dem Kürzel verbirgt sich ein Autorenwerkzeug, das es erlaubt, vielfältige interaktive Aufgaben für das Netz zu erstellen. <span style="font-weight: 400;">Die Bandbreite reicht von Multiple-Choice-Quizzes oder Lückentexten über interaktive Videos bis zu Ausspracheübungen. </span>Gleichzeitig bietet H5P verschiedene Möglichkeiten, um Daten zu sammeln. Je nach Szenario fallen darunter nicht lediglich die erzielten Ergebnisse, sondern auch kleinteilige Ereignisse wie das Springen innerhalb eines Videos oder das Wechseln von einer Antwortoption zur anderen.</p>

<!--more-->

<p>H5P wurde speziell für die Erstellung von offenen Bildungsmaterialien (Open Educational Resources, OER) konzipiert. Daher werden bei der Entwicklung nach Möglichkeit technische Entscheidungen vermieden, die offene Inhalte weniger offen machen (vgl. <a href="#wiley-oj">Wiley, o. J.</a>). Konsequent steht die Software samt Quelltext kostenfrei und offen lizenziert zur Verfügung -- und sie bietet verschiedene Wege, um der Daten Herr zu werden.</p>

<h3>Auf Daten direkt über PlugIns zugreifen</h3>

<p><span style="font-weight: 400;">H5P lässt sich zwar direkt auf der zugehörigen Website verwenden, allerdings ziehen es viele Organisationen und Personen vor, die Software in ihre eigene technische Infrastruktur einzubinden. Das geschieht derzeit über PlugIns, die für </span><a href="https://drupal.org/"><span style="font-weight: 400;">Drupal</span></a><span style="font-weight: 400;">, </span><a href="https://moodle.org/"><span style="font-weight: 400;">moodle</span></a><span style="font-weight: 400;"> und </span><a href="https://wordpress.org/"><span style="font-weight: 400;">WordPress</span></a><span style="font-weight: 400;"> existieren. Die eigene Plattform zeichnet dann bei vielen der möglichen Interaktionen die Eingaben und Ergebnisse der Lernenden auf. Besonders bequem ist dies, wenn moodle eingesetzt wird. Das Lernmanagement-System stellt das sogenannte „Gradebook“ zur Verfügung, in das erzielte Ergebnisse eingetragen werden.  Damit stehen die gewohnten Auswertungsmöglichkeiten von moodle zur Verfügung.</span></p>

<p>Für WordPress existiert das PlugIn <a href="https://wordpress.org/plugins/h5pxapikatchu/">H5PxAPIkatchu</a>, das die Ergebnisse der Inhaltstypen in tabellarischer Form speichert. Es bietet allerdings bewusst keine weiteren Optionen zur Analyse an. Diese Aufgabe überlässt es lieber darauf spezialisierter Software. Die gespeicherten Daten lassen sich dazu in eine CSV-Datei exportieren und individuell auswerten. Vermutlich die geringste Hürde stellt dafür eine Tabellenkalkulation dar. Weitaus mehr Möglichkeiten hat man allerdings mit gängigen Umgebungen zur Datenanalyse, etwa eine Kombination aus <a href="https://python.org/">Python</a>, <a href="https://jupyter.org/">Jupyter</a> und <a href="http://scikit-learn.org/">scikit-learn</a> oder <a href="https://www.r-project.org/">R</a>.</p>

<p>[h5p id="27"]</p>

<p>Für Drupal wiederum existiert ein Plugin mit dem schlichten Namen <a href="https://www.drupal.org/project/quiz">Quiz</a>. Es erweitert das Content-Management-System um diverse Reportingfunktionen für H5P-Inhalte.</p>

<h3>Die ExperienceAPI anzapfen und ...</h3>

<p>Detaillierter und (nach erfolgter Einrichtung) bequemer als mit den zuvor genannten Möglichkeiten wird es, wenn man die ExperienceAPI (<a href="https://xapi.com/">xAPI</a>) verwendet. Dahinter verbirgt sich eine von H5P unterstützte Schnittstelle, die zum Dokumentieren von Lernerfahrungen konzipiert wurde. Es spielt dabei keine Rolle, ob diese Erfahrungen sich auf online-gestützte Aufgaben beziehen, oder ob es sich um Erfahrungen in Präsenzveranstaltungen handelt. Bei Letzteren wird das manuelle Erfassen allerdings aufwändig bis unmöglich. Man kann in diesem Fall etwa zu Sensoren oder Audio- und Videoaufzeichnungen greifen (vgl. <a href="#ochoa_worsley-2016">Ochoa &amp; Worsley, 2016, S. 214-215</a>). Ethische und datenschutzrechtliche Fragen sollten bei aller Sammelleidenschaft allerdings nicht außen vor gelassen werden (vgl. dazu etwa <a href="#ferguson-2016">Ferguson, Hoel, Scheffel &amp; Drachsler, 2016</a>).</p>

<p>Jeder gesammelte Datensatz folgt bei xAPI einem klaren Schema. Es umfasst stets die agierende Person(engruppe), ein Verb und ein Objekt. Einige Beispiele könnten in verbaler Form sein:</p>

<ul>
<li>Schülerin A hat Multiple-Choice-Frage X beantwortet.</li>
<li>Studentengruppe B hat Video Y zu 80 Prozent angesehen.</li>
<li>Teilnehmer C hat an Seminar Z teilgenommen.</li>
</ul>

<p>Hinzu kommen -- je nach Art des Akteurs, des Verbs und des Objektes -- zahlreiche weitere Daten. Das können Zeitpunkte sein, Bearbeitungszeiten, gegebene Antworten, erzielte Punkte, usw.</p>

<h3>... die Daten an einen Learning Record Store weiterleiten</h3>

<p>Jedes der geschnürten xAPI-Pakete lässt sich mit H5P an einen Learning Record Store der Wahl weiterleiten. Dahinter verbirgt sich eine Plattform, die zunächst einmal lediglich darauf spezialisiert ist, die mitunter großen Datenmengen zu speichern und wieder abrufbar zu machen. Je nach Modell bzw. Anbieter gibt es zusätzlich unterschiedliche Möglichkeiten zur Darstellung und Auswertung.</p>

<p>Für die genannten Plattformen existieren jeweils PlugIns, mit denen die gesammelten Daten über die xAPI einer passenden Gegenstelle überlassen werden können:</p>

<ul>
<li><strong>Drupal:</strong> <a href="https://www.drupal.org/project/tincanapi">Tincan API</a></li>
<li><strong>moodle:</strong> <a href="https://github.com/adlnet/xAPIWrapper">xAPIWrapper</a> und etwas Handarbeit (vgl. <a href="#davies-2016">Davies, 2016</a>)</li>
<li><strong>WordPress:</strong> <a href="https://wordpress.org/plugins/wp-h5p-xapi/">wp-h5p-xapi</a></li>
</ul>

<p>Bei allen drei Lösungen muss man im Wesentlichen seine Zugangsdaten zum Learning Record Store sowie dessen Adresse eintragen. Er kann dann unmittelbar mit dem Sammeln beginnen. Wie bereits angedeutet, gibt es verschiedene Angebote, auf die man zurückgreifen kann. Einige seien hier kurz erwähnt:</p>

<ul>
<li><strong><a href="https://www.nextsoftwaresolutions.com/grassblade-lrs-experience-api/">Grassblade</a>:</strong> Kommerzielle Software, die auf einem eigenen Server installiert werden kann bzw. muss. Optional kann man sie über <a href="https://xapi.com/hosted-lrs/">Scorm Cloud</a> oder <a href="http://www.saltbox.com/home.html">Wax LRS</a> mit Erweiterungen und Zusatzdiensten betreiben.</li>
<li><strong><a href="https://learninglocker.net/">Learning Locker</a>:</strong> Kostenfreie Open-Source-Software mit zahlreichen Funktionen. Kann selbst betrieben oder als Dienst genutzt werden.</li>
<li><strong><a href="https://www.watershedlrs.com/">Watershed</a>:</strong> Kommerzielle Software, die gehosted kostenlos zum Sammeln von Daten genutzt werden kann. Umfangreiche Analysemöglichkeiten sind jedoch den kostenpflichtigen Zusatzangeboten vorbehalten.</li>
</ul>

<h3>Alles unter einem Dach serviert bekommen</h3>

<p>In Kürze wird die erste Version der Plattform <a href="https://h5p.com">h5p.com</a> fertiggestellt werden. Sie wird zum einen etwas umfangreicheres Hosting von H5P-Inhalten bieten, als es kostenlos auf h5p.org möglich ist -- und möglich bleiben wird.</p>

<p>Über h5p.com wird es allerdings auch möglich, bisher außen vor gebliebene Lernmanagement-Systeme mit H5P zu verzahnen. Ermöglicht wird dies standardisiert über <a href="http://www.imsglobal.org/activity/learning-tools-interoperability">LTI</a>. Die Notwendigkeit eines PlugIns entfällt. Trotzdem lassen sich die Inhalte und Ergebnisse nahtlos an die angeschlossenen Plattformen weiterreichen und dort auswerten. Dazu zählen dann etwa Blackboard oder Canvas. Wer über kein eigenes Lernmanagement-System verfügt, muss nicht verzagen. Die Plattform h5p.com wird auch selbst Analysewerkzeuge für die erzeugten Daten anbieten. Die Anbindung an einen Learning Record Store ist ebenfalls vorgesehen.</p>

<p><a href="https://www.olivertacke.de/wp-content/uploads/2018/03/LRS-h5p-LMS.png"><img class="aligncenter wp-image-5367 size-full" src="https://openlab.blogs.uni-hamburg.de/wp-content/uploads/2018/03/LRS-h5p-LMS.png" alt="Anbindungsmöglichkeiten an h5p.com" width="800" height="169" /></a></p>

<h3>Mit Daten die Lehre öffnen?</h3>

<p>Allgemein drängt sich mir beim Thema Datenerhebung die Frage auf, weshalb die Daten nicht den produzierenden Personen selbst zur Verfügung gestellt werden. LA bildet keine Ausnahme. Es geht mir nicht einmal zwingend um Transparenz als ideellen Wert, sondern vielmehr um Open Educational Practices. Zu diesen zählt es, Verantwortung für das eigene Lernen zu übernehmen -- ohne dabei allerdings komplett auf sich alleine gestellt zu sein (vgl. <a href="#mayrberger_hofhues-2013">Mayrberger &amp; Hofhues, 2013, S. 63</a>). Warum werden die Lernenden bevormundet, statt ihnen die Daten auch selbst in die Hand zu geben? Dies würde es ihnen gestatten, auch datengestützt ihren eigenen Lernprozess zu reflektieren. Eine aussagekräftige Grafik, die etwa die eigene Prokrastination visualisiert, könnte wirksamer sein als wiederkehrende Ratschläge von Lehrpersonen.</p>

<p>Die Lernenden könnten sich anhand der Daten sogar selbst ein Motivationssystem schneidern, das am besten zu ihnen passt. Die dafür notwendigen Werkzeuge existieren im Prinzip bereits. Sie müssten bloß noch lernen, miteinander zu sprechen. Ein Baustein könnte die Software "<a href="https://ifttt.com/">If This Then That</a>" (IFTTT) sein. Sie kann von zahlreichen Diensten im Internet Daten einlesen und anhand erstellbarer Regeln Aktionen bei anderen Diensten auslösen. Auslöser wie Empfänger können aber auch Sensoren eines Smartphones oder eine Heimautomationsanlage sein.</p>

<p>Mit den gesammelten und verarbeiteten xAPI-Daten von H5P wären dann etwa folgende automatisch ablaufenden Szenarien denkbar:</p>

<ul>
<li>Falls Kurs abgeschlossen, dann bestelle Videospiel bei BestBuy.</li>
<li>Falls fünf Tage lang nichts getan, dann twittere: "Ich brauche Motivationsideen von Euch, jetzt!"</li>
<li>Falls Lernaktivität nachts um Zwei, dann koche Kaffee.</li>
</ul>

<p>Auch wenn dies behavioristische Maßnahmen sind, die nicht bei allen auf Anklang stoßen werden, gibt es einen Unterschied zu üblichen Lehrszenarien: Lernende entscheiden sich freiwillig dafür, sich selbst für einen zukünftigen Zeitpunkt an etwas zu binden. Auch solche Odysseus-Verträge sind nicht zwangsläufig frei von paternalistischen Einflüssen (vgl. etwa <a href="#reamer-1983">Reamer, 1983</a>), aber dennoch können sie offenere Lehrszenarien stützen.</p>

<h3>Fazit</h3>

<p>Die Software H5P erlaubt es auf verschiedenen Wegen, Daten über das Lernen von NutzerInnen zu erheben und auszuwerten. Die größte Flexibilität und das größte Potenzial bildet dabei der Rückgriff auf die standardisierte ExperienceAPI. Sie erfordert allerdings auch mehr Aufwand beim Einrichten und gegebenenfalls beim Betreuen der Infrastruktur. Daher eignet sich eher für Organisationen oder ambitionierte Personen. Scheut man die Mühen nicht, vergrößert man jedoch nicht nur den Datenpool für Educational Data Mining und Learning Analytics. Wenn man die Daten und passende Werkzeuge den Lernenden selbst zur Verfügung stellt, leistet man auch einen Beitrag zur Öffnung der Bildungswelt.</p>

<h3>Literatur</h3>

<ul>
<li><a id="baker_inventado"></a><strong>Baker, R. S., &amp; Inventado, P. S. (2014).</strong> <a href="https://doi.org/10.1007/978-1-4614-3305-7_4#">Educational data mining and learning analytics</a>. In J. Larusson, &amp; B. White (Hrsg), <em>Learning Analytics</em> (S. 61-75). Springer, New York, NY. doi: <a href="https://doi.org/10.1007/978-1-4614-3305-7_4">https://doi.org/10.1007/978-1-4614-3305-7_4</a></li>
<li><a id="davies-2016"></a><strong>Davies, J. (2016).</strong> <a href="http://juliandavis.com/connecting-h5p-interactive-activity-in-moodle-to-an-lrs/">Connecting H5P interactive activity in Moodle to an LRS</a>. Abgerufen auf <a href="http://juliandavis.com/connecting-h5p-interactive-activity-in-moodle-to-an-lrs/">http://juliandavis.com/connecting-h5p-interactive-activity-in-moodle-to-an-lrs/</a></li>
<li><a id="ferguson-2016"></a><strong>Ferguson, R., Hoel, T., Scheffel, M., &amp; Drachsler, H. (2016).</strong> <a href="http://dx.doi.org/10.18608/jla.2016.31.2">Guest editorial: Ethics and privacy in learning analytics</a>. <em>Journal of learning analytics</em>, 3(1), 5-15. doi: <a href="http://dx.doi.org/10.18608/jla.2016.31.2">http://dx.doi.org/10.18608/jla.2016.31.2</a></li>
<li><a id="mayrberger_hofhues-2013"></a><strong>Mayberger, K., &amp; Hofhues, Sandra (2013).</strong> <a href="https://doi.org/10.3217/zfhe-8-04/07">Akademische Lehre braucht mehr „Open Educational Practices“ für den Umgang mit „Open Educational Resources“ – ein Plädoyer</a>. <em>Zeitschrift für Hochschulentwicklung ZFHE</em> 8(4), 56-68. doi: <a href="https://doi.org/10.3217/zfhe-8-04/07">https://doi.org/10.3217/zfhe-8-04/07</a></li>
<li><a id="ochoa_worsley"></a><strong>Ochoa, X., &amp; Worsley, M. (2016).</strong> <a href="http://dx.doi.org/10.18608/jla.2016.32.10">Augmenting Learning Analytics with Multimodal Sensory Data</a>​. <em>Journal of Learning Analytics</em>, 3(2), 213-219. doi: <a href="http://dx.doi.org/10.18608/jla.2016.32.10">http://dx.doi.org/10.18608/jla.2016.32.10</a></li>
<li><a id="reamer-1983"></a><strong>Reamer, F. G. (1983).</strong> <a href="https://doi.org/10.1086/644516">The Concept of Paternalism in Social Work</a>. <em>Social Service Review</em> 57(2), 254-271. doi: <a href="https://doi.org/10.1086/644516">https://doi.org/10.1086/644516</a></li>
<li><a id="wiley-oj"></a><strong>Wiley, D. (o. J.).</strong> <a href="https://opencontent.org/definition/">Defining the "Open" in Open Content and Open Educational Resources</a>. Abgerufen auf <a href="https://opencontent.org/definition/">http://opencontent.org/definition/</a></li>
</ul>

<h3>Downloads</h3>

<ul>
<li><a href="https://www.olivertacke.de/wp-content/uploads/2018/03/tacke_oliver-2018-mit_h5p_daten_sammeln_auswerten_und_vielleicht_die_lehre_oeffnen.bib">Literaturangaben im BibTeX-Format</a></li>
<li><a href="https://www.olivertacke.de/wp-content/uploads/2018/03/LRS-h5p-LMS.svg">Abbildung im SVG-Format</a></li>
</ul>

<h3>Hinweis</h3>

<hr />

<p>Der Text sowie die Abbildung stehen unter der Lizenz <a href="https://creativecommons.org/publicdomain/zero/1.0/deed.de">CC0</a>. Macht damit, was ihr wollt!</p>

<p class="ccl">
  <a rel="license" href="https://creativecommons.org/publicdomain/zero/1.0/"><img alt="Creative Commons Zero Public Domain Dedication" style="border-width:0" src="https://licensebuttons.net/p/zero/1.0/88x31.png" /></a><br />Der Text sowie die Abbildung "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">H5P-API</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/?p=1295" property="cc:attributionName" rel="cc:attributionURL">Oliver Tacke</a> sind freigegeben unter einer <a rel="license" href="https://creativecommons.org/publicdomain/zero/1.0/">CC Zero 1.0 Universal Public Domain Dedication</a>.
</p>

<p><a href="https://creativecommons.org/publicdomain/zero/1.0/deed.de">40</a>: https://creativecommons.org/publicdomain/zero/1.0/deed.de">olivertacke.de</a>.</p>

<hr />

<p><em>Transparency note: I, the author of this post, have been an advocate of initiatives such as the OER movement for many years. Meanwhile, I am also an employee of Joubel. Joubel provides the core team that’s developing the open source software H5P used for creating teaching and learning materials. However, this post is still a personal view.</em></p>

<p>Educational Data Mining (EDM) and Learning Analytics (LA) are among the top buzzwords of the EdTech scene right now. Both are all about storing and evaluating the content and meta data that has been produced by students. Why? You can e.g. recognize progress in learning. Some people also dream about predicting future performance or problems, while others even want to build platforms that can be adjusted to the learners. Throughout that process, while EDM has a rather reductionist focus and a tendency towards automation, LA favors a holistic view and human decision making and interventions. (cmp. <a href="#baker_inventado-2014">Baker &amp; Inventado, 2014, S. 62</a>).</p>

<p>Both approaches share the need for data. One supplier for those could be the software <a href="https://h5p.org">H5P</a>. It is an authoring tool which enables you to create a wide range of interactive tasks for the web. It spans from multiple choice quizzes or clozes over interactive videos and speaking excercises for pronunciation. Additionally, H5P offers several ways to gather data. This does not only mean to record the results of tasks that students have completed, but also to make note of small experiences such as seeking to a position in a video or switching from one answer option to another.</p>

<!--more-->

<p>H5P was designed for creating Open Educational Resources (OER), so the developers try to avoid making technical decisions that could impede openness (cmp. <a href="#wiley-oj">Wiley, n. d.</a>). Consequently, the software is free and the source code is openly licensed — and H5P offers several ways to handle data.</p>

<h3>Get the data directly using plugins</h3>

<p>You can use H5P on the software’s designated website, but many organizations and individual persons prefer to use it on their own technical infrastructure. Currently, there are plugins for <a href="https://drupal.org/">Drupal</a>, <a href="https://moodle.org/">Moodle</a> and <a href="https://wordpress.org/">WordPress</a>. With those, you can record the results of many interaction types. This approach is convenient for moodle in particular, because the learning management system offers a gradebook that will be used for storing data. In consequence, you can use all the tools for analysis that moodle has in stock.</p>

<p>If you’re using WordPress, then you could have a look at the plugin <a href="https://wordpress.org/plugins/h5pxapikatchu/">H5PxAPIkatchu</a>. It allows you to store quite detailed reports in a table, but it does not offer any features for analysis. This job is left to specialized software, and you can export data into a CSV file for transfer. It can then be used in spreadsheet programs easily, but you could also go for tools that are commonly used for data analysis, e.g a combination of <a href="https://python.org/">Python</a>, <a href="https://jupyter.org/">Jupyter</a>, and <a href="http://scikit-learn.org/">scikit-learn</a> or <a href="https://www.r-project.org/">R</a>.</p>

<p>[h5p id="27"]</p>

<p>For Drupal, there’s also a plugin to help you out. It bears the simple name <a href="https://www.drupal.org/project/quiz">Quiz</a>. It will add some functionality for reportingto the content management system, and it can be used for H5P content.</p>

<h3>Tapping the Experience API and …</h3>

<p>While the aforementioned plugins offer basic functionaly, there’s a more versatile approach which is called Experience API (<a href="https://xapi.com/">xAPI</a>). It’s a standardized interface that is supported by H5P, and it was actually specifically designed for keeping record of students’ learning experiences. Furthermore, it can handle both: online based exercises or something that happens on-site — although for the latter it can be very tedious or even impossible to register what’s going on. In that case, you could e.g. use sensors or video recordings that can be interpreted automatically (cmp. <a href="#ochoa_worsley-2016">Ochoa &amp; Worsley, 2016, S. 214-215</a>). However, you should not forget to consider ethical implications and privacy issues (cmp. e.g. <a href="#ferguson-2016">Ferguson, Hoel, Scheffel &amp; Drachsler, 2016</a>).</p>

<p>In xAPI, each dataset has the same basic structure. It always contains information about the actor or group of actors, a verb for describing what happened and an object that is dealt with. Some simple examples in common language are:</p>

<ul>
<li>Student A answered multiple-choice question X.</li>
<li>Student group B watched 80 % of video Y.</li>
<li>Person C attended seminar Z.</li>
</ul>

<p>In addition to these plain data, there can be plenty more depending on the verb and object, e.g. dates, time periods, options chosen, answers given, scores, etc.</p>

<h3>… push the data to a learning record store</h3>

<p>When one of these xAPI statements has been created and wrapped up, H5P can forward it to a Learning Record Store (LRS) of your choice. That’s a platform which is specialized in handling the data that can become “big”. Also, it will provide you with different options for retrieval, visualization and analysis.</p>

<p>For Drupal, moodle and WordPress, there’s a plugin that you can use to send data to an LRS using xAPI:</p>

<ul>
<li><strong>Drupal:</strong> <a href="https://www.drupal.org/project/tincanapi">Tincan API</a></li>
<li><strong>moodle:</strong> <a href="https://github.com/adlnet/xAPIWrapper">xAPIWrapper</a> and some configuring (see <a href="#davies-2016">Davies, 2016</a>)</li>
<li><strong>WordPress:</strong> <a href="https://wordpress.org/plugins/wp-h5p-xapi/">wp-h5p-xapi</a></li>
</ul>

<p>Basically, all you have to do given one of these solutions, is to enter the address of the LRS and your credentials. Then the system can begin its duty right away. I already hinted at the fact that there are multiple LRS that you can choose from. For example, there is:</p>

<ul>
<li><strong><a href="https://www.nextsoftwaresolutions.com/grassblade-lrs-experience-api/">Grassblade</a>:</strong> Commercial software, which needs to be installed on your own server or used as a service, e.g. via <a href="https://xapi.com/hosted-lrs/">Scorm Cloud</a> or <a href="http://www.saltbox.com/home.html">Wax LRS</a> which offer several features on top.</li>
<li><strong><a href="https://learninglocker.net/">Learning Locker</a>:</strong> Free open source software with numerous features. Can be installed yourself or used as a paid service.</li>
<li><strong><a href="https://www.watershedlrs.com/">Watershed</a>:</strong> Commercial Software, which is offered as a service only, but it’s free in the basic version. The extensive funtionality for evaluation is reserved for paying customers however.</li>
</ul>

<h3>Getting everything served on one plate</h3>

<p>Shortly, the first version of <a href="https://h5p.com">h5p.com</a> will be completed and released. It will not only provide some more hosting capabilities than h5p.org offers for free today — and will continue to do so.</p>

<p>Using h5p.com, you will also be able to interleave H5P with many learning management systems that didn’t have this option before, e.g. Blackboard or Canvas. This is made possible by <a href="http://www.imsglobal.org/activity/learning-tools-interoperability">LTI</a>. It will no longer be necessary to use a plugin for a specific platform. Nevertheless, you will be able to seamlessly create content within your system and to retrieve and evaluate the results..</p>

<p>Using h5p.com can also be useful if you don’t have your own learning management system but still want to know more about how your content is used. In the future, you will be able to use some tools for analysis, too, and it is also planned to make use or Learning Record Stores.</p>

<p><a href="https://www.olivertacke.de/wp-content/uploads/2018/03/LRS-h5p-LMS.png"><img class="aligncenter wp-image-5367 size-full" src="https://openlab.blogs.uni-hamburg.de/wp-content/uploads/2018/03/LRS-h5p-LMS.png" alt="Connections possible with h5p.com" width="800" height="169" /></a></p>

<h3>Opening up education with data?</h3>

<p>With all the ongoing hype around gathering data in so many fields, I wonder why they are seldomly given to those who actually produced them in the first place. Learning Analytics is no exception. I am not even arguing from a perspective of transparency as an idealist value, but from the perspective of Open Educational Practices. Among several other aspects, these embrace taking responsibility for one’s own learning process — yet without being left on one’s own (cmp. <a href="#mayrberger_hofhues-2013">Mayrberger &amp; Hofhues, 2013, S. 63</a>). So, why are students often being patronized instead of giving them access to the data that were collected about them?</p>

<p>Students could be empowered to also reflect upon their learning process based on data and aggregated data. For example, seeing a meaningful visualization of their own procrastination could be more effective than recurring reminders by teachers. Also, having the means to track and think about their progress themselves will better help them to become a self-reliant person than constantly being told when to do what by a teacher or even by a computer system.</p>

<p>Based upon the access to the flow of data, the students could even build their own motivational system that suits themselves best. The tools to achieve this are basically in front of our noses already. The only thing left to do is make them talk with each other. One linking pin could be the software "<a href="https://ifttt.com/">If This Then That</a>" (IFTTT) or a clone. It can read data from numerous services on the internet and trigger many actions of other services based on rules that you can define. A simple example could be to automatically store all the tweets that you like in a Google Spreadsheet. It doesn’t stop there though. Instead of services, you could also think of sensors of a smartphone or home automation devices.</p>

<p>Given the xAPI data provided by H5P you could imagine automated scenarios like these that students could set up for themselves:</p>

<ul>
<li>If course completed, order video game at BestBuy.</li>
<li>If not done anything within five days, tweet “I desperately need your ideas for motivation. Now!”</li>
<li>If learning activity takes place at 2 AM, make coffee.</li>
</ul>

<p>Yes, those are behaviorist measures that not everyone is going to like, but still there’s a difference to common scenarios: the students themselves decide voluntarily to bind themselves in the future. While those ulysses pacts are not necessarily free of paternalistic or external influence (cmp. e.g. <a href="#reamer-1983">Reamer, 1983</a>), they can still enable learning settings that are more open than today’s.</p>

<h3>Conclusion</h3>

<p>The software H5P offers different ways to collect and evaluate data about the learning experiences of students. The most flexible and powerful option is to use the standardized Experience API. On the other hand, it still requires some effort for installing, configuration and maintenance. In consequence, it is rather suited for organizations or ambitious individuals. However, if you bother to use it, you do not only enlarge the data pool for Educational Data Mining and Learning Analytics. If you also provide students with the data and the right tools, too, you can also have a share in opening up the world of education.</p>

<h3>Sources</h3>

<ul>
<li><a id="baker_inventado"></a><strong>Baker, R. S., &amp; Inventado, P. S. (2014).</strong> <a href="https://doi.org/10.1007/978-1-4614-3305-7_4#">Educational data mining and learning analytics</a>. In J. Larusson, &amp; B. White (Hrsg), <em>Learning Analytics</em> (S. 61-75). Springer, New York, NY. doi: <a href="https://doi.org/10.1007/978-1-4614-3305-7_4">https://doi.org/10.1007/978-1-4614-3305-7_4</a></li>
<li><a id="davies-2016"></a><strong>Davies, J. (2016).</strong> <a href="http://juliandavis.com/connecting-h5p-interactive-activity-in-moodle-to-an-lrs/">Connecting H5P interactive activity in Moodle to an LRS</a>. Abgerufen auf <a href="http://juliandavis.com/connecting-h5p-interactive-activity-in-moodle-to-an-lrs/">http://juliandavis.com/connecting-h5p-interactive-activity-in-moodle-to-an-lrs/</a></li>
<li><a id="ferguson-2016"></a><strong>Ferguson, R., Hoel, T., Scheffel, M., &amp; Drachsler, H. (2016).</strong> <a href="http://dx.doi.org/10.18608/jla.2016.31.2">Guest editorial: Ethics and privacy in learning analytics</a>. <em>Journal of learning analytics</em>, 3(1), 5-15. doi: <a href="http://dx.doi.org/10.18608/jla.2016.31.2">http://dx.doi.org/10.18608/jla.2016.31.2</a></li>
<li><a id="mayrberger_hofhues-2013"></a><strong>Mayberger, K., &amp; Hofhues, Sandra (2013).</strong> <a href="https://doi.org/10.3217/zfhe-8-04/07">Akademische Lehre braucht mehr „Open Educational Practices“ für den Umgang mit „Open Educational Resources“ – ein Plädoyer</a>. <em>Zeitschrift für Hochschulentwicklung ZFHE</em> 8(4), 56-68. doi: <a href="https://doi.org/10.3217/zfhe-8-04/07">https://doi.org/10.3217/zfhe-8-04/07</a></li>
<li><a id="ochoa_worsley"></a><strong>Ochoa, X., &amp; Worsley, M. (2016).</strong> <a href="http://dx.doi.org/10.18608/jla.2016.32.10">Augmenting Learning Analytics with Multimodal Sensory Data</a>​. <em>Journal of Learning Analytics</em>, 3(2), 213-219. doi: <a href="http://dx.doi.org/10.18608/jla.2016.32.10">http://dx.doi.org/10.18608/jla.2016.32.10</a></li>
<li><a id="reamer-1983"></a><strong>Reamer, F. G. (1983).</strong> <a href="https://doi.org/10.1086/644516">The Concept of Paternalism in Social Work</a>. <em>Social Service Review</em> 57(2), 254-271. doi: <a href="https://doi.org/10.1086/644516">https://doi.org/10.1086/644516</a></li>
<li><a id="wiley-oj"></a><strong>Wiley, D. (o. J.).</strong> <a href="https://opencontent.org/definition/">Defining the "Open" in Open Content and Open Educational Resources</a>. Abgerufen auf <a href="https://opencontent.org/definition/">http://opencontent.org/definition/</a></li>
</ul>

<h3>Downloads</h3>

<ul>
<li><a href="https://www.olivertacke.de/wp-content/uploads/2018/03/tacke_oliver-2018-mit_h5p_daten_sammeln_auswerten_und_vielleicht_die_lehre_oeffnen.bib">aources in BibTeX format</a></li>
<li><a href="https://www.olivertacke.de/wp-content/uploads/2018/03/LRS-h5p-LMS.svg">illustration as SVG format</a></li>
</ul>

<h3>Note</h3>

<hr />

<p>The text and the illustration are licensed under a CC0 license. Feel free to do with it whatever you want to.</p>

<p class="ccl">
  <a rel="license" href="https://creativecommons.org/publicdomain/zero/1.0/"><img alt="Creative Commons Zero Public Domain Dedication" style="border-width:0" src="https://licensebuttons.net/p/zero/1.0/88x31.png" /></a><br />Text and illustration "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">H5P-API</span>" by <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/?p=1295" property="cc:attributionName" rel="cc:attributionURL">Oliver Tacke</a> is published under a <a rel="license" href="https://creativecommons.org/publicdomain/zero/1.0/">CC Zero 1.0 Universal Public Domain Dedication</a>.
</p>
