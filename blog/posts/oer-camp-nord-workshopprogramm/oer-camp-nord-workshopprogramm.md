<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# OERCamp 2017 Nord: Workshop-Programm steht!

## Anmeldungen zu den Workshops nicht verpassen – jetzt anmelden!

Tags:

-->

Seit 2012 gibt es in Deutschland OERcamps, die sich als Treffen von Praktiker*innen im Themenfeld um digitale und offene Lehr-Lern-Materialien (Open Educational Resources [OER]) verstehen. Unter der [Schirmherrschaft der UNESCO](http://www.oercamp.de/2017/04/03/oercamp-schirmherrschaft-unesco/) findet im Jahr 2017 das [OERCamp 2017](http://www.oercamp.de/17/) gleich an vier Orten (Süd, West, Nord und Ost) statt. Das OERCamp 2017 Nord wird in Kooperation mit der Universität Hamburg und dem Projekt SynLLOER im Chinesischen Teehaus Yu Garden sowie den Räumlichkeiten der Schlüterstr. 51 am 23\. und 24.06.2017 stattfinden. Inhaltlich-strukturell teilt sich das OERCamp in rahmende Workshops sowie einen Barcamp-Session-Teil. Das Workshop-Programm des OERCamps Nord wurde nun online veröffentlicht: 48 Workshop-Angebote werden hochspannende Einblicke in zahlreiche Facetten des Themenfeldes OER bieten. Einzusehen ist das Workshop-Angebot unter folgendem Link:

*   [http://www.oercamp.de/17/nord/workshops/](http://www.oercamp.de/17/nord/workshops/)

---

**Achtung:** Da Teilnahmeplätze nach Anmeldungseingang vergeben werden, lohnt sich eine frühzeitige [Anmeldung](http://www.oercamp.de/anmeldung/)!
