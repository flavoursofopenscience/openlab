<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Der Weg zu freien Bildungsmaterialien

## Einführung in das  Urheberrecht, CC-Lizenzen und OER.

Tags:

-->

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=24" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

Dieser Workshop ist für vier Stunden konzipiert und bietet sowohl theoretischen Input zu openness, Urheberrecht, CC-Lizenzen und OER als auch konkrete Anleitungen zur praktischen Auseinandersetzung.

Die auf Folie 30 vorgestellten Arbeitsaufträge sind bearbeitet und ergänzt worden:

*   [Arbeitsaufträge.pdf](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/blob/master/blog/posts/der-weg-zu-freien-bildungsmaterialien/der-weg-zu-freien-bildungsmaterialien-arbeitsauftraege.pdf)
*   [Arbeitsaufträge.odt](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/blob/master/blog/posts/der-weg-zu-freien-bildungsmaterialien/der-weg-zu-freien-bildungsmaterialien-arbeitsauftraege.odt)
*   [Arbeitsaufträge.docx](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/blob/master/blog/posts/der-weg-zu-freien-bildungsmaterialien/der-weg-zu-freien-bildungsmaterialien-arbeitsauftraege.docx)

[Hier](https://docs.google.com/document/d/1_YEBJ2uYGqRDt2g4vWV92NwoyslzzxOTKt0HKQGvHiA/edit?usp=sharing) kann kollaborativ daran weiter gearbeitet werden.

## Lizenz

 <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><a href="https://h5p.org/presentation">H5P</a>-Präsentation "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"Der Weg zu freien Bildungsmaterialien/span>Der Weg zu freien Bildungsmaterialien" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/der-weg-zu-freien-bildungsmaterialien-2/" property="cc:attributionName" rel="cc:attributionURL">Celestine Kleinesper und Astrid Wittenberg für SynLLOER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0 International Lizenz</a>.
