<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# OERInfo-Webtalk zur #OER17 in London

## #OER17-Conference „The Politics of Open“ Webtalk

Tags:

-->

Am 23\. Februar 2017 führte die [Informationsstelle OER](http://open-educational-resources.de/webtalk-oer-conference/) die zweite Ausgabe ihres Webtalks durch, diesmal zum aktuellen Thema der #OER17-Conference „The Politics of Open“, die im April in London stattfindet und bei der [auch das SynLLOER-Team mit einem Beitrag](https://openlab.blogs.uni-hamburg.de/veranstaltung/oer17-the-politics-of-open/) dabei sein wird. Die #OER-Conference hat sich in den vergangenen Jahren zur zentralen internationalen Plattform zum Thema OER entwickelt und wird als Wanderkonferenz jedes Jahr in einer anderen Austragungs-Stadt ausgerichtet. Zu den letzten Stationen zählten Edinburg und Glasgow, und dieses Jahr macht die [#OER17-Conference](https://oer17.oerconf.org/) Station in Englands Hauptstadt London. ![](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/oer-webtalk-london17/fraser-merholz.jpg) Josie Fraser 2014\. Jöran Muuß-Merholz, Foto: Hannah Birr. Beide Fotos unter [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/).

Im aktuellen [Webtalk #2](http://open-educational-resources.de/webtalk-oer-conference/), der durch die Informationsstelle OER als sich mit aktuellen Entwicklungen im Bereich der deutschen und internationalen OER-Community befassendes interaktives Format konzipiert wurde und seit Februar 2017 sowohl live verfolgbar als auch später als Aufzeichnung abrufbar ist, kam Conference Chair Josie Fraser zu Wort, die das Konzept hinter der OER Conference sowie den inhaltlichen Schwerpunkt der diesjährigen Konfernenz beschrieb, einen Ausblick auf das Programm gab und erklärte, warum die [#OER17](https://twitter.com/OERConf) auch für die deutsche Community wichtig ist. Das Video sowie Sekundärmaterialien sind unter folgender URL abrufbar:

*   [http://open-educational-resources.de/webtalk-oer-conference/](http://open-educational-resources.de/webtalk-oer-conference/)
