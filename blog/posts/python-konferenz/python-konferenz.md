<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Python- Konferenz vom 16. bis zum 20.08.2017 in Ossa (Polen)

## Schnittschnelle Informatik &amp; Bildung "Open source" ist jedem Programmierer, Software-Entwickler oder ITler ein gängiger Begriff.

Tags:

-->

Diese Berufsfelder sind meist auf Maschinen ausgerichtet, nicht auf Menschen, daher sind open educational resources (OER) hier ein noch eher unbekanntes Thema. Und das obwohl gerade Software-Entwicklung von einer sich austauschenden und zusammenwirkenden community lebt und frei zur Verfügung stehende Materialien bei der Weiterentwicklung sehr hilfreich sein können.

Ebensosehr sind Schulen auf open-source-software, wie etwa LibreOffice, VLC, MozillaFirefox & Co. angewiesen. Allerdings ist der open-source-Gedanke im Bildungsbereich noch nicht ganz angekommen; damit open-source-software ihrer Definition und Sinnhaftigkeit entspricht, bedarf es eines konstruktiv-anregenden Austauschs zwischen EntwicklerInnen und NutzerInnen. Dies ist bei der ausschließlichen Nutzung einer software nicht der Fall, die EntwicklerInnen und deren software werden lediglich benutzt, und verbleiben durch ausbleibende Rückmeldung mit wenig Chance auf Verbesserung. Daher wäre eine Annäherung zum gemeinsamen und gemeinschaftlichen Wirken dieser bisher divergierenden Fachrichtungen eine vielversprechende Möglichkeit auf verbesserte Produkte und neuartige Erkenntnisse in Bildung und Informatik. Auf der PyCon bot sich mir die Gelegenheit, diesen Sachverhalt vor vielen internationalen und am Zusammenwirken interessierten InformatikerInnen simultan zu thematisieren. Nachdem die Teilnehmenden der PyCon meinen lightning talk* über OER gehört hatten, gab es einen derartigen InteressentInnen- Ansturm, dass ich noch spontan einen freien Vortrag und ein open space veranstaltet habe, um die Thematik zu vertiefen. Diese Begeisterung wurde nicht zuletzt hervorgerufen durch ein kleines soziales Experiment zur Einführung in pädagogische Methoden; Die Gruppenaktivierungsübung "micro wave" ist altersunabhängig, eingängig und schnell im Ablauf, bietet also einen angenehmen Einstieg in Aktivierungsmethoden im Allgemeinen.

## Versuchsprotokoll: "micro wave"

Fragestellung: Was passiert, wenn 300 vorwiegend männliche, hochbegate und eher introvertierte Individuen mit Spezialwissen (´nerds`) aus aller Welt von einer Frau mit Aktivierungmethoden aus dem Bildungsbereich konfrontiert werden?

**Hypothese: Schockschwerenot.**

### Versuchsdurchführung:

1.  Man stelle sich aufrecht hin, strecke die Arme parallel zum Boden vor sich aus, bewege die Finger auf und ab, und summe dabei tief und monoton.
2.  Man drehe sich mit weiterhin ausgestreckten Armen um die eigene Achse und summe währenddessen lauter und höher.
3.  Man strecke einen Arm in die Luft und rufe dabei "Bing!".

#### Versuchsbeobachtung:

![Foto „Jul12_lightning_talks_007_APeviani“ von Alessia Peviani unter der Lizenz CC BY 2.0 via Flickr](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/python-konferenz/microwave-001.jpg)
Foto „Jul12_lightning_talks_007_APeviani“ von Alessia Peviani unter der Lizenz [CC BY 2.0](https://creativecommons.org/licenses/by-nc/2.0/) via [Flickr](https://www.flickr.com/photos/photogenicgreen/35508619570/in/pool-europython17/)

![Foto „Jul12_lightning_talks_008_APeviani“ von Alessia Peviani unter der Lizenz CC BY 2.0 via Flickr](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/python-konferenz/microwave-002.jpg)
Foto „Jul12_lightning_talks_008_APeviani“ von Alessia Peviani unter der Lizenz [CC BY 2.0](https://creativecommons.org/licenses/by-nc/2.0/) via [Flickr](https://www.flickr.com/photos/photogenicgreen/35508619570/in/pool-europython17/)

![Foto „Jul12_lightning_talks_009_APeviani“ von Alessia Peviani unter der Lizenz CC BY 2.0 via Flickr](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/python-konferenz/microwave-003.jpg)
Foto „Jul12_lightning_talks_009_APeviani“ von Alessia Peviani unter der Lizenz [CC BY 2.0](https://creativecommons.org/licenses/by-nc/2.0/) via [Flickr](https://www.flickr.com/photos/photogenicgreen/35508619570/in/pool-europython17/)

![Foto „Jul12_lightning_talks_010_APeviani“ von Alessia Peviani unter der Lizenz CC BY 2.0 via Flickr](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/python-konferenz/microwave-004.jpg)
Foto „Jul12_lightning_talks_010_APeviani“ von Alessia Peviani unter der Lizenz [CC BY 2.0](https://creativecommons.org/licenses/by-nc/2.0/) via [Flickr](https://www.flickr.com/photos/photogenicgreen/35508619570/in/pool-europython17/)

![Foto „Jul12_lightning_talks_011_APeviani“ von Alessia Peviani unter der Lizenz CC BY 2.0 via Flickr](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/python-konferenz/microwave-005.jpg)
Foto „Jul12_lightning_talks_011_APeviani“ von Alessia Peviani unter der Lizenz [CC BY 2.0](https://creativecommons.org/licenses/by-nc/2.0/) via [Flickr](https://www.flickr.com/photos/photogenicgreen/35508619570/in/pool-europython17/)

### Ergebnis:

Entgegen aller Erwartungen hat das Aktivierungsspiel alle hellauf begeistert. Wenn wundert´s, dass in einer Personengruppe, innerhalb derer größtenteils nachts mit gekrümmtem Rücken auf Bildschirme gestarrt wird, ein Aktivierungsspiel seine dreifache Positivwirkungen zu erreichen scheint. Im Zuge einer retrospektiven Diskussion wurde von den Teinehmenden zurückgemeldet, dass ein bildungstheoretischer Vortrag deutlich weniger Aufmerksamkeit und Begeisterung hervorgerufen hätte als die Aktivierungsübung. Um zukünftig eine derart pädagogisch-informative Korrelation wie nach diesem lightning talk zu fördern, könnte also eine aufgeschlossene Haltung und die Vermittlung von fachgebietsinternen Methoden und Inhalten ein erster Schritt sein.

### kleines Konferenzlexikon

*   _open space_: Ansammlung extrovertierter Menschen, die sich zu einem vorgegebenen Thema eine Stunde lang austauschen und unter dem Vorwand, die Mitschrift aller Gedanken und Ideen zu bekommen, Kontaktdaten austauschen
*   _lightning talk_: exakt fünfminütige Präsentation eines beliebigen Themas.
*   _keynote_: ein- bis zweistündiger Vortrag einer offiziell eingeladenen Person zum Thema der Konferenz


## Lizenz

 <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><a href="https://h5p.org/presentation">H5P</a>-Präsentation "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Python- Konferenz vom 16. bis zum 20.08.2017 in Ossa (Polen)“</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/python-konferenz-vom-16-bis-zum-20-08-2017-in-ossa-polen/" property="cc:attributionName" rel="cc:attributionURL">Celestine Kleinesper für SynLLOER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0 International Lizenz</a>.
