<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# SynLLOER openLab: erweiterte Workshop-Übersicht online!

## Das <a href="https://openlab.blogs.uni-hamburg.de/synlloer-openlab/">openLab</a> des SynLLOER-Projekts bietet Hands On-Support zu vielen Fragen bei der Arbeit mit und der Produktion von OER.

Tags:

-->

Neben unseren [Schul-Workshops](https://openlab.blogs.uni-hamburg.de/synlloer-info-workshops/) und dem offenen Werkstatt-Bereich (immer Donnerstags, 14-18 Uhr am Universitätskolleg, Schlüterstr. 51, Raum 4018) gibt es von SynLLOER nun auch eine Auswahl von themenspezifischen Workshops für die Uni!

Neben der Auseinandersetzung mit der Frage, was OER eigentlich sind, wie sie in die Hochschullehre integriert werden können, und was es mit diesen ominösen Creative Commons-Lizenzen auf sich hat, bieten wir auch Workshops zur Arbeit mit Open Source-Tools wie GitLab, H5P, etc. an, die zur Erstellung von OER verwendet werden können.

Die aktuelle und stetig wachsende Workshop-Liste findet Ihr unter [**uhh.de/synlloer-uni-workshops**](https://uhh.de/synlloer-uni-workshops). Zur besseren Planung bitten wir bei Teilnahmeinteresse um Anmeldung per [Online-Formular](https://openlab.blogs.uni-hamburg.de/anmeldung-synlloer-hochschul-workshops/).

Viel Spaß beim Durchstöbern des Workshop-Angebots!
