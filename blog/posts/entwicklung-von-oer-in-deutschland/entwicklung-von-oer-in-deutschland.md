<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Entwicklung von OER in Deutschland - Timeline

## Timeline, die die Entwicklung von Open Educational Resources (OER) in Deutschland skizziert

Tags:

-->

Im Kontext der SynLLOER-[Beitragspräsentation](https://oer17.oerconf.org/sessions/openness-and-oer-in-germany-university-of-hamburgs-engagement-towards-a-culture-of-open-educational-practices-1553/) zur [#oer17](https://twitter.com/hashtag/oer17?f=tweets&vertical=default&src=hash) in London entstand eine Timeline, die die Entwicklung von Open Educational Resources (OER) in Deutschland skizziert. Daraus entwickelte sich nach der [JPEG-Variante](https://uhh.de/synlloer-timeline-germany) hier nun eine interaktive Version, die mithilfe des Tools H5P erstellt wurde und seitdem kontinuierlich gepflegt wird.

Eine Nachlese zur OER17 finden Sie auch [hier](https://openlab.blogs.uni-hamburg.de/nachlese-oer17-london-uk-05-06-april-2017/) in unserem Blog.

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=2" width="818" height="865" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a> <br /> H5P-Timeline "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Development of OER in Germany</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/timeline-development-of-oer-in-germany/" property="cc:attributionName" rel="cc:attributionURL">Tobias Steiner für SynLLOER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0 International Lizenz</a>.
