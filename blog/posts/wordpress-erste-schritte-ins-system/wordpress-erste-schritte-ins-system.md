<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# WordPress: Erste Schritte ins System

## Einführung in die Weblog-Software WordPress

Tags:

-->

In dieser Einführung lernen Sie die Weblog-Software WordPress und deren grundliegende Funktionsweise im Hinblick auf Medieneinbindung und Einsatz im Hochschulkontext der Universität Hamburg kennen. Im Fokus stehen dabei das Kennenlernen des [WordPress Starterkit Userguides](https://synlloer.gitbooks.io/starterkit-user-guide/content/), erste redaktionelle Schritte im Front- sowie Backend und die Einbindung von interaktiven Medien mit dem Tool [H5P](https://h5p.org/).

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=17" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

Darüberhinaus werden Ihnen Administrationseinstellungen für die Benutzerverwaltung vertraut gemacht. Diese helfen Ihnen dabei, NutzerInnen in das System zu integrieren oder mit den benötigten Rechten auszustatten. Abschließend werden noch Systemerweiterungen sog. Plugins vorgestellt und für Ihren Handlungsbedarf im Projektalltag hin diskutiert.

Diese Veranstaltung ist für drei Zeitstunden ausgelegt. Die Teilnehmenden sollten nach der Veranstaltung zwei weitere Zeitstunde für die Nachbereitung der praktischen Übunungen einplanen.

**Voraussetzungen für diesen Kurs:**

*   Internetfähiges Endgerät (vorzugsweise Laptop)

**Unterlagen für diesen Workshop:**

*   [Link zur Präsentation](https://openlab.blogs.uni-hamburg.de/wordpress-erste-schritte-ins-system/)
*   [Link zum Veranstaltungsskript](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/blob/master/workshops/wordpress-introduction/script/wordpress-script-introduction.md)
*   [Link zum WordPress Starterkit Userguide des Universitätskolleg der Universität Hamburg](https://synlloer.gitbooks.io/starterkit-user-guide/content/)
*   [Link zum WordPress Kompendium Band 6 des Universitätskolleg – Erste Schritte zum eigenen Blog](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/blob/master/workshops/wordpress-introduction/material/wordpress-kompendium-band6.pdf)

**Dieser Workshop wird an folgenden Daten angeboten:**

*   [Donnerstag, 16.11.17 von 10-13 Uhr](https://openlab.blogs.uni-hamburg.de/veranstaltung/wordpress-erste-schritte-ins-system-20171116/)
*   [Dienstag, 05.12.17 von 10-13 Uhr](https://openlab.blogs.uni-hamburg.de/veranstaltung/wordpress-erste-schritte-ins-system-20171205/)

**Mindestteilnehmendenzahl: 2 Personen:**

Um [Voranmeldung über das Formular](https://openlab.blogs.uni-hamburg.de/anmeldung-synlloer-hochschul-workshops/) wird gebeten.

## Lizenz

 <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /><br /></a> H5P-Präsentation "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">WordPress: Erste Schritte ins System</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/wordpress-erste-schritte-ins-system/" property="cc:attributionName" rel="cc:attributionURL">Manfred Steger für SynLLOER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0 International Lizenz</a>.
