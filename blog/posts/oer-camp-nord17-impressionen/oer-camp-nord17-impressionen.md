<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Eindrücke vom #OERCamp17 Nord (23. & 24.06.2017)

## Impressionen

Tags:

[embed]https://www.flickr.com/photos/fotoexperiencede/sets/72157682537527134/[/embed]

-->

<a href="http://creativecommons.org/licenses/by/4.0/"><img class="alignleft wp-image-454" title="Creative Commons BY SA 4.0 Lizenz" src="https://openlab.blogs.uni-hamburg.de/wp-content/uploads/2017/04/by.png" alt="Creative Commons BY 4.0 Lizenz" width="120" height="42" /></a>


## Lizenz

Die hier veröffentlichten Inhalte stehen unter der <a href="http://creativecommons.org/licenses/by/4.0/" target="_blank" rel="noopener noreferrer">CC BY 4.0-Lizenz</a>. Der Name des Urhebers soll bei einer Weiterverwendung wie folgt genannt werden: Manfred Steger – Universität Hamburg für <a href="https://openlab.blogs.uni-hamburg.de" target="_blank" rel="noopener noreferrer">SynLLOER</a> - Synergien für Lehren und Lernen durch OER.
