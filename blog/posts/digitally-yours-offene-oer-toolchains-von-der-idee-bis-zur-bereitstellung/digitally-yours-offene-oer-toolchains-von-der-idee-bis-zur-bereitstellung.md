<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Digitally Yours: Offene OER-Toolchains, von der Idee bis zur Bereitstellung

## Workshop auf dem #OERcamp18 Nord am 16.06.2018

Tags:

-->

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=36" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><a href="https://h5p.org/presentation">H5P</a>-Präsentation "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"Digitally Yours: Offene OER-Toolchains/span>Digitally Yours: Offene OER-Toolchains" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/digitally-yours-offene-oer-toolchains-von-der-idee-bis-zur-bereitstellung/" property="cc:attributionName" rel="cc:attributionURL">Tobias Steiner, Manfred Steger, Celestine Kleinesper und Astrid Wittenberg für SynLLOER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0 International Lizenz</a>.
