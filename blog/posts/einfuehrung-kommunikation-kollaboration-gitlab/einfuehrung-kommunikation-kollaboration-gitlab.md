<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Einführung, Teamkommunikation und -Kollaboration in GitLab

## In dieser Einführung lernen Sie das Projektmanagement Tool GitLab und deren grundliegende Funktionsweise für den Hochschulkontext kennen.

Tags:

-->

Im Fokus stehen dabei Teamkommmunikation und -Kollaboration in eigenen **Projekten** und wie sie dort in sog. **Issues** Aufgaben erstellen und gemeinsam daran arbeiten können. Darüberhinaus werden Sie mit den Grundeinstellungen für das eigene Profil und den Benachrichtigungstypen vertraut gemacht. Diese helfen Ihnen, andere Projekte im Auge zu behalten und sich selbst über deren Stand Benachrichtigungen automatisch einzuholen.

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=14" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

Diese Veranstaltung ist für zwei Zeitstunden ausgelegt. Die Teilnehmenden sollten nach der Veranstaltung eine weitere Zeitstunde für die Nachbereitung der praktischen Übunungen einplanen.

**Voraussetzungen für diesen Kurs:**

*   Internetfähiges Endgerät (vorzugsweise Laptop)
*   Abgeschlossene Registrierung ([Link zur UHH-GitLab Registrierung](https://git.universitaetskolleg.uni-hamburg.de/users/sign_in))
*   Ihre B-Kennung und das zugehörige GitLab Passwort (Erstellung bei der Registrierung)

In dieser Einführung lernen Sie das Projektmanagement Tool GitLab und deren grundliegende Funktionsweise für den Hochschulkontext kennen. Im Fokus stehen dabei Teamkommmunikation und -Kollaboration in eigenen **Projekten** und wie sie dort in sog. **Issues** Aufgaben erstellen und gemeinsam daran arbeiten können. Darüberhinaus werden Sie mit den Grundeinstellungen für das eigene Profil und den Benachrichtigungstypen vertraut gemacht. Diese helfen Ihnen, andere Projekte im Auge zu behalten und sich selbst über deren Stand Benachrichtigungen automatisch einzuholen.

Diese Veranstaltung ist für zwei Zeitstunden ausgelegt. Die Teilnehmenden sollten nach der Veranstaltung eine weitere Zeitstunde für die Nachbereitung der praktischen Übunungen einplanen.

**Voraussetzungen für diesen Kurs:**

*   Internetfähiges Endgerät (vorzugsweise Laptop)
*   Abgeschlossene Registrierung ([Link zur UHH-GitLab Registrierung](https://git.universitaetskolleg.uni-hamburg.de/users/sign_in))
*   Ihre B-Kennung und das zugehörige GitLab Passwort (Erstellung bei der Registrierung)

**Unterlagen für diesen Workshop:**

*   [Link zur Präsentation](https://openlab.blogs.uni-hamburg.de/einfuehrung-teamkommunikation-und-kollaboration-in-gitlab/)
*   [Link zum Veranstaltungsskript](https://git.universitaetskolleg.uni-hamburg.de/Manfred/workshops-manfred/blob/master/gitlab-introduction/script/gitlab-script-introduction.md)

**Dieser Workshop wird an folgenden Daten angeboten:**

*   [Donnerstag, 02.11.17 von 10-12 Uhr](https://openlab.blogs.uni-hamburg.de/veranstaltung/team-kommunikation-und-kollaboration-mit-gitlab-20171102/)
*   [Donnerstag, 30.11.17 von 10-12 Uhr](https://openlab.blogs.uni-hamburg.de/veranstaltung/team-kommunikation-und-kollaboration-mit-gitlab-20171130/)

**Mindestteilnehmendenzahl: 2 Personen:**

Um [Voranmeldung über das Formular](https://openlab.blogs.uni-hamburg.de/anmeldung-synlloer-hochschul-workshops/) wird gebeten.

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /> H5P-Präsentation "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Einführung, Teamkommunikation und -Kollaboration in GitLab</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/einfuehrung-teamkommunikation-und-kollaboration-in-gitlab/" property="cc:attributionName" rel="cc:attributionURL">Manfred Steger für SynLLOER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0 International Lizenz</a>.
