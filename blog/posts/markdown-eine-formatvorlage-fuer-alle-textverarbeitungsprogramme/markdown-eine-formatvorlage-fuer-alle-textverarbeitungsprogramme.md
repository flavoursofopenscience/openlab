<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Markdown: Eine Formatvorlage für alle Textverarbeitungsprogramme

## Praxiseinführung in die Auszeichnungssprache Markdown

Tags:

-->

In dieser Einführung lernen Sie die Auszeichnungssprache Markdown kennen und deren grundliegende Funktionsweise im medial geprägten Arbeitsalltag. Im Fokus stehen dabei das Kennenlernen der Syntax, eine Auswahl an Webtools und offline Programme zur Unterstützung des individuellen Schreibprozesses. Abschließend werden die gesammelten Erfahrungen mit Markdown in praktischen Übungen erprobt. Die Übungen richten sich an gängigen Management Aufgaben wie z.B. Protokolle schreiben, ToDo-Listen erstellen, Dokumentationen vorbereiten und dokumentübergreifende Textverarbeitungsaufgaben aus.

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=18" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

Diese Veranstaltung ist für eine Zeitstunden ausgelegt. Die Teilnehmenden sollten nach der Veranstaltung zwei weitere Zeitstunde für die Nachbereitung der praktischen Übungen einplanen.

**Voraussetzungen für diesen Kurs:**

*   Internetfähiges Endgerät (vorzugsweise Laptop)

**Unterlagen für diesen Workshop:**

*   [Link zur Präsentation](https://openlab.blogs.uni-hamburg.de/markdown-eine-formatvorlage-fuer-alle-textverarbeitungsprogramme/)
*   [Link zum Veranstaltungsskript](https://git.universitaetskolleg.uni-hamburg.de/Manfred/workshops-manfred/blob/master/markdown-introduction/script/markdown-script-introduction.md)

**Dieser Workshop wird immer Donnerstags im [openLab](https://openlab.blogs.uni-hamburg.de/openlab/) des Universitätskollegs der Uni Hamburg angeboten.**

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><a href="https://h5p.org/presentation">H5P</a>-Präsentation "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Markdown: Eine Formatvorlage für alle Textverarbeitungsprogramme</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/markdown-eine-formatvorlage-fuer-alle-textverarbeitungsprogramme/" property="cc:attributionName" rel="cc:attributionURL">Manfred Steger für SynLLOER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0 International Lizenz</a>.
