<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# OER suchen, finden und selbst erstellen Creative Commons und Open Education im Schulkontext

## H5P Präsentation zu OER suchen, finden und selbst erstellen Creative Commons und Open Education im Schulkontext

Tags:

-->

Mit und von Matthias Andrasch, Oliver Tacke, Manfred Steger, Universität Hamburg; Lisa Rosenbaum, Hochschule für Wirtschaft und Recht Berlin; Tobias Steiner, Universität Hamburg.

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=20" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /><br /></a> H5P-Präsentation "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Beitrag OER-Fachforum 2017 Berlin: Bandauftritt H5P</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/beitrag-oer-fachforum-2017-berlin-bandauftritt-h5p/" property="cc:attributionName" rel="cc:attributionURL">H5P Band</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0 International Lizenz</a>.
