<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Plakat "Der Weg zu OER" für Schulen & Hochschulen

## Gemeinsam mit der Redaktion des Fachmagazins Synergie hat SynLLOER ein Poster entwickelt, das die verschiedenen Schritte aufzeigt, die zur Erstellung von OER empfohlen werden.

Tags:

-->

Das Poster ist gedacht für die Zielgruppe der Lehrenden an Hamburger Schulen und Hochschulen und möchte in diesem Kontext im oftmals als unübesichtlich wahrgenommenen Informationsdschungel eine erste Orientierungshilfe zum Thema OER bieten. Zudem werden im Poster auch zahlreiche Tipps zu verschiedenen Anwendungsszenarien und offenen Tools gegeben, die die Erstellung von OER ermöglichen. Vielfältige nützliche Hinweise zu den Facetten der einzelnen Schritte unterstützen die Visualisierung des Erstellungsprozesses.

*   Der Link zum Poster und der Synergie Praxis-Broschüre, die die Einführung in das Thema OER etwas detaillierter vornimmt: [https://uhh.de/br409](https://uhh.de/br409)
*   Das Poster als PDF: [https://uhh.de/ibgs7](https://uhh.de/ibgs7)
*   Lehrende und Interessierte, die das Poster gerne in A1-Format kostenlos zugesandt haben möchten, können dem Team der Redaktion Synergie per E-Mail die gewünschte Anzahl und Ziel-Adresse mitteilen: [redaktion.synergie@uni-hamburg.de](mailto:redaktion.synergie@uni-hamburg.de)

Selbstverständlich ist auch die Erstellung des Posters nicht der Weisheit letzter Schluss und als ein erster Impuls gedacht :) Haben wir Details übersehen oder könnte die Darstellung noch anders realisiert werden? Über Feedback hier im Blog, auf Twitter ([@SynLLOER](https://twitter.com/SynLLOER)), per [E-Mail](mailto:synlloer@uni-hamburg.de) oder persönlich im [SynLLOER-openLab](https://openlab.blogs.uni-hamburg.de/synlloer-openlab/) würden wir uns sehr freuen!

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a> <br /> Blog-Beitrag & Abbildung "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Plakat: Der Weg zu OER" für Schulen & Hochschulen</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/plakat-der-weg-zu-oer-fuer-schulen-hochschulen/" property="cc:attributionName" rel="cc:attributionURL">Projekt SynLLOER - Synergien für Lehren und Lernen durch OER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0 International Lizenz</a>.
