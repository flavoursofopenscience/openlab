<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Nachlese: #OER17, London, UK - 05./06. April 2017

## „The Politics of Open“ Konferenz in London

Tags:

-->

Am 05\. und 06\. April 2017 fand unter dem Motto „The Politics of Open“ in London die englischsprachige Konferenz [OER17](https://oer17.oerconf.org/) statt, zu der sich internationales Publikum (ca. 200 Teilnehmende) in ca. 70 Workshops mit zahlreichen Facetten des Openness-Themas und insbesondere den titelgebenden OER (Open Educational Resources, Offene Bildungsmaterialien) austauschte.

![OER17 conference banner](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-oer17-london/oer17-2.jpg)
[![Creative Commons Lizenzvertrag](https://licensebuttons.net/l/by-sa/3.0/80x15.png)](http://creativecommons.org/licenses/by-sa/4.0/)<small> „<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">OER17 conference banner</span>“ von [Tobias Steiner für SynLLOER](https://openlab.blogs.uni-hamburg.de/) ist lizenziert unter einer [CC BY-SA 4.0 International Lizenz](http://creativecommons.org/licenses/by-sa/4.0/).</small>

Während der zwei Tage wurden vier große Themenschwerpunkte facettenreich diskutiert: So stand die aktive offen-kritische Auseinandersetzung mit der Idee der ‚Openness‘ im Fokus und zahlreiche Beiträge beleuchteten mit Fragestellungen wie „Wer ermöglicht Openness“ und „Wem ist Openness sowohl reziptiv als auch produktiv aufgrund ökonomischer, technischer oder soziokultureller Abhängigkeiten verwehrt?“, welche Abhängigkeiten sowie explizite und implizite Machtverhältnisse die Idee von Offenheit mit sich bringt. Hochaktuell in diesem Kontext waren zudem mehrere Beiträge, die sich mit der Wahl von Donald [Trump sowie dem Brexit](https://oer17.oerconf.org/sessions/perspectives-on-open-education-in-a-world-of-brexit-trump-1559/) und den Auswirkungen auf die Openness-Debate beschäftigten.

Als zweiter Fokuspunkt wurde der Blick auf die sog. _Open Pedagogy_ gelenkt. Hier wurde insbesondere die Rolle sowie sich daraus entwickelnde Herausforderungen von Lehrenden und Multiplikatoren im Prozess der Öffnung von Unterricht im Sinne der Openness unter Berücksichtigung von Nachhaltigkeit, Accessibility sowie freier Verfügbarkeit beleuchtet. Als weiterer Schwerpunkt der Agenda stand zudem die Auseinandersetzung mit Lehrunterstützungs-Tools und den größeren technischen Infrastrukturen, die nötig sind, um Bildungsmaterialien und Forschunsdaten dem Openness-Gedanken Rechnung tragend offen und frei zugänglich zu machen.

![OER17 conference banner](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-oer17-london/oer17-4.jpg)
[![Creative Commons Lizenzvertrag](https://licensebuttons.net/l/by-sa/3.0/80x15.png)](http://creativecommons.org/licenses/by-sa/4.0/)<small> „<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">OER17: break between sessions</span>“ von [Tobias Steiner für SynLLOER](https://openlab.blogs.uni-hamburg.de/) ist lizenziert unter einer [CC BY-SA 4.0 International Lizenz](http://creativecommons.org/licenses/by-sa/4.0/).</small>

Neben zahlreichen Beiträge aus Nordamerika und Großbritannien waren auch Fallbeispiele aus Ghana, South Africa, dem Libanon, Australien, Indien, sowie Schweden vertreten und zeigten im Querschnitt auf, welcher Bandbreite an Herausforderungen sich OER-Projekte je nach lokalem Kontext gegenübersehen, und auf wie vielfältige Weise diese Herausforderungen entweder gemeistert wurden oder, in so manchen Fällen, auch nicht. Denn: dem Kernthema der Konferenz – der Begriff der Openness – treu bleibend kamen auch Beispielfälle zu Wort, deren ‚Scheitern‘ als konstruktiv wertvolle Information für andere gesehen und dankbar diskutiert wurde (Beispiel: „[Wie Wales die Tür in Richtung Openness öffnete, aber nicht hindurchging](https://oer17.oerconf.org/sessions/cymru-agored-how-wales-opened-the-door-to-oer-but-didnt-walk-through-1509/)“).

Unter den Teilnehmenden aus zahlreichen Kontinenten waren auch vier deutsche Projekte vor Ort: Während der aktuelle Stand der OERInfo-Förderlinie des BMBF-Förderprogramms „[Digitale Medien in der beruflichen Bildung](https://www.bmbf.de/de/digitale-medien-in-der-bildung-1380.html)“ sowie die zahlreichen Aktivitäten durch die Informationsstelle OER vorgestellt wurde, erläuterte das Verbundprojekt „Multiplikator/innenunterstützung für Open Educational Resources in Niedersachsen ([MOIN](https://oerworldmap.org/resource/urn:uuid:d973a788-072d-4b16-baca-b7f4ec9a34dd))“ seinen institutionsübergreifenden Ansatz. Die Universität Duisburg-Essen wiederum bot Einblicke in die Entwicklungsbestrebungen einer universitären Digitalstrategie.

![OER17 conference banner](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-oer17-london/oer17-4.jpg)
[![Creative Commons Lizenzvertrag](https://licensebuttons.net/l/by-sa/3.0/80x15.png)](http://creativecommons.org/licenses/by-sa/4.0/)<small> „<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">OER17: The audience is listening</span>“ von [Tobias Steiner für SynLLOER](https://openlab.blogs.uni-hamburg.de/) ist lizenziert unter einer [CC BY-SA 4.0 International Lizenz](http://creativecommons.org/licenses/by-sa/4.0/).</small>

Zudem stellte das SynLLOER-Projekt der Universität Hamburg in einem Beitrag die zahlreichen Aktivitäten vor, die in der Metropolregion Hamburg unternommen werden, um eine Kultur Offener Bildungspraktiken nachhaltig zu befördern. Zusammenfassend ist festzustellen, dass das Format der OER17 eine echte Bereicherung für alle Konferenz-Teilnehmenden war, die durch zahlreiche Beiträge sowie Workshops regen Austausch förderte und den vielzitierten Blick über den Tellerrand auf den internationalen Kontext sowie die Problemstellungen anderer Nationen und die dort gefundenen Lösungswege ermöglichte. Im Folgenden werden Links zu konferenzrelevanten Materialien bereitgestellt:

*   Beitragsfolien SynLLOER: [https://uhh.de/synlloer-oer17](https://uhh.de/synlloer-oer17)
*   Timeline "Entwicklung von OER in Deutschland von 1997 bis 2017": [https://uhh.de/synlloer-timeline-germany](https://uhh.de/synlloer-timeline-germany)
*   Timeline interaktiv: [https://openlab.blogs.uni-hamburg.de/timeline-development-of-oer-in-germany/](https://openlab.blogs.uni-hamburg.de/timeline-development-of-oer-in-germany/)
*   Resourcen-Sammlung zur OER17 (Blogeinträge von Teilnehmenden, Folien zu den zahlreichen Beiträgen, etc.) : [https://uhh.de/synlloer-oer17resources](https://uhh.de/synlloer-oer17resources)
*   Podcast-Audionachlese von Christian Friedrich und Markus Deimann: "Feierabendbier: Open Education - Episode 25: Rückblick #OER17": [https://soundcloud.com/foepodcast/rueckblick-oer17-1](https://soundcloud.com/foepodcast/rueckblick-oer17-1)

Quelle Titelbild: "With Openness comes increased responsibility" - sketch by Bryan M Mathers (@bryanMMathers), under a [CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/) licence
