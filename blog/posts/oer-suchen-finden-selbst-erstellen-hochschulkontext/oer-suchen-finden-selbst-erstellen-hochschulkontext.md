<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# OER suchen, finden und selbst erstellen - Open Access und Creative Commons im Hochschulkontext

##

Tags:

-->

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=15" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />H5P-Präsentation "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">OER suchen, finden und selbst erstellen – Creative Commons und Open Education im Hochschulkontext</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/oer-suchen-finden-und-selbst-erstellen-open-access-und-creative-commons-im-hochschulkontext/" property="cc:attributionName" rel="cc:attributionURL">Lucas Jacobsen, Celestine Kleinesper und Tobias Steiner für SynLLOER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0 International Lizenz</a>.
