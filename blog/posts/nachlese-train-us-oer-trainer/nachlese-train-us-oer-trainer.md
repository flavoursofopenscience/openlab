<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Nachlese:#OERjointly17b, Train-us-OER-Trainer, Weimar – 12./13. Juni 2017

## H5P Präsentation zu OER suchen, finden und selbst erstellen Creative Commons und Open Education im Schulkontext

Tags: Die Train-us-OER-Trainer Tagung beim Verbundprojekt JOINTLY

-->

Am 12. und 13. Juni 2017 fand unter dem Motto „Train-us-OER-Trainer“ eine Tagung beim Verbundprojekt JOINTLY ([jointly.info](http://jointly.info/)) in Weimar statt. Im Fokus stand die Unterstützung der OER-Akteure bei der Entwicklung und Verbreitung von offenen Bildungsmaterialien. Die Ergebnisse können im OER-Content-Buffet eingesehen werden ([oer-contentbuffet.info](https://oer-contentbuffet.info/)).

12 OER-Qualifizierungsthemen mit jeweiligem Praxismodul standen auf dem Trainingsplan zum OER-Allrounder. Das Leitkonzept des Jointly Organisationsteams um Annett Zobel und Matthias Hupfer war von Nachhaltigkeit geprägt. Die Teilnehmenden sollten fachliche und  netzwerkübergreifende Kompetenzen ausbilden, damit Sie zukünftig als MultiplikatorInnen in der eingeleiteten OER-Renaissance agieren können.

## Die OER-Qualifizierungsthemen im Überblick:

1. **OER- Was und Warum? (OER Historie und Vision / OER Landkarte / Aufklärung und Sensibilisierung für Lehrende)**
2. **OER Praxis (OER suchen, finden, nutzen / OER Remix / OER verbreiten)**
3. **OER Produzieren**
4. **Rechtliche Aspekte (Urheberrecht und Lizenzen / Verwendungsarten und Anwendung / weitere Rechtsfragen)**
5. Qualitätssicherung
6. **Infrastrukturen (Software zum Erstellen und Nutzen / Software zum OER verwalten / Formate, Metadaten, Schnittstellen, Standards / OER Infrastrukturen / Innovationen u.a, KI)**
7. Community-Entwicklung
8. Kompetenz-Entwicklung
9. Sensibilisierung und Verbreitung (Zielgruppen und Ansatzpunkte / Formate und Situationen / 1.F: We are open! OER in CI und Onlinepräsenz / 2.F: OER Schulungen / Workshops für Lehrende / 3.F: OER Buttons präsent in E-Learning-Tools)
10. Policy Entwicklung (Was gehört in eine Policy und warum? / Der (erfolgversprechende) Weg zur eigenen Policy)
11. Systemische Adoption
12. Finanzierung
13. Forschung & Evaluation

Die Teilnehmenden haben sich vorab darauf verständigt 5 von 13 Themen theoretisch zu beleuchten. Mit best-practises als auch mit hands-on Workshops gelang den Vortragenden der Brückenschlag zwischen fachlich fundierter Einordunung und Handlungsorientierung im Umgang mit OER.

## Orientierung und Personifizierung

Susann Hippler (OpERA), Axel Jindra (OER@RLP) und Annett Zobel (JOINTLY) hielten den Initialvortrag zu **AG Personas**, damit OER zielgruppenspezifisch und menschennah über Masken – sog. Personas –gedacht/entwickelt werden.

- WIKI: [Link zum Wiki](https://wiki.oer-contentbuffet.info/mediawiki/index.php/OERde_Personas)
- OER Contentbuffet: AG Personas → Personas-03.pptx
([Link zum Online Editor Contentbuffet](https://oer-contentbuffet.info/edu-sharing/components/render/9f759e7e-d1a2-4815-baf9-0dce46b97ca2))

## OER Kompetenzen Qualifizierungs- und Sensibilisierungsthemen

Mit praxeologischer Manier veranstalteten Bettina Waffner (Mainstreaming OER) und Susanne Friz (LOERn) eine Challenge:
*“SchnOERzeljagd” - Wie findet man OER-Inhalte*.

Die OER-Challange richtete sich an Einsteiger und Fortgeschrittene, die vorhandene Suchmaschinen-Kompetenzen unter Beweis stellen wollten. Besonders interessant war der Feedback/Review-Anteil nach jeder Aufgabe, der divergente Lösungsansätze den anderen Teilnehmenden präsentierte
([Link zur OER-Schnitzeljagd](https://goo.gl/E4llwS)).

Einen weiteren Schritt auf dem Weg zu OER verfolgte Manfred Steger (SynLLOER). Das Trainingsmodul zu **OER erstellen und produzieren** nahm sich der technischen Umsetzung und deren Problematik zu unterschiedlichen Formaten an. Markdown als universelle Auszeichnungssprache bildete die Basis für die OER Erstellung. Openness war im zweiten Teil ein wichtiger Bestandteil, denn geschlossene Formate verhindern eine gute OER-Praxis nach den 5V-Freiheiten die Teil des weit verbreiteten OER-Postulats von David Wiley sind. Mit dem Online-Tool **[Gitbook.com](http://gitbook.com/)** erstellten die Teilnehmenden eine erste kollaborative OER (ebook) auf Markdown Basis.

**Folien der Präsentation**

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=3" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

## OER Policy

Ein Regelwerk für Bildungseinrichtungen, das Zugänge zu autorenfreundlichen Lizenzen und nutzerorientierte Transparenz schafft, damit OER nicht nur eine verpasste Ausfahrt auf dem digitalen Highway wird – dieser Mammutaufgabe stellten sich Franziska Richter (ProOER) und Claudia Lehmann (MOIN). Policies sind keine festverankerten proklamatorische Gesetze, sondern individuelle Richtlinien in komplexen systematischen Organisationseinheiten. Mit einem assoziativen Gedankenspiel im *Think-Pair-Share* Format vermittelten die Vortragenden den komplexen Begriff anschaulich.

![Das Bild zeigt eine Metaplanwand mit sehr vielen Bildern, die assoziativ OER zugeordnet wurden. So zum Beispiel ein Zaun, der mit "Policy schafft einen Rahmen" assoziiert wurde.](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-train-us-oer-trainer/oer-policies-hoffungen.jpg)
*Bildnachweis: OER-Policy Hoffnungen, Manfred Steger, [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)*

**Brainstorming Pad:**
[Link zum Brainstorming Pad OER Policy](https://pad.okfn.de/p/Policy_Brainstorming)

## Change Management

Einen thematisch Brückenschlag von der Policy zum Change Management ermöglichte der Vortrag von Axel Jindra (OER@RLP), David Schmid (OpERA), Kristina Novy (OpERA), Luca Mollenhauer (OERinfo), Sarah Keller (OER@RLP), Susann Hippler (OpERA), Tim Wiegers (OER@RLP). Im Diskurs entwickelte sich eine *prototypische Musterlösung*, die von Hanno Langfelde visualisiert wurde.

![OER Change Management - Auf dem Weg zur Musterlösung zeigt viele einzelne Bestandteile die im Change Management berücksichtigt werden soll. Einige Aspekte sind: Es dauert lange! Loose control gain influence, auf meiner Ebene wirken explorative Verfahren, bestimmte Formate nutzen, verbündete suchen, persönliche Kontakte nutzen und und und.](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-train-us-oer-trainer/oer-change-management-auf-dem-weg-zur-muesterloesung.jpg)
*Bildnachweis: Foto: OER - Auf dem Weg zur Musterlösung, Manfred Steger, Visualisierung von Hanno Langfelde [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)*

Besonders angeregt zeigte sich der Diskurs in den Bereichen Top-Down und Bottom-Up Management an institutionalisierten Organisationen. Die Teilnehmenden waren sich einig, dass ein gutes Gesamtergebnis entstehen kann, wenn beide Seiten sich annähern und Kompromisse eingeben – mit dem Ziel einen ineffizienten Kontrollapparat schrittweise abzutragen und als *pro bono* Einfluss zu gewinnen (gain influence).

## Recht

Paul Klimpel, Anna Wiggeringloh, Thomas Haubner und Sebastian Horlache hielten einen kontroversen Impulsvortrag zum *jungen* Urheberrecht gegegen ein ursprünglich, gesellschaftlich postuliertes Lizenzmodell der Creative Commons. Zum Einstieg unterstrich Paul Klimpel mit dem paraphrasierten Pioniersgedanken, dass wir (ForscherInnen) im Bereich OER kein Neuland beschreiten – eher altes Wissen an normativen Anforderungen einer zunehmend digitalen Gesellschaft aufbereiten und denken müssen. Das Urheberrecht besteht erst seit ca. 200 Jahren und war von seinen Schaffern nicht für digitale Anwendungszwecke ausgelegt. Es fand kein Transformationsprozess, sondern lediglich eine Eins-zu-ein-Übertragung auf das digitale Medium statt. Digitales Arbeiten bedeutet nämlich immer eine Kopie zu erstellen, sei es im Browser-Cache, im RAM des Computers oder in direkter Form als Dokumenttyp auf der Festplatte – was im klassischen Urheberrecht ein Kardinalproblem darstellt. Auf die immer wiederkehrende Frage, was denn nun der rechtssichere sowie richtige Weg zu OER sei, rieten die ExpertenInnen in biblischer Art und Weise: "Am Anfang steht das Recht". Bei der Thematik OER sollte zu Beginn der rechtliche Rahmen betrachtet und gesetzt werden, nachträgliche Anpassungen sind teilweise rechtswidrig und nur schwer mit hohem Einsatz an Ressourcen zu begradigen.

![Foto eines Laptop Bildschirms mit geöffnetem Markdown Editor und dem Satz: Am Anfang stand das Recht.](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/nachlese-train-us-oer-trainer/oer-recht-paul-klimpel-vortrag.jpg)
*Bildnachweis: OER Recht Klimpel Vortrag Notizen, Manfred Steger, [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/legalcode)*

**Materialien**: [Link zu den Recht Materialien](https://wiki.oer-contentbuffet.info/mediawiki/index.php/OER_Rechtliche_Fragen)

**Tools**

- Lizenzhinweisgenerator (Bilder von Wikimedia/Wikipedia)
- Creative Commons (Lizenzen über Auswahl treffen choose)
- http://www.photosforclass.com/ (beim Download bei flickr wird Lizenz automatisch angehangen)

## Fazit

Die Train-us-OER-Trainer Tagung machte im Bezug auf das SynLLOER-Projekt deutlich, dass eine Schwerpuntksetzung auf ausgewählte Dimensionen des Themas OER in einem Einzelprojekt zwar einerseits nötig sind, andererseits aber natürlich immer noch mehr getan werden kann. So zeigte sich, dass SynLLOER mit den definierten Schwerpunktbereichen "Awareness" und "OER-Erstellung" auf einem guten Weg ist, aber darüberhinaus auch zahlreiche andere Bereiche wie bspw. die Entwicklung von OER-Policies, die nicht Teil des Hamburger Projektes sind, wichtig sind. Somit wurde auch die Vielfalt der Projekte der BMBF-Förderlinie OERInfo deutlich. Um zwischen den verschiendenen Schwerpunktbereichen auch zukünftig Synergien zu schaffen, steht die weitere Vernetzung und der aktive Austausch zu den einzelnen Tätigkeitsfeldern an - beispielsweise beim OERCamp17 Nord.

## Lizenz

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)](http://creativecommons.org/licenses/by/4.0/) Die hier veröffentlichten Inhalte (Beitrag, Foto) „<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Nachlese:#OERjointly17b, Train-us-OER-Trainer, Weimar – 12./13. Juni 2017</span>“ von [Manfred Steger für SynLLOER](https://synlloer.blogs.uni-hamburg.de/nachleseoerjointly17b-train-us-oer-trainer/) ist lizenziert unter einer [CC BY 4.0 International Lizenz](http://creativecommons.org/licenses/by/4.0/).
