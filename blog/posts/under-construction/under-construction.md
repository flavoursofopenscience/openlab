<!-- Intro Text

 Die Toolchains werden gerade vom [SynLLOER-Team](https://synlloer.blogs.uni-hamburg.de/) erarbeitet.

-->

Da es im Internet kein fertig gibt, lediglich ein _always beta_, haben wir uns dazu entschlossen, den live Bearbeitungsstand mit allen Interessierten zu teilen.

Nutzt bitte die Kommentarfunktion wenn ihr aktiv am Geschehen teilnehmen wollt oder besucht das öffentliche [GitLab Repo](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab). Wer sich sogar als Contributor engagieren will, kann per E-Mail Kontakt mit uns aufnehmen ([synlloer@uni-hamburg.de](mailto:synlloer@uni-hamuburg.de)). Wir sind immer offen für konstruktive Kritik, Verbesserungswünsche, zusätzliche Inhalte und Hinweise auf jegliche Probleme.

Falls Ihr die Toolchains schon in Lehr-Lern-Szenarien einsetzen wollt, bedenkt bitte, dass sich die Inhalte täglich verändern können und werden.

Viel Spaß beim lOERnen und mitwOERken.
