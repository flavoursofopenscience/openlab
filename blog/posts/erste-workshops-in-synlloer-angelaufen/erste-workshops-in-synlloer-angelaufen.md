<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Erste Workshops im Projekt SynLLOER angelaufen

## Das Projekt SynLLOER konzentriert sich unter anderem auf die Fort- und Weiterbildung von Pädagog*innen zu den Themen Urheberrecht, Creative Commons und OER.

Tags:

-->

In der Ausbildung von Lehrkräften ist diese Thematik kaum bis gar nicht vertreten und wirft in der digital vernetzten Wirklichkeit vielerlei Fragen auf:

> _Wie an Arbeitsblätter, Methoden oder ganze Konzepte für Unterrichts- oder Workshopeinheiten gelangen? * *Selbst erstellen? * *Kopieren? * *Kolleg_innen fragen? * _Und wie ist das mit dem Urheberrecht - wer hat wann und wie worauf welchen Anspruch? Wo liegen die Grenzen bzw. Grauzonen? Wie viel darf ich kopieren und scannen? Was darf ich weitergeben und was nicht?_ Diese und weit umfangreichere Fragen stellen sich im beruflichen Alltag. Klare Antworten gibt es darauf selten. Das Projekt SynLLOER hat daher ein Workshopangebot entwickelt, welches sich an Dozent
>
> _innen, Lehrer_innen, Referendar_innen und Student_innen richtet. Es konzentriert sich dabei auf die Auseinandersetzung mit eingangs erwähnten Fragen und Problemfeldern in der beruflichen Realität. Hierbei wird der Einsatz von CC-lizenziertem Material und OER als Lösungsansatz vorgestellt. Die Workshops sind sowohl für Schulen als auch für berufsbildende Schulen und Hochschulen zugeschnitten.

### Liste der aktuell angebotenen Workshops

*   Copy & Paste - darf ich das?! Eine Einführung in Urheberrecht und Creative Commons
*   Wie und wo finde ich frei verwendbare Arbeitsmaterialien für den Unterricht? - CC-Lizenzen und OER-Materialien in der Praxis
*   Freie Lernmaterialien (OER) barrierefrei erstellen und mit Anderen teilen
*   Open Educational Resources - eine praxisorientierte Einführung in freie Lehr- und Lernmaterialien
*   Workshop zu Konzept- und Strukturentwicklung zur Einführung von freien Lehr- und Lernmaterialien (OER) in der Schule
*   Bedarfsorientierte Fortbildungen und Workshops an Schulen nach Absprache

### Zielsetzung der Workshops

Die Workshops sind frei miteinander kombinierbar und eignen sich auch für die Zusammenstellung zu einem Modul. Hierbei werden kurz-, mittel-, und langfristige Ziele verfolgt.

#### Kurz- und mittelfristig

sollen Teilnehmende der Workshops für das Thema **Urheberrecht im pädagogischen Alltag** sensibilisiert werden. Nicht zuletzt sind es Lehrende, die bei den Arbeiten von Schüler_innen penibel auf korrekte Quellenverweise achten. Dies ist richtig und wichtig, schließlich ist das Erlernen korrekter Zitationstechnik im Rahmen der Schulausbildung vorgesehen. Problematisch wird es, wenn ausschließlich das Zitationsrecht behandelt, das Urheberrecht als Rahmengefüge aber nicht berücksichtigt wird. Eine Basis des Urheberrechts bildet übrigens der [Erschöpfungsgrundsatz](http://wiki.llz.uni-halle.de/Ersch%C3%B6pfungsgrundsatz). "Der so genannte Erschöpfungsgrundsatz ist im Urheberrechtsgesetz (UrhG) verankert und besagt, dass sich das Verbreitungsrecht des Urhebers erschöpft, sobald er sein Werk erstmalig in Verkehr gebracht hat. Das bedeutet, er kann nach der Veräußerung nicht mehr bestimmen, welchen weiteren Weg das Werkstück nimmt, und der Erstkäufer kann ohne Zustimmung des Herstellers entscheiden, ob und an wen er es weiterverkauft."[[1]](https://www.usedsoft.com/de/gebrauchte-software/usedsoft-wiki/glossar/lizenzrecht/erschoepfungsgrundsatz-im-urheberrecht/) Diese rechtliche Grundlage ist jedoch im vor-digitalen Verbreitungsmedium entstanden. Die Weitergabe und Vervielfältigung von Inhalten ist heute jedoch leichter denn je. Der Frage nach dem Urheberrecht kann sich also nicht mehr entzogen werden. Deshalb gilt es nicht nur die Arbeit von Schüler_innen genauer Prüfung zu unterziehen, sondern das eigene Tun ebenso zu hinterfragen. Die Workshops beinhalten eine Einheit zu Urheberrechtsverletzungen im pädagogischen Alltag. Rechtlich nicht konforme Praxis in der Lehre wird oftmals für selbstverständlich gehalten und ohne weitere Auseinandersetzung umgesetzt. Die sogenannten [Schrankenregelungen](http://magazin.sofatutor.com/lehrer/2015/11/26/das-urheberrecht-was-lehrer-wissen-muessen/) für Schule und Ausbildung sind den meisten Lehrenden zwar geläufig, eine strikte Einhaltung erfolgt jedoch selten. Innerhalb der Workshops wird versucht für ein Umdenken zu sensibilisieren, da Lehrende in ihrer Funktion auch ein Vorbild für ihre Schülerinnen darstellen. Hier einmal eine Sammlung von Urheberrechtsverletzungen im pädagischen Alltag, die während der ersten Workshops entstanden sind: [metaslider id=713] Um eine Sensibilisierung zu erwirken, lernen die Workshopteilnehmenden durch das **Nutzen von Creative Commons und OER** eine Lösungsmöglichkeit kennen, rechtliche Problemzonen zu umgehen. Ihnen soll somit eine Möglichkeit zu rechtlich sichererem Handeln eröffnet werden.

#### Langfristig

sollen Lehrende dazu bewegt werden eigene Inhalte durch die CC-Lizenzierung als OER zu erstellen und diese mit Anderen zu teilen. Das Workshopangebot zielt also nicht nur darauf ab theoretische Kenntnisse über das Urheberecht im pädagogischen Alltag zu gewinnen und für die recht restriktiven Schrankenregelungen für Schulen Lösungsansätze aufzuzeigen. Die Beteiligten an Prozessen des Lehrens und Lernens sollen vielmehr als Multiplikator*innen gewonnen werden offene Bildungsinhalte zu nutzen und zu verbreiten. Hierdurch wird ein positiver Einfluss auf pädagogische Praxis erwirkt, der auch Lernenden ermöglicht einen rechtlich sichereren Umgang mit Fragen des Urheberrechts zu erlernen.

### Was ist bislang passiert?

Bislang fanden vier Workshops zu den oben genannten Themenbereichen statt. Zwei davon am Landesinstitut für Lehrerbildung und Schulentwicklung [(LI)](http://li.hamburg.de) und zwei weitere für Lehramtsstudierende am Medienzentrum der Fakultät für Erziehungswissenschaft an der Uni Hamburg [(MZ)](https://www.ew.uni-hamburg.de/service/medienzentrum). Am LI werden die Workshops als offizielle Fortbildungsinhalte angeboten und die Teilnahme mit Punkten zertifiziert. Regelmäßige Workshops folgen nach Ende der Hamburger Sommerferien. **Beobachtungen aus den Workshops** Das konkrete Aufzeigen der Schrankenregelungen im dt. Urheberrecht für den Schulbetrieb sorgte für erhebliche Diskussionen und Detailfragen zu eingebrachten Einzelfällen. Das Hauptinteresse der Teilnehmenden lag auf dem Suchen und Finden von fertigen Arbeitsmaterialien (Arbeitsblätter, Unterrichtskonzepte, Schulbücher, etc.). Es war dementsprechend schwer die Teilnehmenden zur Erstellung eigener OER zu bewegen. Nicht zuletzt die Angst vor Rechtsverletzungen hielt sie davon ab, sich vertiefend mit der Erstellung und Veröffentlichung auseinander zu setzen. Die Teilnehmenden meldeten zurück, dass es ihnen zwar gefällt eine Möglichkeit kennengelernt zu haben, sich rechtssicherer im Schulalltag zu bewegen; dennoch fehlen klar ersichtliche Vorteile. Zeit- und Kostenersparnis waren große Aspekte, die sie benannten. Die dezentrale Struktur des Internets mit hundertfachem OER-Angebot zu suchen und zu finden, schien die Teilnehmenden noch zu überfordern. **Abrufangebote** Haben Sie Interesse an unserem Angebot? Wir kommen auch an Ihre Schule: Das Workshopangebot orientiert sich ganz an den Bedürfnissen Ihrer Schulen. Kommen Sie gerne mit uns ins Gespräch und wir richten einen ihren Bedürfnissen entsprechenden Workshop an Ihrer (Hoch)Schule aus. Schreiben Sie uns unter [synlloer@uni-hamburg.de](mailto:synlloer@uni-hamburg.de).

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />"<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Beitrag: Erste Workshops im Projekt SynLLOER angelaufen</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/erste-workshops-…lloer-angelaufen/" property="cc:attributionName" rel="cc:attributionURL">Klaas Opitz für SynLLOER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0 International Lizenz</a>.
