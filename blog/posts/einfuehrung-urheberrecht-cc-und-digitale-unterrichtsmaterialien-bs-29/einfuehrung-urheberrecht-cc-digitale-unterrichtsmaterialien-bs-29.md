<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Eine Einführung in Urheberrecht, Creative Commons und digitale Unterrichtsmaterialien - BS 29

## Workshop am 11.04.2018 an der beruflichen Schule Uferstraße zu den Themen Urheberrecht, Creative Commons und digitalen Unterrichtsmaterialien.

Tags:

-->

Ziel ist ein erster Überblick der gegebenen Themen und Darlegung von Möglichkeiten um auf digitale Lehr- Lernmaterialien zuzugreifen.

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=29" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a> <br /> H5P-Präsentation "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Eine Einführung in Urheberrecht, Creative Commons und digitale Unterrichtsmaterialien</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/open-educational-resources-im-schulkontext/" property="cc:attributionName" rel="cc:attributionURL">Christina Schwalbe, Lucas Jacobsen, Klaas Opitz für SynLLOER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0 International Lizenz</a>.
