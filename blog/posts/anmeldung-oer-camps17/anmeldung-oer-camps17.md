<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Anmeldung zu den OERCamps 2017 geöffnet

##

Tags:

-->

Das OERcamp wird 2017 nicht nur an einem, sondern gleich an vier Orten in Deutschland stattfinden:

*   München [am 05./06.05.2017](https://openlab.blogs.uni-hamburg.de/veranstaltung/oercamp-sued-muenchen/) – in Kooperation mit dem [Institut für Film und Bild in Wissenschaft und Unterricht](http://fwu.de/) (FWU)
*   Köln [am 12./13.05.2017](https://openlab.blogs.uni-hamburg.de/veranstaltung/oercamp-west-th-koeln/) – in Kooperation mit der [TH Köln](https://www.th-koeln.de/)
*   Hamburg [am 23./24.06.2017](https://openlab.blogs.uni-hamburg.de/veranstaltung/oercamp-nord-hamburg-uhh/) – in Kooperation mit der [Universität Hamburg](https://uhh.de/synlloer)
*   Berlin [im Herbst 2017](https://openlab.blogs.uni-hamburg.de/veranstaltung/oercamp-ost-berlin/) (Details folgen)

### Seit heute ist die Anmeldung dazu freigeschalten!

*   [http://www.oercamp.de/anmeldung/](http://www.oercamp.de/anmeldung/)
