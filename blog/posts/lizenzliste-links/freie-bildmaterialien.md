Die in den [Toolchain-Videos](https://www.youtube.com/watch?v=Jnl2khkGcNs&list=PLPHQKIULQvNpfeOIYRdxduxeIGQx-DKbX) genutzten Bildmaterialien stehen alle unter eine [CC0](https://creativecommons.org/publicdomain/zero/1.0/) oder einer [CC BY](https://creativecommons.org/licenses/by/2.0/de/) Lizenz, sowie auch das genutzte Lied.
Für die bessere Auffindbarkeit haben wir sie hier mit den jeweiligen Links zu den Grafiken/ Fotos etc. aufgelistet.

> **Tipp:** Sollte ein Link sich nicht mit einem einfachen Klick öffenen lassen, versucht ihn mit Rechtsklick in einem neuen Tab zu öffnen!

---
Musik: [Happy Ukulele](https://freemusicarchive.org/music/Scott_Holmes/Happy_Music/Happy_Ukulele) von [Scott Holmes](https://freemusicarchive.org/music/Scott_Holmes/) unter der [Lizenz CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/) via freemusicarchive.org

---

## Freie Ausmalbilder im Handumdrehen selbst erstellen

| Nr.   |      Lizenz TULLU-Regel      |  Zuletzt besucht |
|----------|:-------------:|------:|
| 1 |  "[Al in love](https://openclipart.org/detail/91555/al-in-love)" von dominiquechappard unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 18.04.2018 |
| 2 |  "[yellow detached house](https://openclipart.org/detail/179978/yellow-detached-house)" von rdevries unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 18.04.2018 |
| 3 |  "[Oak tree](https://openclipart.org/detail/154699/oak-tree)" von rdevries unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 18.04.2018 |
| 4 |  "[playschool](https://pixabay.com/de/kindergarten-kinder-menschliche-151938/)" von OpenClipart-Vectors unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 18.04.2018 |
| 5 |  "[Handsaw](https://openclipart.org/detail/2586/handsaw)" von Machovka unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 18.04.2018 |
| 6 |  "[Hammer 3](https://openclipart.org/detail/2555/hammer-3)" von Machovka unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 18.04.2018 |
| 7 |  "[Toy train with 5 wagons](https://openclipart.org/detail/218332/toy-train-with-5-wagons)" von cweiske unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 18.04.2018 |
| 8 |  "[Building Block Toys](https://openclipart.org/detail/254023/building-block-toys)" von GDJ unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 18.04.2018 |
| 9 |  "[Al happy](https://openclipart.org/detail/91285/al-happy)" von dominiquechappard unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 18.04.2018 |
| 10 |  "[Light Bulb on](https://openclipart.org/detail/48397/light-bulb-on)" von palomaironique unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 18.04.2018 |
| 11 |  "[AL-brush-teeth](https://openclipart.org/detail/101353/albrushteeth)" von paro unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 18.04.2018 |
| 12 |  "[Book, sketched](https://openclipart.org/detail/167607/book-sketched)" von mi_brami unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 18.04.2018 |
| 13 |  "[Toy rocket](https://openclipart.org/detail/21988/toy-rocket)" von nicubunu unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 18.04.2018 |
| 14 |  "[Simple Flower](https://openclipart.org/detail/16629/simple-flower)" von HakanL unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 18.04.2018 |
| 15 |  "[Simple Flower, bw](https://openclipart.org/detail/215702/simple-flower-bw)" von malenki unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 18.04.2018 |

---

## Kein passendes Schaubild für den Unterricht?

| Nr.   |      Lizenz TULLU-Regel      |  Zuletzt besucht |
|----------|:-------------:|------:|
| 1 |  "[Aiga toiletsq men](https://commons.wikimedia.org/wiki/File:Aiga_toiletsq_men.svg?uselang=de)" von aiga unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via openclipart.org| 15.04.2018 |
| 2 |"[Classroom](https://openclipart.org/detail/297309/classroom)" von oksmith unter der [Lizenz CC 0](https://wiki.creativecommons.org/wiki/Openclipart) via openclipart.org| 15.04.2018 |
| 3 | "[Laptop](http://www.freestockphotos.biz/stockphoto/17115)" von Johnny_Automatic unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/) via freestockphotos.biz| 15.04.2018 |
| 4 |  "[Overhead Projector](https://pixabay.com/de/overhead-projektor-hellraum-projektor-341408/)" von Sandra_Schoen unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/) via pixabay.com| 15.04.2018 |
| 5 |  "[Lena Road Schoolhouse June 2012](https://commons.wikimedia.org/wiki/File:Lena_Road_Schoolhouse_June_2012.jpg)" von Royalbroil unter der [Lizenz CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en) via commons.wikimedia.org| 15.04.2018 |
| 6 |  "[Wooden Background](https://www.publicdomainpictures.net/en/view-image.php?image=209546&picture=wooden-background)" von Petr Kratochvil unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/) via publicdomainpictures.net| 15.04.2018 |
| 7 | "[Binary file icon](https://openclipart.org/detail/149755/binary-file-icon)" von jhnri4 unter der [Lizenz CC 0](https://wiki.creativecommons.org/wiki/Openclipart) via openclipart.org| 15.04.2018 |
| 8 | "[Photosynthesis.gif](https://commons.wikimedia.org/wiki/File:Photosynthesis.gif)" von At09kg unter der [Lizenz CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en) via commons.wikimedia.org| 15.04.2018 |
| 9 | "[Sci-Fi Op](https://pixabay.com/de/sci-fi-op-sci-fi-op-2992797/)" von alan9187 unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via pixabay.com| 15.04.2018 |

---

## Vom Tweet zum OER Arbeitsblatt in 30 Minuten

| Nr.   |      Lizenz TULLU-Regel      |  Zuletzt besucht |
|----------|:-------------:|------:|
| 1 |  "[Empty Classroom](https://commons.wikimedia.org/wiki/File:Empty-classroom.jpg)" von Imager23 unter der [Lizenz CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en) via commons.wikimedia.org| 15.04.2018 |
| 2 |  "[Berthold Brecht, Bundesarchiv, Bild 183-W0409-300](https://commons.wikimedia.org/wiki/File:Bertolt-Brecht.jpg)" von Kolbe, Jörg unter der [Lizenz CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en) via commons.wikimedia.org| 15.04.2018 |
| 3 |"[Johann Wolfgang von Goethe, WAF 1048](https://commons.wikimedia.org/wiki/File:Goethe_(Stieler_1828).jpg)" von Joseph Karl Stieler unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via commons.wikimedia.org| 15.04.2018 |
| 4 | "[DNA_Chemical_Structure](https://commons.wikimedia.org/wiki/File:DNA_chemical_structure_la.svg)" von Madprime unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)  via commons.wikimedia.org| 15.04.2018 |
| 5 |"[Head Platon Glyptothek Munich, Inv. 548](https://commons.wikimedia.org/wiki/File:Head_Platon_Glyptothek_Munich_548.jpg)" von Silanion unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via commons.wikimedia.org| 15.04.2018 |
| 6 | "[Twitter Social Media Icon](https://pixabay.com/de/twitter-social-media-icon-soziale-2430933/)" von joshborup unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via pixabay.com| 15.04.2018 |
| 7 | "[CC BY-SA icon white.svg](https://commons.wikimedia.org/wiki/File:CC-BY-SA_icon_white.svg)" von Creative Commons unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via commons.wikimedia.org| 15.04.2018 |
| 8 | "[Übung macht den Meister](https://twitter.com/m4nUE_212/status/953661315205169154)" von Manuela SWA unter der [Lizenz CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en) via twitter.com| 15.04.2018 |
|9 | "[Aiga toiletsq men](https://commons.wikimedia.org/wiki/File:Aiga_toiletsq_men.svg?uselang=de)" von Aiga unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via commons.wikimedia.org| 15.04.2018 |

---

## Präsentationen inklusiv(er) darstellen

| Nr.   |      Lizenz TULLU-Regel      |  Zuletzt besucht |
|----------|:-------------:|------:|
| 1 |  "[Professor Speaking on the Podium](https://commons.wikimedia.org/wiki/File:Professor_Speaking_on_the_Podium_GIF_Loop.gif)" von VideoPlasty unter der [Lizenz CC-BY-SA 4.0](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via commons.wikimedia.org| 17.04.2018 |
| 2 |  "[overhead-projektor-hellraum-projektor](https://pixabay.com/de/overhead-projektor-hellraum-projektor-341408/)" von sandra_schoen unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via  pixabay.com| 17.04.2018 |
| 3 |  "[Müll Bin Können](https://pixabay.com/de/m%C3%BCll-bin-k%C3%B6nnen-glas-papierkorb-154197/)" von OpenClipart-Vectors unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via  pixabay.com | 17.04.2018 |
| 4 |   "[Menge Masse Hörsaal](https://pixabay.com/de/menge-masse-h%C3%B6rsaal-menschen-307354/)" von Clker-Free-Vector-Images unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via  pixabay.com| 17.04.2018 |
| 5 |   "[Junge Jugendlicher Teenager](https://pixabay.com/de/junge-jugendlicher-teenager-jung-1691781/)" von giusy64 unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via  pixabay.com| 17.04.2018 |
| 6 |  "[Wolke Computer Ideen Kommunikation](https://pixabay.com/de/wolke-computer-ideen-kommunikation-24864/)" von Clker-Free-Vector-Images unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via  pixabay.com| 17.04.2018 |
| 7 |   "[Computer Laptop Notebook](https://pixabay.com/de/computer-laptop-notebook-einfache-2023252/)" von OpenClipart-Vectors unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via  pixabay.com| 17.04.2018 |
| 8 |   "[Flach Design Symbol](https://pixabay.com/de/flach-design-symbol-icon-www-2126883/)" von janjf93 unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via  pixabay.com| 17.04.2018 |
| 9 |   "[Tablette Hand Technologie Internet](https://pixabay.com/de/tablette-hand-technologie-internet-812708/)" von fajarbudi86 unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via  pixabay.com| 17.04.2018 |
| 10 |   "[Download Symbol Symbole](https://pixabay.com/de/download-symbol-symbole-1586558/)" von bartekhdd unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via  pixabay.com|  17.04.2018 |
| 11 |   "[Lautsprecher Ton Laut](https://pixabay.com/de/lautsprecher-ton-laut-lautheit-310810/)" von Clker-Free-Vector-Images unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via  pixabay.com|  17.04.2018 |
| 12 |   "[Lautsprecher Ton Laut](https://pixabay.com/de/lautsprecher-ton-laut-lautheit-310810/)" von Clker-Free-Vector-Images unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via  pixabay.com|  17.04.2018 |
| 13 |   "[Thiman lecture hall at UCSC](https://commons.wikimedia.org/wiki/File:Thiman_lecture_hall_at_UCSC.jpg)" von Ian Rickard unter der [Lizenz CC-BY 4.0](https://creativecommons.org/licenses/by/4.0/) via  commons.wikimedia.org| 17.04.2018 |
| 14 |   "[Bildschirm Projektion Präsentation](https://pixabay.com/de/bildschirm-projektion-pr%C3%A4sentation-309390/)" von Clker-Free-Vector-Images unter der [Lizenz CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de) via  pixabay.com|  17.04.2018 |
| 15 |  "[Professor Pointing Cartoon](https://commons.wikimedia.org/wiki/File:Professor_Pointing_Cartoon.svg)" von VideoPlasty unter der [Lizenz CC-BY-SA 4.0](https://creativecommons.org/licenses/by/4.0/) via commons.wikimedia.org| 17.04.2018 |

---

## Digitaler Zeitstrahl im OER-Format

| Nr.   |      Lizenz TULLU-Regel      |  Zuletzt besucht |
|----------|:-------------:|------:|
| 1 |  "[tafel-jungen-kinder-klassenzimmer](https://pixabay.com/de/tafel-jungen-kinder-klassenzimmer-1299841/)" von OpenClipart-Vectors unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 29.05.2018 |
| 2 |  "[automobil-auto-farbe-ford-grün](https://pixabay.com/de/automobil-auto-farbe-ford-gr%C3%BCn-2029746/)" von OpenClipart-Vectors unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 29.05.2018 |
| 3 |  "[gebäude-burg-fort-befestigung](https://pixabay.com/de/geb%C3%A4ude-burg-fort-befestigung-2024446/)" von OpenClipart-Vectors unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 29.05.2018 |
| 4 |  "[charakter-krieger-ritter](https://pixabay.com/de/charakter-krieger-ritter-3208295/)" von 13smok unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 29.05.2018 |
| 5 |  "[gladiatoren-rom-römisch-antike](https://pixabay.com/de/gladiatoren-rom-r%C3%B6misch-antike-1775109/)" von Mysticsartdesign unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 29.05.2018 |
| 6 |  "[kolosseum-rom-denkmal-berühmt](https://pixabay.com/de/kolosseum-rom-denkmal-ber%C3%BChmt-309629/)" von Clker-Free-Vector-Images unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 29.05.2018 |
| 7 |  "[dinosaurier-triceratops-ausgestorben](https://pixabay.com/de/dinosaurier-triceratops-ausgestorben-47924/)" von Clker-Free-Vector-Images unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 29.05.2018 |
| 8 |  "[dinosaurier-antike-reptil](https://pixabay.com/de/dinosaurier-antike-reptil-46294/)" von Clker-Free-Vector-Images unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 29.05.2018 |
| 9 |  "[empire-state-building-wolkenkratzer](https://pixabay.com/de/empire-state-building-wolkenkratzer-33290/)" von Clker-Free-Vector-Images unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 29.05.2018 |
| 10 |  "[zeigefinger-zeiger-klicken-sie-hand](https://pixabay.com/de/zeigefinger-zeiger-klicken-sie-hand-303579/)" von Clker-Free-Vector-Images unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 29.05.2018 |
| 11 |  "[pyramiden-ägypten-ägyptische-wüste](https://pixabay.com/de/pyramiden-%C3%A4gypten-%C3%A4gyptische-w%C3%BCste-1496253/)" von BookBabe unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 29.05.2018 |
| 12 |  "[büro-arbeit-ort-geschäft-computer](https://pixabay.com/de/b%C3%BCro-arbeit-ort-gesch%C3%A4ft-computer-3383853/)" von BookBabe unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 29.05.2018 |
| 13 |  "[monitor-bildschirm-computer-anzeige](https://pixabay.com/de/monitor-bildschirm-computer-anzeige-160942/)" von OpenClipart-Vectors unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 29.05.2018 |

---

## Interaktive Arbeitsblätter erstellen und anreichern

| Nr.   |      Lizenz TULLU-Regel      |  Zuletzt besucht |
|----------|:-------------:|------:|
| 1 |  ["Landscape"](https://openclipart.org/detail/175353/landscape) von jsstrn unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 07.05.2018 |
| 2 |  ["Emojione 1F3EB"](https://commons.wikimedia.org/wiki/File:Emojione_1F3EB.svg) von emojione unter der [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en) via commons.wikimedia.org | 07.05.2018 |
| 3 |  ["student desk"](https://openclipart.org/detail/216906/student-desk) von barnheartowl unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 07.05.2018 |
| 4 |  ["Female Instructor (#6)"](https://openclipart.org/detail/296124/female-instructor-5) von oksmith unter der [Lienz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 07.05.2018 |
| 5 |  ["Old man with Butterfly"](https://openclipart.org/detail/298851/old-man-with-butterfly) von oksmith unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 07.05.2018 |
| 6 |  ["Box with folders"](https://openclipart.org/detail/22046/box-with-folders) von nicubunu unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org| 07.05.2018 |
| 7 |  ["Classroom Silhouette"](https://openclipart.org/detail/221799/classroom-silhouette) von GDJ unter der [Lizenz CCo 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org| 07.05.2018|
| 8 |  ["Bulle / bubble"](https://openclipart.org/detail/125887/bulle-bubble) von Improulx unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org| 07.05.2018 |
| 9 |  ["Spot-of-Ink"](https://openclipart.org/detail/223945/Spot-of-Ink) von arking unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org| 20.04.2018 |
| 10 | ["HeartDiagramAnnotated"](https://openclipart.org/detail/274066/heart-diagram-annotated) von Firkin unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org| 20.04.2018 |
| 11 | ["bulb_on"](https://openclipart.org/detail/213322/light-bulb-lit) von maus80 unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org| 20.04.2018 |
| 12 | ["Smartboard"](https://pixabay.com/de/whiteboard-pr%C3%A4sentation-brett-297484/) von Clker-Free-Vektor-Images unter der [Lienz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com| 20.04.2018 |
| 13 | ["Animal Cell"](https://openclipart.org/detail/66733/animal-cell) von Ramchand unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)| 07.05.2018 |
| 14 | ["Desktop Monitor"](https://openclipart.org/detail/202625/desktop-monitor) von Dustwin unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) | 07.05.2018 |
| 15 | ["Stack of white paper](https://openclipart.org/detail/256933/stack-of-white-paper)" von maestrawalker unter der [Lizenz CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org| 07.05.2018 |



---

## Der gemeinschaftliche Text leicht gemacht

| Nr.   |      Lizenz TULLU-Regel      |  Zuletzt besucht |
|----------|:-------------:|------:|
| 1 |   "[Paris](https://unsplash.com/photos/CB0Qrf8ib4I)" von Ilnur Kalimullin unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via unsplash.com | 19.04.2018 |
| 2 |  "[Bücher](https://unsplash.com/photos/Oaqk7qqNh_c)" von Patrick Tomasso unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via unsplash.com |  19.04.2018 |
| 3 |  "[Stadt](https://unsplash.com/photos/WMSvsWzhM0g)" von Ruslan Bardash unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via unsplash.com |  19.04.2018 |
| 4 |  "[Sigmund Freud LIFE](https://commons.wikimedia.org/wiki/File:Sigmund_Freud_LIFE.jpg)" von Max Halberstadt unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via commons.wikimedia.org |  19.04.2018 |

---

## Get stuff done – die kollaborative ToDo-Liste

| Nr.   |      Lizenz TULLU-Regel      |  Zuletzt besucht |
|----------|:-------------:|------:|
| 1 |  ["Four-Kids-And-Circle"](https://pixabay.com/de/kinder-freund-kreis-die-gruppe-2758831/) von Kidaha unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com  | 20.04.2018 |
| 2 |  ["african-instructor-man"](https://openclipart.org/detail/296521/african-instructor) von j4p4n unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 20.04.2018 |
| 3 |  ["thought-bubble"](https://openclipart.org/detail/82327/cartoon-thought-bubble) via purzen unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org  | 20.04.2018 |
| 4 |  ["bts3"](https://pixabay.com/de/junge-m%C3%A4dchen-hand-in-hand-kinder-160168/) via Open-Clipart-Vectors unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com  | 20.04.2018 |
| 5 |  ["messy-lined-papers"](https://openclipart.org/detail/140251/messy-lined-papers) von snifty unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 20.04.2018 |
| 6 |  ["StefanvonHalenbach-Teacher-L-mpe"](https://openclipart.org/detail/10362/teacher-lampel) von StefanvonHalenbach unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org| 20.04.2018 |
| 7 |  ["nicubunu-Paper-plane"](https://openclipart.org/detail/21982/paper-plane) von nicubunu unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org| 20.04.2018 |
| 8 |  ["light-bulb"](https://openclipart.org/detail/291617/light-bulb) von pascallapalme unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org| 20.04.2018 |
| 9 |  ["list-850185_1920"](https://openclipart.org/detail/227170/shopping-list-clipboard-tablet) von GDJ unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org|  20.04.2018 |
| 10 | ["diversegroup"](https://openclipart.org/detail/284727/diverse-group) von j4p4n unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org| 20.04.2018 |
| 11 | ["list-850178_1920_2"](https://pixabay.com/de/liste-einkaufsliste-tablet-850178/) von geralt unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com| 20.04.2018 |
| 12 | ["printer"](https://openclipart.org/detail/171421/printer) von cyberscooty unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org| 20.04.2018 |


---

## Kein Platz für die Posterausstellung?

| Nr.   |      Lizenz TULLU-Regel      |  Zuletzt besucht |
|----------|:-------------:|------:|
| 1 |  "[Gebäude1](https://stocksnap.io/photo/3PU5PJDXQP)" von Omair Khan unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via stocksnap.io | 19.04.2018 |
| 2 |  "[Architecture Drawing Ruler Pen](https://negativespace.co/architecture-drawing-ruler-pen/)" von Lorenzo Cafaro unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via negativespace.co | 19.04.2018 |
| 3 |  "[Gebäude2](https://stocksnap.io/photo/5L4E1SOTI7)" von  	Samuel Zeller  unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via stocksnap.io | 19.04.2018 |
| 4 |  "[Gebäude3](https://stocksnap.io/photo/2H4NZ2HAN3)" von  	Fuse Brussels  unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via stocksnap.io | 19.04.2018 |
| 5 | "[	vib architecture 建築師事務所 2005-2015 作品集 Portfolio - Page 35](https://www.flickr.com/photos/eager/19325077968/in/photostream/)" von 準建築人手札網站 Forgemind ArchiMedia unter der [Lizenz CC BY 2.0](https://creativecommons.org/licenses/by/2.0/) via flickr.com | 19.04.2018 |
| 6 | "[	vib architecture 建築師事務所 2005-2015 作品集 Portfolio - Page 31](https://www.flickr.com/photos/eager/19326517379/in/photostream/)" von 準建築人手札網站 Forgemind ArchiMedia unter der [Lizenz CC BY 2.0](https://creativecommons.org/licenses/by/2.0/) via flickr.com | 19.04.2018 |
| 7 | "[vib architecture 建築師事務所 2005-2015 作品集 Portfolio - Page 28](https://www.flickr.com/photos/eager/19326522939/in/photostream/)" von 準建築人手札網站 Forgemind ArchiMedia unter der [Lizenz CC BY 2.0](https://creativecommons.org/licenses/by/2.0/) via flickr.com | 19.04.2018 |
| 8 | "[vib architecture 建築師事務所 2005-2015 作品集 Portfolio - Page 21](https://www.flickr.com/photos/eager/19325114930/in/photostream/)" von 準建築人手札網站 Forgemind ArchiMedia unter der [Lizenz CC BY 2.0](https://creativecommons.org/licenses/by/2.0/) via flickr.com | 19.04.2018 |
| 9 | "[vib architecture 建築師事務所 2005-2015 作品集 Portfolio - Page 08](https://www.flickr.com/photos/eager/18890596884/in/photostream/)" von 準建築人手札網站 Forgemind ArchiMedia unter der [Lizenz CC BY 2.0](https://creativecommons.org/licenses/by/2.0/) via flickr.com | 19.04.2018 |
| 10| "Architekturbilder" von Anna Seum & Elisabeth Haentjes unter der [Lizenz CC BY 2.0](https://creativecommons.org/licenses/by/2.0/) | 24.04.2018 |
| 11 |  "[Wall, texture, surface](https://unsplash.com/photos/e9qInqPiYwk)" von rawpixel unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via unsplash.com | 17.05.2018 |

---

## Schulhefte lebt wohl, digitales Lerntagebuch Ahoi!

| Nr.   |      Lizenz TULLU-Regel      |  Zuletzt besucht |
|----------|:-------------:|------:|
| 1 |  "[Hintergrund](https://pixabay.com/de/werbung-hintergrund-plakat-schwarz-1846965/)" von Pexels unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 18.04.2018 |
| 2 |  "[Lehrerin](https://pixabay.com/de/klassenzimmer-pr%C3%A4sentation-schule-1297780/)" von OpenClipart-Vectors unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com  | 18.04.2018 |
| 3 | "[Flagge](https://pixabay.com/de/fahne-flagge-gro%C3%9Fbritannien-2292674/)" von RonnyK unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com  | 18.04.2018 |
| 4 |  "[Kaffeefleck](https://pixabay.com/de/fleck-kaffee-kaffeeflecken-1831699/)" von ElisaRiva unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com  | 18.04.2018 |
| 5 |  "[Fleck](https://pixabay.com/de/farbe-splatter-spritzen-tinte-2209585/)" von TheRokon unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 18.04.2018 |
| 6 |  "[Kreuz](https://pixabay.com/de/kreuz-l%C3%B6schen-entfernen-abbrechen-296507/)" von Clker-Free-Vector-Images unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 18.04.2018 |
| 7 |  "[Buch](https://pixabay.com/de/buch-freigestellt-offenes-buch-leer-3246186/)" von darkmoon1968 unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 18.04.2018 |
| 8 |  "[Lehrerin2](https://pixabay.com/de/klassenzimmer-comic-figuren-1297781/)" von OpenClipart-Vectors unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)" via pixabay.com | 18.04.2018 |
| 9 |  "[Denkblase](https://openclipart.org/detail/218059/thought-bubble)" von Speedster unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 18.04.2018 |
| 10 |  "[Blog](https://pixabay.com/de/laptop-registerkarte-stift-computer-1443559/)" von Monoar unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 18.04.2018 |
| 11 |  "[Euro-Schein](https://pixabay.com/de/geldschein-500-euro-geld-banknote-166312/)" von stux unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 18.04.2018 |
| 12 |  "[Kamera-Logo](https://pixabay.com/de/kamera-logo-symbol-digital-linse-2868622/)" von coffeecups unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 18.04.2018 |
| 13 |  "[Pfeil](https://pixabay.com/de/pfeil-gestrichelt-muster-bunt-2207748/)" von geralt unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 18.04.2018 |
| 14 |  "[WordPress](https://pixabay.com/de/soziale-netzwerk-freigeben-kontakt-2470875/)" von johnthan unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode)  via pixabay.com | 18.04.2018 |
| 15 |  "[Fragezeichen](https://openclipart.org/detail/50161/question-mark)" von rejon unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via openclipart.org | 18.04.2018 |
| 16 |  "[Ausrufezeichen](https://pixabay.com/de/ausrufezeichen-antwort-schraffur-2426852/)" von geralt unter der [Lizenz CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/legalcode) via pixabay.com | 18.04.2018 |
