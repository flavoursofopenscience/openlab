<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Offene Bildungsmaterialien und Open Education

## Beitrag von Tobias Steiner zur Ringvorlesung "Open Knowledge" der HAW Hamburg am 21.06.2018.

Tags:

-->

Die Rahmung der Ringvorlesung wie folgt:

>In einer vernetzten, digitalen Gesellschaft ist ein freier und unbeschränkter Zugang zu Wissen und Information zu jeder Zeit an jedem Ort möglich geworden. Neue Informations- und Kommunikationstechnologien erlauben den Bürgern mehr Teilhabe, verbesserte Kommunikations- und Kooperationsmöglichkeiten beispielsweise in Bezug zum staatlichen Verwaltungshandeln oder beim Zugang zu öffentlich finanzierten Lehrangeboten und Forschungsergebnissen.
>
>Die Ringvorlesung Open Knowledge führt in die „Openness-Kultur“ ein und stellt innovative Openness-Projekte näher vor. Dazu sind ab Ende April jeweils Donnerstagabend öffentliche Vorträge von Experten geplant zu Themen wie Open Data, Open Education, Open Access, Open Government, Offene Lizenzen und Informationsfreiheit.
>
>Die wöchentlichen Veranstaltungen bieten Raum für Diskussionen und Austausch für die Hochschulöffentlichkeit und alle interessierten Bürgerinnen und Bürger. Es können einzelne Vorträge oder die ganze Ringvorlesung besucht werden. Der Besuch der Vorträge ist kostenlos, eine Anmeldung ist nicht erforderlich.


**Präsentationsfolien:**

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=37" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><a href="https://h5p.org/presentation">H5P</a>-Präsentation "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Offene Bildungsmaterialien und Open Education" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/offene-bildungsmaterialien-und-open-education/" property="cc:attributionName" rel="cc:attributionURL">Tobias Steiner für Universität Hamburg</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0 International Lizenz</a>.
