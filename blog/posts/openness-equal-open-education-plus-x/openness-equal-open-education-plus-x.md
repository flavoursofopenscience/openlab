<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Openness = Open Education + x?

## Universität Hamburg’s integrated approach to opening up Education and Science

Tags:

-->

As the 2nd OER World Congress in Ljubljana has recently shown, open education has come a long way since its digital renaissance during the early 2000s (Nyberg 2010 [1975]; Farrow, Deimann 2012). In Germany, more and more HEIs now strive to implement OER strategies to allow for institutional development of open education. Similarly, a lot of energy is invested in the production and dissemination of Open Educational Resources as a hands-on manifestation of what it means to teach, learn and work in the open (Orr, Neumann, Muuss-Meerholz 2017).

Within the City of Hamburg, openness has played a crucial role ever since the city’s mayor and senate decided on the governmental strategy of “Digitization of the Metropolitan Area”. Drawing from a selection of projects that have been realized at Universität Hamburg in that context, we discuss aspects of openness that have emerged from this push towards digitization and openness in the Hamburg metropolitan area. As we will show along preliminary results from ongoing projects such as Hamburg Open Online University (HOOU), SynLLOER (federal government-funded OER awareness), and Hamburg Open Science (HOS), Universität Hamburg is striving towards an implementation of openness that extends beyond a focus on Open Educational Practices.

Based on an understanding of the requirements of everybody working in higher education to not only engage in teaching and education, but also often conducting academic research in one way or the other, the convergence of experiences made in the outlined projects led to an integrated approach of an ‘openLab’ (DeRosa, Blickensderfer 2017) that focuses on a combination of open education with the field of open science by including aspects of open access, open data and open source – with the goal to foster a culture of openness that includes OEP as well as Practices of Open Scholarship (POS).

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=31" width="760" height="453" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>


## Lizenz

<a rel="license" href="https://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a> <br /> H5P presentation "<span xmlns:dct="https://purl.org/dc/terms/" property="dct:title">Openness = Open Education + x? Universität Hamburg’s integrated approach to opening up Education and Science</span>" by <a xmlns:cc="https://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/openness-open-education-x-universitaet-hamburgs-integrated-approach-to-opening-up-education-and-science/" property="cc:attributionName" rel="cc:attributionURL">Prof. Dr. Kerstin Mayrberger, Tobias Steiner, Nina Rüttgens and Franziska Bellinger</a> is licensed under a <a rel="license" href="https://creativecommons.org/licenses/by/4.0/">CC BY 4.0 International license</a>.
