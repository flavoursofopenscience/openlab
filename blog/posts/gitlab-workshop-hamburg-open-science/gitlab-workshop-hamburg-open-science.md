<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# GitLab Workshop Hamburg Open Science

## Einführung, Teamkommunikation und -kollaboration in GitLab

Tags:

-->

Workshop am Donnerstag, den 08.03.2018 zu Teammkommunikation und -kollaboration mit GitLab für die hochschulübergreifende Forschungsprogramm der Hamburg Open Science Community.

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=26" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

## Lizenz

 <a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />H5P-Präsentation "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Hamburg Open Science – Einführung, Teamkommunikation und -kollaboration in GitLab</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/gitlab-workshop-hamburg-open-science/" property="cc:attributionName" rel="cc:attributionURL">Manfred Steger und Sophia Zicari für SynLLOER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY 4.0 International Lizenz</a>.
