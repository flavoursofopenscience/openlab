<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Jointly Train-us-OER-Trainer 12./13.06.17

## Weimar, es kann losgehen!

Tags:

-->

Wir freuen uns schon darauf am 12./13.06.17 beim [Jointly Train-us-OER-Trainer Event](http://jointly.info/announcement/12-13-6-17-transferworkshop-train-us-oer-trainer-fuer-oerinfo-projekte/) aktiv mitzuwirken. Im geplanten Workshop erarbeiten wir zuerst das OER-Produktions-Modul: _OER erstellen und verteilen._

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=3" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

Die Präsentation ist ein handlungsorientierter Leitfaden zur Entwicklung einer OER mit der Auszeichnungssprache Markdown und der Plattform [gitbook.com](https://www.gitbook.com), die mithilfe des Tools [H5P](https://h5p.org/) erstellt wurde. Auch unabhängig des Jointly Events kann der Workshop mitgemacht und ganz nach den [5 V-Freiheiten](http://open-educational-resources.de/5rs-auf-deutsch/) genutzt werden.

![Die 5V s– Foto von Jöran Muuß-Merholz](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/train-us-oer-trainer/5vs-merholz.jpg)

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/80x15.png)](http://creativecommons.org/licenses/by/4.0/) Die 5Vs [Jöran Muuß-Merholz](https://git.universitaetskolleg.uni-hamburg.de/oer/openlab/raw/master/blog/posts/train-us-oer-trainer/5vs-merholz.jpg) ist lizenziert unter einer [Creative Commons Namensnennung 4.0 International Lizenz](http://creativecommons.org/licenses/by/4.0/).

Im Anschluss wird Bezug zu den geplanten und zum Teil gehaltenen OER-Workshops am Landesinstitut für Lehrerbildung Hamburg (LI) genommen:

*   Copy & Paste – darf ich das?! Eine Einführung in Urheberrecht und Creative Commons
*   Wie und wo finde ich frei verwendbare Unterrichtsmaterialien? – CC-Lizenzen und OER-Materialien in der Praxis
*   Freie Lernmaterialien (OER) barrierefrei erstellen und mit Anderen teilen

und das Konzept/Strategie von SynLLOER im Blitzlichtverfahren vorgestellt sowie diskutiert.
