<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Copy and Paste - darf ich das?! Eine Einführung in Urheberrecht und die Creative Commons

## OER-Informations-Modul zum Urheberrecht und Creative Commons

Tags:

-->

Im Kontext der SynLLOER-Workshopreihe, die in Kooperation mit dem [Landesinstitut für Lehrerbildung und Schulentwicklung in Hamburg](http://li.hamburg.de/) konzipiert ist, entstand das OER-Informations-Modul: **Copy & Paste – darf ich das?!** Eine Einführung in Urheberrecht und Creative Commons.

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=10" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />H5P-Präsentation "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Copy & Paste - darf ich das?! Eine Einführung in Urheberrecht und die Creative Commons</span>" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/copy-and-paste-darf-ich-das-eine-einfuehrung-in-urheberrecht-und-die-creative-commons/" property="cc:attributionName" rel="cc:attributionURL">Christina Schwalbe, Lucas Jacobsen, Klaas Opitz für SynLLOER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0 International Lizenz</a>.
