<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Medienkompetenz und Digitalisierung an Berufsschulen

## Dieser Workshop ist für drei Stunden konzipiert und bietet sowohl theoretischen Input zu openness, Urheberrecht, CC-Lizenzen und OER

Tags:

-->

Die Präsentation dient zusätzlich auch als konkrete Anleitungen zur praktischen Auseinandersetzung mit offenen Programmen, der Erstellung von OER und tool chains. Die Linksammlung ab Folie 37 bietet dabei Hilfe beim Suchen, Finden und Erstellen.

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=35" width="818" height="485" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

## Lizenz

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /><a href="https://h5p.org/presentation">H5P</a>-Präsentation "<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"Der Weg zu freien Bildungsmaterialien/span>Der Weg zu freien Bildungsmaterialien" von <a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/der-weg-zu-freien-bildungsmaterialien-2/" property="cc:attributionName" rel="cc:attributionURL">Celestine Kleinesper und Astrid Wittenberg für SynLLOER</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">CC BY-SA 4.0 International Lizenz</a>.
