<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Die tOERch ist in Hamburg gelandet!

## Offizielle OERCamp-Fackel am Hamburger Austragungsort des OERCamp17 Nord

Tags:

-->

Am heutigen Mittwoch ist die offizielle OERCamp-Fackel - die <a href="https://twitter.com/hashtag/tOERch?src=hash" target="_blank" rel="noopener noreferrer">#tOERch</a> - am Hamburger Austragungsort des OERCamp17 Nord, dem Chinesischen Teehaus Yu Garden, durch das Team SynLLOER in Empfang genommen worden!

Nachdem während des #OERCamp17 West an der TH Köln Christina den OERCamp-Staffelstab in Form der Fackel schon übernehmen durfte, erreichte die tOERch nun also ihre nächste Station.

Das OERCamp17 Nord findet am 23. und 24.06.2017 im Chinesischen Teehaus sowie den Räumlichkeiten des Hamburger Zentrums für Universitäres Lehren und Lernen (HUL) sowie des Universitätskollegs der Uni Hamburg statt.

Das Workshop-Programm des OERCamps Nord bietet mit mehr als 40 Workshops ein facettenreiches Angebot, mit dem hochspannende Einblicke in die zahlreichen Facetten des Themenfeldes OER geboten werden. Die Barcamp-Sessions, während derer die Teilnehmenden selbst Impulse einbringen können, erweitern das Spektrum zudem.

Einzusehen ist das Workshop-Angebot unter folgendem Link:

*   [http://www.oercamp.de/17/nord/workshops/](http://www.oercamp.de/17/nord/workshops/)

---

**Achtung:** Da Teilnahmeplätze nach Anmeldungseingang vergeben werden, lohnt sich eine frühzeitige [Anmeldung](http://www.oercamp.de/anmeldung/)!
