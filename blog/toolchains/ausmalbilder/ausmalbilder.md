<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Freie Ausmalbilder im Handumdrehen selbst erstellen

## Mal ganz kreativ. Motive suchen und ein Ausmalbild daraus erstellen.

> geschätzer Zeitaufwand: 20 - 30 Minuten

Tags: Umrissbilder, OpenClipart, CC Search, Ausmalbilder Vorlage, KindOERgarten, Pixlr, DIN A4, 4Teachers, Online Photo Editor, LibreOffice

## User Story ([Persona Fin Bonde](https://git.universitaetskolleg.uni-hamburg.de/synlloer/oer-know-how/blob/master/Unterlagen/Personas-03.pdf))

-->

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/WM2WCIMw6k4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Inhaltsverzeichnis

- [Bildersuche und Creative Commons](#bildersuche-und-creative-commons)
- [Bildbearbeitung mit Pixlr](#bildbearbeitung-mit-pixlr)
	- [Bilder laden und anordnen](#bilder-laden-und-anordnen)
	- [Hintergründe von Bildern entfernen](#hintergründe-von-bildern-entfernen)
	- [Collage speichern](#collage-speichern)
- [Umrissbild erstellen](#umrissbild-erstellen)

![Die Illustration stellt die Persona Fin Bonde dar. Die Persona zeigt den Oberkörper eines hellhäutigen Mann, der vor dem Kreishintergrund steht und in Richtung des Betrachters / der Betrachterin blickt. Neben dem Oberkörper befindet sich auf dem Kreishintergrund ein blauer Handabdruck und ebenfalls ein kleinerer Handabdruck in blau neben seinem Kopf. Der Mann trägt ein helltaubenblaugraues Shirt und darüber eine offene orangefarbene Kapuzenjacke. Der Mund des Mannes ist geschlossen und die Lippen sowie die Nase nur angedeutet. Die Augen sind dunkelbeige gezeichnet und blicken in Richtung Betrachter/Betrachterin. Der Mann hat volle braune Haare, die ohne Unterbrechung in einen üppigen braunen Voll- und Schnurbart übergehen und damit den Mundbereich vollständig einrahmen. Der linke Arm hängt herunter, der rechte Arm ist leicht angewinkelt und darauf sitzt ein Kind. Das Kind trägt eine dunkelgraue Hose, weiße Socken, blaue Schuhe und einen blauen ärmellosen Pullover über einem langärmligen weißen Shirt. Der Kopf des Kindes ist leicht gedreht, so dass die Blickrichtung am Kopf des Mannes vorbei nach schräg rechts aus dem Bild zu gehen scheint. Der hellhäutige Gesichtsbereich des Kindes ist nicht mit Augen, Nase oder Mund ausgestaltet - es fällt jedoch der Schatten des Kopf des Mannes darauf. Der Ohrbereich ist in der Profilansicht des hellhäutigen Kopfes von den mittellangen dunkelblonden Haaren freigelassen, die am Hinterkopf zu einem Pferdeschwanz zusammengebunden sind und daher andeuten, dass es sich bei dem Kind um ein Mädchen handelt.](https://gitlab.com/flavoursofopenscience/openlab/raw/master/blog/personas/uhh-personas-fin-bonde/uhh-personas-fin-bonde.png)

Fin ist Erzieher in einem kleinen ländlichen Kindergarten, der nur wenig finanzielle Mittel für neue Spielsachen und Materialien hat. Er möchte sich ein Beispiel an seiner Kollegin nehmen, die als gelernte Handwerkerin mit den Kindern direkt vor Ort selbstgemachte Holzspielsachen anfertigt. Daher hat er sich überlegt, ein eigenes Ausmalbuch zu gestalten, da die kommerziellen Produkte sehr teuer, rasch aufgebraucht und leider auch schnell beschädigt sind.

## Lösungsansatz

Fin benötigt für ein Ausmalbild ein [Foto](https://ccsearch.creativecommons.org/) oder eine [Grafik](https://openclipart.org/), ein Programm, das aus der Bilddatei ein [Umrissbild](http://umrissbilder.de/) erstellt und ein [Layoutprogramm](https://de.libreoffice.org/) zur Anordnung. Nicht nur in Malbüchern können Umrissbilder zum Einsatz kommen; Umrissbilder sind oftmals auch Teil eines [beschriftbaren Schaubilds](https://openlab.blogs.uni-hamburg.de/kein-passendes-schaubild-fuer-den-unterricht/). Diese können z. B. in Prüfungsaufgaben abgedruckt werden. Außerdem eignen sich Ausmalbilder für Overheadfolien und deren Beschriftung am Projektor oder an der digitalen Tafel.

## Lernziele

- kostenfreie Bilder online suchen und finden
- Lizenzhinweise verstehen und anwenden lernen
- freie Werkzeuge im Internet kennenlernen
- service-übergreifendes Arbeiten erlernen (Zwischenspeichern am Computer bzw. URL sharing)
- eine CC-Lizenzierungsmöglichkeit anwenden können (z.B. [CC BY](https://creativecommons.org/licenses/by/2.0/))
- einen Remix richtig auszeichnen können
- den erstellten OER-Remix lizenzgerecht auf einer Austauschplattform zur Verfügung stellen

## Schritt-für-Schritt-Anleitung  {#schritt-fuer-schritt-anleitung}

Ausmalbilder sind vielseitig einsetzbar, beispielsweise im Kindergarten als Malvorlage oder daheim als Fensterbild. Der Kreativität sind dabei keine Grenzen gesetzt und mit dem Internet kommt die Vielfalt auch ins eigene Buch oder Fenster. Wie dies in nur wenigen Minuten umgesetzt werden kann, könnt ihr in der folgenden Anleitung herausfinden.

### Bildersuche und Creative Commons  {#bildersuche-und-creative-commons}

Zunächst wird eine passende Vorlage benötigt. Bildsuchmaschinen können ihre Suchergebnisse mit den richtigen Einstellungen nach freiem Bildmaterial filtern.

- [Pixabay](https://pixabay.com/) (vorwiegend CC0)
- [CC Search](https://ccsearch.creativecommons.org/) (gemischt)
- [Flickr](https://www.flickr.com/) (gemischt)

Es gibt auch ganze Bilddatenbanken wie z.B. [OpenClipart](https://openclipart.org/), die ausnahmslos [gemeinfreie Werke (CC0)](https://de.wikipedia.org/wiki/Creative_Commons#CC_Zero) anbieten. OpenClipart hat sich auf Cliparts spezialisiert, die wegen ihrer vereinfachten Formen und reduzierten Farben für Ausmalbilder besonders geeignet sind.

1. Öffnet im Browser die Internetseite [OpenClipart](https://openclipart.org/).
2. Gebt einen beliebigen Begriff in das Suchfeld ein. Für das Kindermalbuch suchen wir gezielt nach Haustieren. Da wir uns auf einer englischsprachigen Seite befinden, sollten die Suchbebegriffe auf Englisch eingetragen werden. In diesem Beispiel wurde nach `animals`, `couch`, `sofa` und `reading girl` gesucht.
4. Falls ihr die passende Vokabel nicht parat habt, kann euch das [freie Wörterbuch dict.cc](https://www.dict.cc/) weiterhelfen.
5. Wählt das passende Bild mit einem Klick aus. Ihr gelangt danach auf die Detailseite des jeweiligen Cliparts. Dort befinden sich weitere Informationen zum Bild sowie der Button `Big Image PNG`. Ladet das Clipart auf den Computer herunter. Es wird eine Datei mit der Dateiendung **.png** gespeichert.
6. Klickt ihr auf den Button `Download` <i class="fas fa-download"></i>, wird ein **.svg**-Format gespeichert, das mit dem nachfolgenden Programm nicht geöffnet werden kann.

### Bildbearbeitung mit Pixlr  {#bildbearbeitung-mit-pixlr}

Für ein klassisches Ausmalbild muss der Hintergrund transparent sein und gegebenenfalls störende Elemente müssen entfernt werden. Diese Veränderungen nehmen wir in den nächsten Schritten mit dem Online-Bildeditor [Pixlr](https://pixlr.com/) vor.

#### Bilder laden und anordnen  {#bilder-laden-und-anordnen}

1. Öffnet den Online-Editor von Pixlr über die Internetadresse [pixlr.com/editor](https://pixlr.com/editor/). In den gängigen Browsern ist das benötigte Flash-Plugin meist erst nach Zustimmung des Benutzers aktiv. Bestätigt z.B. bei Mozilla Firefox die Meldung `Adobe Flash ausführen` mit einem Klick auf den Hinweis.
2. Legt nun über den Startbildschirm eine leere Gestaltungsvorlage mit dem Button `Neues Bild erzeugen` an.
3. Es erscheint eine Eingabemaske für unterschiedliche Vorgaben. Tragt unter dem Feld **Namen** den Projektnamen und für die Größe `Breite: 3508` und `Höhe: 2480` ein. Diese Werte entsprechen dem DIN-A4-Querformat in Pixeln. Benötigt ihr ein anderes Format, könnt ihr die Tabelle von [din formate faq](http://din-formate.de/reihe-a-din-groessen-mm-pixel-dpi.html) zu Rate ziehen.
4. Über das Hauptmenü `Ebene > Open image as layer` können die einzelnen Bilder geladen und zu einer Collage zusammengefügt werden. Die Funktion `Open image as layer` öffnet die einzelnen Bilder auf einer separaten Ebene. Ebenen verhalten sich wie transparente Folien, auf denen das Bild aufgeklebt wurde.
5. Öffnet nun ein Bild für das Ausmalbuch.
6. Die Größe des Bildes nach dem Öffnen hängt von der ursprünglichen Größe des heruntergeladenen Bildes ab. In diesem Schritt passen wir die Bildgröße nach unseren Vorstellungen an. Klickt in der Werkzeugleiste auf der linken Seite das Auswahlwerkzeug <i class="fas fa-mouse-pointer"></i> an. Geht in das Haupmenü und wählt den Befehl `Bearbeiten > Frei transformieren`.
7. Das aktive Bild erhält mehrere sogenannte Anfasser rund um das Bild. Damit das Bild verhältnismäßig korrekt (nicht gestaucht oder gequetscht) skaliert (vergrößert oder verkleinert) werden kann, muss nun bei gedrückt gehaltener Shift-Taste (Hochstelltaste) das Bild an einer Ecke zusammen- oder auseinandergezogen werden. Passt den Inhalt somit an die gewünschte Größe an.
8. Eine Bilddrehung ist im selben Schritt möglich, wenn ihr euch mit dem Mauszeiger außerhalb des Bildbereichs an eine Ecke annähert. Dreht das Bild um den gewünschten Winkel.
9. Ladet nun nach und nach die gewünschten Bilder und passt sie in Größe und Ausrichtung an.
10. Die Position einzelner Bilder kann nachträglich angepasst werden. Hierzu muss wieder das Auswahlwerkzeug <i class="fas fa-mouse-pointer"></i>  aus der Werkzeugleiste aktiv sein. Auf der rechten Seite im Palettenmenü unter **Ebenen** sind die einzelnen geladenen Bilder aufgelistet. Klickt ihr mit der Maus auf eine Ebene, kann diese nun im Hauptfenster mit geklickt gehaltener Maustaste verschoben werden.
11. In der Ebenenpalette können die einzelnen Bilder in der Liste auch nach oben und unten verschoben werden. Überdecken sich Bildinhalte, so sind die Bilder weiter oben in der Liste sichtbar und darunterliegende werden ausgeblendet.
12. Passt nun die einzelnen Bilder in Position und Sichtbarkeit an.

#### Hintergründe von Bildern entfernen  {#hintergruende-von-bildern-entfernen}

Einige Bilder haben einen weißen Hintergrund und sollen womöglich auf einem farbigen Hintergrund Verwendung finden. In diesem kurzen Schritt erfahrt ihr, wie ein einfarbiger Hintergrund mit nur wenigen Klicks von einem Bild entfernt wird.

1. Aktiviert das gewünschte Bild im Palettenmenü unter Ebenen mit einem Klick. Ihr erkennt die aktive Ebene an der blauen Hinterlegung.
2. Wählt in der Werkzeugpalette das Zauberstab-Werkzeug <i class="fas fa-magic"></i> aus. Klickt mit dem Zauberstab auf einen Teil des Hintergrunds. Es erscheint eine gestrichelte Linie um das Motiv herum. Wählt im Hauptmenü den Befehl `Bearbeiten > Löschen` aus. Anschließend im selben Menü `Bearbeiten > Auswahl aufheben` ausführen.
3. Wählt nun in der Werkzeugpalette wieder das Auswahlwerkzeug <i class="fas fa-mouse-pointer"></i> an und verschiebt das freigestellte Objekt auf die gewünschte Position.

#### Collage speichern  {#collage-speichern}

Damit die Collage zu einem Ausmalbild wird, muss sie zunächst als Bild auf dem Computer gespeichert werden.

1. Speichert das Bild im Hauptmenü mit dem Befehl `Datei > Speichern`.
2. Es erscheint ein Speichern-Dialog, in dem ihr noch die Bildqualität einstellen könnt. Stellt die Qualität mit dem Regler auf 100. Somit wird das Bild in höchster Qualität gespeichert. Bestätigt mit dem Button `OK` und speichert das Bild auf dem Computer.

### Umrissbild erstellen  {#umrissbild-erstellen}

Ausmalbilder sind im Prinzip Strichzeichnungen ohne farbliche Inhalte. Mit dem kostenlosen Online-Tool von [umrissbilder.de](http://umrissbilder.de/) können vorhandene Bilder mit wenigen Klicks umgewandelt werden.

1. Öffnet die Internetseite [umrissbilder.de](http://umrissbilder.de/).
2. Klickt auf den Button `Eigenes Bild hochladen` und wählt die gespeicherte Collage aus dem letzten Schritt.
3. Nach kurzer Wartezeit werden verschiedene Varianten zur Auswahl gestellt. Sucht euch ein passendes Bild aus und ladet es mit einem Klick auf das Bild herunter.

<!-- Erweitern durch einen online Book-Generator oder LibreOffice oder Gravit Designer ... -->

## Lizenz  {#lizenz}

<a class="cc-license" rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"><a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/freie-ausmalbilder-im-handumdrehen-selbst-erstellen/" property="cc:attributionName" rel="cc:attributionURL">Freie Ausmalbilder im Handumdrehen selbst erstellen</a></span> von <em><a href="https://www.manfred-steger.de/" target="_blank">Manfred Steger</a></em> für das Universitätskolleg Digital der Universität Hamburg ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">CC BY 4.0 International Lizenz</a>.
