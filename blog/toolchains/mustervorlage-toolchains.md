*Kurzbeschreibung Toolchain*.

### User Story ([Persona Vorname Nachname](https://git.universitaetskolleg.uni-hamburg.de/synlloer/backlog/blob/master/Material/Personas-03.pptx))

Person ist ...

### Lernziele

- Ziel 1
- Ziel 2
- Ziel 3
- usw.

### Self-Assessment

[Link hier rein](#)

### Gliederung modularisierte Schritt-für-Schritt-Anleitung

1. z.B.: CC Bildersuche (Sonne, Wasser und Blatt) (Bezug Synergie Praxis: [*04 Offenes Material finden*](https://synlloer.gitbooks.io/synergie-praxis-open-educational-resources-2017/content/04-wo-finde-ich-offenes-material-von-anderen.html))
    - **[OpenClipart](https://openclipart.org/) (CC 0)**
    - ...

2. ...

3. ...
    
### Ausgearbeitete Videotutorials

[Titel und Link zum Repo](https://git.universitaetskolleg.uni-hamburg.de/synlloer/oer-know-how/tree/master/Videotutorials/tutorial-photosynthese-collage)
