<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Kein Platz für die Posterausstellung?

## Wir zeigen euch, wie ihr einen digital showroom erstellt und mit Leben füllen könnt.

> geschätzer Zeitaufwand: 15 - 20 Minuten

> Schwierigkeitsgrad: 4/5 Punkte

Tags: Online Whiteboard, PDF to PNG Converter, Online Tafel, Realtime Board, Convertio, prezi.com, slideshare, To Png Converter, AWW App, Zamzar

### User Story ([Persona Holger van Bik](https://git.universitaetskolleg.uni-hamburg.de/synlloer/backlog/blob/master/Material/Personas-03.pptx))

-->

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/biBjADM8t7g" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Inhaltsverzeichnis

- [Online-Tafel öffnen](#online-tafel-oeffnen)
- [Poster als PNG exportieren](#poster-als-png-exportieren)
- [Posterbilder einfügen](#posterbilder-einfuegen)
- [Simultan arbeiten](#simultan-arbeiten)
- [AWW-board navigieren](#aww-board-navigieren)
- [AWW-board speichern](#aww-board-speichern)

![Die Illustration stellt die Persona Holger van Bik dar. Die Illustration hat im Hintergrund einen hellgrauen Kreis, vor dem der Rest der Darstellung teilweise die Kreisränder überlappt und damit den verfügbaren Raum stärker ausfüllt. Im Kreis ist eine bemasste Skizze eines Grundrisses für einen Raum aus hellblauen Linien dargestellt, wobei der Raum im unteren Bereich einen Eingang ohne Tür hat und sich im Raum zwei ungehbare Einschlüsse befinden, die z.B. einen Schornstein oder einen Raum einer Nachbarwohnung darstellen können. Der Grundriss wird teilweise vom Kopf der Persona verdeckt. Die Persona zeigt den Oberkörper eines hellhäutigen Mann, der ein schwarzes Jackett über einem weißen Shirt und eine schwarze Brille trägt. Die Blickrichtung des Mannes ist nach schräg rechts aus dem Bild heraus und der Mann sitzt an einer hellgrauen Tischplatte. Der Mund ist geschlossen und genau wie die Nase nur angedeutet, die Augen sind hinter der Brille nicht erkennbar. Der Mann hat kurze hellbraunen Haare und trägt hinter dem rechten Ohr einen roten Bleistift, der nach schräg oben rechts zeigt. Der linke Arm liegt mit der Hand auf der Tischplatte auf, der rechte Arm ist auf der Tischplatte am Gelenk aufgesetzt und nach schräg rechts oben erhoben, wobei die Hand geöffnet ist und Daumen und Zeigefinger auf den Grundriss im Hintergrund deuten. Der Mann wirkt als würde er zum Sprechen ansetzen.](https://gitlab.com/flavoursofopenscience/openlab/raw/master/blog/personas/uhh-personas-holger-van-bik/uhh-personas-holger-van-bik.png)

Holger ist Architekturprofessor, der von seinen Studierenden einen Leistungsnachweis in Form einer Plakatvorstellung fordert. Die Anzahl der Teilnehmenden ist in diesem Semester deutlich höher als sonst und alle Wände des Flurs sowie die des Seminarraums sind bereits belegt. Daraufhin sucht er nach einer Lösung, um die übrigen Plakate nicht linear zu präsentieren.

## Lösungsansatz

Eine Kollegin empfiehlt Holger nach einer Art Online-Tafel zu suchen und, um die Anzahl der Suchergebnisse noch zu steigern, dafür den englischen Begriff des "Online-Whiteboards" zu nutzen. Er entscheidet sich für den Anbieter [AWW App](https://awwapp.com/#), bei dem er die Tools und Funktionen des Whiteboards direkt und ohne jegliche Anmeldung nutzen kann. Zudem kann er einen zugehörigen Link für seine Studierenden freigeben, sodass diese ihre Dateien im Bildformat selbst in das Online-Whiteboard einarbeiten können. Zu guter Letzt kann er nach der Bearbeitung den Teil der Arbeiten eingrenzen, den er speichern und mit seinem Kollegium teilen möchte.

## Lernziele

- die Vorteile des unbegrenzten Online-Spaces verstehen
- ökologisch arbeiten, indem auf das Drucken und das Papier verzichtet wird
- Teilen oder Veröffentlichung der Projekte werden vereinfacht
- Nachteil erkennen, dass hierdurch eine Person für mehrere Personen entscheidet, mit welchem Projekt gestartet wird

## Schritt-für-Schritt-Anleitung

### Online-Tafel öffnen {#online-tafel-oeffnen}

Um die Online-Tafel **AWW-Board** zu nutzen, muss dafür lediglich der Link zur [AWW Board App](https://awwapp.com/) aufgerufen und der Button `Start drawing` angeklickt werden.

### Poster als PNG exportieren {#poster-als-png-exportieren}

Um nun ein Poster hochladen zu können, solltet ihr sicherstellen, dass es sich um eine PNG-Datei handelt. Eine Konvertierung kann mit dem Open-Source-Programm [LibreOffice](https://de.libreoffice.org/) für verschiedene Präsentationsformate vorgenommen werden. Für den Download könnt ihr mit der [nutzerfreundlichen Suchmaschine](https://donttrack.us/) [Duckduckgo.com](https://duckduckgo.com/) nach den Begriffen `LibreOffice Download` suchen; ansonsten kann der Download mit dem Direktlink [Download Link](https://de.libreoffice.org/download/libreoffice-still/) mit Klick auf die aktuellste Version sofort begonnen werden. Nach erfolgreichem Download lässt sich LibreOffice starten und  die Präsentation mit Klick auf den Menüpunkt `Open File` öffnen. Darauffolgend muss die Datei in das gewünschten Format exportiert werden. Die Exportfunktion von LibreOffice befindet sich im Hauptmenü unter dem Reiter `File > Export`. Wählt im Dialog das gewünschte Speicherformat sowie den Speicherort und bestätigt zuletzt mit dem Button `Export`.

### Posterbilder einfügen {#posterbilder-einfuegen}

Nun kann die PNG-Datei per Klick auf den Button `Upload File` <i class="fas fa-plus"></i> (unten links) in das AWW-Board eingefügt werden. Die Größe und die Position des Bildes lassen sich jetzt verändern. Zum Fixieren des Bildes müsst ihr den Button `Place Image` drücken; die Größe ist im Nachhinein dann nicht mehr veränderbar, jedoch lässt sich mit dem Auswählen des Buttons `Select` <i class="fas fa-mouse-pointer"></i> das Bild noch verschieben. Ist die Tafel voll, könnt ihr das Handsymbol <i class="fas fa-hand-paper-o"></i> in der rechten Menüleiste anklicken und mit gedrückter Maus in die gewünschte Richtung ziehen.

### Simultan arbeiten {#simultan-arbeiten}

Sollen nun auch andere Nutzende die Möglichkeit bekommen, Bilder einzufügen, könnt ihr diese per Klick auf `Collaborate` <i class="fas fa-user-plus"></i> einladen. Wählt hier unter anderem zwischen einer Einladung per E-Mail oder einem teilbaren Link. So kann beispielsweise der Link per Klick auf den Button `Text` <i class="fas fa-font"></i> in das AWW-Board eingefügt werden, damit weitere Teilnehmende sich diesen abschreiben und auf ihren Geräten aufrufen können. Mit dem geteilten Link können alle weiteren Nutzenden dann auf dieselbe Art und Weise ihre Inhalte einfügen.
Wer gerade online ist und an dem AWW-Board arbeitet, wird den Nutzenden im Fenster unten rechts angezeigt. Dieses taucht nur dann auf, wenn KollaborationspartnerInnen eingeladen wurden.

### AWW-Board navigieren {#aww-board-navigieren}

Möchtet ihr euch eine Übersicht verschaffen oder ein Bild genauer anschauen, könnt ihr dafür das Symbol <i class="fas fa-plus"></i> zum Vergrößern und das Symbol <i class="fas fa-minus"></i> zum Verkleinern verwenden.

### AWW-Board speichern {#aww-board-speichern}

Um nun das bearbeitete AWW-Board zu speichern, müsst ihr auf den Button `Export` klicken und die Option `Export as an image` ausgewählen. Mit dem Link lässt sich das Board jedoch auch nach dem Schließen der Seite weiterhin aufrufen.

> Tipp: Als registrierte Nutzerin oder registrierter Nutzer kann das Board auch als hochauflösendes PDF abgespeichert werden.

## Lizenz

<a class="cc-license" rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"><a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/kein-platz-fuer-die-posterausstellung/" property="cc:attributionName" rel="cc:attributionURL">Kein Platz für die Posterausstellung?</a></span> von <em>Sophia Zicari</em> für das Universitätskolleg Digital der Universität Hamburg ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">CC BY 4.0 International Lizenz</a>.

<!-- Gliederung modularisierte Schritt-für-Schritt-Anleitung

1. Plakate im Bildformat speichern
  - [Libre Office](https://de.libreoffice.org/download/libreoffice-still/)
  - [PDF to PNG](http://pdf2png.com/)
  - [Convertio](https://convertio.co/de/)
  - [Zamzar](https://www.zamzar.com/)

2. Plakate online stellen
  - [AWW App](https://awwapp.com/)
  - [RealtimeBoard](https://realtimeboard.com/)

3. Plakatübersicht speichern und veröffentlichen (Bezug Synergie Praxis: [_08 Die Veröffentlichung_](https://synlloer.gitbooks.io/synergie-praxis-open-educational-resources-2017/content/08-die-veroffentlichung.html))
  - [AWW App](https://awwapp.com/)-->
