<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Get stuff done – die kollaborative To-do-Liste

## Einkaufsliste, Aufgabenplanung oder Ideensammlung - mit Checklisten den Alltag erleichtern.

> geschätzer Zeitaufwand: 10 - 15 Minuten

> Schwierigkeitsgrad: 1/5 Punkte

Tags: Online Listen erstellen, Link verkürzen, URL Shortener, PDF Editor, TinyURL, Wunderlist, Trello, Online PDF Editor

### User Story ([Persona Fatih Ayoub](https://git.universitaetskolleg.uni-hamburg.de/synlloer/oer-know-how/blob/master/Unterlagen/Personas-03.pdf))

-->

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/lXzqAkTOZuY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Inhaltsverzeichnis

- [Wunderlist öffnen](#wunderlist-oeffnen)
- [Liste erstellen](#liste-erstellen)
	- [Listenpunkte einfügen](#listenpunkte-einfuegen)
	- [Listenpunkte als erledigt markieren](#listenpunkte-als-erledigt-markieren)
	- [Liste teilen und herunterladen](#liste-teilen-und-herunterladen)
- [PDF-Editor öffnen](#pdf-editor-oeffnen)
	- [Liste bearbeiten](#liste-bearbeiten)
	- [Bearbeitete Liste herunterladen](#bearbeitete-liste-herunterladen)

![Die Illustration stellt die Persona Fatih Ayoub dar. Die Illustration hat im Hintergrund einen hellgrauen Kreis, vor dem der Rest der Darstellung teilweise die Kreisränder überlappt und damit den verfügbaren Raum stärker ausfüllt. Die Persona zeigt den Oberkörper eines hellhäutigen Mann, der vor einer hellbraunen Tischplatte an einem aufgeklappten Notebook sitzt. Er tippt mit beiden Händen auf der Tastatur das Notebooks und schaut auf das Display. Das Notebook hat einen hellblaugrauen Rahmen und eine taubenblaue Tastatur. Der Mann sitzt auf einem schwarzen Polsterstuhl, dessen Rückenlehne teilweise erkennbar ist. Neben dem Gesicht des Mannes und über dem Notebook schweben vor dem Kreishintergrund drei mittelgrüne OK-Haken übereinander, die mit zunehmender Höhe größer werden. Der Mann trägt einen mittelblauen Pullover über einem roten Shirt. Der Mann hat einen kaum erkennbaren Mund und eine kaum erkennbare Nase und die Augenhöhlen sind dunkelbeige angedeutet. Der Mann hat schwarze kurze Haare, die ohne Unterbrechung in einen gut getrimmten schwarzen Backen-, Kinn- und Schnurrbart übergehen und damit den Mundbereich vollständig einrahmen.](https://gitlab.com/flavoursofopenscience/openlab/raw/master/blog/personas/uhh-personas-fathi-ayoub/uhh-personas-fathi-ayoub.png)

Fatih unterrichtet in einer 1. Klasse. Aus eigener Erfahrung weiß er, dass Schulmateriallisten meist bei allen Lehrenden unterschiedlich sind und des Öfteren nicht bei den Eltern ankommen. So kommt es, dass den Kindern dann Materialien fehlen oder Lehrende erst im Elterngespräch darauf hinweisen. Für die Eltern seiner 1. Klasse wünscht er sich nun eine digitale Einkaufsliste, die ihnen jederzeit flexibel und individuell veränderbar für die Zusammenarbeit mit allen Lehrenden zur Verfügung steht.

## Lösungsansatz

Fatih benötigt eine sogenannte [To-do-Liste](https://www.wunderlist.com/de/), die es erlaubt, Personen einzuladen und die Liste in einem digitalen Format zu speichern (z.B. PDF). Mit einem [PDF-Editor](https://www.sejda.com/pdf-editor) kann Fatih nachträglich noch das Logo der Schule und die Jahrgangsstufe dazuschreiben. Das Editieren von PDFs kann in vielen Anwendungsbereichen sinnvoll sein. Freie Materialen decken oft einen großen Prozentteil der Erwartungen und werden oft nur wegen ein oder zwei Unzulänglichkeiten nicht verwendet. Mit der nachträglichen Bearbeitung kann das Dokument an inviduelle Bedürfnisse angepasst werden.

## Lernziele

- eine teilbare Liste online erstellen
- kollaborativ an einer Liste arbeiten
- Export einer Liste in verschiedene Formate
- nachträgliche Bearbeitung von PDF-Dateien kennenlernen

## Schritt-für-Schritt-Anleitung

### Wunderlist öffnen {#wunderlist-oeffnen}

Um eine Online-Liste zu erstellen, kann mit der [nutzerfreundlichen Suchmaschine](https://donttrack.us/) [Duckduckgo.com](https://duckduckgo.com/) nach dem Begriff `Wunderlist` gesucht werden. Als Suchergebnis wird dann der Link zu der Webseite von [**Wunderlist**](https://www.wunderlist.com/de/) angezeigt. Um eine Liste auf Wunderlist erstellen zu können, müsst ihr euch einmalig auf der Webseite registrieren. Nachdem die Registrierung per Klick auf den via E-Mail versandeten Registrierungslink bestätigt ist, kann es losgehen.

### Liste erstellen {#liste-erstellen}

Zum Erstellen einer Liste könnt ihr direkt nach der Registrierung aus einer Reihe von Listen-Vorlagen auswählen oder direkt eine eigene erstellen. Für die zweite Variante müsst ihr ganz unten links auf `Liste erstellen` <i class="fas fa-plus"></i> klicken. Daraufhin wird ein Fenster erscheinen, in dem der Name der Liste eingegeben werden muss und weitere Listenmitglieder, per Eingabe der E-Mail-Adressen, eingeladen werden können. Die Eingaben müsst ihr per Klick auf `Speichern` bestätigen.

#### Listenpunkte einfügen {#listenpunkte-einfuegen}

Links im Menü ist die Liste nun sichtbar und es können Elemente hinzugefügt werden. Um ein Element einzufügen, müsst ihr lediglich in der Spalte, auf der **Einen Eintrag hinzufügen** steht, das Gewünschte eintippen und mit der Eingabetaste auf der Tastatur hinzufügen.

#### Listenpunkte als erledigt markieren {#listenpunkte-als-erledigt-markieren}

Um Listenpunkte als erledigt zu markieren, muss auf das Kästchen vor dem Eintrag geklickt werden. Die erledigten Listenpunkte könnt ihr per Klick auf die Option `Erledigte Einträge anzeigen` wieder anschauen und so auch wieder, per Klick auf das Kästchen, in die Liste zurückschieben.

#### Liste teilen und herunterladen {#liste-teilen-und-herunterladen}

Oben rechts befindet sich der Button `Mehr` <i class="fas fa-ellipsis-h"></i>, der angeklickt werden muss. Hier könnt ihr zwischen den Optionen, die Liste per E-Mail zu versenden oder sie zu drucken, auswählen. Um die Liste herunterzuladen, klickt auf `Liste drucken`. Im Dialog müsst ihr die Option `Als PDF sichern` wählen; normalerweise befindet sich diese unten links. Daraufhin werden die Nutzenden aufgefordert, den gewünschten Speicherort auszuwählen und die Auswahl mit Klick auf `Sichern` zu bestätigen.

### PDF-Editor öffnen {#pdf-editor-oeffnen}

Um die heruntergeladene Liste nun etwas aufzuhübschen, könnt ihr den [**Online-PDF-Editor**](https://www.sejda.com/pdf-editor) nutzen. Mit dem Direktlink [https://www.sejda.com/pdf-editor](https://www.sejda.com/pdf-editor) kann sofort mit der Bearbeitung der Liste gestartet werden. Die Liste müsst ihr dafür mit Klick auf den Button `Upload PDF file` <i class="fas fa-file-pdf"></i> auf die Webseite hochladen.

#### Liste bearbeiten {#liste-bearbeiten}

An dieser Stelle könnt ihr die Liste mit den Werkzeugen aus der Menüleiste bearbeiten. Mit dem Button `Text` <i class="fas fa-font"></i> und dem Klicken auf die zu verändernde Textstelle sind alle weiteren Bearbeitungen möglich. So können Textstellen gelöscht, aber auch neue Texte im selben Stil eingefügt werden.

>Tipp: Störende Elemente könnt ihr auch mit dem Button `Whiteout` <i class="fas fa-eraser"></i> verdecken.

#### Bearbeitete Liste herunterladen {#berabeitete-liste-herunterladen}

Klickt zuletzt auf den Button `Apply changes`. Die bearbeitete Liste steht den Nutzenden nach kurzer Bearbeitungszeit dann zur Verfügung und kann per Klick auf `Download` <i class="fas fa-download"></i> heruntergeladen werden.

## Lizenz

<a class="cc-license" rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"><a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/get-stuff-done-die-kollaborative-todo-liste/" property="cc:attributionName" rel="cc:attributionURL">Get stuff done – die kollaborative To-do-Liste</a></span> von <em>Sophia Zicari</em> für das Universitätskolleg Digital der Universität Hamburg ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">CC BY 4.0 International Lizenz</a>.

<!--### Gliederung modularisierte Schritt-für-Schritt-Anleitung

1. Online Liste erstellen
  - [**Wunderlist**](https://www.wunderlist.com/de/)
  - [doitdoitdone](http://www.doitdoitdone.com/viewList?editKey=mR4jlX5smG&listId=el58fDm6aK)
  - [listmoz](http://listmoz.com/#b4m6hh4pKjGmFs5yCF)

2. ~~Listen-Link verkürzen~~(immer das gleiche und nicht wirklich sinnvoll hier)
  - [Bitly URL Shortener](https://bitly.com/)
  - [Google URL Shortener](https://goo.gl/)
  - [TinyURL](https://tinyurl.com/)

3. PDF Editor
   - [Online PDF Editor](https://www.sejda.com/pdf-editor)

4. Link Teilen
  - E-Mail
  - Soziale Netzwerke -->
