<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Dokumente inklusiv(er) gestalten

## Super Inhalte aber Schrift zu klein oder Bild ohne Bildbeschreibung? In dieser Anleitung gehen wir Barrieren auf den Grund.

> geschätzer Zeitaufwand: 15 - 20 Minuten

> Schwierigkeitsgrad: 2/5 Punkte

Tags: Barrierefreiheit, barrierefreie Dokumente, Inklusion, accessibillity, Pave PDF, Arbeitsblatt, OER, online convert, Text to Speech, TTS, accessibillity check

## User Story (Persona Eike Lindemann)

-->

## Inhaltsverzeichnis

- [Freie Dokumente finden](#freie-dokumente-finden)
- [Arbeitsblatt inklusiv(er) aufbereiten](#arbeitsblatt-inklusiver-aufbereiten)
- [Zugänglichkeit überprüfen](#zugaenglichkeit-ueberpruefen)
- [Zugänglichkeit verbessern](#zugaenglichkeit-verbessern)

Eike ist Physiklehrerin an einer Stadtteilschule, die Inklusion in beinahe allen Klassenverbänden umgesetzt hat. Über die Jahre hat sie feststellen müssen, dass die Erstellung von differenzierten Unterrichtsmaterialien sehr zeitaufwendig und oftmals nicht von Erfolg gekrönt war. Eike möchte nun ihre Materialien für möglichst alle Lernenden gleichermaßen gut aufbereiten und zur Verfügung stellen.

## Lösungsansatz {#loesungsansatz}

Eike benötigt für ein barrierefreies Dokument eine [Vorlage](https://www.edutags.de/) oder ein [Programm](https://tutory.de/), das Dokumente erstellen und als PDFs speichern kann. Die gespeicherten PDFs enthalten sogenannte Tags, die das Dokument beschreiben. Damit fehlende oder unzureichend ausgewiesene Tags innerhalb der PDFs verändert werden können, benötigt Eike einen [accessibillity check](http://pave-pdf.org/), der die Fehler erkennt und im besten Fall auch direkt ausbessern kann.

## Lernziele  {#lernziele}

- ein Dokument auf Barrierefreiheit überprüfen
- Parameter, die zur Barrierefreiheit beitragen, erkennen und ergänzen
- Dokumente in verschiedene Formate konvertieren
- Bilder barrierefrei umsetzen

## Schritt-für-Schritt-Anleitung  {#schritt-fuer-schritt-anleitung}

Barrierefreie Dokumente sind nicht ausschließlich für sonderpädagogische Zwecke von Nutzen und bieten für alle Nutzergruppen einen Mehrwert. Ein barrierefreies PDF kann z.B. mit dem Handy während der Autofahrt vorgelesen, oder ein Roman in wenigen Schritten in ein Hörbuch umgewandelt werden. Nicht nur in der Schule sondern auch im Alltag bringt diese universale Form viele neue Möglichkeiten zum Vorschein. Bringt ein paar Minuten eurer Zeit dafür auf, es wird sich lohnen!

## Freie Dokumente finden  {#freie-dokumente-finden}

Die Grundlage für ein barrierefreies Dokument ist eine digitale Vorlage wie z. B. PDF, Office-Dokument (OpenOffice/Word), eine Präsentation oder ein ganzes digitales Buch (EPUB/MOBI). Im Internet gibt es zahlreiche Plattformen und Suchmaschinen, die sich auf frei verwendbare Dokumente spezialisiert haben:

- [tutory](https://tutory.de/) (Creative-Commons-Arbeitsblätter)
- [edutags.de](https://www.edutags.de/) (Social Bookmarking für den Bildungsbereich)
- [archive.org](https://archive.org) (Public Domain)
- [OpenLibrary](https://openlibrary.org/) (Public Domain)
- [Lehrer-Online](https://www.lehrer-online.de) (Kooperation mit kommerziellen Anbietern – Lizenzen müssen für jedes Material separat betrachtet und überprüft werden.)
- [Projekt Gutenberg](https://www.gutenberg.org/) (Public Domain – zurzeit in Deutschland wegen unterschiedlichen Urheberrechtsgesetzen (DE / US) geblockt).

Für den Unterricht bietet sich [tutory.de](https://tutory.de/) und im Allgemeinen [edutags.de](https://www.edutags.de/) für die Dokumentenrecherche an.

> **Tipp:** Wer Tutory von der Pike auf lernen möchte, ist [hier](https://openlab.blogs.uni-hamburg.de/interaktive-arbeitsblaetter-erstellen-und-anreichern/) genau richtig.

1. Ruft im Browser (z.B. [Chrome](https://www.google.de/chrome/?brand=CHBD&ds_kid=[*TrackerID*]&gclid=EAIaIQobChMIqqvQ18n72QIVy5ztCh3FNQ7wEAAYASAAEgIpUPD_BwE) oder [Firefox](https://www.mozilla.org/de/firefox/new/)) die Internetseite [tutory.de](https://www.tutory.de/) auf. Für das Portal wird ein Account benötigt, welcher in der Basisversion kostenfrei ist. Dieser reicht für das Erstellen, Suchen und Kopieren von bestehenden Materialien aus.
2. Nach Registrierung und Anmeldung stehen drei Optionen zur Verfügung: **Vorlagen**, **Eigene** und **Entdecken**.
3. Klickt auf **Entdecken**.
4. Gebt nun im Suchfeld ein gewünschtes Schlagwort ein. In diesem Beispiel wurde das Schlagwort `Hebel` verwendet, um ein passendes Arbeitsblatt zum Hebelgesetz für den Physikunterricht zu finden.
5. Klickt auf das gewünschte Arbeitsblatt und es erscheint eine Detailseite mit weiteren Informationen.
6. Auf der Detailseite habt ihr die Optionen, das Arbeitsblatt über den Button `PDF` <i class="fas fa-file-pdf"></i> direkt herunterzuladen oder mit dem Button `Private Kopie aus dieser Vorlage` <i class="fas fa-plus-circle"></i> eine veränderbare bzw. erweiterbare Kopie zu erstellen.

<!-- ![Screenshot 3 Optionen von Tutory: Vorlagen, Eigene, Entdecken, von Manfred Steger](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/barrierefreie-pdfs/barrierefreie-pdfs-tutory-entdecken.png)
Manfred Steger, "Tutory 3 Optionen" [CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)
-->

<!-- ![Screenshot 3 Optionen von Tutory: Vorlagen, Eigene, Entdecken, von Manfred Steger](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/barrierefreie-pdfs/barrierefreie-pdfs-tutory-hebel.png)
Manfred Steger, "Tutory freie Arbeitsblätter entdecken" [CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)
-->

## Arbeitsblatt inklusiv(er) aufbereiten  {#arbeitsblatt-inklusiver-aufbereiten}

Auf der Detailseite zum Arbeitsblatt wird sichtbar, dass die Schriftgröße und der Zeilenabstand z. B. für Menschen mit einer Sehbeeinträchtigung viel zu klein sind. In diesem Schritt wird aus der Vorlage eine Kopie erstellt und der Schriftgrad optimiert.

1. Erstellt über die Detailseite des Arbeitsblatts mit dem Button `Private Kopie aus dieser Vorlage` <i class="fas fa-plus-circle"></i> eine Kopie.
2. Im Hauptmenü am oberen Browserrand befindet sich der Button `AAA`, mit dem die generelle Schriftgröße des Arbeitsblatts seiten- und absatzübergreifend angepasst werden kann. Klickt darauf und wählt die Option **Groß** aus.
3. Die Schriftgröße wird auf allen Seiten vergrößert; dabei kann es passieren, dass einzelne Absätze überlappen. Ihr solltet also alle Absätze nachträglich prüfen. Überlappende Absätze könnt ihr mit der Maus verschieben und anschließend vergrößern.
4. Zum Vergrößern eines Textfeldes muss dieses mit der linken Maustaste angeklickt werden. Ein aktives Textfeld könnt ihr mit der Maus an den Ecken anpassen. Passt nun alle Medienelemente auf dem Arbeitsblatt an.
5. Speichert zu guter Letzt das überarbeitete Arbeitsblatt über `Hauptmenü > PDF > Arbeitsblatt` auf dem Computer.

<!-- ![Screenshot Tutory seiten- und absatzübergreifende Schriftanpassung, von Manfred Steger](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/barrierefreie-pdfs/barrierefreie-pdfs-tutory-schriftanpassung.png)
Manfred Steger, "Tutory generelle Schriftanpassung" [CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)
-->

<!-- ![Screenshot Tutory Medienelemente arrangieren, von Manfred Steger](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/barrierefreie-pdfs/barrierefreie-pdfs-tutory-medienelemente-arrangieren.png)
Manfred Steger, "Tutory Medienelemente arrangieren" [CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)
-->

## Zugänglichkeit überprüfen  {#zugaenglichkeit-ueberpruefen}

Jetzt wird das erstellte oder bereits vorhandene Dokument auf Barrierefreiheit überprüft und verbessert. Das Online-Tool [Pave-PDF](http://pave-pdf.org/) ist hierfür ein geeignetes Werkzeug. Damit ein Dokument barrierefrei wird, müssen viele Parameter berücksichtigt werden, die für die Lesenden zunächst nicht sichtbar sind, z.B. sogenannte Tags und Metatags. Diese sind im Hintergrund des Dokuments eingebunden und steuern z. B. die Leseabfolge für einen Screenreader oder die Beschreibung einer Bilddatei.

1. Öffnet die Internetseite von [Pave-PDF](http://pave-pdf.org/).
2. Über den Button `PAVE Starten` wird das Programm im Browser gestartet.
3. Klickt auf den Button `Upload file` <i class="fas fa-upload"></i> und wählt das eigene oder heruntergeladene PDF aus.
4. Nach kurzer Wartezeit werden im Browser die einzelnen PDF-Seiten in einer Vorschau und auch eine Liste an Ereignissen angezeigt.
5. Auf der linken Seite werden unter dem Reiter **Tasks** die automatisch behobenen und die noch ausstehenden Probleme angezeigt.

<!-- ![Screenshot PAVE PDF Startseite, von Manfred Steger](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/barrierefreie-pdfs/barrierefreie-pdfs-pave-tags.png)
Manfred Steger, "PAVE PDF Startseite" [CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)
-->

<!-- ![Screenshot PAVE PDF Korrektur, von Manfred Steger](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/barrierefreie-pdfs/barrierefreie-pdfs-pave-correction.png)
Manfred Steger, "PAVE PDF Korrektur" [CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)
-->

## Zugänglichkeit verbessern  {#zugaenglichkeit-verbessern}

Die aufgelisteten Probleme unter **Tasks** können nun Schritt für Schritt abgearbeitet werden.

1. **"Language is not specified"**: Das Dokument hat kein Sprachattribut; Suchmaschinen oder der Screenreader wissen demnach nicht, um welche Sprache es sich handelt. Dies hat oft weitreichende Folgen wie z. B. eine fehlende Rechtschreibkorrekturanzeige, die Computerstimme liest das Dokument in einer falschen Sprache vor, uvm.
2. Klickt nun auf den Button `Edit properties`, um die fehlenden Informationen nachzutragen. Gebt unter **Titel** einen aussagekräftigen Titel ein. Im Falle des Physik-Arbeitsblatts zur Hebelwirkung kann dieser wie folgt lauten: "Arbeitsblatt mit Übungsaufgaben zur Hebelwirkung in der Physik".
3. **"Some content is not tagged"**: Medieninhalte wie Bilder müssen mit einem sogenannten Alternativtext ausgezeichnet werden. Falls das Bild nicht dargestellt werden kann, erscheint an dessen Stelle der Text. Ein Screenreader würde in diesem Zusammenhang den Alternativtext vorlesen. Klickt auf den Button `Show issue details`.
4. Je nach Umfang des PDFs werden die einzelnen Seiten mit fehlenden Tags in einer Liste dargestellt. Mit einem Klick auf die jeweilige Seite werden die seitenspezifischen Probleme aufgelistet.
5. Die Problemstellen sind in der Vorschau auf der rechten Seite rot hinterlegt. In diesem Beispiel sind es die Bilder, die keinen Alternativtext besitzen. Mit einem Klick auf ein Bild öffnet sich auf der linken Seite das Tagging-Menü.
6. Im Tagging-Menü wird in vier Punkten die weitere Vorgehensweise erklärt.
7. Wählt nun im Falle eines Bildes unter Punkt 2 **Select element type** die Option `figure` an.
8. Im Punkt 4 **Enter alternative text** muss eine Beschreibung eingefügt werden, die der Abbildung möglichst genau entspricht. Hier wurde folgender Alternativtext gewählt: "Darstellung eines Muskelskeletts mit markiertem Biceps brachii am linken und rechten Oberarm".
9. Klickt am Ende der Liste auf den Button `Save`.

Taggt nun alle Elemente, die Probleme aufweisen. Nach Abschluss kann noch die Lesereihenfolge überprüft und das inklusive(re) Dokument gespeichert werden.

1. Sind alle Medieninhalte mit Tags versehen, erscheint anstelle des Hinweises **"Some content is not tagged"** der Hinweis **"All content is tagged"**. Mit einem Klick auf den Button `Review reading order` könnt ihr die Struktur der einzelnen Inhalte (Absätze und Bilder) überprüfen und anpassen.
2. Im Reiter **Reading order** wird eine Liste der einzelnen Seitenelemente dargestellt. Beim Mouseover über ein Listenelement wird das zugehörige Element in der Vorschau markiert. Fahrt nun jedes einzelne Listenelement von oben nach unten ab und kontrolliert bei jedem Element, ob es in der logischen Lesereihenfolge erscheint. Gibt es Unstimmigkeiten, könnt ihr die Listenreihenfolge mit `Drag & Drop` anpassen.
3. Ist diese Aufgabe erledigt, kann das PDF unter dem Reiter **Tasks** am Ende der Liste über den Button `Download` gespeichert werden.
4. Es erscheint ein Pop-Up. Über den Button `Download document` am unteren rechten Bildrand könnt ihr das verbesserte PDF auf dem Computer speichern.

<!-- PDF to TXT und TXT to TTS nicht möglich, da Konverter keine Bildunterschriften mit übersetzt – anderen Konverter suchen und weitermachen  -->

<!-- ![Screenshot PAVE PDF Tagging, von Manfred Steger](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/barrierefreie-pdfs/barrierefreie-pdfs-pave-issue-details.png)
Manfred Steger, "PAVE PDF Tagging" [CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)
-->

<!-- ![Screenshot PAVE PDF Lesereihenfolge, von Manfred Steger](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/barrierefreie-pdfs/barrierefreie-pdfs-pave-reading-order.png)
Manfred Steger, "PAVE PDF Lesereihenfolge" [CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)
-->

<a class="cc-license" rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"><a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/dokumente-inklusiver-gestalten/" property="cc:attributionName" rel="cc:attributionURL">Dokumente inklusiv(er) gestalten</a></span> von <em><a href="https://www.manfred-steger.de/" target="_blank">Manfred Steger</a></em> für das Universitätskolleg Digital der Universität Hamburg ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">CC BY 4.0 International Lizenz</a>.

<!-- ### Gliederung und Alternativen

1. OER Arbeitsblatt suchen/finden/speichern
  - [Projekt Gutenberg](https://www.gutenberg.org/) (Public Domain) – Deutschland wegen 70 Jahre Copyright geblockt
  - [archive.org](https://archive.org) (Public Domain)
  - [OpenLibrary](https://openlibrary.org/) (Public Domain)
	- **[Tutory](https://tutory.de/) (CC)**
  - [Lehrer-Online](https://www.lehrer-online.de)
2. PDF auf Barrierefreiheit überprüfen und verbessern
  - **[Pave PDF](http://pave-pdf.org/)**
3. PDF als Plain Text speichern (Bildbeschreibungen werden nicht übernommen, daher weggelassen)
  - **[Online-Convert.com](https://document.online-convert.com/convert-to-txt)**
4. Text to Speech
  - [YAKiToMe!](https://www.yakitome.com/upload/from_text)

-->
