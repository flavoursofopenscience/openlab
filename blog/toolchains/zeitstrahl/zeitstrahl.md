<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Digitaler Zeitstrahl im OER-Format

## In diesem Tutorial wird ein analoger Zeitstrahl in eine digitale Version umgesetzt und online frei zugänglich gemacht.

> geschätzer Zeitaufwand: 40 - 60 Minuten

> Schwierigkeitsgrad: 2/5 Punkte

Tags: Timeline Tool, Code Timeline, H5P, HTML5, Duckduckgo, Time Graphics

## User Story ([Persona Susanne Rössel](https://git.universitaetskolleg.uni-hamburg.de/synlloer/oer-know-how/blob/master/Unterlagen/Personas-03.pdf))

-->

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/9fYglsK1N70" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe src="https://openlab.blogs.uni-hamburg.de/wp-admin/admin-ajax.php?action=h5p_embed&id=38" width="818" height="625" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://openlab.blogs.uni-hamburg.de/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

## Inhaltsverzeichnis

- [Online-Tools: Recherche](#online-tools-recherche)
- [Online-Tools: Begutachtung](#online-tools-begutachtung)
- [H5P Timeline](#h5p-timeline)
	- [Ausprobieren](#ausprobieren)
	- [Einrichten](#einrichten)
	- [Erstellen](#erstellen)
	- [Mit freien Inhalten verschönern und ergänzen](#mit-freien-inhalten-verschoenern-und-ergaenzen)
- [Zeitstrahl auf ein Arbeitsblatt übernehmen](#zeitstrahl-auf-ein-arbeitsblatt-uebernehmen)
	- [QR-Code](#qr-code)
	- [Kurzlink](#kurzlink)
	- [Übertragung auf das Arbeitsblatt](#uebertragung-auf-das-arbeitsblatt)

![Die Illustration stellt die Persona Susanna Rössel dar. Die Illustration hat im Hintergrund einen hellgrauen Kreis, vor dem der Rest der Darstellung teilweise die Kreisränder überlappt und damit den verfügbaren Raum stärker ausfüllt. Die Persona zeigt den Oberkörper einer hellhäutigen Frau, die einen roten Blazer über einem weißen Shirt trägt und ein Arbeitsheft vor dem Körper mit beiden Armen umfasst hält. Die Frau hat orangfarbene schulterlange Haare sowie orangfarbene Augenbrauen, der Mund ist zu einem breiten Lächeln geöffnet, die Nase angedeutet und sie sieht in Richtung Betrachter/in mit angedeuteten grauen Augenhöhlen. Hinter der Frau ist eine dickere dunkelgraue Linie, die etwas über Schulterhöhe links beginnt und hinter ihrem Kopf nach rechts oben verläuft und ungefähr doppelt so lang wie der Körper der Frau breit ist. Auf der Linie sind drei Markierungskarten stilisiert dargestellt, auf denen eine Beschriftung angedeutet ist und von denen zwei oberhalb und die mittlere Karte unterhalb der Linie aufgesetzt ist.](https://gitlab.com/flavoursofopenscience/openlab/raw/master/blog/personas/uhh-personas-susanne-roessel/uhh-personas-susanne-roessel.png)

Susanne ist Referendarin an einer Grund- und Gesamtschule und hat einen Zeitstrahl über die Entstehung der Welt bis hin zur Neuzeit im Klassenzimmer ihrer 5. Klasse aufgehängt. Während des Unterrichts haben die Lernenden immer darauf Zugriff. Nun soll der Zeitstrahl auch für die Hausaufgaben in einer digital aufbereiteten Version für die SchülerInnen zur Verfügung stehen.

## Lösungsansatz

Susanne sucht im Internet nach einem geeigneten Werkzeug für die Umsetzung. Mit den Stichwörtern (keywords) ```Code Timeline, Zeitstrahl, Tool, online, HTML5, kostenlos, free``` bekommt sie viele Möglichkeiten angezeigt. Sie vergleicht einige Suchmaschinenergebnisse und entscheidet sich aufgrund des unkomplizierten sowie kostenfreien Zugangs und des staatlich geförderten Betreibers für die Timeline von [H5P](https://h5p.org/timeline). Nach der Registrierung kann die Online-Bearbeitung direkt beginnen. Dabei entdeckt Susanne die Möglichkeit, die einzelnen Epochen ihres Zeitstrahls mit Wikipedia-Einträgen und zusätzlichen Bildern anzureichern.

## Lernziele

- ein Werkzeug für die Umsetzung des Zeitstrahls (timeline) im Internet finden (Suchmechanismen kennenlernen)
- einen kostenlosen Service analysieren und auf Vertrauenswürdigkeit überprüfen (Impressum, Pricing, Datenschutz)
- Umsetzung vom analogen zum digitalen Produkt planen und realisieren
- lizenzrechtliche Vorgehensweise verstehen und anwenden lernen
- das Endprodukt den Lernenden und der Community zur Verfügung stellen

## Schritt-für-Schritt-Anleitung

### Online-Tools: Recherche {#online-tools-recherche}

Im ersten Schritt wird gezeigt, wie Online-Tools mit Hilfe der richtigen Schlagwörter im Internet gefunden werden können.
Susanne möchte einen _Zeitstrahl_ erstellen; mit diesem Begriff kann eine gängige Suchmaschine wie [DuckDuckGo](https://duckduckgo.com/) oder [Google](https://www.google.de/) bereits sehr gute Resultate hervorbringen. Wird gezielt nach einem Online-Tool gesucht, bietet sich ein zweites Schlagwort wie `online` an.

1. Öffnet einen gängigen Browser wie z.B. [Mozilla Firefox](https://www.mozilla.org) und gebt in die Adresszeile `duckduckgo.com` ein.
2. Gebt im Suchfeld der Suchmaschine die Begriffe `zeitstrahl online` ein und startet den Suchvorgang.
3. Die Resultate können bei jeder Suchmaschine und zu jedem Zeitpunkt variieren. An dieser Stelle gilt es, die Suchergebnisse zu bewerten. Der erste Eintrag mit dem Titel **7 Zeitstrahl-Tools für Lehrer/innen** wirkt vielversprechend. Dieser Titel verspricht eine Auflistung für 7 verschiedene Online-Zeitstrahl-Tools - eigentlich genau das, wonach gesucht wird.
4. Es empfiehlt sich, die ersten 3-4 Beschreibungen der Suchergebnisse durchzulesen, um anschließend 1-2 Seiten zu besuchen und weitere Informationen einzuholen.
5. An dieser Stelle betreten wir die Seite zu den **7 Zeitstrahl-Tools für Lehrer/innen**. Ein Tipp am Rande: Oftmals lohnt es sich, ganz am Anfang solcher Ratgeber ans Ende des Beitrags zu wandern, um nach einem Fazit zu suchen, welches Tool denn nun für welchen Einsatzzweck am besten geeignet ist.
6. Nach 5 Minuten Einlesezeit ist **Timeline JS / TimeMapper** mit seinen Vor- und Nachteilen das gewünschte Tool.

### Online-Tools: Begutachtung {#online-tools-begutachtung}

Im ersten Schritt wurden gängige Suchmechanismen mit Tags eingeführt. Nun geht es darum, bestimmte Suchergebnisse auf ihre Tauglichkeit zu überprüfen.

1. Wir nehmen nun den **Timeline JS / TimeMapper** genauer unter die Lupe. Im Ratgeber ist ein Link zur Herstellerseite angegeben ([timeline.knightlab.com](http://timeline.knightlab.com/)). Ein altbekannter Rat an Kinder besagt: _Nimm keine Bonbons von Fremden an_ - im Internet sollte mit Links zu unbekannten Seiten mit gleicher Sorgfalt umgegangen werden.
2. Startet in der Suchmaschine einen erneuten Suchvorgang mit den Schlagwörtern des Herstellers und dem gewünschten Tool. Im Fall des Timeline JS / TimeMapper können diese `timeline knight lab online` (Hersteller) oder alternativ `timeline js online` (Toolname) lauten.
4. Dubiose Herstellerseiten oder _fake services_ werden bei der gezielten Suche meist direkt durch Nutzerbeiträge gebrandmarkt.
3. Lest nun die Beschreibungen der einzelnen Suchergebnisse auf der ersten Seite durch. Es fallen keine negativen Rezensionen auf und das Tool wird in mehreren Beiträgen erwähnt.
4. Beim Querlesen ist uns folgende Beschreibung aufgefallen: "Timeline | H5P:
This is Timeline.js developed by Knight Lab, packaged as an H5P content type in order to make timelines easily editable, shareable and reuseable." H5P ist eine bekannte Software aus Norwegen und bietet eine Zeitstrahl-Funktion an.
5. Nun soll die Vertrauenswürdigkeit weiter geprüft werden. Geht auf die Herstellerseite von H5P (Link in der Suchmaschine oder [h5p.org](https://h5p.org)).
6. Sucht auf der Seite nach einem **Impressum** (Imprint) oder einem **Über uns** (About Us). Kommerzielle Angebote haben eine Impressumspflicht, sodass die betreibende Firma oder Person genannt werden muss. Im Falle von H5P steht im Menü ganz am Ende der Startseite unter **About** ein Link zu [About the Project](https://h5p.org/about-the-project).
7. In der Projektbeschreibung kann die Firma Joubel als betreibende Firma entnommen werden. Die Firmenadresse wurde in der Beschreibung direkt verlinkt. Für weitere Recherchen kann die Firmenwebsite zusätzlich aufgerufen werden. Auf dieser Seite befinden sich Profile zu sozialen Medien, Telefonnummern zum direkten Kontakt, eine Firmenanschrift uvm., die gegebenenfalls noch überprüft werden können.

### H5P Timeline {#h5p-timeline}

#### Ausprobieren {#ausprobieren}

In diesem Schritt soll die [H5P Timeline](https://h5p.org/timeline) für den gewünschten Einsatzzweck ausprobiert werden.

1. Springt über den gefundenen Link der Suchmaschine oder der URL [https://h5p.org/timeline](https://h5p.org/timeline) zur H5P Timeline.
2. Nach der kurzen Beschreibung ist ein beispielhafter Zeitstrahl eingebunden. An diesem könnt ihr nun die Funktionalität überprüfen.
4. An der rechten oberen Ecke der eingebetteten H5P Timeline befindet sich ein  `Vollbildschirm`-Button <i class="fas fa-expand"></i>. Vergrößert nun die Ansicht.
5. Mit den Pfeilen auf der linken und der rechten Seite könnt ihr zwischen den einzelnen Zeitabschnitten vor- und zurückgehen. Dabei ändert sich sowohl die Beschreibung als auch die Zeiteinteilung am unteren Ende des Zeitstrahls. Die Epochen in der Zeitleiste können direkt angeklickt werden; so ist ein nicht-lineares Betrachten der einzelnen Einträge möglich. Auf der linken unteren Seite im Zeitstrahl ermöglichen das Reinzoomen <i class="fas fa-search-plus"></i> und das Rauszoomen <i class="fas fa-search-minus"></i> eine Anpassung der Ansicht.
6. Nach dem ausgiebigem Testen kann der Vollbildmodus mit der ESC-Taste verlassen werden.

#### Einrichten {#einrichten}

In diesem Abschnitt wird mit dem kostenlosen Tool H5P ein Zeitstrahl erstellt.

1. [H5P.org](https://h5p.org/) bietet nach der Registrierung die Möglichkeit, den Zeitstrahl und viele andere [interaktive Elemente](https://h5p.org/content-types-and-applications) direkt anzulegen und mit der Welt zu teilen.
2. Klickt im Hauptmenü auf den Button `Create free account`.
3. Füllt das Formular mit einer gültigen E-Mail-Adresse und einem [sicheren Passwort](http://www.sicherespasswort.com/) aus.
4. Loggt euch nun über den Button `Log in` ein.
5. Nach dem Login erscheint die Profilseite. Dort steht unter der Überschrift **quick links** der Button `Create new content`. Mit einem Klick gelangt ihr auf das Erstellungsformular.
6. Tragt einen passenden Titel in das erste Formularfeld ein.
7. Im zweiten Feld `Select content type` werden euch alle Funktionen von H5P zur Verfügung gestellt. Sucht nun nach dem Zeitstrahl und wählt diesen aus.
8. Mit einem Klick auf `Save` werden die Änderungen übernommen.

#### Erstellen {#erstellen}

Nach dem Einrichten geht es direkt weiter zum Beschreiben der einzelnen Zeitabschnitte. Wie das geht, erfahrt ihr in den nächsten Schritten.

1. Im Feld `Headline` tragt ihr den gewünschten Titel ein. Dieser erscheint später auf der Startansicht. In unserem Beispiel lautet er "Jungsteinzeit bis Neuzeit".
2. Im zweiten Feld `Body text` könnt ihr eine Unterüberschrift eintragen, die genau unter der Hauptüberschrift erscheinen wird.
3. Geht nun weiter zum Reiter **Dates**; die übersprungenen Einträge dienen lediglich zur Verschönerung, womit wir uns zuletzt beschäftigen werden.
4. Ein Zeitabschnitt beginnt mit einem Startdatum, das über das Feld `Body start date` im Format `YYYY,MM,DD` (Jahr, Monat, Tag) eingegeben werden muss. Es wird mindestens eine Jahresangabe im vierstelligen Format (z.B. 1409) benötigt. Bei Zeitangaben vor unserer Zeitrechnung (BC) muss ein Minuszeichen vor die Jahreszahl gestellt werden (z.B. -7000).
5. Damit die Zeitspanne komplett ist, wird zum Startzeitpunkt auch ein Endzeitpunkt verlangt. Dieser kann über das Feld `End date` bestimmt werden.
6. Im Feld `Body text` kann ein optionaler Beschreibungstext eingegeben werden. Beim Beispiel "Jungsteinzeit" könnten hier eine Aufgabe oder die wichtigsten epochalen Ereignisse stehen. Das Beschreibungsfeld hat keine Zeichenbeschränkung.
7. Klickt auf den Button `Add item`, um weitere Zeitstrahleinheiten hinzuzufügen.
8. Wiederholt nun die oben genannten Schritte, bis euer Zeitstrahl vollständig ist.

#### Mit freien Inhalten verschönern und ergänzen {#mit-freien-inhalten-verschoenern-und-ergaenzen}


Der Zeitstrahl ist prinzipiell fertig und könnte den Lernenden zur Verfügung gestellt werden. Ab jetzt geht es darum, den Zeitstrahl ansprechender zu gestalten und freie Inhalte über Wikipedia direkt in der H5P zu verankern. Daraus ergeben sich tolle Möglichkeiten für Lehr-Lern-Situationen. Wie das geht und aussehen kann, seht ihr in den nächsten Schritten.

1. Zunächst wollen wir ein Hintergrundbild für die recht langweilig wirkende Timeline suchen und einbinden.
2. Auf der freien Bildsuchmaschine [pixabay.com](https://pixabay.com/) können sogenannte gemeinfreie Werke kostenlos gesucht, heruntergeladen, für alle Verwendungszwecke (gewerblich wie privat) genutzt und sogar verändert werden. Sucht nun ein passendes Bild. Wir haben den Tag `weltall` eingegeben.
3. Mit einem Klick auf das gewünschte Bild gelangt ihr auf die Detailseite. Dort erhaltet ihr weitere Informationen zum Bild und seinem Ursprung. Klickt nun auf den Button `Kostenlose Downloads` und wählt die gewünschte Größe der Bilddatei aus. Falls euch die Werte nichts sagen, bleibt bei der Standardeinstellung. Klickt zu guter Letzt auf `Download` <i class="fas fa-download"></i>.
4. Mitglieder erhalten im Anschluss direkt das Bild; als Gast erscheint ein Pop-Up mit der Überprüfung eines sogenannten Captcha. Diese Sicherheitsabfrage dient dazu, auszuschließen, dass ihr kein Mensch seid. Klingt komisch? Ist aber sinnvoll, da im Internet nicht nur Menschen, sondern auch viele Programme Seiten besuchen. Die Böswilligen unter ihnen legen oftmals ganze Webseiten oder sogar Server lahm. Löst also das einfache Rätsel, macht den gezielten Klick auf ein Feld oder beantwortet die mathematische Aufgabe und erhaltet im Anschluss das Bild.
5. Wechselt wieder zurück zur **H5P-Timeline**.
6. Sucht am Anfang des Zeitstrahls nach dem Feld `Background image` und klickt auf den Button `Add` <i class="fas fa-plus"></i>. Sucht nun auf der Festplatte des Computers nach dem heruntergeladenen Bild von [pixabay.com](http://pixabay.com/).
7. Nach dem Hochladen ist das Bild für alle Epochen hinterlegt.
8. Wechselt nun zu einer gewünschten Zeiteinheit, sucht nach dem Feld `Assets` (Anhänge) und klickt auf die Überschrift <i class="fas fa-caret-square-right"></i>. Es erscheinen weitere Felder für zusätzliche Inhalte. Folgende Webservices werden unterstützt: [Twitter](https://twitter.com/), [YouTube](https://www.youtube.com/), [Flickr](https://www.flickr.com/), [Vimeo](https://vimeo.com/), [Wikipedia](https://www.wikipedia.org/), [Google Maps](https://www.google.de/maps) und [SoundCloud](https://soundcloud.com/).
9. In diesem Beispiel wurde der passende [Wikipedia-Artikel zur Jungsteinzeit](https://de.wikipedia.org/wiki/Jungsteinzeit) der Zeitspanne hinzugefügt.
10. Mit einem Klick auf `Save` werden die Änderungen übernommen.

### Zeitstrahl auf ein Arbeitsblatt übernehmen {#zeitstrahl-auf-ein-arbeitsblatt-uebernehmen}


Der Zeitstrahl steht nun online auf [h5p.org](https://h5p.org/) für die Öffentlichkeit bereit. Dies muss aber nicht zwingend bedeuten, dass Lernangebote immer online sein müssen. In diesem Beispiel übertragen wir einen Verweis (Link) auf ein analoges Arbeitsblatt, damit man zu Hause oder mobil darauf zugreifen kann.

#### QR-Code {#qr-code}

Es gibt mehrere Möglichkeiten, lange Links oder ganze Texte auf ein Arbeitsblatt zu überträgen. Eine davon ist der QR-Code. Wie ein QR-Code online entsteht, folgt in den folgenden Schritten.

1. Es gibt sehr viele kleine Helferchen im Internet, die mit dem Schlagwort `Generator` gefunden werden können. Öffnet nun eine Suchmaschine (z.B. [DuckDuckGo](https://duckduckgo.com/)) und verwendet die Suchbegriffe `qr code generator kostenlos`. So können auch andere Online-Generatoren gefunden werden: `PDF, Barcode, sicheres Passwort` uvm.
2. In diesem Beispiel wird der Sucheintrag von [qrcode-generator.de](https://www.qrcode-generator.de/) gewählt. Navigiert nun auf diese Seite und ruft anschließend in einem neuen Tab im Browser die **H5P Timeline** auf.
3. Kopiert aus der Adresszeile des Browsers die URL; in diesem Beispiel lautet sie `https://h5p.org/node/169659`.
4. Wechselt wieder in den Tab des QR-Code-Generators und tragt in das Feld `Website (URL)` die kopierte URL der Timeline ein.
5. Klickt auf den Button `QR-Code erstellen` <i class="fas fa-sync"></i>.
6. Nach kurzer Wartezeit kann der QR-Code auf der rechten Seite über den Button `Herunterladen` <i class="fas fa-arrow-alt-circle-down"></i> auf dem Gerät gespeichert werden. Als Gast ohne Nutzerkonto startet der Download nach ein paar Sekunden automatisch.

#### Kurzlink {#kurzlink}

Eine weitere Möglichkeit, die kryptische URL der Timeline zu vereinfachen, ist ein sogenannter Kurzlink (shortlink). Dabei lassen sich beliebig lange URLs auf eine überschaubare Zeichenlänge reduzieren.

1. Der Online-Dienst [bit.ly](https://bitly.com/) erstellt Kurzlinks in Sekundenschnelle und ohne Anmeldung. Navigiert auf diese Seite und ruft anschließend in einem neuen Tab im Browser die **H5P Timeline** auf.
2. Kopiert aus der Adresszeile des Browsers die URL; in diesem Beispiel lautet sie `https://h5p.org/node/169659`.
4. Wechselt wieder in den Tab des Kurzlink-Generators und tragt in das Feld `Paste a link to shorten it` die kopierte URL der Timeline ein.
5. Klickt auf den Button `Shorten`.
6. Anschließend wird ein Kurzlink ausgegeben, der wie folgt aussehen kann: `http://bit.ly/2IqUd20`. Klickt rechts neben dem Kurzlink den Button `Copy`. Nun ist der Kurzlink im Zwischenspeicher abgelegt.

#### Übertragung auf das Arbeitsblatt {#uebertragung-auf-das-arbeitsblatt}

Der erstellte QR-Code und/oder der Kurzlink können jetzt auf dem Arbeitsblatt hinterlegt werden.

1. Öffnet ein bereits erstelltes Arbeitsblatt in einem Editor eurer Wahl. In diesem Beispiel wird das kostenlose Schreibprogramm [LibreOffice](https://de.libreoffice.org/) genutzt. Frei verwendbare Arbeitsblätter für den Unterricht gibt es z.B. im [ZUM-Wiki](https://wiki.zum.de/wiki/Hauptseite) oder nach kostenloser Anmeldung unter [tutory.de](https://tutory.de/).
2. Öffnet das Arbeitsblatt über das Hauptmenü unter `File > Open ...`.
3. Fügt an einer passenden Stelle den kopierten Kurzlink mit Copy & Paste ein.
4. Der QR-Code kann über das Hauptmenü unter `Insert > Image ...` ausgewählt und eingefügt werden.
5. Das Bild wird nach dem Einfügen in der Mitte über jeglichem Inhalt stehen. Wählt den QR-Code mit einem Linksklick an und öffnet anschließend mit einem Rechtklick das Kontextmenü. Wählt dort den Eintrag `Properties` aus und wechselt im Pop-Up Menü zum Reiter **Wrap**. Wählt dort die Option `Through` aus. Bestätigt mit dem Button `OK`.
6. Das Bild kann nun frei verschoben werden, ohne dass sich der dahinterliegende Text verändert.
7. Mit den Anfassern an den Ecken des Bildes kann auch die Größe noch angepasst werden.
8. Das Arbeitsblatt kann nun ausgedruckt und verteilt werden. Wer den Zeitstrahl ansehen möchte, kann mit einem QR-Code-Scanner oder dem Kurzlink auf das Internetangebot zugreifen.

## Lizenz

<a class="cc-license" rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"><a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/digitaler-zeitstrahl-im-oer-format/" property="cc:attributionName" rel="cc:attributionURL">Digitaler Zeitstrahl im OER-Format</a></span> von <em><a href="https://www.manfred-steger.de/" target="_blank">Manfred Steger</a></em> für das Universitätskolleg Digital der Universität Hamburg ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">CC BY 4.0 International Lizenz</a>.

<!-- 1. Tool Recherche (Suchbegriffe: Timeline, Zeitstrahl, Tool, online, HTML5, kostenlos, free) (Bezug Synergie Praxis: [*01 Das Ausgangsmaterial*](https://synlloer.gitbooks.io/synergie-praxis-open-educational-resources-2017/content/01-das-ausgangsmaterial.html) [*07 Das Format*](https://synlloer.gitbooks.io/synergie-praxis-open-educational-resources-2017/content/07-das-format.html))
    - Suchmaschinenen:
      - **[Duckduckgo](https://duckduckgo.com/)** (nicht personalisiert – Netzneutralität!)
      - [Google](https://www.google.de/) (Datenkrake – findet das, was wir bereits gefunden haben)
    - **[H5P](https://h5p.org/timeline)**(kostenlos/online Repository)
    - [timegraphics](https://time.graphics/de/editor) (ohne Registrierung / Live Editing)

2. Tool auf Vertrauenswürdigkeit und OER-Tauglichkeit überprüfen (Bezug Synergie Praxis: [*05 Offenes Material korrekt verwenden*](https://synlloer.gitbooks.io/synergie-praxis-open-educational-resources-2017/content/05-offenes-material-korrekt-verwenden.html))
    - Impressum einsehen (kein Impressum = nicht vertrauenswürdig)
    - Finanzierung / Pricing nachsehen (Nachhaltigkeit überprüfen und ob kostenlose Nutzung einen gewissen Grad der Kommunikation und Funktionstüchtigkeit besitzt oder lediglich ködert)

3. Den analogen Zeitstrahl in ein digital aufbereitetes Pendant überführen

4. Das digitale Pendant mit zusätzlichen interaktiven Inhalten anreichern – also über die gewohnte analoge Lehr-Lern-Logik hinaus einsetzen (z.B. Video embed) (Bezug Synergie Praxis: [**02 Material Aufbereitung**](https://synlloer.gitbooks.io/synergie-praxis-open-educational-resources-2017/content/02-material-didaktisch-aufbereiten.html))

5. Lizenzen für die verwendeten Inhalte angeben und eine eigene Lizenz für das Produkt bewusst wählen (Bezug Synergie Praxis: [**03 Lizenzierung**](https://synlloer.gitbooks.io/synergie-praxis-open-educational-resources-2017/content/02-material-didaktisch-aufbereiten.html))

6. Das Produkt in ein reales Lehr-Lern-Szenario überführen (z.B. Vorführung auf der Herstellerseite oder embed auf einen Blog oder Einbindung in eine Präsentation) -->
