<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Gemeinsam an Texten arbeiten

## Kollaborativ Texte erstellen und als open access und/oder OER online veröffentlichen.

> geschätzer Zeitaufwand: 60 Minuten

> Schwierigkeitsgrad: 2/5 Punkte

Tags: Gemeinsam Text schreiben, Text gemeinsam bearbeiten, HackMD.io, Etherpad, flickr, GitBook, authorea.com, Kollaboratives Arbeiten, WordPress Blog, Blogwerkstatt, Arbeiten mit Blogs

### User Story ([Persona Ulrike Lange](https://git.universitaetskolleg.uni-hamburg.de/synlloer/oer-know-how/blob/master/Unterlagen/Personas-03.pdf))

-->

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/eTo-EnBZ7rg" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Inhaltsverzeichnis

- [HackMD öffnen](#hackmd-oeffnen)
- [Schreiben in Markdown](#schreiben-in-markdown)
 - [Fett schreiben](#fett-schreiben)
 - [Überschriften](#ueberschriften)
 - [Markdown-Befehle](#markdown-befehle)
- [TeilnehmerInnen einladen](#teilnehmerinnen-einladen)
- [Dokument speichern](#dokument-speichern)
- [Markdown in ein PDF konvertieren](#markdown-in-ein-pdf-konvertieren)

![Die Illustration stellt die Persona Ulrike Lange dar. Die Illustration hat im Hintergrund einen hellgrauen Kreis, vor dem der Rest der Darstellung teilweise die Kreisränder überlappt und damit den verfügbaren Raum stärker ausfüllt. Die Persona zeigt den Oberkörper einer hellhäutigen Frau, die einen braunen Cardigan über einem weißen Shirt trägt und an einer hellbraunen Tischplatze dem Betrachter / der Betrachterin zugewandt sitzt. Auf dem Tisch steht ein aufgeklapptes Notebook mit taubenblauer Tastatur und hellblaugrauer Rückseite und Rahmen, welches einen dunkelbraunen Schatten auf den Tisch wirft. An der Tischkante liegt ein weißes Blatt Papier, welches die Frau mit der linken Hand festhält und mit der rechten Hand mit einem rotem Stift darauf schreibt. Die Frau hat lange dunkelblonde Haare, der Mund ist geschlossen und genau wie die Nase nur angedeutet. Die Frau trägt eine dunkelgraue Brille, hinter die Augen nicht erkennbar sind. Rechts neben dem Kopf der Frau und damit oberhalb des aufgeklappten Notebooks ist ein abwärtszeigender dunkelgrauer Pfeil gezeichnet, der anstelle eines senkrechten Stricht hinter der Pfeilspitze aus vier einzelnen gebogenen Linien zusammengesetzt ist, von denen zwei sich übereinander im Bogen nach links neigen und zwei an einer imaginären Mittellinie gespiegelt Linien sich als Bogen nach rechts neigen.](https://gitlab.com/flavoursofopenscience/openlab/raw/master/blog/personas/uhh-personas-ulrike-lange/uhh-personas-ulrike-lange.png)

Ulrike ist wissenschaftliche Mitarbeiterin am Lehrstuhl für Psychologie. Sie arbeitet momentan zusammen mit zwei weiteren Kollegen aus verschiedenen Ländern an einem Bericht für ihr aktuelles Projekt. In der Vergangenheit hat sie schlechte Erfahrungen mit Office-Dokumenten gemacht, da das Zusammenführen meist sehr zeitaufwendig war und oftmals redundante Inhalte entstanden sind. Nun sucht sie nach einer Möglichkeit, um zu dritt gleichzeitig an einem Dokument arbeiten zu können.

## Lösungsansatz

Ulrike ist nach einiger Recherche auf das Tool [HackMD.io](https://hackmd.io/OwNghmAMCmDMBGBaATARgCxke6xiIE4BWdVRVMADgBMAzA6kYAYxEqA=) gestoßen. Sie hat von Kolleginnen und Kollegen aus der Informatik die Empfehlung bekommen, ein Markdown-basiertes Programm zu nutzen, da die Zusammenführung von Texten unkomplizierter sei. Sie möchte am Ende der kollaborativen Arbeit, den Text als PDF speichern und entscheidet sich dafür den im [Dillinger](https://dillinger.io/) einzufügen und damit zu konvertieren.


## Lernziele

- Vor- und Nachteile von kollaborativem Arbeiten erkennen
  - Daten versehentlich überschreiben
  - keine Doppelung bei verschiedenen Versionen
  - ohne permanenten Internetzugang keine Bearbeitung möglich
- Freigabeeinstellungen für Texte verstehen und anwenden können
- Versionierung kennenlernen und anwenden können
- Medieninhalte formatgerecht einbinden können
  - statistische Berechnungen/Datenblätter qualitativ hochwertig einbinden
  - Schaubilder einbinden

## Schritt-für-Schritt-Anleitung

### HackMD öffnen {#hackmd-oeffnen}

Für die kollaborative Texterstellung müsst ihr zunächst die Webseite [https://hackmd.io/](https://hackmd.io/) öffnen. Auf der Startseite findet man oben rechts den  Button `Neue Gast Notiz` <i class="fas fa-plus"></i>. Sobald ihr diesen angeklickt habt, öffnet sich das Online-**HackMD**-Dokument und ihr könnt mit der Eingabe eures Textes beginnen.

### Schreiben in Markdown {#schreiben-in-markdown}

Zu Anfang wird automatisch ein geteilter Bildschirm angezeigt. Auf der linken Seite tippt ihr euren Text ein und auf der rechten Seite wird euch angezeigt, wie das Dokument dann final aussehen wird.
Möchtet ihr euren Text formatieren, müsst ihr spezielle **Markdown-Befehle** eingeben.

#### Fett schreiben {#fett-schreiben}

Das erste Werkzeug in der Reihe ist der Button `Bold` <i class="fas fa-bold"></i>, mit dem Wörter fett geschrieben werden können. Klickt ihr auf den Knopf, wird der Markdown-Befehl für 'fett schreiben' angezeigt: `**Beispieltext**`. Möchtet ihr euch sparen, jedes mal auf den Knopf zu drücken, könnt ihr stattdessen also den Markdown-Befehl eingeben, sprich, jeweils vor und nach dem Wort zwei Sternchen `**` einfügen. Dann wird das Wort fettgedruckt angezeigt.

#### Überschriften {#ueberschriften}

Möchtet ihr eine Überschrift hinzufügen, müsst ihr den Button `Heading` <i class="fas fa-header"></i> anklicken und könnt dann durch die Anzahl an `#` entscheiden, wie groß die Überschrift sein soll. Mit jedem weiteren `#` rutscht die Überschrift eine Ebene tiefer und wird dementsprechend kleiner.

#### Markdown-Befehle {#markdown-befehle}

Mit den weiteren Werkzeugen verhält es sich ähnlich: Entweder nutzt ihr die Knöpfe im Menü oder die direkten Markdown-Befehle. Eine Übersicht der wichtigsten Befehle könnt ihr euch per Klick auf den Button `Hilfe` <i class="fas fa-question-circle"></i>  anzeigen lassen.

> Tipp: Bei der Erstellung von Tabellen ist es sinnvoll, den Button `Insert Table` <i class="fas fa-table"></i> zu verwenden, da die händische Eingabe des zugehörigen Befehls ein wenig komplizierter ist.

### TeilnehmerInnen einladen {#teilnehmerinnen-einladen}

Um nun weitere Teilnehmerinnen und Teilnehmer einzuladen, müsst ihr den Link des HackMDs mit ihnen teilen. Sobald sie diesen aufrufen, könnt ihr gemeinsam einen Text verfassen und an ihm arbeiten. Oben rechts zeigt der Button `Online` <i class="fas fa-users"></i> an, wie viele Nutzende aktuell im Dokument aktiv sind.

> Tipp: Du kannst den langen Link über die Webseite [https://bitly.com/](https://bitly.com/) kürzen. Dafür muss man den zu teilenden Link mit Klick auf `Copy to Clipboard` kopieren und auf der Startseite von **Bitly** in die mittige Eingabeleiste einfügen. Hier wird der Link dann automatisch gekürzt und ihr könnt ihn per Klick auf `Copy` wieder herauskopieren.

### Dokument speichern {#dokument-speichern}

Ist der Text fertig bearbeitet, kann mit Klick auf den Button `Menü` ein Speicherort oder ein Downloadformat ausgewählt werden. Ihr solltet die Datei als Markdown-Datei herunterladen.

### Markdown in ein PDF konvertieren {#markdown-in-ein-pdf-konvertieren}

<!--Eine Markdown-Datei lässt sich nämlich einfach in ein PDF konvertieren. Um einen passenden Konvertierer zu finden, könnt ihr mit der [nutzerfreundlichen Suchmaschine](https://donttrack.us/) [Duckduckgo.com](https://duckduckgo.com/) nach den Begriffen `Markdown to PDF` suchen oder mit dem Direktlink [http://www.markdowntopdf.com/](http://www.markdowntopdf.com/) direkt den Konvertierer **Markdown to PDF** öffnen. Hier klickt ihr auf den Button `Select File` und wählt eure zuvor gespeicherte Markdown-Datei aus. Die Konvertierung passiert dann automatisch und der Download muss nur noch bestätigt werden.

> Tipp: Passt auf, dass der Titel der Markdown-Datei keine Sonderzeichen oder Punkte enthält, denn sonst kann es nicht konvertiert werden.-->

Eine Markdown-Datei lässt sich nämlich einfach in ein PDF konvertieren. Um einen passenden Konvertierer zu finden, könnt ihr mit der [nutzerfreundlichen Suchmaschine](https://donttrack.us/) [Duckduckgo.com](https://duckduckgo.com/) nach den Begriffen `Markdown converter` suchen oder mit dem Direktlink [https:///dillinger.io](https://dillinger.io/) direkt den Konvertierer **Dillinger.io** öffnen. Dort müsst ihr auf der linken Seite, alle Inhalte über die Webseite herauslöschen und eure Markdowninhalte dort einfügen. Diese könnt ihr dann mit Klick auf den `Export As` als PDF exportieren.

>Tipp: Mit dem Programm [markdowntopdf.com](http://www.markdowntopdf.com/) geht dies auch, jedoch werden Tabellen nicht richtig angezeigt und die Markdowndatei kann nur expotiert werden, wenn der Titel keine Sonderzeichen oder Punkte enthält.  Deswegen eignet sich dieses Programm eher für weniger komplexe Zusammenschriften.
>

##### *Möchtest du deinen Text in eine "Buchform" bingen findest du [hier](https://openlab.blogs.uni-hamburg.de/oer-produzieren-und-veroeffentlichen-mit-gitbook-com/) praktische Tipps dazu.*

## Lizenz

<a class="cc-license" rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"><a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/der-gemeinschaftliche-text-leicht-gemacht/" property="cc:attributionName" rel="cc:attributionURL">Gemeinsam an Texten arbeiten</a></span> von <em>Sophia Zicari</em> für das Universitätskolleg Digital der Universität Hamburg ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">CC BY 4.0 International Lizenz</a>.

<!--### Gliederung modularisierte Schritt-für-Schritt-Anleitung

1. Text schreiben und Mitglieder einladen
  - [HackMD.io](https://hackmd.io/OwNghmAMCmDMBGBaATARgCxke6xiIE4BWdVRVMADgBMAzA6kYAYxEqA=)
  - [Etherpad](http://etherpad.org/)
  - [Google Docks](https://www.google.de/intl/de/docs/about/)

2. Text in Buchformat oder PDF speichern und veröffentlichen
  - [GitBook](https://www.gitbook.com/)
  - [Markdowntopdf](http://www.markdowntopdf.com/)
  - [Book Creator](https://bookcreator.com/)
  - [Authorea.com](https://www.authorea.com)

3. Text mit OER Grafiken und Bildern anreichern
  - [Clipart](https://openclipart.org/)
  - [Pexels](https://www.pexels.com/de/)
  - [koken](http://koken.me/)

Markdown Befehle
Eine gute Übersicht aller Markdown-Befehle findest du über die
[nutzerfreundlichen Suchmaschine](https://donttrack.us/) [Duckduckgo.com](https://duckduckgo.com/). Gib als Suchbegriffe `Markdown cheatsheet`ein und dir wird eine Tabelle mit den wichtigsten Befehlen angezeigt werden.

  -->
