<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Vom Tweet zum OER-Arbeitsblatt in 30 Minuten

## Mit kostenlosen online Tools aus einem Sketchnote Foto ein digitales Arbeitsblatt erstellen.

> geschätzer Zeitaufwand: 30 - 60 Minuten

> Schwierigkeitsgrad: 5/5 Punkte

Tags: Twitter, Bildbearbeitung, Vector editor, Polarr, Photo Editor, Tutory, Arbeitsblatt erstellen, Worksheet creator, Vectorization.org


## User Story Annie Li

-->

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/fjQ4v6UsyYY" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Inhaltsverzeichnis

- [Die Lizenz](#die-lizenz)
- [Vorlage und Formate](#vorlage-und-formate)
- [Bildbearbeitung](#bildbearbeitung)
	- [Polarr: Vorbereitung](#polarr-vorbereitung)
	- [Bildoptimierung](#bildoptimierung)
	- [Speichern inklusive Metadaten](#speichern-inklusive-metadaten)
- [Aus Pixel wird Vektor](#aus-pixel-wird-vektor)
	- [Platzieren und Bildausschnitt wählen](#platzieren-und-bildausschnitt-waehlen)
	- [Grafikelemente anpassen und speichern](#grafikelemente-anpassen-und-speichern)
- [Arbeitsblatt erstellen](#arbeitsblatt-erstellen)
	- [Arbeitsblatt anlegen und Überschriften hinzufügen](#arbeitsblatt-anlegen-und-ueberschriften-hinzufuegen)
	- [Sketchnote-Grafiken hinzufügen](#sketchnote-grafiken-hinzufuegen)
	- [Creative Commons richtig ausweisen](#creative-commons-richtig-ausweisen)
	- [Beschreibbar machen und veröffentlichen](#beschreibbar-machen-und-veroeffentlichen)
	- [PDF für den Druck erstellen](#pdf-fuer-den-druck-erstellen)

![Die Illustration stellt die Persona Annie Li dar. Die Illustration hat im Hintergrund einen hellgrauen Kreis, vor dem der Rest der Darstellung teilweise die Kreisränder überlappt und damit den verfügbaren Raum stärker ausfüllt. Die Persona zeigt den Oberkörper einer hellhäutigen Frau, die einen hellgrünen Blazer über einem weißen Shirt trägt und vor sich ein Blatt Papier hält, während sie vor einem aufgeklappten Notebook an einem angedeuteten Tisch sitzt. Die Tastatur des Notebooks und das Papier sind taubenblau, der Rest des Notebooks hellblaugrau und die Tischoberfläche mittelgrau. Die Frau hat schwarze schulterlange Haare, der Mund ist geschlossen und genau wie Nase und Augen nur angedeutet. Die Frau schaut auf das Papier in ihren Händen.](https://gitlab.com/flavoursofopenscience/openlab/raw/master/blog/personas/uhh-personas-annie-li/uhh-personas-annie-li.png)

Annie ist gymnasiale Deutsch- und Philosophielehrerin. Sie soll eine Vertretung im Fach Deutsch in der 9. Klasse übernehmen. Zufällig stößt sie beim Scrollen durch ihren Twitter-Feed auf eine passende Sketchnote zur anstehenden Schulstunde mit dem Thema „Was ist ein gutes Referat?“. Das Foto ist mit der Creative-Commons-Lizenz „CC BY-SA“ ausgestattet. Julia kennt sich ein wenig mit Lizenzen aus und weiß, dass sie es im Unterricht verwenden darf. Nun möchte sie damit ein Arbeitsblatt erstellen.

## Lösungsansatz

In sozialen Netzwerken wie z. B. Twitter oder in Fachgruppen auf Facebook verbreiten sich kostenfrei lizenzierte Inhalte rasch. Julia hat die gewünschte Sketchnote heruntergeladen und benötigt für die Umsetzung in ein Arbeitsblatt nun ein [Bildbearbeitungswerkzeug](https://polarr.co), einen [Vektorisierungsdienst](http://www.vectorization.org/), ein [Illustrationsprogramm](https://gravit.io/) und ein Layoutprogramm für das [Arbeitsblatt](https://tutory.de/). Klingt sehr zeitaufwendig? In 30-60 Minuten ist das Arbeitsblatt einsatzbereit.

## Lernziele

- Lizenzen erkennen und rechtliche Möglichkeiten ableiten
- Bildformate kennenlernen und speichern
- Datenformate einordnen und richtig anwenden
- Datenformate exportieren und importieren
- Grundlagen der Bildbearbeitung und Bildverbesserung kennenlernen und anwenden
- Unterschiede von pixel- und vektorbasierten Bildformaten kennenlernen
- nicht-destruktive Veränderungen an Vektoren exemplarisch durchführen
- Export von Teilvektordateien
- Medienformate in einem digitalen Arbeitsblatt zusammenführen (OER remix)

## Schritt-für-Schritt-Anleitung

### Die Lizenz {#die-lizenz}

Die Voraussetzung für freie Bildungsmaterialien sind die passenden Lizenzen der genutzen Medieninhalte. Im folgenden Beispiel ist die Sketchnote im Bild mit einer [CC BY-SA](https://creativecommons.org/licenses/by-sa/4.0/) Lizenz ausgezeichnet und darf mit dem richtigen Lizenzhinweis für alle Einsatzzwecke genutzt werden.

![Sketchnote von der Twitterteilnehmerin Manuela SWA unter dem Nutzernamen @m4nUE_212 mit dem Hashtag #BayernEdu zum Thema: Wie gelingt ein gutes Referat?](https://gitlab.com/flavoursofopenscience/openlab/raw/master/blog/toolchains/sketchnote-remix/uebung-macht-den-meister-sketchnote-cc-by-sa-swa.jpg)
[SWA](https://twitter.com/search?q=%40fraudromedar%20%23bayernedu&src=typd) "Übung macht den Meister!" [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

> **Tipp:** Suchmaschinen wie [Google](https://www.google.de/search?biw=1440&bih=804&tbs=sur%3Afmc&tbm=isch&sa=1&ei=3JKnWqGTB8mSkwXlzZqwBg&q=sketchnote&oq=sketchnote&gs_l=psy-ab.3..0j0i67k1l2j0l7.3909.4959.0.5628.2.2.0.0.0.0.90.156.2.2.0....0...1c.1.64.psy-ab..0.2.156....0.eieuSI49-BM) und Bilderportale wie z.B. [Flickr](https://www.flickr.com/search/?license=2%2C3%2C4%2C5%2C6%2C9&text=sketchnote&advanced=1) können speziell nach Creative-Commons-Material filtern.

### Vorlage und Formate {#vorlage-und-formate}

Für den anstehenen Praxisteil wird ein Ausgangsbild benötigt. Ihr könnt die oben gezeigte Sketchnote nach einem Rechtsklick speichern oder selbstverständlich auch ein eigenes Bild nehmen.

> Tipp: Im pädagogischen Umfeld ist der Flickr-Nutzer [**Ralf Appelt** (adesigna)](https://www.flickr.com/photos/adesigna) mit zahlreichen Sketchnotes sehr zu empfehlen.

In den nächsten Schritten entsteht aus einem sogenannten Pixelbild eine Vektorgrafik. _Warum ist das wichtig?_ Vektorgrafiken können nachträglich verlustfrei vergrößert und verkleinert werden, sprich, wir können die erstellten Grafiken auf einer Briefmarke, auf dem Arbeitsblatt oder auf einem großen Poster mit gleichbleibender Qualität abbilden. Ein weiterer Vorteil ist, dass auch die Farben der einzelnen Elemente nachträglich angepasst werden können.

### Bildbearbeitung {#bildbearbeitung}

In diesem Schritt erfahrt ihr die Grundlagen der Bildbearbeitung mit dem Online-Tool [**polarr.co**](https://polarr.co).

#### Polarr: Vorbereitung {#polarr-vorbereitung}

1. Öffnet den Online-Editor von polarr.co unter folgender Adresse: [photoeditor.polarr.co](https://photoeditor.polarr.co/).
2. Das Programm wird in zwei Darstellungsvarianten für Anfänger und Profis angeboten. Wählt hier **Express** (deutsch: ausdrücken) aus und klickt auf den Button `Erste Schritte mit Express`. Wer bereits Erfahrungen mit Bildbearbeitung hat, kann auch die Profi-Variante für den Vollzugriff auf alle Werkzeuge und Bildeinstellungen auswählen.
3. Beim ersten Besuch wird eine kurze Tour gestartet (1 Minute). Absolviert diese, um eine Funktionsübersicht zu erhalten.
4. Klickt anschließend auf den Button `Öffnen von Fotos` und fügt die Sketchnote ein.

#### Bildoptimierung {#bildoptimierung}

1. Mit dem Zuschneiden-Werkzeug <i class="fas fa-crop"></i> im Menü auf der rechten Seite kann das Bild bei Bedarf gedreht und zugeschnitten werden. Mit einem Klick auf die Ecken kann die Größe und rechts neben der Zuschnittsfläche am Gradmesser die Neigung des Bilds angepasst werden.
2. Ist das Bild angepasst, könnt ihr die Änderungen mit einem Klick auf den Haken <i class="fas fa-check"></i> im Menü auf der rechten Seite bestätigen.
3. Auch die Bildqualität (Helligkeit und Farbe) lässt sich optimieren: Mit einem Klick auf das Helligkeitsymbol <i class="fas fa-sun"></i> im rechten Menü öffnet sich die "Licht"-Einstellungspalette.
4. Die Einstellungen variieren je nach Motiv und Aufnahmeeinstellungen der Kamera. Bei der stark unterbelichteten (zu dunkel) Aufnahme der Sketchnote wurden folgende Einstellungen vorgenommen:
   * **Dehaze**: 0
   * **Belichtung**: 88
   * **Helligkeit**: 20
   * **Kontrast**: 36
   * **Highlights**: 100
   * **Schatten**: -100
   * **Weiß**: 67
   * **Schwarz**: -100

#### Speichern inklusive Metadaten {#speichern-inklusive-metadaten}

In diesem Abschnitt wird das optimierte Bild mit Metadaten versehen und für das nächste Tool unserer Toolchain bereitgestellt.

1. Zunächst muss der Export-Dialog geöffnet werden. Dies geschieht mit einem Klick auf das Export-Icon <i class="fas fa-upload"></i> in der unteren linken Ecke des Browsers.
2. Im Dialog-Fenster befinden sich mehrere Tabs. Im Reiter **Metadaten** könnt ihr dem Bild eigenständige Informationen über den Button `Hinzufügen von Metadaten` hinzufügen. Unter **Artist** tragt ihr die Urheberin ein (Manuela SWA), unter **Copyright** den Lizenzhinweis (CC BY-SA) und unter Beschreibung den Alternativtext (Sketchnote: Wie gelingt ein gutes Referat?).
3. Wechselt nun zum Reiter **Bild** und wählt unter der Rubrik **Format** das Bildformat **PNG** aus.
4. Abschließend könnt ihr auf den Button `Kopie speichern` <i class="fas fa-images"></i> klicken und das Bild nach der Fertigstellung herunterladen.

### Aus Pixel wird Vektor {#aus-pixel-wird-vektor}

Dieser Schritt zeigt euch, wie aus einem Pixelbild eine veränderbare Vektordatei entsteht. Mit dem Online-Illustrationsprogramm [Gravit.io](https://designer.gravit.io/) können Layoutdateien kostenlos und ohne Anmeldung erstellt werden.

1. Jedes Layoutprogramm benötigt eine sogenannte Zeichenoberfläche, ähnlich dem Papiermaß beim Zeichnen. Im ersten Schritt wird eine DIN-A4-große Zeichenfläche in den Feldern **width** (Breite) und **height** (Höhe) angelegt.
2. Tragt dazu die Maße vom DIN A4 quer in die dafür vorgesehenen Felder ein: `Breite: 297 mm` und `Höhe: 210 mm`. Standardmäßig ist die Maßeinheit **px** (Pixel) gewählt; ändert diese mit einem Klick auf **mm** (Millimeter) ab. Abschließend auf den Button `Create!` klicken. Es öffnet sich eine leere Seite, die mit Leben erfüllt werden kann.
3.  Bevor wir mit dem diesem Tool beginnen können, muss aus dem optimierten Pixelbild eine Vektorgrafik werden. Öffnet hierzu einen Browser eurer Wahl und navigiert zum Webservice [Vectorization.org](http://www.vectorization.org/).
4. Öffnet das zuvor gespeicherte Bild über den Button `Durchsuchen`.
5. Als **output format** solltet ihr entweder **SVG** oder **EPS** auswählen und mit dem Button `Start` den Konvertierungsvorgang in Gang setzen.
6. Nach kurzer Wartezeit könnt ihr mit einem Klick auf den blau hinterlegten Link die Vektorgrafik auf dem Computer speichern.

#### Platzieren und Bildausschnitt wählen {#platzieren-und-bildausschnitt-waehlen}

Jetzt wird die umgewandelte Sketchnote in einzelne Elemente aufgeteilt und in der Farbgebung angepasst.

1. Wechselt wieder in den Tab zu **Gravit** und öffnet über das Hauptmenü des Programms (am oberen Bildschirmrand) `File > Import > Place image ...` die gespeicherte SVG- bzw. EPS-Datei.
2. Nach erfolgreichem Import der Gesamtdatei kann diese über die Ebenenpalette (layer) auf der linken Seite in einzelne Objekte aufgeteilt werden. Aktiviert die Ebene mit einem Klick. Mit einem Rechtsklick öffnet sich das Kontextmenü - dort kann die Gruppierung mit dem Button `Split selection` aufgehoben werden.
3. Mit der Werkzeugleiste am oberen Bildschirmrand könnt ihr den Betrachtungsausschnitt des Programms verändern. Für die folgende Detailarbeit ist eine nahe Ansicht von Vorteil. Ändert über die Prozentanzeige die Zoomstufe; meist ist ein Wert von 200% angemessen.
4. Nach dem Zoom kann der Bildausschnitt mit dem Hand-Werkzeug <i class="fas fa-hand-paper-o"></i> verschoben werden. Klickt dazu ebenfalls im Werkzeugmenü den Hand-Icon an. Mit gedrückter Maustaste könnt ihr den Arbeitsbereich verschieben.

#### Grafikelemente anpassen und speichern {#grafikelemente-anpassen-und-speichern}

Mit dem Verschieben-Werkzeug <i class="fas fa-mouse-pointer"></i> können einzelne freistehende Grafiken ausgewählt und bereits jetzt in Position und Farbe verändert werden. Teile der Sketchnote werden in diesem Abschnitt verändert und für das Arbeitsblatt fertig gemacht.

1. Wird ein Element mit dem Verschieben-Werkzeug <i class="fas fa-mouse-pointer"></i> angeklickt, erscheint eine blaue Markierung um das Objekt. Bei gedrückter Maustaste könnt ihr es nun verschieben. Die Größe könnt ihr bei gedrückter `Shift-Taste` (Großschreibetaste) an den Ecken der Markierung anpassen.
2. Ist die Markierung aktiv, kann an der rechten Seite über das Paletten-Menü unter **Fills** die Farbe des Elements verändert werden. Klickt dazu auf den Farbwähler <i class="fas fa-eye-dropper"></i> und wählt die gewünschte Farbe aus.
3. Erstellt nun aus der Auswahl ein **Symbol**. Klickt mit der rechten Maustaste in der Ebenenpalette auf der linken Seite auf ein ausgewähltes Element (grau hinterlegt). Es öffnet sich das Kontextmenü. Sucht dort den Eintrag `Create Symbol`. Tragt einen Namen für das Symbol ein.
4. In diesem Schritt wird aus dem Symbol ein sogenanntes **slice**. Klickt hierfür in der unteren linken Ecke des Bildschirmrands in der Exportpalette auf den Button `Create Slice`. Die türkis hinterlegte Fläche um das Grafikobjekt zeigt den endgültigen Exportbereich; dieser kann wieder angepasst werden. Rahmt nun das gewünschte Objekt ein.
5. Anschließend auf den kleinen Export-Pfeil <i class="fas fa-caret-right"></i> klicken. Es öffnet sich ein Untermenü, in dem unter der Einstellung **Format** der Dateityp **SVG** anstelle der Grundeinstellung PNG gewählt werden muss. Danach auf den Button `Export` klicken. Speichert jetzt das einzelne Vektorbild in dem gewünschten Ordner auf dem Computer ab.

> Diese fünf Schritte können für die gewünschten Grafiken wiederholt werden.

### Arbeitsblatt erstellen {#arbeitsblatt-erstellen}

Im letzten Teil dieser Toolchain werden die angepassten Grafiken in ein Arbeitsblatt eingefügt und mit Text sowie Beschriftungsfeldern ausgestattet.

#### Arbeitsblatt anlegen und Überschriften hinzufügen {#arbeitsblatt-anlegen-und-ueberschriften-hinzufuegen}

1. Öffnet in einem modernen Browser wie z.B. [Firefox](https://www.mozilla.org/de/firefox/new/) die Seite [tutory.de](https://www.tutory.de/). Auf Tutory wird ein Account benötigt, welcher in der Basisversion kostenlos ist. Dieser reicht für das Erstellen anschaulicher, übersichtlicher und interaktiver Arbeitsblätter vollkommen aus.
2. Nach der Registrierung erscheint auf der Arbeitsblattübersicht der Button `Neues privates Arbeitsblatt` <i class="fas fa-plus-circle"></i>  erstellen. Füllt die drei Pflichtfelder **Titel**, **Fachbereich** und **Klassenstufe** aus. Während der Erstellung ist das Arbeitsblatt nicht öffentlich.
3. Fügt nun über die linke Seitenleiste (sidebar) über den Button `Text > Überschrift > +`  <i class="fas fa-text-height"></i> eine Überschrift in das Arbeitsblatt ein. Klickt mit einem Doppelklick auf die Platzhalterüberschrift. Auf der rechten Seite öffnet sich ein weiteres Menü zum Beschriften. Tragt dort den gewünschten Titel und die gewünschte Darstellungsgröße ein. Die Größe kann im Reiter **Überschriftsebenen** festgelegt werden. Dabei gilt es zu beachten, dass die Größe `Titel` nur einmal pro Dokument vergeben werden und die Standardüberschrift in `Ebene 1` sein sollte.

#### Sketchnote-Grafiken hinzufügen {#sketchnote-grafiken-hinzufuegen}

1. Nun werden die einzelnen **slices** aus dem vorherigen Schritt nacheinander auf das Arbeitsblatt  übertragen. Klickt dazu auf der linken Seitenleiste auf den Button `Bild > Eigenes Bild > +` <i class="fas fa-image"></i>.
2. Auf dem Arbeitblatt wird ein Platzhalterbild mit Bildunterschrift eingefügt. In diesen Platzhalter muss nun ein Teil der gespeicherten Sketchnote eingefügt werden. Klickt dazu das eingefügte Bild an, damit es aktiv wird. Ähnlich wie bei **Gravit Designer** sind aktive Elemente mit einer blauen Linie und Anfassern an den Ecken gekennzeichnet. Klickt nun am oberen rechten Bildende den kleinen Button `Bearbeiten` <i class="fas fa-pencil-alt"></i> an.
3. Auf der rechten Seite öffnet sich, wie bei den Überschriften, ein Kontextmenü mit mehreren Optionen. Zunächst lädt man eine gespeicherte Grafik hoch. Dies geschieht über den Button `Bild hier hochladen oder hineinziehen` <i class="fas fa-upload"></i>.
4. Das Bild wird nun in den Platzhalter gelegt. Über den Regler im Bild selbst kann der Bildausschnitt angepasst werden. Zieht den Regler nach links, um die Grafik zu verkleinern, und nach rechts, um den Bildauschnitt zu vergrößern. Über die Eckpunkte des Bildes kann das Format nachträglich angepasst werden. Sollte das Bild angeschnitten sein, zieht über die Ecken den Bildauschnitt größer.
5. Bei Bedarf kann noch eine Bildunterschrift über das rechte Seitenmenü unter dem Feld **Bilduntertitel** angelegt werden.
6. Abschließend müsst ihr eine Lizenz für das verwendete Bild angeben. Ohne diesen Schritt kann das Arbeitsblatt nicht veröffentlicht werden. Ihr könnt das Arbeitsblatt an dieser Stelle **mit den Schritten 1-5 fertigstellen** und den nachfolgenden Schritt am Ende in einem Rutsch erledigen.

#### Creative Commons richtig ausweisen {#creative-commons-richtig-ausweisen}

In diesem Schritt werden die verwendeten Medieninhalte mit Lizenzhinweisen der Creative Commons rechtssicher angelegt.

1. Tutory hat bereits einen CC-Lizenzgenerator integriert und verhindert eine Veröffentlichung, bevor nicht alle Medieninhalte ausgezeichnet sind. Mit einem Klick auf  `Creative Commons` <i class="fas fa-creative-commons"></i> öffnet ihr das Lizenzfenster.
2. Im Reiter **Baustein wählen** seht ihr die Medieninhalte ohne Lizenz. Wählt nun ein Bild unter _2. Wer hat das Werk geschaffen (Urheber)?_ aus und wählt die Option `Ein Anderer`. Tragt nun unter _3. Lizenz generieren_ die **Lizenz der Quelle** (in diesem Beispiel BY-SA), die **Lizenzversion** (4.0) und den **Namen des Urhebers** (SWA) ein. Gebt unter dem Feld _Link zum Inhalt_ den Ursprungsort der Mediendatei ein; in diesem Beispiel wäre es die URL zum Tweet. Mit einem Klick auf den Button `Speichern und weiter` werden die Lizenzangaben hinterlegt.
3. Wurden alle Medieninhalte mit Lizenzen ausgestattet, kann das Dokument optional über den Button `Creative Commons` <i class="fas fa-creative-commons"></i> öffentlich gemacht werden.

#### Beschreibbar machen und veröffentlichen {#beschreibbar-machen-und-veroeffentlichen}

Im letzten Schritt werden Bereiche des Arbeitsblatts liniert, damit die Lehrenden und/oder Lernenden das Arbeitsblatt beschriften können. Ist dies geschafft, wird das Arbeitsblatt der Öffentlichkeit zur Verfügung gestellt.

1. Erstellt über den Button `Text > Linien und Kästen > +` <i class="fas fa-text-height"></i> einen linierten Rahmen. Die Länge und Breite kann über die Anfasser an den Rändern des Feldes angepasst werden. Mit einem Klick auf den Button `Bearbeiten` <i class="fas fa-pencil-alt"></i> öffnet sich das Kontextmenü, worüber das Papiermuster **liniert, kariert** und **Millimeter** bei Bedarf verändert werden kann.
2. Erstellt an den nötigen Stellen nun die beschreibbaren Textfelder.
3. Zu guter Letzt kann das Dokument über den Button `Privat gespeichert` <i class="fas fa-lock"></i>, am oberen Bildschirmrand in der Mitte, für Kolleginnen und Kollegen freigegeben werden. Es erscheint entweder ein Lizenzhinweis - dann sind nicht alle Elemente lizenziert - oder eine Zusammenfassung der Einstellungen. Überprüft diese und klickt zum Abschluss auf `Veröffentlichen`

#### PDF für den Druck erstellen {#pdf-fuer-den-druck-erstellen}

Benötigt ihr nun das Arbeitsblatt als Druckvorlage oder als Overheadfolie, kann über den Button `PDF > Arbeitsblatt` <i class="fas fa-file-pdf"></i> eine Kopie auf dem Computer gespeichert werden.

## Lizenz

<a class="cc-license" rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"><a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/vom-tweet-zum-oer-arbeitsblatt-in-30-minuten/" property="cc:attributionName" rel="cc:attributionURL">Vom Tweet zum OER-Arbeitsblatt in 30 Minuten</a></span> von <em><a href="https://www.manfred-steger.de/" target="_blank">Manfred Steger</a></em> für das Universitätskolleg Digital der Universität Hamburg ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">CC BY 4.0 International Lizenz</a>.

<!-- ## Gliederung modularisierte Schritt-für-Schritt-Anleitung

1. [Twitter Bild aus Feed](https://twitter.com/m4nUE_212/status/953661315205169154) speichern (Bezug Synergie Praxis: [*04 Offenes Material finden*](https://synlloer.gitbooks.io/synergie-praxis-open-educational-resources-2017/content/01-das-ausgangsmaterial.html))
    - Tweets auf Twitter durchsuchen
    - **Bereits gefundenen Tweet verwenden**
2. Bildaufbereitung / Retusche
    - [**polarr.co**](https://polarr.co) online
    - Gimp
    - Photoshop
    - Affinity Photo
3. Vektorisierung
    - [**vectorization.org**](http://www.vectorization.org/)
4. Vektormanipulation
    - [**Gravit.io**](https://gravit.io/)
5. Arbeitsblatt Erstellung
    - [**Tutory**](https://tutory.de/) -->
