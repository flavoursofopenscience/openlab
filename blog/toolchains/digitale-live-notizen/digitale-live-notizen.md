<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Präsentationen inklusiv(er) darstellen

## Schlechte Lichtverhältnisse? Problematische Raumsituation? Kein Beamer? Mit diesen Tricks die Präsentation live streamen.

> geschätzer Zeitaufwand: 30 - 45 Minuten

> Schwierigkeitsgrad: 3/5 Punkte

Tags: Xodo, LibreOffice, PDF Converter, File Converter, Dateien konvertieren, Convertio, PDF Expert App, Handschriftliche Notizen PDF, Handwritten notes on PDF, google live stream, Screencast, Bildschirmaufnahme

### User Story ([Persona Giovanni Rossi](https://git.universitaetskolleg.uni-hamburg.de/synlloer/backlog/blob/master/Material/Personas-03.pptx))

-->

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Jnl2khkGcNs" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Inhaltsverzeichnis

- [Kostenlose Registrierung auf Xodo](#kostenlose-registrierung-auf-xodo)
- [Präsentation als PDF vorbereiten](#praesentation-als-pdf-vorbereiten)
- [Hochladen der Präsentation](#hochladen-der-praesentation)
 - [Präsentation zugänglich machen](#praesentation-zugaenglich-machen)
 - [Präsentationslink kürzen](#praesentationslink-kuerzen)
 - [Präsentationslink teilen](#praesentationslink-teilen)
- [Erstellen handschriftlicher Notizen auf der Präsentation](#erstellen-handschriftlicher-notizen-auf-der-praesentation)
- [Download der beschrifteten Präsentation](#download-der-beschrifteten-praesentation)

![Die Illustration stellt die Persona Giovanni Rossi dar. Die Illustration hat im Hintergrund einen hellgrauen Kreis, vor dem der Rest der Darstellung teilweise die Kreisränder überlappt und damit den verfügbaren Raum stärker ausfüllt. Die Persona zeigt den Oberkörper eines hellhäutigen Mann, der in der rechten Hand ein aufgeklapptes Notebook hält und mit den Fingern der linken Hand darauf tippt. Der Mann scheint aufrecht zu stehen und blickt leicht schräg nach links aus dem Bild. Der Mann trägt ein mittelgraues langärmeliges Hemd mit weißem Kragen. Der Mann hat einen geschlossenen Mund, eine kaum erkennbare Nase und trägt eine schmale dunkelbraune Brille und kurzes braunes Haar sowie einen kleinen geteilten Schnurrbart und einen kleinen Spitzbart. Im grauen Kreishintergrund ist hinter dem Kopf des Mannes ein ungefülltes hellblaues Dreieck zu erkennen, neben dem in hellblauer Schrift mathematische Zeichen durch Klammern, ein x-Quadrat und einen Bruchstrich sowie undeutliche weitere Zeichen angedeutet sind.](https://gitlab.com/flavoursofopenscience/openlab/raw/master/blog/personas/uhh-personas-giovanni-rossi/uhh-personas-giovanni-rossi.png)

Giovanni ist Informatikprofessor und kommentiert seine Vorlesungen schriftlich mit
Hilfe von gedruckten Präsentationen auf Overheadfolien. Die Notizen und Kommentare, die erst während der Vorlesung entstehen, gehen nach der Vorlesung verloren. Auf Nachfrage einer Studierenden mit einer Sehbehinderung möchte er nun die Notizen live am Beamer in das Vorlesungsskript einarbeiten und damit allen Studierenden die Möglichkeit geben, die Notizen digital abrufen zu können.

## Lösungsansatz

Giovanni beauftragt sein Team damit, eine Softwarerecherche sowie eine passende Geräterecherche für diese Problemstellung anzustellen. Sie stellen ihm das kostenlose Programm [Xodo](https://www.xodo.com/) vor, bei dem er seine Notizen mit Hilfe eines Notebooks und eines dazu passenden digitalen Stiftes erstellen kann. Damit die Präsentation in Xodo geöffnet werden kann, konvertiert er die Datei mit [LibreOffice](https://de.libreoffice.org/download/libreoffice-still/) in ein PDF. Diese Notizen können seine Studierenden in Echtzeit auf ihren eigenen Geräten abrufen und brauchen um dies zu tun lediglich den Link zu der Präsentation.

## Lernziele

- lernen, analoge Methoden mit digitalen Tools zu kombinieren
- nachhaltiges/barrierefreies Arbeiten durch Digitalisierung von Lehrmaterialien
- Vorteile und Nachteile von Online-Notizen erfahren
  - keine Folienwechsel
  - Eingewöhnung an digitale Stifte

## Schritt-für-Schritt-Anleitung

### Kostenlose Registrierung auf Xodo {#kostenlose-registrierung-auf-xodo}

Begonnen wird mit dem Abrufen der Internetseite [www.xodo.com](https://www.xodo.com/) und dem einmaligen Klicken auf den Button `Launch now`. Um alle Funktionen in Gänze nutzen zu können, müsst ihr euch einmalig für die Webseite mit Klick auf den Button `Guest` <i class="fas fa-user"></i> oben rechts registrieren. Die Registrierung muss anschließend noch per Klick auf den via E-Mail versendeten Registrierungslink bestätigt werden.

### Präsentation als PDF vorbereiten {#praesentation-als-pdf-vorbereiten}

Um nun eine Präsentation hochladen zu können, müsst ihr sicherstellen, dass es sich um eine PDF-Datei handelt. Eine Konvertierung kann mit dem Open-Source-Programm [LibreOffice](https://de.libreoffice.org/) für verschiedene Präsentationsformate vorgenommen werden. Für den Download könnt ihr mit der [nutzerfreundlichen Suchmaschine](https://donttrack.us/) [Duckduckgo.com](https://duckduckgo.com/) nach den Begriffen `LibreOffice Download` suchen oder mit dem Direktlink [https://de.libreoffice.org/download/libreoffice-still/](https://de.libreoffice.org/download/libreoffice-still/) mit Klick auf die aktuellste Version den Download sofort beginnen. Nach erfolgreichem Download kann LibreOffice gestartet und darin die Präsentation mit Klick auf den Menüpunkt `Open File` geöffnet werden. Darauffolgend müsst ihr die Datei in das gewünschte Format exportieren. Die Exportfunktion von LibreOffice befindet sich im Hauptmenü unter dem Reiter `File > Export PDF`. Im Dialog können die Standardeinstellungen verwendet und mit dem Button `Export` bestätigt werden. Wählt nun den gewünschten Speicherort aus.

### Hochladen der Präsentation {#hochladen-der-praesentation}

Jetzt kann die Präsentation als PDF-Datei auf der Webseite des Online-Programms Xodo hochgeladen werden. Dies könnt ihr über die Startseite oder mit Klick auf den Reiter **Bookshelf** in der linken Menüleiste tun. Das exportierte PDF könnt ihr mit dem Button `Add File` <i class="fas fa-desktop"></i> im oberen Menü auf der rechten Seite hochladen.

>Tipp: Die zweite Variante hat den Vorteil, dass das Dokument direkt online geschaltet ist und alle externen Nutzenden darauf zugreifen können.

#### Präsentation zugänglich machen {#praesentation-zugaenglich-machen}

Damit andere Nutzende auf das Dokument zugreifen und die Bearbeitungen mitverfolgen können, müsst ihr das Dokument zunächst per Doppelklick im **Bookshelf** öffnen. Hier sind mehrere Symbole zur Bearbeitung des Dokuments eingeblendet. Die Präsentation könnt ihr mit einem Klick auf den Button `Add user` <i class="fas fa-user-plus"></i> teilen. Es erscheint ein Optionsmenü. Wählt unter **share via** den Eintrag **Link** aus.

> Tipp: Hier kann zwischen der Einladung via E-Mail und Link entschieden werden. In dieser Toolchain wird bewusst der Link gewählt, da dieser im Präsentationsraum den Lernenden schnell und einfach zur Verfügung gestellt werden kann.

#### Präsentationslink kürzen {#praesentationslink-kuerzen}

Den langen Link könnt ihr über die Webseite [https://bitly.com/](https://bitly.com/) kürzen und anschließend auf der in Xodo geöffneten Präsentation einfügen. Um den Link zu kürzen, müsst ihr den zu teilenden Link der Präsentation mit Klick auf `Copy to Clipboard` kopieren. Auf der Startseite von **Bitly** muss der Link in die mittige Eingabeleiste eingefügt werden. Hier wird der Link dann automatisch gekürzt und kann von euch per Klick auf `Copy` wieder kopiert werden.

#### Präsentationslink teilen {#praesentationslink-teilen}

Den verkürzten Link könnt ihr mit einem Klick auf den Button `Free Text` <i class="fas fa-text-height"></i> in die Xodo-Präsentation einfügen. Die Lernenden können den Link dann auf ihren jeweiligen Geräten abrufen und die weiteren Bearbeitungen in Echtzeit mitverfolgen.

### Erstellen handschriftlicher Notizen auf der Präsentation {#erstellen-handschriftlicher-notizen-auf-der-praesentation}

Nun kann per Klick auf den Button `Free Hand` <i class="fas fa-paint-brush"></i> mit den handschriftlichen Notizen begonnen werden. Um eine Notiz zu löschen, müsst ihr auf das geschriebene Wort klicken und `Delete` auswählen.

### Download der beschrifteten Präsentation {#download-der-beschrifteten-praesentation}

Die Änderungen speichern sich automatisch im Bookshelf von Xodo und können von dort jederzeit abgerufen werden. Möchtet ihr die bearbeitete Präsentation jedoch als PDF herunterladen, könnt ihr das mit einem Klick auf den `Download`-Button <i class="fas fa-download"></i> tun. Das kann nützlich sein, wenn ihr eure kommentierten Präsentationen anschließend auf einer anderen Plattform für weitere Nutzende bereitstellen möchtet.

## Lizenz

<a class="cc-license" rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"><a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/praesentationen-inklusiver-darstellen/" property="cc:attributionName" rel="cc:attributionURL">Präsentationen inklusiv(er) darstellen</a></span> von <em>Sophia Zicari</em> für das Universitätskolleg Digital der Universität Hamburg ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">CC BY 4.0 International Lizenz</a>.

<!--### Gliederung modularisierte Schritt-für-Schritt-Anleitung

1. Erstellte Präsentation in ein PDF konvertieren/ exportieren mit
  - [Libre Office](https://de.libreoffice.org/download/libreoffice-still/)
  - [To PDF](http://topdf.com/)
  - [Convertio](https://convertio.co/de/)
  - [Microsoft Power Point](https://products.office.com/de-de/powerpoint)

2. Präsentation als PDF online stellen
  - [Xodo](https://www.xodo.com/)

3. Präsentationslink teilen
  - [Xodo](https://www.xodo.com/)
  - E-Mail (Verteiler etc.)
  - Soziale Netzwerke

4. Präsentationslink kürzen
  - [Bitly URL Shortener](https://bitly.com/)
  - [Google URL Shortener](https://goo.gl/)
  - [TinyURL](https://tinyurl.com/)

5. Präsentation präsentieren und schriftlich kommentieren
  - [Xodo](https://www.xodo.com/)

6. Präsentation speichern und veröffentlichen (Bezug Synergie Praxis:  [_08 Die Veröffentlichung_](https://synlloer.gitbooks.io/synergie-praxis-open-educational-resources-2017/content/08-die-veroffentlichung.html))
  - [Xodo](https://www.xodo.com/)-->
