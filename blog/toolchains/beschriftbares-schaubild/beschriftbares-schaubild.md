<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Kein passendes Schaubild für den Unterricht?

## Mit dieser Kurzanleitung Schaubilder einfach zusammenstellen und auf der digitalen Tafel beschriften.

> geschätzer Zeitaufwand: 15 - 20 Minuten

Tags: Gravit Designer, Open Clipart, Pixabay, Pexels Bilder, CC0 Pictures, Photoshop Express Web App, Classroom Screen, Vector Design App, Vector Graphics Editor, ZUM Wiki

## User Story ([Persona Martin Cremer](https://git.universitaetskolleg.uni-hamburg.de/synlloer/backlog/blob/master/Material/Personas-03.pptx))

-->

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/dA4Ltuwq23Q" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Inhaltsverzeichnis

- [Bildersuche und Creative Commons](#bildersuche-und-creative-commons)
- [Schaubild collagieren](#schaubild-collagieren)
	- [Grundeinstellungen des Zeichenprogramms](#grundeinstellungen-des-zeichenprogramms)
	- [Cliparts laden und platzieren](#cliparts-laden-und-platzieren)
	- [Schaubild ergänzen](#schaubild-ergaenzen)
	- [Objekte vervielfältigen](#objekte-vervielfaeltigen)
	- [Arbeitsergebnis speichern](#arbeitsergebnis-speichern)
- [Schaubild an der digitalen Tafel präsentieren und beschriften](#schaubild-an-der-digitalen-tafel)

![Die Illustration stellt die Persona Martin Cremer dar. Die Illustration hat im Hintergrund einen hellgrauen Kreis, vor dem der Rest der Darstellung teilweise die Kreisränder überlappt und damit den verfügbaren Raum stärker ausfüllt. Die Persona zeigt den Oberkörper eines hellhäutigen Mann, der vor dem Kreishintergrund steht und den rechte Arm erhoben hat und mit dem Zeigefinger auf etwas im Kreishintergrund zu zeigen scheint. Im Kreishintergrund sind in der Nähe der Hand mit weißen dicken Strichen Objekte und Beschriftungen skizziert, die beispielsweise eine schwimmende Ratte von übergebenen Nesseltieren darstellen könnten. Der Mann blickt in Richtung seines ausgestreckten Arms nach links oben, der rechte Arm hängt am Körper einfach herunter. Der Mann trägt ein hellblaues langärmliges Hemd mit einem ärmellosen mittelgrünen Pullover darüber. Der Mund es Mannes ist leicht geöffnet, die Nase im Profil deutlich erkennbar und die Augen dunkelbeige scharf gezeichnet, ebenso ist das Ohr des Mannes im Profil durch einen Schatten deutlich erkennbar. Der Mann hat volle orangefarbene Haare, die ohne Unterbrechung in einen orangefarbenen Voll- und Schnurrbart übergehen und damit den Mundbereich vollständig einrahmen.](https://gitlab.com/flavoursofopenscience/openlab/raw/master/blog/personas/uhh-personas-martin-cremer/uhh-personas-martin-cremer.png)

Martin ist Biologielehrer an einem Gymnasium und möchte in der Jahrgangsstufe 6 ein Schaubild zur Fotosynthese im Unterricht zeigen, um es gemeinsam mit den Lernenden zu bearbeiten. Normalerweise nutzt er dafür seine Overheadfolien. Nach dem Umzug in das neue Schulgebäude gibt es jedoch keine Overheadprojektoren mehr; sie wurden durch digitale Tafeln ersetzt. Aus diesem Grund möchte Martin nun eine eigene digitale Vorlage erstellen, nutzen und mit seinem Kollegium teilen.

## Lösungsansatz

Martin benötigt für ein beschriftbares Schaubild eine [Grafik](https://openclipart.org/) sowie ein Programm, das [Vektorgrafiken](https://designer.gravit.io/) öffnen und bearbeiten kann. Schaubilder haben einen vielfältigen Anwendungsbereich. Verkleinert und mit Text angereichert sind diese zur visuellen Darstellung in Präsentationen oder einem [Arbeitsblatt](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/interaktives-arbeitsblatt/interaktives-arbeitsblatt.md) eine Bereicherung für den Unterricht.

## Lernziele

- spezifische OER suchen und finden
- CC0 / Public-Domain-Lizenz erkennen und verstehen lernen
- unterschiedliche Bild- bzw. Dateiformate erkennen und nach ihren spezifischen Eigenschaften gezielt auswählen können
- Bilder in einem gängigen Format (z.B. DIN A4 quer) als Collage zusammenführen
- Vorteile von kostenlosen Online-Tools im Bezug auf den Schulalltag exemplarisch erkennen (keine Installation und somit keine Konsolidierung mit IT/Schulleitung nötig; keine zusätzliche Kosten; plattform- und geräteübergreifendes Arbeiten ohne Aufwand möglich)
- Nachteile vom Online-Arbeiten erkennen (ohne Internet läuft nichts; Online-Dienste verändern sich unentwegs und vor allem nahtlos)

## Schritt-für-Schritt-Anleitung

Schaubilder finden ihren Einsatz in vielen verschiedenen Szenarien, sei es im Biologieunterricht zur Fotosynthese oder im Handballverein zur taktischen Schulung. Wie ihr stilistisch gute Schaubilder kostenfrei online zusammenstellen könnt, erfahrt ihr in den nächsten Schritten.

### Bildersuche und Creative Commons {#bildersuche-und-creative-commons}

Zunächst wird eine passende Vorlage benötigt. Bildsuchmaschinen können mit den richtigen Einstellungen ihre Suchergebnisse nach freiem Bildmaterial filtern.

- [Pixabay](https://pixabay.com/) (vorwiegend CC0)
- [CC Search](https://ccsearch.creativecommons.org/) (gemischt)
- [Flickr](https://www.flickr.com/) (gemischt)

Es gibt auch ganze Bilddatenbanken wie z.B. [OpenClipart](https://openclipart.org/), die lediglich [gemeinfreie Werke (CC0)](https://de.wikipedia.org/wiki/Creative_Commons#CC_Zero) anbieten. OpenClipart hat sich auf Cliparts spezialisiert, die wegen ihrer vereinfachten Formen und reduzierten Farben für Schaubilder besonders geeignet sind.

1. Öffnet im Browser die Internetseite [OpenClipart](https://openclipart.org/).
2. Gebt einen beliebigen Begriff in das Suchfeld ein. Für das Fotosynthese-Szenario werden ein Blatt und eine Sonne benötigt. Da wir uns auf einer englischsprachigen Seite befinden, sollten die Suchbebegriffe auf Englisch eingetragen werden. In diesem Beispiel wurde nach `grape leaf` und `sun` gesucht.
3. Wählt das passende Bild mit einem Klick aus. Ihr gelangt danach auf die Detailseite des jeweiligen Cliparts. Dort befinden sich weitere Informationen zum Bild und der `Download`-Button <i class="fas fa-download"></i>.
4. Ladet das Clipart auf den Computer herunter. Es wird eine Datei mit der Dateiendung **.svg** gespeichert. Anders als gewöhnliche Bildformate wie z.B. **.jpg** beinhaltet dieses Format sogenannte Vektoren, die nachträglich verlustfrei bearbeitet werden können.

### Schaubild collagieren {#schaubild-collagieren}

#### Grundeinstellungen des Zeichenprogramms  {#grundeinstellungen-des-zeichenprogramms}

Vektordateien können mit Illustrations- und Bildbearbeitungsprogrammen nachbearbeitet werden. Mittlerweile gibt es im Internet viele Programme, die im Browser laufen und keine Installation oder Registrierung benötigen. In diesem Abschnitt wird gezeigt, wie Layouts angelegt und die Cliparts angeordnet, verändert und in einer Collage mit Hilfe des kostenlosen Online-Tools [Gravit Designer](https://designer.gravit.io/) gespeichert werden.

1. Öffnet den Browser und sucht mit einer Suchmaschine eurer Wahl nach `gravit designer online` oder gebt die nachfolgende URL direkt ein: [designer.gravit.io](https://designer.gravit.io/).
2. Jedes Layoutprogramm benötigt eine sogenannte Zeichenoberfläche, ähnlich dem Papiermaß beim Zeichnen. Im ersten Schritt wird eine DIN-A4-große Zeichenfläche in den Feldern **width** (Breite) und **height** (Höhe) angelegt.
3. Tragt dazu die Maße eines queren DIN A4  in die dafür vorgesehen Felder ein: `Breite: 297 mm` und `Höhe: 210 mm`. Standardmäßig ist die Maßeinheit **px** (Pixel) gewählt; ändert diese mit einem Klick auf **mm** (Millimeter) ab. Abschließend auf den Button `Create!` klicken.

#### Cliparts laden und platzieren {#cliparts-laden-und-platzieren}

In diesem Schritt werden die heruntergeladenen Cliparts zu einem Schaubild angeordnet.

1. Öffnet über das Hauptmenü des Programms (am oberen Bildschirmrand) `File > Import > Place image ...` die gespeicherte **SVG**-, **EPS**- oder **PDF**-Datei.
2. Wird ein Element mit dem Verschieben-Werkzeug <i class="fas fa-mouse-pointer"></i> angeklickt, erscheint eine blaue Markierung um das Objekt. Bei gedrückter Maustaste kann es nun verschoben werden. Die Größe kann bei gedrückter Shift-Taste (Großschreibetaste) an den Ecken der Markierung angepasst werden.
3. Alternativ kann die Größe auch ohne Tastenkombination angepasst werden. Hierfür ist eine weitere Einstellung nötig, damit das Objekt proportional, sprich im gleichen Seitenverhältnis, skaliert wird. Wird dieser Schritt ausgelassen, passiert es schnell, dass die Objekte horizontal oder vertikal gestaucht werden. Klickt also (bei ausgewähltem Objekt) im rechten Palettenmenü den Halbkreis beim Eintrag **Size** zwischen `W` und `H` an. Nun kann das Clipart über die Ecken verkleinert werden.
4. Ist die Markierung aktiv, kann die Grafik an den Ecken gedreht werden. Geht mit dem  Verschieben-Werkzeug <i class="fas fa-mouse-pointer"></i> von außen an eine Ecke heran. Es erscheint ein Doppelpfeil <i class="fas fa-arrows-alt-h"></i>. Dreht nun mit gedrückter Maustaste die Grafik.
5. Erstellt jetzt aus der Auswahl ein **Symbol**. Klickt mit der rechten Maustaste in das gewünschte Objekt. Es öffnet sich das Kontextmenü; sucht dort nach dem Eintrag `Create Symbol`. Tragt einen beliebigen Namen für das Symbol ein.

> Diese fünf Schritte können für die jeweiligen heruntergeladenen Grafiken wiederholt werden.

#### Schaubild ergänzen {#schaubild-ergaenzen}

Die folgenden Schritte zeigen, wie das Schaubild mit unterstützenden Hinweisen wie z.B. Pfeilen oder Textfeldern komplettiert wird.

1. Eines der beliebtesten Objekte eines Schaubilds sind einfache Pfeile oder auch Doppelpfeile. Ein einfacher Pfeil kann mit dem Werkzeug **Shape** <i class="fas fa-window-minimize"></i> im Werkzeugmenü am oberen Bildschirmrand erstellt werden. Erstellt mit Klicken und Ziehen einen Strich mit gewünschter Länge.
2. Fügt dem Strich über das Palettenmenü unter `Fills > Borders` eine Farbe hinzu. Klickt dazu auf den Kreis mit der Pipette <i class="fas fa-eye-dropper"></i>. Wählt die gewünschte Farbe aus.
3. Rechts neben dem Farbfeld erscheint eine Zahl mit Punkt-Angabe (z.B. `1pt`). Diese Maßeinheit gibt die Strichstärke der Linie an. Probiert ein wenig herum; ein guter Wert bei DIN A4 ist `3pt`.
4. Nun fehlen lediglich die Pfeile an den Strichenden. Diese können ebenfalls im Palettenmenü unter `Fills > Borders` eingestellt werden. Rechts neben der Überschrift **Borders** befindet sich der Button `Advanced stroke settings` <i class="fas fa-sliders-h"></i>, der mit einem Klick ein Untermenü öffnet. Zwei Felder sind dort mit `none` beschriftet, klickt darauf und wählt den gewünschten Stil des Strichendes aus. In diesem Beispiel wurde `Bullet` und `Fat arrow` gewählt.

#### Objekte vervielfältigen {#objekte-vervielfaeltigen}

Schaubilder besitzen oftmals die gleichen Elemente an unterschiedlichen Positionen und in unterschiedlichen Ausrichtungen. Damit die oben genannten Schritte nicht jedes Mal erneut angewendet werden müssen, bedienen wir uns nun der Duplikat-Funktion.

1. Wählt das Objekt, welches ihr vervielfältigen wollt, mit dem Verschieben-Werkzeug <i class="fas fa-mouse-pointer"></i> an. Es erscheint eine blaue Markierung um das Objekt herum. Im Hauptmenü kann unter `Edit > Duplicate` das aktive Objekt dupliziert werden. In diesem Beispiel wurde der zuvor erstellte Pfeil vervielfacht.
2. Die Pfeile sind nun etwa deckungsgleich auf der Zeichenfläche platziert. Mit dem  Verschieben-Werkzeug <i class="fas fa-mouse-pointer"></i> kann der Pfeil verschoben werden. Möchtet ihr z.B. die Lage der Spitze, sprich die Ausrichtung, verändern, müsst ihr an den Pfeilenden das kleine blaue Rechteck anklicken, geklickt halten und an die gewünschte Position ziehen.

> Wiederholt diese Schritte mit den vorhandenen Objekten, bis das Schaubild fertiggestellt ist.

#### Arbeitsergebnis speichern {#arbeitsergebnis-speichern}

Damit das Schaubild eingesetzt und zu einem späteren Zeitpunkt wieder verändert werden kann, muss es nun in einem passenden Ausgabeformat gespeichert werden. Dabei handelt es sich um zwei unterschiedliche Formate: zum einen das Projektformat **.gvdesign** und zum anderen das Bildformat **.png** für die anschließende Präsentation. Projektdateien eignen sich nämlich für ein erneutes Bearbeiten des Schaubildes, dafür aber nicht für eine Präsentation des Ergebnisses. Umgekehrt ist das Bildformat für Präsentationen, aber nicht für eine nachträgliche Bearbeitung, geeignet.

1. Das Speichern des Projekts geschieht unter dem Hauptmenüpunkt `File > Download File...`. Es wird eine Datei mit der Endung **.gvdesign** auf dem Computer gespeichert. Möchtet ihr diese erneut laden, geschieht dies unter dem Hauptmenüpunkt `File > Open file...`.
2. Das Bildformat für die Präsentation kann ebenfalls im Hauptmenü unter `File > Export > PNG Image (.png)` und in weiteren Formaten (SVG, PDF, JPEG ...) heruntergeladen werden.

### Schaubild an der digitalen Tafel präsentieren und beschriften {#schaubild-an-der-digitalen-tafel}

In diesem Schritt geht es darum, das Schaubild digital in einem Lehr-Lernszenario einzusetzen. Hier kommt ein kostenloses Online-Tool namens [ClassroomScreen](https://classroomscreen.com/) zum Einsatz.

1. Öffnet den Browser und gebt in die Adresszeile die URL [classroomscreen.com](https://classroomscreen.com/) ein.
2. Der nächste Schritt ist nun, das erstellte Schaubild auf den Bildschirm zu bekommen und dieses zu beschriften. Nach kurzer Ladezeit erscheint im Online-Tool ein vollflächiges Vorschaubild und am unteren Bildschirmrand ein Menü mit 12 Buttons. Wählt den Button `Hintergrund` im Menü aus (zweiter von links). Es öffnet sich eine Vorschau an zufälligen Bildern. Darunter befindet sich der Button `Upload` <i class="fas fa-upload"></i>. Mit einem Klick öffnet sich das Auswahlmenü des jeweiligen Betriebssystems. Wählt das vorher gespeicherte Schaubild im PNG-Format aus.
3. Das Schaubild ist jetzt als aktiver Hintergrund gesetzt. Damit nun eine Beschriftung möglich ist, muss eine transparente Zeichenfolie - ähnlich einer Overheadfolie - über das Bild gelegt werden. Dies geschieht mit einem Klick auf den Button `Zeichnung` (sechster von links).
4. Auf der linken Seite erscheint ein **Zeichenmenü**; dort wählt nun das `weiße Quadrat mit dem roten Querstrich an`. Dieses steht bei sehr vielen Grafikprogrammen für einen transparenten Hintergrund.
5. Anschließend erscheint das Schaubild wieder. Nun sind die Zeichenwerkzeuge aktiv. Benutzt ihr gerade eine digitale Tafel, kann das Schaubild mit den zugehörigen Stiften beschriftet bzw. ergänzt werden.
6. Die unterschiedlichen Farben und Strichstärken können ebenfalls links im Zeichenmenü eingestellt werden.

<a class="cc-license" rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"><a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/kein-passendes-schaubild-fuer-den-unterricht/" property="cc:attributionName" rel="cc:attributionURL">Kein passendes Schaubild für den Unterricht?</a></span> von <em><a href="https://www.manfred-steger.de/" target="_blank">Manfred Steger</a></em> für das Universitätskolleg Digital der Universität Hamburg ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">CC BY 4.0 International Lizenz</a>.

<!-- ### Gliederung und Alternativen

1. CC Bildersuche (Sonne, Wasser und Blatt) (Bezug Synergie Praxis: [*04 Offenes Material finden*](https://synlloer.gitbooks.io/synergie-praxis-open-educational-resources-2017/content/04-wo-finde-ich-offenes-material-von-anderen.html))
    - **[OpenClipart](https://openclipart.org/) (CC 0)**
    - [Pexels](https://www.pexels.com/) (vorwiegend CC 0)
    - [Pixabay](https://pixabay.com/) (vorwiegend CC 0)
    - [ccsearch](https://ccsearch.creativecommons.org/) (gemischt)
    - [Flickr](https://www.flickr.com/) (gemischt)

2. Schaubild collagieren (Bezug Synergie Praxis: [*02 Aufbereitung* finden](https://synlloer.gitbooks.io/synergie-praxis-open-educational-resources-2017/content/02-material-didaktisch-aufbereiten.html))
    - **[Gravit Designer](https://designer.gravit.io/)**
    - [Gimp online](https://www.rollapp.com/launch/gimp) oder ~~[Gimp Portable](https://portableapps.com/apps/graphics_pictures/gimp_portable)  – da nicht Plattform übergreifend~~

3. Präsentieren
    - [classroomscreen](https://classroomscreen.com/)

4. OER erstellen (Bezug Synergie Praxis: [*07 Das Format*](https://synlloer.gitbooks.io/synergie-praxis-open-educational-resources-2017/content/07-das-format.html) und [*08 Die Veröffentlichung*](https://synlloer.gitbooks.io/synergie-praxis-open-educational-resources-2017/content/08-die-veroffentlichung.html))
    - [4teachers](https://www.4teachers.de) Materialupload

5. Optional: Auf eine Folie drucken -->
