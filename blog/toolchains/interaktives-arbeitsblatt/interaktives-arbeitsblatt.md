<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Interaktive Arbeitsblätter erstellen und anreichern

## Hübsche Arbeitsblätter gibt's nicht? Dann kennt ihr diese Toolchain noch nicht.

[Link zur GitLab Issue #18](https://git.universitaetskolleg.uni-hamburg.de/synlloer/oer-know-how/issues/18)

*Toolchain Anleitung am Beispiel eines interaktiven Arbeitsblatts für den Biologieunterricht*.

> geschätzer Zeitaufwand: 60 - 90 Minuten

> Schwierigkeitsgrad: 3/5 Punkte

Tags: tutory, Arbeitsblätter online gestalten, Handy Fotos verbessern, Fotos auf dem Handy bearbeiten, Kahoot!, Gamification Tools, learningapps, Links verkürzen, Medienpädagogik Praxis, socrative.com

## User Story ([Persona Julia Hibbert](https://git.universitaetskolleg.uni-hamburg.de/synlloer/oer-know-how/blob/master/Unterlagen/Personas-03.pdf))

-->

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/hYFTjPNkebw" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Inhaltsverzeichnis

- [Arbeitsblatt aus Vorlage erstellen](#arbeitsblatt-aus-vorlage-erstellen)
- [Arbeitsblatt mit Musterlösung erstellen](#arbeitsblatt-mit-musterloesung-erstellen)
- [Eine eigene (interaktive) App erstellen](#eine-eigene-interaktive-app-erstellen)
- [Interaktive Apps in ein Arbeitsblatt integrieren](#interaktive-apps-in-ein-arbeitsblatt-integrieren)
- [Ein Online-Quiz erstellen mit Kahoot](#ein-online-quiz-erstellen-mit-kahoot)
- [Arbeitsblätter auf Tutory teilen und andere Arbeitsblätter finden](#arbeitsblaetter-auf-tutory-teilen)
- [PDF-Export und Teilen des Arbeitsblatts auf einer Austauschplattform](#pdf-export-und-teilen)

![Die Illustration stellt die Persona Julia Hibbert dar. Die Illustration hat im Hintergrund einen hellgrauen Kreis, vor dem der Rest der Darstellung teilweise die Kreisränder überlappt und damit den verfügbaren Raum stärker ausfüllt. Die Persona zeigt den Oberkörper einer hellhäutigen Frau, die eine dunkelblaugraue Weste mit hochgekrempelten Ärmeln über einem orangfarbenen Shirt trägt und aufrecht stehend nach rechts aus dem Bild schaut. In der linken Hand hält sie vor dem Bauch ein weißes Blatt Papier, den rechten Arm hat sie bis über Kopfhöhe angewinkelt erhoben und hält in der Hand einen türkisfarbenen Stift, aus dem nach links eine hellgraue Flüssigkeit sich in weitem Bogen verteilt. Die Frau hat kurze hellbraune Haare, der Mund ist im Profil leicht geöffnet und Nase und Augen grob skizziert.](https://gitlab.com/flavoursofopenscience/openlab/raw/master/blog/personas/uhh-personas-julia-hibbert/uhh-personas-julia-hibbert.png)

Julia ist Biologielehrerin an einem Gymnasium und hat von ihrem mittlerweile pensionierten Vorgänger eine Sammelmappe mit Unterrichtsmaterialien überreicht bekommen. Sie möchte die inhaltlich wertvollen Materialien für ihren eigenen Unterricht nutzen, aber die teils sehr alten, handschriftlichen und mehrfach kopierten Unterlagen sind nur schwer lesbar. Sie entschließt sich dazu, die Materialien zu digitalisieren und für den Unterricht an einer digitalen Tafel interaktiv anzureichern.

## Lösungsansatz

Julia benötigt für die Umsetzung der [Arbeitsblätter](https://www.tutory.de/) und der [interaktiven Anreicherung](https://learningapps.org/) mehrere Programme. Ein eigens für die Erstellung von Arbeitsblättern entwickelter [Editor](http://tutory.de) liefert Unterstützung bei der Gestaltung und Einbindung von Medien. Mit einem [Online-Quiz](http://kahoot.it) können Lerninhalte aus dem Arbeitsblatt spielerisch abgefragt und mit den Lernenden gemeinsam erörtert werden. Nicht nur im Unterricht finden diese Methoden Verwendung; auch im Privatbereich können z.B. Rezepte damit verschriftlicht und mit ansprechenden Bildern zu einem Kochbuch zusammengestellt werden.

## Lernziele

- eigenes verwenden oder OER-Arbeitsblätter zur Thematik suchen und finden (z.B. [teachsam](http://www.teachsam.de/geschichte/ges_deu_1648-1790/ges_deu_1648-1790_0.htm) oder [SeGu](https://segu-geschichte.de/))
- Arbeitsblatt mit einem einfachen Tool erstellen (z.B. [Tutory](https://www.tutory.de))
- verschiedene Varianten eines Arbeitsblatte
s erstellen (Graduierung/Differenzierung)
- Lernzielkontrolle mit einem Arbeitsblatt verzahnen
- ein interaktives Element einbringen (z.B. mit [Kahoot](http://kahoot.it) oder dem [Erstellen einer eigenen App](https://learningapps.org/))
- Austauschplattform für Materialien kennenlernen (z.B. [Lehrer Online](https://www.lehrer-online.de/) oder [ZumWiki](https://wiki.zum.de/wiki/Hauptseite))
- Vor- und Nachteile von Online-Repositorien verstehen lernen
- Argumentation für eine gemeinsame Austauschplattform für das Fach Geschichte in der Fachtagungsrunde vortragen können

## Schritt-für-Schritt-Anleitung

Im Hinblick auf die fortschreitende Digitalisierung des Alltags und somit auch des Unterrichts in Schulen gewinnen digitale Medienkompetenzen an immer größerer Bedeutung. Es eignet sich, Lehr- und Lernanlässe mit Aspekten des digitalen Mediums anzureichern, damit Lernenden die Relevanz des Themas auf implizite Weise nähergebracht wird. Dies kann spielerisch und vor allem einfach geschehen, z.B. durch [Apps](https://learningapps.org/), die sich zum Lernen eignen, durch ein [interaktives Quiz](http://kahoot.it) oder durch das Arbeiten mit der digitalen Tafel.
Wie man gegebene, analoge Lerninhalte in Form von Arbeitsblättern [digitalisieren](https://www.tutory.de/) und somit für genau diese Aufgaben - für den Einsatz an der digitalen Tafel oder für das Teilen mit anderen auf Online-Plattformen - nutzen kann, wird in dieser Schritt-für-Schritt-Anleitung dargelegt.

### Arbeitsblatt aus Vorlage erstellen {#arbeitsblatt-aus-vorlage-erstellen}

Um aus einer Vorlage ein Arbeitsblatt erstellen zu können, muss diese selbstverständlich zugänglich sein. Im folgenden Beispiel wurde ein Foto des alten, ausgedruckten Arbeitsblatts gemacht, welches nun auf dem Computer als Datei vorliegt.
Hier stehen dann alle Möglichkeiten offen, wie dieses (idealerweise digital) transformiert werden kann.

1. Das abfotografierte Arbeitsblatt wird geöffnet, um die Inhalte übernehmen zu können. Im Folgenden sollte das Augenmerk auf inhaltlicher und methodischer Ergänzung sowie der Digitalisierung der gegebenen Inhalte liegen.
2. Im Web-Browser (z.B. [Chrome](https://www.google.de/chrome/?brand=CHBD&ds_kid=[*TrackerID*]&gclid=EAIaIQobChMIqqvQ18n72QIVy5ztCh3FNQ7wEAAYASAAEgIpUPD_BwE) oder [Firefox](https://www.mozilla.org/de/firefox/new/)) wird die Seite [tutory.de](https://www.tutory.de/) geöffnet. Auf Tutory wird ein Account benötigt, welcher in der Basisversion kostenfrei ist. Dieser reicht für das Erstellen anschaulicher, übersichtlicher und interaktiver Arbeitsblätter vollkommen aus. Nach der Anmeldung auf Tutory besteht nun die Option, ein neues Arbeitsblatt zu erstellen. Dies wird automatisch und nur für uns sichtbar gespeichert.
3. Im vorliegenden Beispiel werden die einzelnen Aufgaben eines Arbeitsblatts über das Mikroskopieren nacheinander in das digitale Arbeitsblatt übernommen. Durch diverse Formatierungsmöglichkeiten und verschiedene andere Funktionen des Tools kann das neue Arbeitsblatt übersichtlicher und anschaulicher gestaltet werden.
4. Zunächst wird eine Aufgabenstellung mit dem Inhalt der ersten Aufgabe des bereits gegebenen Arbeitsblattes erstellt. Zusätzlich wird eine Hinweisbox hinzugefügt (in Tutory links in der Menüleiste der modularen Arbeitsblattinhalte zu finden unter **Hinweis**), die den Lernenden hilft, richtig mit dem Mikroskop umzugehen. Anschließend wird die zweite Aufgabe des Arbeitsblatts übernommen.
5. Aus der dritten Aufgabe des vorgegeben Arbeitsblatts wird ein Lückentext erstellt. So besteht die Möglichkeit, abgefragte Leistungen selbst zu überprüfen.
Später geht es darum, wie mit einem einfachen Klick ein zu dem Lückentext gehöriges Lösungsblatt erstellt werden kann.

> **Tipp:** Wer lernen möchte, wie ein OER-Arbeitsblatt in nur 30 Minuten erstellt werden kann, klickt [hier](https://openlab.blogs.uni-hamburg.de/vom-tweet-zum-oer-arbeitsblatt-in-30-minuten/).

<!-- ![Screenshot des ersten Schrittes: Arbeitsblatt aus Vorlage erstellen, von Lucas Jacobsen](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/interaktives-arbeitsblatt/ss1.png)
Lucas Jacobsen, "Digitales und analoges Arbeitsblatt im Vergleich" [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
-->

### Arbeitsblatt mit Musterlösung erstellen {#arbeitsblatt-mit-musterloesung-erstellen}

Oben links auf der Seite befindet sich der  Button `PDF`. Mit einem Klick auf diesen habt ihr die Option, das erstellte Arbeitsblatt entweder mit oder ohne Lösungen als PDF zu exportieren. Somit kann das Arbeitsblatt leicht im Kollegium geteilt, ausgedruckt, verschickt etc. werden.

<!-- ![Screenshot des zweiten Schrittes: Arbeitsblatt mit Musterlösung erstellen, von Lucas Jacobsen](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/interaktives-arbeitsblatt/ss2.png)
Lucas Jacobsen, "Lösungsblatt erstellen" [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
-->

### Eine eigene (interaktive) App erstellen {#eine-eigene-interaktive-app-erstellen}

1. Im Browser wird die Seite [learningapps.org](https://learningapps.org/) geöffnet.
2. Auch auf dieser Seite ist ein Account nötig. Es wird jedoch ein Minimum an Informationen benötigt. Die Erstellung des Accounts nimmt nur wenig Zeit in Anspruch.
3. Klickt auf den Button `App erstellen` oben mittig auf der Seite.
4. Nun erscheint eine Vielzahl unterschiedlicher Apps, die nach Belieben selbst gestaltet werden können. Da den Lernenden etwas über den Aufbau eines Mikroskops vermittelt werden soll, scheint die App [Zuordnung auf Bild](https://learningapps.org/createApp.php) gut geeignet zu sein.
5. Hier kann ein beliebiges Bild eingefügt und mit Stecknadeln versehen werden. Diese Stecknadeln können wiederum mit Fragen, Bildern oder anderen Dingen ausgestattet werden.
Im Eingangsbeispiel wird das Bild eines Mikroskops verwendet; die Stecknadeln werden an jeder zu beschreibenden Komponente des Mikroskops gesetzt.
6. Ist die App fertig entwickelt, könnt ihr sie speichern, testen und, wenn gewollt, anderen die Nutzung erlauben. Hierfür muss als erstes auf den Button `Fertigstellen und Vorschau anzeigen` geklickt werden. Nun erscheint die gerade erstellte App genau so, wie andere sie sehen würden, wenn sie die App lösen. Dann könnt ihr die App testen. Seid ihr mit dem Test zufrieden, könnt ihr unten rechts auf den Button `App speichern` klicken.
7. Die gespeicherte App kann mit Hilfe der gegeben URLs geöffnet oder in einer Webseite eingebettet werden.

<!-- ![Screenshot des Schrittes 3.4: Eine eigene (interaktive) App erstellen, von Lucas Jacobsen](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/interaktives-arbeitsblatt/ss3.png)
Lucas Jacobsen, "App-Auswahl" [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
-->
<!-- ![Screenshot des Schrittes 3.5: Eine eigene (interaktive) App erstellen, von Lucas Jacobsen](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/interaktives-arbeitsblatt/ss4.png)
Lucas Jacobsen, "eigene App" [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
-->
<!-- ![Screenshot des Schrittes 3.6: Eine eigene (interaktive) App erstellen, von Lucas Jacobsen](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/interaktives-arbeitsblatt/ss5.png)
Lucas Jacobsen, "eigene App speichern" [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
-->
<!-- ![Screenshot des Schrittes 3.7: Eine eigene (interaktive) App erstellen, von Lucas Jacobsen](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/interaktives-arbeitsblatt/ss6.png)
Lucas Jacobsen, "eigene App teilen" [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
-->

### Interaktive Apps in ein Arbeitsblatt integrieren {#interaktive-apps-in-ein-arbeitsblatt-integrieren}

Nach der Erstellung interaktiver Inhalte sollen diese den Lernenden zugänglich gemacht werden. In diesem Fall wird dazu ein URL-Shortener genutzt; dies ist eine Funktion, mit der lange URLs zu kurzen, individuellen URLs umgewandelt werden können.

> **Tipp:** Zum Integrieren digitaler, interaktiver Inhalte gibt es viele Methoden. Kürzungen von URLs können z.B. mit Hilfe von [Bitly](https://bitly.com/) oder mit einem [QR-Code-Generator](https://www.qrcode-generator.de/) vorgenommen werden.

1. Um die erstellte App in das zuvor erstellte Arbeitsblatt zu integrieren, wird der URL-Shortener [Bitly](https://bitly.com/) genutzt. Dieser kreiert aus einem beliebig langen einen sehr kurzen und übersichtlichen Link, der sich gut in Arbeitsblättern integrieren lässt.
2. Kopiert den Link der App.
3. Öffnet die Webseite [https://bitly.com](https://bitly.com/) und fügt den Link in den URL-Shortener ein (eine weiße Box in der Mitte der Webseite). Automatisch erhaltet ihr einen kurzen Link, den ihr mit einem einfachen Klick auf `Copy` kopieren könnt.
4. Nun muss lediglich die kurze URL in das Arbeitsblatt eingefügt werden. Dann haben alle Lernenden die Möglichkeit, die interaktive, selbst gestaltete App zu nutzen, sobald sie den gekürzten Link auf einem digitalen Endgerät öffnen.

<!-- ![Screenshot des vierten Schrittes: Interaktive Apps in ein Arbeitsblatt integrieren, von Lucas Jacobsen](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/interaktives-arbeitsblatt/ss6.png)
Lucas Jacobsen, "eigene App teilen" [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
-->

### Ein Online-Quiz erstellen mit Kahoot {#ein-online-quiz-erstellen-mit-kahoot}

1. Öffnet die Webseite [kahoot.com](http://kahoot.it) und erstellt einen neuen Account; dies geschieht durch das Klicken auf `Sign up`.
2. Durch einen Klick auf den Button `New K` oben rechts auf der Webseite werden verschiedene Möglichkeiten eröffnet, um interaktive Inhalte zu gestalten. In diesem Schritt wird ein Quiz erstellt, welches mit einer Vielzahl an Menschen gemeinsam gespielt werden kann.
3. Durch einen Klick auf den Button `Quiz` könnt ihr dem Quiz einen Namen geben und eine kurze Beschreibung des Inhalts beifügen. Die Sprache und die Zielgruppe des Quiz sollen ebenfalls ausgewählt werden. Ist dies geschehen, kommt ihr mit einem Klick auf der rechten oberen Seite auf die jeweils nächste.
4. Frage für Frage könnt ihr nun das Quiz erstellen. Hierfür gebt ihr oben die Frage ein und unten die dazugehörigen verschiedenen Antwortmöglichkeiten, wobei neben der jeweiligen Antwort ein Haken als Option vorhanden ist, um die Antwort als richtig zu markieren.
5. Nach Fertigstellung des Quiz ist dieses im privaten Account gespeichert und kann fortan gespielt, getestet, geteilt oder verändert werden.
6. Teilnehmende können auf [kahoot.it](https://kahoot.it/) dem Quiz beitreten. Für die Teilnahme benötigen sie den sogenannten Game PIN, welcher oben auf der Seite steht. Das Quiz kann z.B. über eine digitale Tafel mit einer gesamten Schulklasse gespielt werden.

<!-- ![Screenshot des vierten Schrittes: Ein Online Quiz erstellen mit kahoot, von Lucas Jacobsen](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/interaktives-arbeitsblatt/ss7.png)
Lucas Jacobsen, "online Quiz" [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
-->

### Arbeitsblätter auf Tutory teilen und andere Arbeitsblätter finden {#arbeitsblaetter-auf-tutory-teilen}

Nach der Digitalisierung analoger Lehr- und Lernmaterialien stellt sich die Frage, wie z.B. das eingangs erwähnte Arbeitsblatt mit anderen geteilt werden kann. Mindestens ebenso interessant ist die Frage, wie andere Arbeitsblätter oder ganze Unterrichtskonzepte gefunden und für eigene Kontexte verwendet werden können.

> **Tipp:** Beim Teilen eigener Arbeitsblätter und der Nutzung 'fremder' Materialien sollte immer auf die urheberrechtliche Lizenzierung geachtet werden!
> Auf der Webseite von [Tutory](https://www.tutory.de/leitfaden-oer) findet man einige nützliche Tipps zum Lizenzieren und zu den gängigen Creative-Commons-Lizenzen.

1. Oben mittig auf der Webseite [tutory.de](https://www.tutory.de/) befindet sich der grüne Button `Privat gespeichert`. Ist dieser zu sehen, bedeutet es, dass das selbst erstellte Arbeitsblatt nur in der eigenen Sammlung zu finden ist. Wollt ihr es nun mit dem Kollegium oder der Community teilen, klickt ihr auf diesen grünen Button.
2. Durch den Klick öffnen sich die Lizenzierungsoptionen, die ausgefüllt werden müssen.
3. Wenn dieser Schritt abgeschlossen ist, befindet sich unten rechts der Button `Jetzt Veröffentlichen`.
4. Eine letzte Überprüfung kann vorgenommen und das Arbeitsblatt veröffentlicht werden. Daraufhin erhaltet ihr den Link zur PDF-Version sowie zum Tutory-Dokument.
5. Wenn ihr links oben auf den Button `Tutory` klickt, erscheinen drei Optionen: **Vorlagen**, **Eigene Arbeitsblätter** und unter der Rubrik **Entdecken** die Sammlung des gesamten, bereits veröffentlichten Materials. Dort findet ihr unter vielen anderen Arbeitsblättern nun auch das eigens Veröffentlichte.
6. Klickt ihr auf ein Arbeitsblatt, besteht die Möglichkeit, dieses als PDF aufzurufen oder es in die eigene Sammlung zu importieren und je nach Bedürfnissen und Kontext zu verändern.

### PDF-Export und Teilen des Arbeitsblatts auf einer Austauschplattform {#pdf-export-und-teilen}

1. Oben links auf Tutory, unter dem bereits bekannten `PDF`-Button, können sowohl das Arbeits- als auch das Lösungsblatt als PDF-Datei heruntergeladen werden.
2. Öffnet die Seite, auf der das Arbeitsblatt geteilt werden soll (z.B. [ZumWiki](https://wiki.zum.de/wiki/Hauptseite)). Für das Hochladen von Dateien auf dieser Seite wird ein Account benötigt. Diesen zu beantragen kann unter Umständen (jedoch nur sehr selten) einige Tage in Anspruch nehmen, da die Betreiber der Webseite alle gemeinnützig arbeiten.
3. Links befindet sich die Rubrik **Werkzeuge**. Ein Klick öffnet mehrere Optionen, unter welchen sich der Button `Datei hochladen`befindet.
4. Falls Unsicherheiten zum Urheberrecht bestehen, findet man dort auch ein sich automatisch öffnendes Bild, welches urheberrechtliche Fragen übersichtlich klärt. Inhalte, die auf ZumWiki hochgeladen werden, müssen unter einer Creative-Commons-Lizenz stehen.

<!-- ![Screenshot des sechsten Schrittes: PDF Export und Teilen des Arbeitsblattes auf einer Austauschplattform, von Lucas Jacobsen](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/interaktives-arbeitsblatt/datei-hochladen-zumwiki-cc-by-sa-lucas-jacobsen.png)
Lucas Jacobsen, "Datei hochladen bei ZumWiki" [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)
-->

>**Tipp:** Weiterführende Informationen zum Urheberrecht an Schulen und zu Creative-Commons-Lizenzen befinden sich auf dem [Blog des Open Lab](https://openlab.blogs.uni-hamburg.de/vom-tweet-zum-oer-arbeitsblatt-in-30-minuten/), der Webseite von [CreativeCommons.org](https://creativecommons.org/about/), [irights.info](https://irights.info/kategorie/themen/creative-commons-lizenzen) oder dem Magazin [sofatutor](https://magazin.sofatutor.com/lehrer/2017/05/08/neues-urheberrecht-lehrer-sollen-mehr-rechte-bekommen/).

## Lizenz  {#lizenz}

<a class="cc-license" rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"><a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/interaktive-arbeitsblaetter-erstellen-und-anreichern/" property="cc:attributionName" rel="cc:attributionURL">Interaktive Arbeitsblätter erstellen und anreichern</a></span> von <em><a href="#" target="_blank">Lucas Jacobsen</a></em> für das Universitätskolleg Digital der Universität Hamburg ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">CC BY 4.0 International Lizenz</a>.

<!-- ## Gliederung und Alternativen

0. Freies Arbeitsblatt beschaffen

**Interaktives [Arbeitsblatt](http://www.fachdidaktik-einecke.de/7_Unterrichtsmethoden/arbeitsblaetter-methode.htm)**

1. Arbeitsblatt aus Vorlage erstellen [Tutory](https://www.tutory.de/) (Editor/Plattform)
  - Arbeitsblatt (Inhalte/Input übernehmen/erstellen) ```(Kapitel 1: ein eigenes Video)```
  - Musterlösung ```(Kapitel 2: ein eigenes Video)```
  - Teilen ```(Kapitel 3: ein eigenes Video)```
    - Tutory Plattform online (zeigen wo nun das Arbeitsblatt auftaucht und wie man es findet)
    - PDF Export für andere Wege (Email, Dropbox, ZUM Wiki usw.)

2. Interaktive(n) Inhalt(e) extern erstellen und in das Tutory Arbeitsblatt einbetten [bit.ly](https://bitly.com/) (Interaktion)
- [**kahoot.it**](http://kahoot.it) (Gamification Tool) ```(Kapitel 4: ein eigenes Video)```
- [**bit.ly**](https://bitly.com/) (URL Shortener) ```(Kapitel 5: ein eigenes Video)```
- [eigene App erstellen](https://learningapps.org/createApp.php) die es bei Tutory nicht gibt

3. Teilen des Arbeitsblattes auf einer Austauschplattform oder über ein soziales Netzwerk
 - [ZUM WIKI](https://wiki.zum.de/wiki/Hauptseite) ```(Kapitel 6: ein eigenes Video)```

-->
