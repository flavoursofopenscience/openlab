<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# OER entwickeln und bereitstellen mit Git und WordPress

## Doppelte oder dreifache Büchfürung bei der Veröffentlichung und Weiterentwicklung von OER? In dieser original IT-Toolchain bauen wir eine Entwicklungsumgebung für einen OER-Blog.

> geschätzer Zeitaufwand: 8 Stunden

> Schwierigkeitsgrad: 5/5 Punkte

Tags: Git, GitLab, GitHub, GFM, Versionierung, digital publishing, WordPress, sync, crossmedia, desktop publishing, OER

## User Story (Persona Manfred Steger)

-->

## Inhaltsverzeichnis

- [Zentrale Datei- und Versionsverwaltung](#zentral)
	- [GitLab Repositorium](#repo)
		- [Erstellen](#erstellen)
		- [Einrichten](#einrichten)
	- [Dateien verwalten aka verteilte Versionsverwaltung](#verwalten)
		- [Lokales Repositorium anlegen](#lokal)
		- [Änderungen synchronisieren](#sync)
- [Digitales Publizieren](#digital-publishing)
	- [Einheitliche (Dokumenten-) Sprache](#auszeichnungssprache)
	- [Dokumente erstellen und abgleichen](#dokumente-erstellen)
		- [Online](#online)
		- [Offline](#offline)
		- [Kollaboration](#kollaboration)
	- [Dokument als Online-Artikel publizieren](#dokument-publizieren)
		- [WordPress im Zusammenspiel mit GitLab](#wordpress-gitlab)
		- [WordPress-Artikel erstellen](#artikel-erstellen)
	- [Zwischenfazit](#zwischenfazit)
		- [Vermeidung von negativen Effekten](#negative-effekte)
		- [Profitieren von neuen Möglichkeiten](#profitieren)
		- [Kultur des Schreibens](#kultur-des-schreibens)

Manfred ist wissenschaftlicher Mitarbeiter in einem OER-Projekt. Er erstellt freie OER-Schulungsmaterialien für Schulen und Hochschulen in verschiedenen Formaten. Die Aktualisierung des Materialbestands (Internetseite, PDF, Office Dokumente und Repositorien) konnte in seinem kleinen Team zeitlich kaum aufgefangen werden. Er sucht nun nach einer Möglichkeit kollaborativ, formatübergreifend und effektiv mit seinen KollegInnen an den Dokumenten zu arbeiten.

## Lösungsansatz {#loesungsansatz}

Manfred benötigt für dieses Unterfangen einen zentralen Dateiverwaltungsdienst (filehosting), der über eine Programmierschnittstelle ([API](https://de.wikipedia.org/wiki/Programmierschnittstelle)) ansprechbar ist und ein Versionierungssystem (wie z.B. _git_) unterstützt. Für die kontinuierliche Veröffentlichung und Darstellung der Materialien im Web bietet sich ein erweiterbares Blogsystem wie [WordPress](https://wordpress.org/) an. Für die Erstellung und den automatischen Abgleich der Materialien wird eine einfache und schnell erlernbare Auszeichnungssprache wie [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) benötigt. Für die Umwandlung in weitere dokumentbasierte Dateiformate wie OpenOffice und PDF wird ein Parser für Multidokumentenformate wie [Pandoc](https://de.wikipedia.org/wiki/Pandoc) Einsatz finden.

## Lernziele  {#lernziele}

- dokumentspezifische Eigenschaften kennenlernen
- den Einsatzzweck von Markdown kennen- und verstehen lernen
- Dokumente in verschiedene Formate konvertieren
- ein git-Repositorium als zentralen Speicherort nutzen
- Versionierung verstehen und anwenden lernen
- erste Schritte im cross- und multipublishing machen
- administrative Einrichtung von Maschine-zu-Maschine-Kommunikation (GitLab / WordPress)

## Schritt-für-Schritt-Anleitung  {#schritt-fuer-schritt-anleitung}

Digital Publishing ist mittlerweile nicht nur für Verlage oder Agenturen ein täglicher Bestandteil; auch im wissenschaftlichen und schulischen Umfeld gehören digitale Publikationen zum Alltag. Umso wichtiger wird es, die Bestände feinsäuberlich zentralisiert zu _lagern_, zu _verwalten_ und _zeiteffizient_ auf einem aktuellen Stand zu halten. Teilautomatisierung spielt bei dieser Toolchain eine tragende Rolle und kann für viele mediale Produkte und sogar für die Kommunikation im Team ein zunehmend positiver Koeffizient sein.

> Tipp: Alle Beiträge des Blogs [openlab.blogs.uni-hamburg.de](https://openlab.blogs.uni-hamburg.de) sind mit dieser Toolchain entstanden und werden damit verwaltet.

## Zentrale Datei- und Versionsverwaltung  {#zentral}

Damit Dokumente von mehreren Personen kollaborativ genutzt werden können, wird ein Dateiverwaltungsdienst benötigt. Je nach Anwendungsfall oder Organisation steht zu Beginn die Entscheidung an, ob der Dienst selbst gehostet oder online bei einem Anbieter gemietet werden soll. Im Nutzungsszenario von OER bietet sich ein sogenanntes **öffentliches Repositorium** an, das bei den Online-Anbietern [GitLab.com](https://about.gitlab.com/) und [GitHub.com](https://github.com/) kostenlos und nur mit einer nennenswerten Klausel behaftet ist – alles ist immer öffentlich und darf auch genutzt ([fork](https://de.wikipedia.org/wiki/Fork)) werden.

<!-- ![Screenshot GitLab OpenLab Overview, von Manfred Steger](https://gitlab.com/flavoursofopenscience/openlab/blob/master/blog/toolchains/digital-publishing/gitlab-openlab-overview.png)
Manfred Steger, "GitLab OpenLab Overview" [CC 0](https://creativecommons.org/publicdomain/zero/1.0/deed.de)
-->

Diese Toolchain bezieht sich auf [GitLab.com](https://about.gitlab.com/), da dieser Anbieter seine Software auch als [open source software](https://de.wikipedia.org/wiki/Open_Source) zur Verfügung stellt. Für kleine Teams und im privaten Bereich kann GitLab sogar mit einem Minicomputer, wie z.B. dem [RaspberryPi 3](https://de.wikipedia.org/wiki/Raspberry_Pi), für weniger als 50 Euro selbst zusammengestellt werden.

### GitLab Repositorium  {#repo}

Der erste Schritt ist es, ein öffentliches Repositorium, folglich kurz _Repo_ genannt, bei [GitLab.com](https://about.gitlab.com/) anzulegen.

> Kleinteilige Schritte und Anleitungen können aus der [Workshop Dokumentation: Einführung, Teamkommunikation und -kollaboration in GitLab](https://gitlab.com/flavoursofopenscience/openlab/blob/master/workshops/gitlab-introduction/script/gitlab-script-introduction.md) entnommen werden.

#### Erstellen  {#erstellen}

1. Erstellt ein kostenloses Konto auf [GitLab.com](https://gitlab.com/users/sign_in).
2. Meldet euch anschließend mit euren Benutzerdaten an.
3. Erstellt ein öffentliches [Projekt](https://gitlab.com/flavoursofopenscience/openlab/blob/master/workshops/gitlab-introduction/script/gitlab-script-introduction.md#projects) mit einem einfachen und prägnanten Projektnamen. Beachtet dabei die Schreibweise und verwendet folgendes Format: `uebung-macht-den-meister`.

Nach dem Erstellungsvorgang erscheint ein leeres Repo.

#### Einrichten  {#einrichten}

Damit das Repo genutzt werden kann, muss mindestens eine Datei erstellt werden. Auf der Startseite des Repos wird automatisch die `Datei README.md` visuell dargestellt. Wie dies funktiontiert, erfahrt ihr in den nächsten Schritten.

1. Klickt im rechten Sidebar Menü auf den Button `Overview` und wählt dort den Button `New file` aus.
2. Er erscheint ein "File name"-Eingabefeld, tragt dort `README.md` ein.
3. Darunter folgt ein Online-Editor; dort können Projektinformationen wie z.B. Termine, eine Beschreibung, was dieses Projekt darstellt, usw. eingetragen werden. Die Standardsprache ist hier [Markdown](https://gitlab.com/flavoursofopenscience/openlab/blob/master/workshops/markdown-introduction/script/markdown-script-introduction.md#die-wichtigsten-befehle).
4. Scrollt nun ans Ende der Website und schließt die Dokumenterstellung mit dem Button `Commit changes` ab.
5. Ihr werdet automatisch wieder zurück zur Repo-Startseite (Overview) geleitet und der eingetragene Text wird euch in der Vorschau angezeigt.

### Dateien verwalten aka verteilte Versionsverwaltung  {#verwalten}

GitLab bzw. die Technologie _git_ protokolliert und speichert Änderungen im Repositorium ab und behält diese vorsorglich. Die sog. _history_ wird im `Sidebar-Menü > Repository > Commits` dargestellt. Der Unterschied zu einer normalen Dateiverwaltung ist die Möglichkeit, Änderungen über einen langen Zeitraum hinweg zu überblicken und gegebenenfalls auf einen vorherigen Zeitpunkt _zurückzudrehen_ – im Prinzip wie eine Zeitmaschine. Damit diese Möglichkeit effektiv genutzt werden kann, sollten **keine** Dateien direkt auf der GitLab-Website hochgeladen werden. Im nächsten Schritt erfahrt ihr nun, wie ein Repo auf den lokalen Computer geklont, synchronisiert und im Team kollaborativ genutzt werden kann.

#### Lokales Repositorium anlegen  {#lokal}

Eine verteilte Versionsverwaltung überwacht Dateiänderungen an unterschiedlichen Orten und synchronisiert diese bei Bedarf bidirektional (in beide Richtungen). Bei nicht-binären Dateien (reine Textdateien – keine Office Dokumente) ist es sogar möglich, Änderungen an der selben Datei zu erkennen und diese automatisch zusammenzuführen. Für diesen Vorgang wird ein _git_-fähiges Programm benötigt.

1. Ladet das kostenlose Programm [GitHub Desktop](https://desktop.github.com/) herunter und installiert es für das von euch verwendete Betriebssystem (Win / OSX / Linux).
2. Beendet den Installationsvorgang und überspringt die Anmeldung bei GitHub.com.
3. Wählt anschließend im `Hauptmenü > File > Clone Repository`.
4. Wählt den Reiter `URL` aus.
5. Navigiert im Browser auf GitLab.com und wählt im gewünschten Projekt `Sidebar Menü auf Overview` aus. Kopiert den `HTTPS-Link`. Anstelle von `HTTPS` kann auch `SSH` stehen, in diesem Fall `SSH` anklicken und im Drop-down-Menü `HTTPS` auswählen.
6. Kopiert den Link des Repositoriums nach GitHub Desktop in das Feld `URL or username/repository`.
7. Wählt noch einen Speicherort unter Local Path aus. Es empfiehlt sich, einen Hauptordner mit dem Namen **GitLab** anzulegen, um dort bei Bedarf weitere Repos an einem Ort unterzubringen. Bedenkt, dass eine nachträgliche Speicherort-Änderung nachträglicher Anpassung in GitHub Desktop bedarf.
8. Bestätigt den Kopiervorgang mit einem Klick auf den Button `Clone`.
9. Ihr werdet nach den GitLab.com-Benutzerdaten gefragt; gebt diese ein und bestätigt die Eingabe.
10. Anschließend werden alle Online-Inhalte an den angegebenen Speicherort kopiert.

#### Änderungen synchronisieren  {#sync}

Der Vorteil von GitHub Desktop besteht darin, dass alle Inhalte im geklonten Ordner automatisch überwacht werden. Sobald sich etwas an einer Datei ändert, neue Dateien hinzukommen oder Dateien gelöscht bzw. verschoben werden, merkt sich die _git-Technologie_ diese Veränderung. Bei reinen Textdateien werden lediglich die veränderten Passagen vermerkt und abgespeichert. In den folgenden Schritten werden wir nun den Synchronisierungsvorgang exemplarisch durchführen.

> Tipp: Ein einfaches Beispiel zeigt den Mehrwert dieser Technologie. Ein von uns geschriebener Artikel wurde während seiner Entstehung 100 Mal abgespeichert und bei jedem Speichervorgang synchronisiert. Auf GitLab gibt es in der _history_ 101 commits, wobei wir jeden Speicherpunkt nachträglich ansehen können. Würden wir dasselbe mit einem binären Format, wie z.B. einem Office Dokument (.odt oder .docx) machen, lägen 101 Dokumente auf dem Server und die _history_ könnte die jeweiligen Textänderungen nicht darstellen. Ein weiterer Nachteil wäre die entstandene Dateimenge, die bei einem 2-Megabyte-großen Office Dokument insgesamt 404 Megabyte auf der Festplatte ausmachen würde. Die abgespeicherten Versionen/Referenzen einer Textdatei sind weitaus kleiner und liegen bei 101 Speichervorgängen unter 1 Megabyte.

1. Navigiert auf dem Computer zum angelegten Projektordner.
2. In diesem Ordner liegt mindestens eine Datei, die wir bereits angelegt haben, nämlich `README.md`.
3. Öffnet die Datei mit einem Doppelklick.
4. Nehmt eine Änderung vor: Fügt zum Beispiel entweder Text hinzu oder löscht etwas.
5. Speichert die Datei ab; passt dabei auf, dass die Dateiendung (Dateiformat) auf `.md` bleibt und nicht zum Beispiel .docx daraus gemacht wird.
6. Öffnet oder wechselt zu GitHub Desktop.
7. Im linken `Menü > Changes` wird nun die Datei `README.md` angezeigt.
8. Rechts neben dem Dateipfad symbolisiert entweder ein grünes Zeichen, dass die Datei hinzugefügt, ein oranges Zeichen, dass die Datei verändert oder ein rotes Zeichen, dass die Datei entfernt wurde.
9. Klickt nun auf den Dateipfad im `Menü > Changes`. Nun wird euch die Textdatei im rechten Fenster angezeigt und die Änderungen mit grüner und roter Hinterlegung hervorgehoben.
10. Navigiert nun ins Feld `Summary` und tragt dort eine kurze Zusammenfassung ein. Diese wird beim Synchronisierungsvorgang mitgesendet und wird euch später in der _history_ als Titel angezeigt.
11. Klickt anschließend auf den Button `Commit to master`.
12. Im oberen Repository-Menü wechselt der Button `Fetch origin` zu `Push origin`. Klickt nun auf `Push origin`, um die Synchronisierung zu starten.
13. Wechselt nun wieder zum Online-Repository; dort sind nach erfolgreichem Abgleich die Dateien auf dem neuesten Stand.

> Tipp: Zu jedem Arbeitsbeginn empfiehlt es sich, GitHub Desktop zu öffnen und den Button `Fetch origin` zu klicken. Alle Änderungen aus dem Online-Repositorium werden dann mit dem lokalen Ordner abgeglichen. Zudem sollten Änderungen nicht zu lange (Tage) lokal verändert liegen bleiben, da das Risiko größer wird, dass die Datei von z.B. einem anderen Nutzer verändert wird. Synchronisiert also in regelmäßigen Abständen; somit entsteht eine feinsäuberliche Dokumentation der erledigten Arbeit bzw. Teilarbeit.

## Digitales Publizieren  {#digital-publishing}

Ein Dokument hat mittlerweile viele Gesichter – es kann als gedruckter Artikel in einer Zeitschrift, als Blogbeitrag in einem Online-Magazin, in einem Sammelband als Buch bzw. im EPUB-Format auf unterschiedlichen digitalen Endgeräten dem Nutzenden zur Verfügung stehen. Unterschiedliche Ausgabemedien erfordern mehrere unterschiedliche Dokumenttypen bzw. Formate – viele Dokumente bedeuten mehr Arbeitsaufwand beim Erstellen bzw. Abgleichen. In diesem Abschnitt wird gezeigt, wie ein zentrales Dokument mit einer einheitlichen Auszeichnungssprache für mehrere Anwendungszwecke genutzt werden kann, damit redundante Inhalte nicht entstehen und somit die Effiktivität erhöht wird und sich zugleich Fehler beim Abgleichen auf ein Minimum reduzieren lassen.

### Einheitliche (Dokumenten-) Sprache  {#auszeichnungssprache}

Ein zentrales Dokument ist der Ausgangspunkt für das digitale _crossmedia publishing_ und eine einfache Auszeichnungssprache der Schlüssel zum Erfolg. Bereits beim Einrichtes des GitLab-Repo haben wir eine solche Datei mit der passenden Sprache angelegt: die `README.md`. Die Dateiendung `.md` steht für `Markdown`, eine Auszeichnungssprache, die während des Formatierens bereits 'menschenlesbar' gestaltet ist. Eine ausführliche Erklärung erhalten Sie in der [Workshop Dokumentation: Markdown – Ein Format für Vieles](https://gitlab.com/flavoursofopenscience/openlab/blob/master/workshops/markdown-introduction/script/markdown-script-introduction.md#markdown-ein-format-f%C3%BCr-vieles). In diesem Abschnitt erlernt ihr die wichtigsten Befehle und den Abgleich mit GitLab.

**Die wichtigsten Markdown-Befehle im Überblick**

| Markdown Befehl  | Ergebnis  |
|---|---|
| \# Überschrift 1  | **Überschrift 1**  |
| \#\# Überschrift 2  | Überschrift 2  |
| \#\#\# Überschrift 3  | Überschrift 3  |
| \*Kursiv Auszeichnung\*  | *Kursivauszeichnung*  |
| \*\*Fett Auszeichnung\*\*  | **Fett Auszeichnung**  |
| \- Listenelement unsortiert  |  • Listenelement unsortiert |
| 1. Listenelement sortiert  |  1. Listenelement sortiert  |

> Tipp: In diesem [Online Tutorial von CommonMark.org](http://commonmark.org/help/tutorial/) können die wichtigsten Markdown-Befehle als **SOL** (**s**elbst**o**rganisierte **L**erneinheit) in wenigen Minuten interaktiv erlernt werden.

> Tipp: Eine gute Übersicht aller Markdown-Befehle findet ihr über die [nutzerfreundliche Suchmaschine](https://donttrack.us/) [Duckduckgo.com](https://duckduckgo.com/). Gebt als Suchbegriffe `markdown cheatsheet` ein. Es wird direkt eine Übersicht eingeblendet, die über den Button `show more` erweitert werden kann.

### Dokumente erstellen und abgleichen  {#dokumente-erstellen}

#### Online  {#online}

1. Navigiert in euer Repo auf GitLab.com.
2. Klickt im rechten Sidemenu auf den Menüpunkt **Repository** und anschließend rechts neben `master` auf das `Pluszeichen > New file`.
3. Tragt bei `File name` den gewünschten Dokumentnamen mit der Endung `.md` ein.
4. Im Editorfenster können nun Markdown-Befehle eingefügt werden.
5. Nehmt nun ein Kapitel aus einem eigenem Text (kein Copyright-geschütztes Material) und formatiert es mit den erlernten Markdown-Befehlen.
6. Tragt bei `commit message` eine kurze Beschreibung ein, wie z.B. `Artikel digital publishing angelegt`.
7. Klickt auf den Button `Commit changes`.
8. Öffnet GitHub Desktop und klickt den Button `Fetch origin`.
9. Navigiert auf dem Computer zum Repo-Ordner und überzeugt euch davon, dass die online erstellte Datei inklusive Inhalt vorhanden ist.

#### Offline  {#offline}

Der Online-Editor sollte nur in Ausnahmefällen benutzt werden, z.B. wenn ein Fremdgerät genutzt wird. Generell sollten lokale Dateien im Repo mit funktionstüchtigen Offline-Editoren bearbeitet werden.

**In dieser Liste finden Sie unterschiedliche OpenSource (kostenlose) Markdown-fähige Programme:**

- [MacDown](https://macdown.uranusjr.com/) (Mac)
- [Typora](https://typora.io/) (Win/Linux/Mac)
- [Bear Writer](http://www.bear-writer.com/) ([IOS](https://itunes.apple.com/us/app/bear-beautiful-writing-app/id1016366447?ls=1&mt=8)/[Mac](https://itunes.apple.com/us/app/bear-beautiful-writing-app/id1091189122?ls=1&mt=12))
- [MarkdownX](https://play.google.com/store/apps/details?id=com.ryeeeeee.markdownx) (Android)
- [Atom Editor](https://atom.io/) (Profi-Tool Win/Linux/Mac)

1. Installiert einen Editor passend zum Betriebssystem.
2. Öffnet den Editor und ladet eine Markdown-Datei aus dem Repo.
3. Nehmt die Änderungen vor und speichert diese über den Editor.
4. Führt mit GitHub Desktop einen **commit** aus und synchronisiert diesen mit dem Online-Repo.

#### Kollaboration  {#kollaboration}

Damit weitere Personen am gleichen Artikel schreiben oder z.B. parallel Medieninhalte wie Bilder einfügen können, müssen diese dem Projekt hinzugefügt werden.

1. Navigiert in euer Repo auf GitLab.com.
2. Klickt im rechten Sidemenu auf den Menüpunkt **Settings > Members** und sucht nach dem bereits registrierten GitLab.com-Nutzenden im Feld `Select members to invite`.
3. Nehmt im Feld `Choose a role permission` die Berechtigkeitseinstufung vor. Eine detaillierte Übersicht der unterschiedlichen Berechtigungen und deren Auswirkungen befindet sich in der [offiziellen GitLab.com Dokumentation](https://docs.gitlab.com/ee/user/permissions.html#project-members-permissions).
4. Wählt nun mindestens das Recht `Developer` aus; für eine _einfache_ Zusammenarbeit auf Augenhöhe (gleiche Rechte) wählt die Rolle `Master` (empfohlen) aus. **Diese Einstellung ist für den textbasierten wissenschaftlichen Handlungsalltag und zur Reduzierung der Komplexizität für GitLab-Neulinge zielführend. Im IT-Bereich wäre diese laissez-faire Rollenvergabe undenkbar.**
5. Klickt auf den Button `Add to project`.

KollegInnen und auch die Öffentlichkeit können somit am Projekt teilhaben. Je nach Berechtigungsstufe können sie das Repo herunterladen, Änderungen einreichen, Teammitglieder und Aufgaben verwalten.

> Tipp: Wer völlig neu im GitLab-Universum ist, sollte zunächst allein Erfahrungen sammeln und danach mit wenigen vertrauten Mitgliedern Rechte und Rollen erproben. Achtet besonders bei unbekannten Mitgliedsanfragen auf die Rechtevergabe und wählt bei Ungewissheit die Rolle `Reporter`.

> Tipp: Erklärt neuen Mitgliedern die Verfahrensweise, wie bereits im Kapitel **Dateien verwalten aka verteilte Versionsverwaltung** dargestellt ist. Dies fördert das eigene Verständnis, da bei Rückfragen das eigene Handeln betrachtet, erneut überdacht und mit eigenen Worten erklärt werden muss.

### Dokument als Online-Artikel publizieren  {#dokument-publizieren}

Vor Beginn des folgenden Abschnitts stellt sich erst einmal die Frage: Was unterscheidet das erstellte Dokument im GitLab.com-Repo von einem Online-Artikel?

> Darstellungsform / Gestaltung

Auszeichnungssprachen legen die Struktur eines Dokuments fest, jedoch nicht das endgültige Aussehen. Die Darstellung der Inhalte auf der GitLab.com-Plattform ist für Arbeitsformen optimiert und nicht für den Endnutzer eines Artikel, den Leser, bestimmt. Für die verbesserte Anzeige des Artikels aus Nutzendensicht (usability) wird ein Redaktionssystem mit Gestaltungsvorlagen zum Einsatz kommen. Wir haben uns in diesem Szenario für das Blogsystem [WordPress](https://wordpress.org/) entschieden.

> Tipp: Wer bereits über Fortgeschrittenen- oder Expertenwissen im Bereich Content-Management-Systeme besitzt, sollte als mögliches Front- und Backend [Grav CMS](https://getgrav.org/) in Betracht ziehen, da es einige Synergieeffekte in Zusammenhang mit GitLab aufweist.

#### WordPress im Zusammenspiel mit GitLab  {#wordpress-gitlab}

Die Erstellung und Einrichtung eines WordPress-Blogs wird hier nicht weiter erklärt. In unserer [WordPress-Workshop-Dokumentation](https://gitlab.com/flavoursofopenscience/openlab/blob/master/workshops/wordpress-introduction/script/wordpress-script-introduction.md) findet ihr einige wichtige Hinweise dazu. Die Grundvoraussetzung für das weitere Vorgehen ist eine _eigene WordPress-Installation_ (kostenlos) von [WordPress.org](https://wordpress.org/) auf einem Webspace / lokalem Webserver oder ein _business subscription plan_ (kostenplichtig) von [WordPres.com](https://wordpress.com/pricing/), da wir _third party plugins_ für die Kommunikation zwischen WordPress und GitLab benötigen.

1. Ladet das Plugin [wp-gfm](https://github.com/makotokw/wp-gfm) über den Button `Clone or download` von GitHub.com herunter. Mit dieser WordPress-Erweiterung können Markdown-Dateien im sogenannten _raw_ Auslieferungszustand in WordPress-Artikel eingebettet werden.
2. Installiert das Plugin in WordPress über das `Sidebar-Menü > Plugins > Installieren > Plugin hochladen`.
3. wp-gfm benötigt keine weiteren Einstellungen und ist nach der Installation einsatzbereit.
4. Ladet das Plugin [WP Editor.md](https://wordpress.org/plugins/wp-editormd/) auf WordPress.org herunter.
5. Installiert das Plugin in WordPress über das `Sidebar-Menü > Plugins > Installieren > Plugin hochladen`.
6. Klickt im `Sidebar-Menü > Einstellungen > Schreiben` und setzt den Haken bei der Checkbox `Markdown für Beiträge und Kommentare verwenden.`

#### WordPress-Artikel erstellen  {#artikel-erstellen}

1. Erstellt im WordPress-Backend (Administrationsbereich) über das `Sidebar-Menü > Beiträge > Erstellen` einen neuen Artikel.
2. Vergebt einen Titel wie z.B. in diesem Beitrag `OER – Digital Publishing mit Git und WordPress`.
3. Fügt darunter im Editor den folgenden Embed Code ein: `[embed_markdown url=""]`
4. Wechselt zu GitLab.com in das Repo und navigiert zur gewünschten Markdown-Datei.
5. Klickt z.B. die digital-publishing.md an.
6. Klickt auf den Button `Open Raw`; dieser befindet sich an **fünfter Stelle von rechts neben dem roten `delete`-Button**.
7. Es öffnet sich ein neuer Browser-Tab mit reinem Text. Kopiert die vollständige URL aus der Browser-Adresszeile.
8. Wechselt zurück zu WordPress und fügt zwischen die zwei Anführungszeichen `"URL"` die GitLab Raw URL ein.
9. Dies sieht dann beispielsweise so aus: `[embed_markdown url="https://gitlab.com/manfredsteger/digital-publishing-openlab/raw/master/digital-publishing.md"]`
10. Nehmt euch gegebenenfalls noch die sekundären Einstellungen des WordPress-Artikels vor (Kategorien, Tags, Beitragsbild, usw.) und veröffentlicht den Artikel mit dem Button  `Veröffentlichen`.

### Zwischenfazit  {#zwischenfazit}

#### Vermeidung von negativen Effekten  {#negative-effekte}

**Casus Knacksus Datenbank**

Der Artikel steht nun im Frontend (Besucheransicht) bereit. Die Toolchain, für einen möglichen digital Publikationsprozess, ist damit abgeschlossen. Für sich betrachtet ein erheblicher Aufwand für das Erstellen eines einfachen _blog post_. Unter Berücksichtigung weiterer Faktoren nähern wir uns jedoch schnell dem _[break even](https://de.wikipedia.org/wiki/Gewinnschwelle)_ an. Die größte Rolle spielt dabei die WordPress Datenbank, die neben Inhalte, eben auch essenzielle Systeminformationen speichert. Im Falle eines defekts oder eines Hackerangriffs ist das ganze System betroffen (z.B. SQL-Injection) und oftmals nur durch eine komplette Neuinstallation des Systems oder der Datenbank zu beheben. Ohne Backup sind die Daten meist verloren oder nur mit kostenintensiven Bereinigungsprozessen durch Fachpersonal wiederherzustellen.

**Das Datenbank Dilemma**

Moderne Schadsoftware schreibt nicht nur schädlichen Code in Systemdateien und installiert Skripte, die z.B. für Phishing Zwecke fungieren, sondern verunreinigen auch Inhalte aller posts mit Links und Textpassagen. Fachpersonal kann Schadsoftware anhand der Code-Signatur erkennen, jedoch nicht kontextualisiert feststellen, ob der Text eines Beitrags gewünscht oder verunreinigt ist. Tritt dieser Fall ein, müssen alle Inhalte erneut durchforstet und neu aufbereitet werden.

Bei dieser Toolchain wäre der schlimmste Fall, dass das System neu aufgesetzt und die einzelnen Posts mit Titel und Embed Code neu eingestellt werden müssen. Die Inhalte sind extern gehostet und demnach vor Veränderungen gesichert.

#### Profitieren von neuen Möglichkeiten  {#profitieren}

In der voranschreitenden digitalen Welt ist Sicherheit ein zunehmender Faktor. Daher die Entscheidung im Zwischenfazit mit einem Negativbeispiel zu starten.

**Seriell vs simulatan**

Meines Erachtens viel wichtiger als negative Randeffekte sind die neuen Möglichkeiten und Formen der Zusammenarbeiten, die mit dieser Toolchain entstehen. Kollaboration ist mit WordPress möglich. Das Redaktionssystem gewährt mit hierarchischer Rollen- und Rechtevergabe **Person A** das Schreiben eines Artikels, der **anschließend** von **Person B** redaktionell begleitet und **danach** von **Person C** zurückgewiesen oder veröffentlicht wird. Dieser serielle Arbeitsprozess birgt große Schwachstellen, da erst nach Vollendung eines Arbeitsschrittes der nächste folgen kann. Wie ist nun aber der Unterschied zur Toolchain mit GitLab?

**Person A** schreibt an einem Artikel im lokalen Repo und synchronosiert in kurzen Zeitabständen die Inhalte. Während **Person A** **gerade eben** schreibt, kann **Person B** **simultan** z.B. ein Lektorat vornehmen. **Person B** speichert die Änderungen ab und synchronisiert die Änderungen mit GitLab. Speichert und synchronisiert nun **Person A** die Änderungen zeigt z.B. GitHub Desktop an, dass neue Änderungen im Repo vorhanden sind. Die Änderungen von **Person B** werden in die aktuelle Version übernommen, ein sog. `merge`. Im Idealfall muss **Person A** die zusammengeführten Änderungen in GitHub Desktop mit einem `commit` ins online Repositorium übertragen. Wurde hingegen ein und dieselbe Textzeile von beiden Personen gleichzeitig geändert, kann das System nicht entscheiden, welche Version nun die Richtige ist. In diesem Fall wird ein `merge conflict` angezeigt. Dieser Fehler kann nur manuell behoben werden.

**Volle Transparenz und Nachvollziehbarkeit**

Betrachtet man nun die beiden Arbeitsweisen wird deutlich, dass mit der Toolchain eine neue Form der Kollaboration zustande kommt. Für redaktionelle Zwecke gibt es noch einen weiteren Vorteil. Jeder _commit_ wird in der _history_ in GitLab gespeichert. Wird bei einer Textdatei etwas verändert, speichert _git_ die Veränderungen ab und eben nicht ein neues Dokument. Diese Veränderungen werden systematisch gespeichert und können nachträglich mit vorherigen Varianten verglichen werden, die sog. *Blame-Funktion*. Diese Funktion kann bei jeder Datei im online Repo über den `Button: Blame` aktiviert oder in GitHub Desktop unter dem `Reiter: History` für den jeweils zuvor gespeicherten Stand angezeigt werden. WordPress bietet diese Funktionen mittlerweile auch, die sog. `Revisionen`. Nachteil der WordPress Versionen besteht jedoch darin, dass die Datenbank mit jedem Speicherpunkt wächst. Sowohl bei WordPress als auch bei GitLab besteht die Möglichkeit im Nachhinein alte Versionen aufzurufen und daraüberhinaus auch einzusehen, wer hat diese Änderungen gemacht.

Ein einfaches Beispiel zeigt welche Möglichkeiten und Vorteile dieses Verfahren beinhaltet. `Person A, B, C und D` schreiben gemeinsam an einem Zeitschriften Artikel. Es steht ein Lektorat von `LektorIn A` an. Die LektorIn findet eine widersprüchliche Aussage in Textzeile 340 und weiß nicht, ob diese beabsichtigt ist. Die LektorIn hat eine zusammengeführtes Office Dokument erhalten, bei dem nicht ersichtlich wird, wer für welchen Absatz verantwortlich ist. Sie verfasst eine E-Mail an alle beteiligten Personen, die nun folgendes tun:

- **Person A und B** öffnen die E-Mail und lesen diese.
- **Person A und B** suchen die zutreffende Stelle und wissen, das **Person C** zuständig ist.
- **Person A** schreibt **LektorIn A** eine E-Mail, dass **Person C** zuständig ist.
- **Person B** hat die E-Mail von **Person A** noch nicht zur Kenntnis genommen und schickt mit der E-Mail-Funktion `allen antworten` **Person A und C** sowie **LektorIn A** eine E-Mail, dass **Person B** dafür verantwortlich sei.
- **Lektorin A** erhält zwei E-Mails die sie lesen muss und antwortet mit der E-Mail-Funktion `allen antworten`, dass sie nun auf die Rückmeldung von **Person C** warten wird.
- **Person C** erhält nun 4 E-Mails, liest diese und antwortet **nur LektorIn A**.
- Kurze Zeit später fragt sich **Person A** ob das Problem geklärt wurde und verfasst eine E-Mail an alle Beteiligte.
- _to be continued_ ...

Über die _history_ Funktion wird die Kommunikation automatisch geschrieben und im oben dargestellten Beispiel könnte die **LektorIn A** zurückblättern, bis die betroffene Stelle erscheint und selbstständig nachvollziehen wer dafür verantwortlich ist.

#### Kultur des Schreibens  {#kultur-des-schreibens}

Das kollaborative Arbeiten hat zu Beginn einige Schwierigkeiten, die es zu überwinden gilt. Ähnlich wie beim Lernen einer neuen Sprache wird der Wortschatz zu Beginn aufgebaut. Im Verlauf ist der wesentliche und ausschlaggebende Faktor aber die Grammatik. Im übertragenem Sinne reicht es aus, den commit und sync Knopf zu kennen, jedoch gehen ohne Regeln viele zusätzliche Bonis verloren.

_Am Beispiel der OpenLab Blogbeiträge kann dies verdeutlicht werden._

Jede Markdown Datei hat einen Header mit Informationen, die auf WordPress eingetragen wurden wie z.B. der Titel (Freie Ausmalbilder im Handumdrehen selbst erstellen) und Untertitel (Mal ganz kreativ. Motive suchen und ein Ausmalbild daraus erstellen.) sowie der geschätzte Zeitaufwand für die Lerneinheit (20 - 30 Minuten) und die Tags.

```
<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Freie Ausmalbilder im Handumdrehen selbst erstellen

## Mal ganz kreativ. Motive suchen und ein Ausmalbild daraus erstellen.

> geschätzer Zeitaufwand: 20 - 30 Minuten

Tags: Umrissbilder, OpenClipart, CC Search, Ausmalbilder Vorlage, KindOERgarten, Pixlr, DIN A4, 4Teachers, Online Photo Editor, LibreOffice

## User Story ([Persona Fin Bonde](https://git.universitaetskolleg.uni-hamburg.de/synlloer/oer-know-how/blob/master/Unterlagen/Personas-03.pdf))

-->

```

Danach folgt das Tutorial und am Ende eines jeden Beitrags steht der zugehörige Lizenztext.

```
<a class="cc-license" rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"><a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/freie-ausmalbilder-im-handumdrehen-selbst-erstellen/" property="cc:attributionName" rel="cc:attributionURL">Freie Ausmalbilder im Handumdrehen selbst erstellen</a></span> von <em><a href="https://www.manfred-steger.de/" target="_blank">Manfred Steger</a></em> für das Universitätskolleg der Universität Hamburg ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">CC BY 4.0 International Lizenz</a>.
```

Wird nun ein Beitrag über GitLab oder WordPress betrachtet stehen jeweils alle Informationen zur Verfügung. Gehen z.B. durch eine defekte Datenbank in WordPress Informationen verloren, sind diese über das Dokument immer noch vorhanden. Darüberhinaus können öffentliche Repositorien mit einem sog. **fork** in neue Projekte einfließen und alle Informationen werden in den Ableger integriert.

## Lizenz

<a class="cc-license" rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"><a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/oep-digital-publishing-mit-git-und-wordpress/" property="cc:attributionName" rel="cc:attributionURL">OER entwickeln und bereitstellen mit Git und WordPress</a></span> von <em><a href="https://www.manfred-steger.de/" target="_blank">Manfred Steger</a></em> für das Universitätskolleg DIGITAL der Universität Hamburg ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">CC BY 4.0 International Lizenz</a>.


<!-- ### Gliederung und Alternativen

1. Zentrale Dateiverwaltung
  - **[GitLab](https://about.gitlab.com/)**
  - [GitHub](https://github.com/)
2. Verteilte Versionsverwaltung
  - **[GitHub Desktop](https://desktop.github.com/)**
  - [GitKraken](https://www.gitkraken.com/)
3. Auszeichnungssprache
  - **[Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)**
4. Blogsystem mit git Raw Plugin
  - **[WordPress](https://wordpress.org/)**
    - **[wp-gfm](https://github.com/makotokw/wp-gfm)**
    - **[WP Editor.md](https://wordpress.org/plugins/wp-editormd/)**
5. Offline Multidokumentenformate Editor
  - **[Atom](https://atom.io/)**
    - **[Plugin: Language Markdown](https://atom.io/packages/language-markdown)**
    - **[Plugin: Pandoc Convert](https://atom.io/packages/pandoc-convert)**
    - **[Plugin: Project Manager ](https://atom.io/packages/project-manager)**
  - [MacDown](https://macdown.uranusjr.com/)
  - [Notepad++](https://notepad-plus-plus.org/)
-->
