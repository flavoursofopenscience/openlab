<!-- Titel, Intro Text and Reading Time in Wordpress for display in tiles

# Schulhefte lebt wohl, digitales Lerntagebuch Ahoi!

## Mit einem Blog das Reisen und Lernen verbinden. Wir zeigen Euch wie integrierte Mediendidaktik aussehen kann.

> geschätzer Zeitaufwand: 40 - 60 Minuten

> Schwierigkeitsgrad: 3/5 Punkte

Tags: Blog, WordPress, blogger.com, Lerntagebuch, online Portfolio, Disqus Plugin Kommentare, Monosnap, Screencast, Vimeo, Bildschirmaufnahme, H5P

### User Story ([Persona Louisa Smith](https://git.universitaetskolleg.uni-hamburg.de/synlloer/oer-know-how/blob/master/Unterlagen/Personas-03.pdf))

-->

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/CY9Ju79vqms" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## Inhaltsverzeichnis

- [Registrieren eines Blogs auf WordPress](#registrieren-eines-blogs-auf-wordpress)
- [Erster Blogeintrag](#erster-blogeintrag)
 - [Beitragsbild festlegen](#beitragsbild-festlegen)
 - [Beitrag veröffentlichen](#beitrag-veröffentlichen)
- [Profil aktualisieren](#profil-aktualisieren)
- [Theme wählen](#theme-waehlen)
- [BenutzerInnen hinzufügen](#benutzerinnen-hinzufuegen)
- [Monosnap nutzen](#monosnap-nutzen)
 - [Bildschirmaufnahme starten](#bildschirmaufnahme-starten)
 - [Bildschirmaufnahme beenden und speichern](#bildschirmaufnahme-beenden-und-speichern)

![Die Illustration stellt die Persona Louisa Smith dar. Die Illustration hat im Hintergrund einen hellgrauen Kreis, vor dem der Rest der Darstellung teilweise die Kreisränder überlappt und damit den verfügbaren Raum stärker ausfüllt. Die Persona zeigt den Oberkörper einer dunkelhäutigen Frau, die einen orangfarbenen geschlossenen Cardigan trägt und auf ein Tablet schaut, das sie vor sich in der Hand hält, während sie mit der anderen Hand darauf tippt. Das Tablet hat ein dunkelgraues Display und einen hellblaugrau Rahmen. Neben dem Gesicht der Frau schweben zwei gegenläufige Sprechblasen in hellbraun, unter denen vier waagerechte Striche in gleicher Breite gezeichnet sind. Die Frau hat kurze schwarze Haare, Mund und Nase sind im Profil zu erkennen, das eine Auge im Profil dunkelbraun als Augenhöhle angedeutet. Der Gesichtsausdruck der Frau wirkt konzentriert.    Bildbeschreibung:  Die Illustration stellt die Persona Annie Li dar. Die Illustration hat im Hintergrund einen hellgrauen Kreis, vor dem der Rest der Darstellung teilweise die Kreisränder überlappt und damit den verfügbaren Raum stärker ausfüllt. Die Persona zeigt den Oberkörper einer hellhäutigen Frau, die einen hellgrünen Blazer über einem weißen Shirt trägt und vor sich ein Blatt Papier hält, während sie vor einem aufgeklappten Notebook an einem angedeuteten Tisch sitzt. Die Tastatur des Notebooks und das Papier sind taubenblau, der Rest des Notebooks hellblaugrau und die Tischoberfläche mittelgrau. Die Frau hat schwarze schulterlange Haare, der Mund ist geschlossen und genau wie Nase und Augen nur angedeutet. Die Frau schaut auf das Papier in ihren Händen.](https://gitlab.com/flavoursofopenscience/openlab/raw/master/blog/personas/uhh-personas-louisa-smith/uhh-personas-louisa-smith.png)

Louisa ist Englischlehrerin und möchte, dass ihre Lernenden während des Schüleraustauschs ein Lerntagebuch führen, um von Aktivitäten zu berichten und Beiträge von anderen zu kommentieren. Bisher geschah dies in Heften, die jedoch manchmal verloren gingen oder beschädigt wurden. Privat führt Louisa ein kostenpflichtiges digitales Reisetagebuch und wünscht sich jetzt eine kostenfreie Alternative für die Klasse sowie eine Anleitung dazu, um den Einstieg für die SchülerInnen einfach zu gestalten.

## Lösungsansatz

Louisa braucht für das Online-Lerntagebuch ein Werkzeug, das Beiträge chronologisch sammeln und darstellen kann, wie das z. B. bei einem [Blog](https://de.wordpress.org/) der Fall ist. Öffentliche Lernvideos zur jeweiligen Software können mit entsprechenden Stichworten (Keywords) bei Vimeo oder YouTube in hoher Qualität und reichlicher Auswahl gefunden werden, z.B. ```WordPress, Einführung, Artikel, Schreiben```. Mit einer [Screen Recording Software](https://monosnap.com/welcome) können Vorgaben, wie z. B. der individuell gewünschte Aufbau der Texte, für die Lernenden dokumentiert und auf einer Videoplattform zur Verfügung gestellt werden.

## Lernziele

- Bedürfnisse und Gewohnheiten der SchülerInnen erkennen, die tagtäglich von aktuellen Medien Gebrauch machen
  - Smartphone
  - soziale Medien (SnapChat, WhatsApp, Telegram, Twitter, Instagram, ...)
- Tools für die Erstellung von Anleitungen kennenlernen und anwenden können
- bereits vorhandene Video-Anleitungen gezielt suchen und finden
- vorhandene Video-Anleitungen in das eigene Tutorial rechtssicher einbinden können

## Schritt-für-Schritt-Anleitung

>Antrags-Tipp 1: Angehörige der Universität Hamburg können über die UHH-Blogfarm einen eigenen, durch die Uni gehosteten Blog erhalten, der noch mehr kann als das Angebot bei wordpress.com. Wenn ihr eine E-Mail-Adresse habt, die mit `@studium.uni-hamburg.de`, `@public.uni-hamburg.de` oder `@uni-hamburg.de` endet, könnt ihr ganz einfach über die Seite der Blogfarm eine eigene WordPress-Instanz beantragen (und könnt diesen ersten Schritt der Anleitung überspringen und unter "Erster Blogeintrag" weitermachen). Weitere Details zur UHH-Blogfarm & Online-Antrag unter: [www.blogs.uni-hamburg.de](https://www.blogs.uni-hamburg.de/)

>Antrags-Tipp 2: Studierende und Mitarbeitende anderer Universitäten haben oft auch ein entsprechendes Angebot von durch die Hochschule bereitgestellten Blogs. Fragt dazu einfach mal bei Euren entsprechenden E-Learning-Einrichtungen bzw. Einrichtungen für digitale Medien nach.

### Registrieren eines Blogs auf WordPress.com {#registrieren-eines-blogs-auf-wordpress}

Um ein Lerntagebuch auf **WordPress** zu erstellen, muss zu Beginn ein Benutzerkonto auf [WordPress.com](https://de.wordpress.com/) angelegt werden. Mit Klick auf den Button `Jetzt beginnen` kommt ihr auf die Registrierungsseite, auf der einige Angaben zu eurer zukünftigen Webseite eingetragen werden müssen. Diese Webseite wird in Zukunft euer Lerntagebuch sein, auf dem ihr und andere BenutzerInnen Beiträge erstellen und gegenseitig kommentieren könnt. Sobald alles ausgefüllt ist, geht es mit Klick auf den Button `Fortfahren` weiter zu der Erstellung der Webadresse. Hier müsst ihr den Wunschnamen der Webseite oder Stichwörter, die sie gut beschreiben, eingeben. Nachdem der Name eingegeben ist, könnt ihr die erste vorgeschlagene Webadresse auswählen, die mit **Free** gekennzeichnet ist. Auch auf der folgenden Seite wählt ihr den Button `Beginne mit Free` aus. Abschließend muss noch eine E-Mail-Adresse eingegeben werden, über die ihr die Webseite laufen lassen möchtet, genauso auch einen Benutzernamen sowie ein Passwort. Nachdem ihr auf den Button `Fortfahren` und  auf den via E-Mail versendeten Registrierungslink geklickt habt, kann es mit der Gestaltung eurer Webseite auch schon losgehen.

### Erster Blogeintrag {#erster-blogeintrag}

WordPress gibt euch zu Anfang hilfreiche Hinweise. So erscheint mittig ein Schriftzug mit der Aufschrift **Erster Blogeintrag** - wenn dieser geklickt wird erscheint ein Erklärungstext, in dem der Satz "beginne einen neuen Beitrag" angeklickt werden kann. Daraufhin wird ein neuer Beitrag geöffnet und es kann der Titel, das Titelbild und der Inhalt des Beitrags eingefügt werden. In der ersten Spalte müsst ihr den Titel des Eintrags und in der zweiten Spalte den Inhalt eingeben. Mit dem Button `Hinzu` <i class="fas fa-plus-circle"></i> könnt ihr weitere Inhalte wie z.B. Bilder einfügen.

>Tipp: Mit Klick auf den <i class="fab fa-wordpress"></i> - Button oben links, könnt ihr unter dem Menüpunkt **Verwalten** auch einen neuen Beitrag hinzufügen.


#### Beitragsbild festlegen {#beitragsbild-festlegen}

Möchtet ihr ein Beitragsbild einstellen, müsst ihr in der rechten Menüleiste auf **Beitragsbild** klicken und mit Klick auf den Button `Beitragsbild festlegen` entweder ein Bild von eurem lokalen Speicher auswählen oder über eine Webadresse hochladen. Für die erste Option muss lediglich auf den Button `Hinzufügen` <i class="far fa-image"></i> geklickt und das gewünschte Bild gewählt werden. Um über die Webadresse ein Bild hochzuladen, muss auf den Pfeil daneben geklickt und darauffolgend die URL des Bildes eingegeben werden.

#### Beitrag veröffentlichen {#beitrag-veroeffentlichen}

Um die Veränderungen zu übernehmen und den Blogbeitrag zu veröffentlichen, müsst ihr rechts im Menü auf `Veröffentlichen` gehen. Bei weiteren Veränderungen könnt ihr darauffolgend auf den Button `Aktualisieren` klicken. Ihr könnt euch außerdem anschauen wie die Webseite für Gäste aussieht, indem ihr den Button `Vorschau` klickt. Rechts oben wird die Bearbeitung mit `Schließen` beendet.

> Tipp: In der rechten Menüleiste kann unter dem Reiter **Status** eingestellt werden, wer die Beiträge lesen darf.

### Profil aktualisieren {#profil-aktualisieren}

Oben rechts befindet sich das Symbol <i class="fas fa-user"></i>, auf das ihr klicken müsst, um Kontoeinstellungen und Profilinformationen zu bearbeiten.

### Theme wählen {#theme-waehlen}

Oben links befindet sich der Button `Meine Webseite` <i class="fab fa-wordpress"></i>, den ihr anklicken müsst, um weitere Anpassungen eurer Webseite vorzunehmen. Links wird sich dann ein Menü mit verschiedenen Unterpunkten öffnen. Um das Design eurer Webseite zu verändern, findet ihr in der Leiste, unter dem Menüpunkt **Personalisieren**, die Spalte **Anpassen**, unter der ihr sogenannte **Themes** findet. Mit diesen Themes könnt ihr über den Aufbau und das Aussehen eurer Webseite entscheiden. Dafür muss man auf `Themes` gehen und unter den verschiedenen Layouts aussuchen, was am besten zur jeweiligen Webseite passt.


### BenutzerInnen hinzufügen {#benutzerinnen-hinzufuegen}

Damit andere Nutzende auch Beiträge verfassen können und ein kollaboratives Lerntagebuch entsteht, müsst ihr in der linken Menüleiste neben dem Reiter **Personen** <i class="fas fa-user"></i> auf den Button `Hinzu` klicken. Daraufhin kann man in der oberen Spalte die E-Mail-Adressen der weiteren Nutzenden eingeben. In der zweiten Spalte, die standardmäßig auf **Follower** eingestellt ist, müsst ihr auswählen, welche Rechte die weiteren Nutzenden auf der Webseite haben. Beim Lerntagebuch, wo jeder seine eigenen Beiträge schreibt, ergibt es Sinn, die Rolle des **Mitarbeiters** auszuwählen. Dieser darf eigene Beiträge verfassen und hochladen, jedoch werden diese erst veröffentlicht, wenn ihr als AdministratorIn das erlaubt. Kommentieren dürfen alle, die Zugriff auf die Beiträge haben.

> Tipp: Unter der Rollenauswahl findet ihr den Schriftzug `Mehr über Rollen erfahren`, auf den ihr klicken könnt, um mehr über die Nutzerrollen und ihre Rechte zu erfahren.

In der letzten Spalte könnt ihr eine Nachricht eingeben, die an alle eingeladenen Nutzenden verschickt wird.


### Monosnap nutzen {#monosnap-nutzen}

Eine einfache Anleitung lässt sich mit einer Aufnahme des Bildschirms machen. Dafür eignet sich das Programm [Monosnap](https://www.monosnap.com/welcome), das ihr über diesen Direktlink herunterladen könnt: [https://www.monosnap.com/welcome](https://www.monosnap.com/welcome). Nachdem der Download und die Installation erfolgreich waren, könnt ihr das Programm auf eurem Rechner öffnen. Sobald das Programm per Doppelklick geöffnet wurde, wird das Monosnap-Logo <i class="fas fa-bolt"></i> in der Menüleiste (MAC) bzw. Taskleiste (Windows) auftauchen.

#### Bildschirmaufnahme starten {#bildschirmaufnahme-starten}

Mit Klick auf das Logo könnt ihr zwischen den Optionen wählen, ein Bildschirmfoto aufzunehmen **(Capture Area)** oder ein Video eures Bildschirms zu erstellen **(Record Video)**. Für die Anleitung eignet sich die zweite Option am besten. Klickt ihr auf `Record Video`, wird sich ein transparentes Fenster öffnen, das ihr mit der Maus an die Stelle verschieben könnt, die ihr aufnehmen wollt. Um die Aufnahme zu bestätigen, müsst ihr auf `Record` <i class="fas fa-video"></i> klicken. Falls ihr jedoch den ganzen Bildschirm aufnehmen wollt, muss auf das Symbol <i class="fas fa-expand-arrows-alt"></i> in der rechten Ecke und dann erneut auf das Logo gedrückt werden, um dort die Option **Start Recording** auszuwählen.

> Tipp: Nachdem ihr **Start Recording** gedrückt habt, wird euch das Programm mitteilen, dass ihr ohne Ton aufnehmt. Falls ihr das ändern möchtet, könnt ihr, nachdem ihr die Option **Record Video** gewählt habt, auf den Button `Settings` gehen und den Ton einschalten. Zudem könnt ihr, falls vorhanden, die Innenkamera des Rechners ein- und ausschalten.

#### Bildschirmaufnahme beenden und speichern {#bildschirmaufnahme-beenden-und-speichern}

Um die Bildschirmaufnahme zu beenden, muss wieder auf den Button `Record` <i class="fas fa-video"></i> gedrückt oder per Klick auf das Logo die Option **Finish Recording** ausgewählt werden. Daraufhin wird ein Fenster geöffnet, in dem ihr euch eure Bildschirmaufnahme anschauen könnt. Im oberen Feld könnt ihr der Bildschirmaufnahme einen Namen geben und in der unteren Leiste könnt ihr den Anfangs- und Endpunkt verschieben. Sobald alles eingestellt ist, müsst ihr den Button `Save` drücken und den Speicherort auswählen. Um das Programm zu beenden, klickt ihr erneut auf das Logo und wählt `Quit`.

## Lizenz

<a class="cc-license" rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title"><a xmlns:cc="http://creativecommons.org/ns#" href="https://openlab.blogs.uni-hamburg.de/schulhefte-lebt-wohl-digitales-lerntagebuch-ahoi/" property="cc:attributionName" rel="cc:attributionURL">Schulhefte lebt wohl, digitales Lerntagebuch ahoi!</a></span> von <em>Sophia Zicari</em> für das Universitätskolleg Digital der Universität Hamburg ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank">CC BY 4.0 International Lizenz</a>.

<!-- ### Gliederung und Alternativen

1. Erfahrungsberichte online stellen
  - [**Wordpress**](https://de.wordpress.org/)
  - [Jimdo](https://de.jimdo.com/blog-erstellen/)

2. Kommentarfunktion
  - [**WordPress**](https://de.wordpress.org/)
  - [Disqus](https://disqus.com/)

3. Videographie von Desktopinhalten / Erstellung einer Anleitung
  - [**Monosnap**](https://monosnap.com/welcome)
  - [iShowUHD](https://www.shinywhitebox.com/ishowu-hd-pro)
  - [OBS](https://obsproject.com/de)

4. Bereitstellung der Lernvideos
  - [YouTube](https://www.youtube.com/)
  - [Jimdo](https://de.jimdo.com/blog-erstellen/)
  - [H5P](https://h5p.org/)
-->
