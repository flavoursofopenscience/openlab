# Einführung, Teamkommunikation und -Kollaboration in GitLab

In dieser Einführung lernen Sie das Projektmanagement Tool GitLab und deren grundliegende Funktionsweise für den Hochschulkontext kennen. Im Fokus stehen dabei Teamkommmunikation und -Kollaboration in eigenen **Projekten** und wie sie dort in sog. **Issues** Aufgaben erstellen und gemeinsam daran arbeiten können. Darüberhinaus werden Sie mit den Grundeinstellungen für das eigene Profil und den Benachrichtigungstypen vertraut gemacht. Diese helfen Ihnen, andere Projekte im Auge zu behalten und sich selbst über deren Stand Benachrichtigungen automatisch einzuholen.

Diese Veranstaltung ist für zwei Zeitstunden ausgelegt. Die Teilnehmenden sollten nach der Veranstaltung eine weitere Zeitstunde für die Nachbereitung der praktischen Übunungen einplanen.

**Voraussetzungen für diesen Kurs:**

- Internetfähiges Endgerät (vorzugsweise Laptop)
- Abgeschlossene Registrierung ([Link zur UHH-GitLab Registrierung](https://git.universitaetskolleg.uni-hamburg.de/users/sign_in))
- Ihre B-Kennung und das zugehörige GitLab Passwort (Erstellung bei der Registrierung)

In dieser Einführung lernen Sie das Projektmanagement Tool GitLab und deren grundliegende Funktionsweise für den Hochschulkontext kennen. Im Fokus stehen dabei Teamkommmunikation und -Kollaboration in eigenen **Projekten** und wie sie dort in sog. **Issues** Aufgaben erstellen und gemeinsam daran arbeiten können. Darüberhinaus werden Sie mit den Grundeinstellungen für das eigene Profil und den Benachrichtigungstypen vertraut gemacht. Diese helfen Ihnen, andere Projekte im Auge zu behalten und sich selbst über deren Stand Benachrichtigungen automatisch einzuholen.

Diese Veranstaltung ist für zwei Zeitstunden ausgelegt. Die Teilnehmenden sollten nach der Veranstaltung eine weitere Zeitstunde für die Nachbereitung der praktischen Übunungen einplanen.

**Voraussetzungen für diesen Kurs:**

- Internetfähiges Endgerät (vorzugsweise Laptop)

- Abgeschlossene Registrierung ([Link zur UHH-GitLab Registrierung](https://git.universitaetskolleg.uni-hamburg.de/users/sign_in))

- Ihre B-Kennung und das zugehörige GitLab Passwort (Erstellung bei der Registrierung)

**Unterlagen für diesen Workshop:**

- [Link zur Präsentation](https://openlab.blogs.uni-hamburg.de/einfuehrung-teamkommunikation-und-kollaboration-in-gitlab/)
- [Link zum Veranstaltungsskript](https://gitlab.com/flavoursofopenscience/openlab/blob/master/workshops/gitlab-introduction/script/gitlab-script-introduction.md)

**Dieser Workshop wird an folgenden Daten angeboten:**

- [Donnerstag, 02.11.17 von 10-12 Uhr](https://synlloer.blogs.uni-hamburg.de/veranstaltung/team-kommunikation-und-kollaboration-mit-gitlab-20171102/)
- [Donnerstag, 30.11.17 von 10-12 Uhr](https://synlloer.blogs.uni-hamburg.de/veranstaltung/team-kommunikation-und-kollaboration-mit-gitlab-20171130/)

**Mindestteilnehmendenzahl: 2 Personen:**

Um [Voranmeldung über das Formular](https://openlab.blogs.uni-hamburg.de/anmeldung-synlloer-hochschul-workshops/) wird gebeten.

---

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)
](http://creativecommons.org/licenses/by/4.0/)

Die Dokumentation **Einführung, Teamkommunikation und -Kollaboration in GitLab** von [Manfred Steger für SynLLOER]((https://gitlab.com/flavoursofopenscience/openlab/blob/master/workshops/gitlab-introduction/script/gitlab-script-introduction.md) ist lizenziert unter einer [CC BY 4.0 International Lizenz](http://creativecommons.org/licenses/by/4.0/).

---

## Fahrplan

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Einführung, Teamkommunikation und -Kollaboration in GitLab](#einführung-teamkommunikation-und-kollaboration-in-gitlab)
	- [Fahrplan](#fahrplan)
	- [Benutzereinstellungen in GitLab](#benutzereinstellungen-in-gitlab)
		- [Registrierung](#registrierung)
		- [Profil](#profil)
		- [Account](#account)
		- [Notifications](#notifications)
	- [Projects](#projects)
		- [Suchen - Beobachten - Mitwirken](#suchen-beobachten-mitwirken)
			- [Suchen](#suchen)
			- [Beobachten](#beobachten)
			- [Favorisieren (star)](#favorisieren-star)
			- [Ableger (fork)](#ableger-fork)
			- [Repository Link](#repository-link)
			- [Download Repository](#download-repository)
			- [Dateiablage Ansicht (Repository view)](#dateiablage-ansicht-repository-view)
			- [Mitwirken](#mitwirken)
	- [Issues](#issues)
		- [Board](#board)
		- [Detailansicht](#detailansicht)
			- [Dokumentieren](#dokumentieren)
			- [Aufgaben übernehmen](#aufgaben-übernehmen)
		- [Kommunikation](#kommunikation)
		- [Referenzieren](#referenzieren)
			- [Interne Referenzen](#interne-referenzen)
			- [Externe Referenzen](#externe-referenzen)
	- [Nützliche Links](#nützliche-links)

<!-- /TOC -->

## Benutzereinstellungen in GitLab

### Registrierung

Folgen Sie dem Link: [GitLab Registrations Universität Hamburg](https://git.universitaetskolleg.uni-hamburg.de/profile) und wählen sie den Tab Register.

![Das Bild zeigt die GitLab Registrierungsseite, die sie über den vorherigen Link erreichen. Sie müssen vom aktiven Tab "Benutzerkennung" auf den Tab "Register" wechseln, um das Eingabeformular für die Registrierung zu erhalten.](https://gitlab.com/flavoursofopenscience/openlab/blob/master/workshops/gitlab-introduction/script/gitlab-registration-tab.png)

Füllen Sie das Formular aus und verwenden Sie ihre Mitarbeiter E-Mailadresse aus. Mit dieser werden Sie automatisch im System erfasst und freigeschalten. Falls Sie kein Mitglied der Universität Hamburg sind, müssen Sie ihre Bitte zur Registrierung an technik.kolleg@uni-hamburg.de richten.

### Profil

Sie gelangen auf Ihre Profilseite mit dem nachfolgenden Link: [git.universitaetskolleg.uni-hamburg.de/`profile`]([https://git.universitaetskolleg.uni-hamburg.de/profile)

>Tipp: Wenn Sie diese Anleitung für Ihre Zwecke anpassen möchten, sollten Sie an dieser Stelle den Link zu Ihrer GitLab Instanz mit suchen und ersetzen abändern. Ersetzen Sie folgen Teil der URL: `https://git.universitaetskolleg.uni-hamburg.de` mit Ihrer URL z.B. `http://www.manfred-steger.de`.

![In diesem Bild wird die Übersicht der Profilseite angezeigt. Dieses Bild enthält keine zusätzlichen Informationen.](gitlab-profile-private.png)

_Die wichtigsten Punkte des Profils sind:_

- Name (Anzeige Name bei Benachrichtigungen und in Projekte sowie Issues)
- Email (Bei Nennungen und Updates erhalten Sie eine Email mit den wichtigsten Informationen)
- Preferred language (Vollständigkeit nur bei der englischen Sprache gewährleistet)

Überprüfen Sie bitte, ob ihr Name richtig angezeigt und die Mitarbeiter Emailadresse eingetragen sind. Sie können bei beim Formularfeld _Notification email_ mehrere Emailadressen hinterlegen.

### Account

Sie gelangen auf Ihre Accounteinstellungen mit dem nachfolgenden Link: [git.universitaetskolleg.uni-hamburg.de/`profile/account`]([https://git.universitaetskolleg.uni-hamburg.de/profile/account)

_Die relevante Einstellung für diesen Workshop ist:_

- Change username (**Vorsicht!** wird für alle dirketen Nennungen sog. Mentions benutzt und hat Auswirkung auf von Ihnen erstellten Projekte)

Der `username` ist ein zentrales Element in GitLab und sollte nur mit Abstimmung eines Expertens verändert werden. Verändern Sie diesen, können sie nur noch mit dem neuen Namen direkt genannt werden. Zudem ändern sich die festen Projekt URLs ab.

_Beispiele:_

In diesem Beispiel ändere ich meinen username: `Steger` in `Manfred` danach kann ich mit dem Mention-Befehl `@Manfred` aber nicht mehr wie gewohnt mit `@Manfred` direkt benannt werden.

Meine angelegten Projekte ändern ihre _permanente URL_ von:

[git.universitaetskolleg.uni-hamburg.de/`Steger`/workshops-manfred](https://git.universitaetskolleg.uni-hamburg.de/Steger/workshops-manfred)

in

[git.universitaetskolleg.uni-hamburg.de/`Manfred`/workshops-manfred](https://git.universitaetskolleg.uni-hamburg.de/Manfred/workshops-manfred)

Die Projektnamen (z.B. workshops-manfred) selbst sind nicht davon betroffen, auch andere Projekte werden dadurch nicht beeinflusst. Sie sollten Ihren KollegInnen diese Einstellung mitteilen, da ich im oben gezeigten Beispiel nicht mehr mit `@Steger` im System gefunden werde.

![In diesem Bild wird die Übersicht der Accounteinstellungen angezeigt. Dieses Bild enthält keine zusätzlichen Informationen.](gitlab-profile-account.png)

### Notifications

Sie gelangen auf Ihre Benachrichtigungseinstellungen mit dem nachfolgenden Link: [git.universitaetskolleg.uni-hamburg.de/`profile/notifications`]([https://git.universitaetskolleg.uni-hamburg.de/profile/notifications)

Benachrichtigungen sind eine bequeme Art die GitLab Kommunikation mit dem alltäglichen Email-Workflow zu verbinden.

Sie sollten als `global notification level` den Benachrichtigungstyp `Teilnehmen bzw. Participate` wählen, damit erhalten Sie eine Email wenn Sie direkt genannt oder bei einem Update einer Unterhaltung an der Sie aktiv teilgenommen haben.

> Tipp: Falls Ihr zentrales Arbeitsmittel Emails sind, aktivieren Sie die Option: `Receive notifications about your own activity` damit erhalten Sie einen kompletten Gesprächsverlauf inklusive Ihrer Antworten auf Kommentare und Nennungen.

![In diesem Bild wird die Übersicht der Benachrichtigungstypen angezeigt. Dieses Bild enthält keine zusätzlichen Informationen.](gitlab-profile-notifications.png)

## Projects

Sie gelangen auf Ihre Projektübersicht (Dashboard) mit dem nachfolgenden Link: [git.universitaetskolleg.uni-hamburg.de/`dashboard/projects`]([https://git.universitaetskolleg.uni-hamburg.de/dashboard/projects)

Projekte sind die zentralen Dreh- und Angelpunkte im großen Dachverband GitLab. Stellt man sich GitLab als ein großes Haus vor, sind die Projekte die tragenden Wände.

![In diesem Bild wird die Übersicht der bereits angelegten und an teilgenommenen Projekten angezeigt. Dieses Bild enthält keine zusätzlichen Informationen.](gitlab-dashboard-projects.png)

Im Reiter _Your Projects_ wird eine Übersicht aller bereits angelegten und an teilgenommenen Projekten angezeigt. Mit einem Klick auf den Titel gelangt Ihr in die Projektansicht.

> Tipp: Halten Sie die Command-Taste (Mac) / STRG-Taste (Win) vor dem Klicken auf den Titel gedrückt, damit öffnet sich die Projektansicht in einem neuen Tab im Browser und sie können jeder Zeit schnell zur Projektübersicht zurückkehren.

### Suchen - Beobachten - Mitwirken

#### Suchen

Sie gelangen auf die Projektsuche mit dem nachfolgenden Link: [https://git.universitaetskolleg.uni-hamburg.de/`explore/projects`]([https://git.universitaetskolleg.uni-hamburg.de/explore/projects)

Unter dem Reiter **Explore Projets** finden Sie alle internen und öffentliche Projekte die diese GitLab Instanz zur Verfügung stellt.

_Es gibt drei unterschiedliche Projekttypen/-Voreinstellungen:_

- Private (nur für Sie und Mitglieder sichtbar)
- Internal (für alle registrierte Nutzer dieses GitLabs sichtbar)
- public (für alle InternetnutzerInnen sichbar)

#### Beobachten

Sie gelangen auf das öffentliche Projekt gitlab-workshop-manfred mit dem nachfolgenden Link: [git.universitaetskolleg.uni-hamburg.de/`Manfred/gitlab-workshop-manfred`]([https://git.universitaetskolleg.uni-hamburg.de/Manfred/gitlab-workshop-manfred)

![In diesem Bild wird die Projekt Übersichtsseite des öffentlichen Projekts gitlab-workshop-manfred angezeigt. Zudem werden die wichtigsten Funktionen und Eigenschaften erklärt – die Erklärung folgt im nächsten Absatz des Scripts.](gitlab-project-details.png)

Damit Sie ein Projekt beobachten können, widmet wir uns nun der Projekt Detailansicht. Es werden nun die wichtigsten Funktionen und Arbeitswege für diesen Workshop erklärt:

- Favorisieren (star)
- Ableger (fork)
- Repository Link
- Download Repository
- Dateiablage Ansicht (Repository view)

Der große Unterschied zwischen einem Projekt Mitglied (member) und Beobachter (star) sind dessen Rechte. Rechte können individuell für jedes Projekt erstellt und angepasst werden.

##### Favorisieren (star)
Wenn Sie ein Projekt _favorisieren_ erhalten Sie bei Updates und/oder Ankündigungen eine Benachrichtigung. Sie können sich also, ohne Mitglied dieses Projekts zu sein, auf dem Laufenden halten.

> Sie arbeiten an mehreren Projekten, die später für ein Gesamtprodukt zusammengeführt werden? In diesem Fall lohnt es sich, die dazugehörigen Projekte zu favorisieren. Ein anderes Beispiel wäre ein Workshop Repository, dass alle Ankündigungen und Bereitstellung von Materialien über ein Projekt realisieren – auch hier lohnt es sich, als stiller Beobachter updates über z.B. neue Termine oder Materialien zu erhalten.

##### Ableger (fork)
Ein Ableger erschafft eine Kopie der Dateiablage (Repo) in einem neuen Projekt. Darüberhinaus blickt der neue Abkömmling immer wieder auf die Änderungen des ursprünglichen Projekts. Dieses Feature ist der Markenkern von GitLab, GitHub und Co.

> Ein Fork ist ein komplexer Vorgang und sollte nur nach Absprache mit einem Experten vorgenommen werden. Im wissenschaftlichen Kontext wäre ein gutes Beispiel für das Arbeiten mit _forks_ eine digitale Bibliothek für hausinterne Journals. Im Hauptprojekt werden fertige Werke im Repository angeboten. Die Bibliothek schreibt diese Werke aber nicht selbst und jede Fakultät erstellt einen Ableger des Projects und arbeitet an neuen Inhalten. Alsbald ein Inhalt fertig ist, wird der Fork zum Ursprungsprojekt zurückgeführt und alle anderen Abkömmlinge erhalten eine Update-Nachricht. Bei diesem Workflow sind stets alle Werke überall up-to-date.

##### Repository Link
Dieser Link dient zur Verknüpfung mit einem Git-Client wie z.B. [GitKraken](https://www.gitkraken.com/) oder [GitHub Desktop](https://desktop.github.com/). Diese Programme erschaffen eine lokale Kopie des Repos auf Ihrem Endgerät, zeichnen Änderungen auf und synchronisieren diese mit dem online Repository.

> Tipp: Arbeiten Sie viel mit Dateien? Dann lohnt es sich einen Blick auf einen Git-Client zu werfen. Sobald Sie einen Git-Client mit dem Repository verbunden haben, können Sie von Ihrem Rechner aus alle Dateien mit dem für Sie gewohnten Programm bearbeiten. Der Einrichtungsaufwand für ein Repository ist gering aber nicht zu unterschätzen, auch hier empfiehlt es sich einen Expertenrat einzuholen.

##### Download Repository
Mit einem Klick auf den Download Button, rechts neben dem Repository Link, werden alle Dateien in der Dateiablage als komprimierte Datei heruntergeladen.

##### Dateiablage Ansicht (Repository view)
Unterhalb der Funktionsleiste wird die Dateiablage angezeigt. Sie finden dort alle abgelegten Dateien. In der Standardeinstellung können Sie online keine Änderungen vornehmen, außer Sie haben Administrationsrechte oder erweiterte Rechte erhalten. Im wissenschaftlichen Alltag macht diese Beschränkung wenig Sinn, als Programmierer hingegen, eine lebenswichtige Funktion. In diesem Workshop wird nicht weiter auf das Repo eingegangen, da es überaus komplex ist.

> Tipp: Das Repository ist das eigentliche Herzstück von GitLab und überaus mächtig. Hierzu wird ein separater Workshop angeboten, wie sie das Repository für wissenschaftliche Zwecke und Alltagsarbeiten nutzen können.

#### Mitwirken

Es gibt unterschiedliche Wege wie Sie in einem Projekt aufgenommen oder selbst sich hinzufügen können.

> Tipp: Sie sind MitarbeiterIn eines Projektes? Bitten Sie den oder die Projektverantwortliche für die Aufnahme zum gewünschten Projekt, da jedes Projekt unterschiedliche Berechtigungseinstellungen haben kann.

## Issues

Ein Issue (Themengebiet/Aufgabe/Problem) ist eine überschaubare Aufgabe in einem Projekt. Sie gelangen auf die Issue Übersicht mit dem nachfolgenden Link: [git.universitaetskolleg.uni-hamburg.de/`Manfred/gitlab-workshop-manfred/issues`]([https://git.universitaetskolleg.uni-hamburg.de/Manfred/gitlab-workshop-manfred/issues)

![In diesem Bild wird die Issue Übersichtsseite des öffentlichen Projekts gitlab-workshop-manfred angezeigt. Dieses Bild enthält keine zusätzlichen Informationen.](gitlab-issue-list.png)

Issues sind projektbezogene Aufgaben bzw. Probleme, die von einem Team angegangen bzw. gelöst werden sollen.

> Tipp: Ein goldener Weg, effektiv mit Issues zu arbeiten besteht darin, die nötigen Schritte in möglichst kleine und überschaubare Aufgaben zu unterteilen.

Je nach Berechtigung sind Sie in der Lage neue Issues über den Button _new issue_ zu erstellen. In diesem Workshop wird davon ausgegangen, dass Sie an Issues mitarbeiten und diese nicht selbst erstellt werden.

Bei einem Projekt ist es der gängige Weg, eine Einladung zu erhalten, bei den Issues ist es genau umgekehrt. **Sie können keinen Issues zugeordnet werden!**

Dies erfordert eine kurze Exkursion zum agilen Projektmanagement in Form von _Scrum_. Scrum ist das Gegenstück zu hierarchischen Organisationsformen (Command & Control) abseits von direkten Befehlen, schafft das selbstorganisierte/verantwortliche Organisationsprinzip (Commitment & Confidence) dem Team Raum für Entscheidungen – vor allem auf der Ausführungsebene.

> Ein sehr vereinfachter aber weitestgehend zutreffender Leitsatz für Scrum NeueinsteigerInnen wäre: Die Chefs (Stakeholder/Product Owner) entscheiden was wir machen sollen, aber nicht **wer (Person)**, **wann (Zeitraum)** und vor allem nicht **wie (Technologien)**.

GitLab baut seine Funktionsweise vereinfacht dargestellt auf diesem Prinzip auf.

### Board

Sie gelangen auf die Kanban-Board Ansicht mit dem nachfolgenden Link: [git.universitaetskolleg.uni-hamburg.de/Manfred/gitlab-workshop-manfred/`issues/board`]([https://git.universitaetskolleg.uni-hamburg.de/Manfred/gitlab-workshop-manfred/issues/board)

![In diesem Bild wird Issue Board Ansicht des öffentlichen Projekts gitlab-workshop-manfred angezeigt. Dieses Bild enthält keine zusätzlichen Informationen.](gitlab-issues-kanban.png)

Issues können in Form ein Liste und geordnet in einem sog. Kanban-Board dargestellt werden.

Das Board hat den Vorteil, alle Aufgaben und deren Status zu überblicken. Einzelne Aufgaben können via Drag&Drop von einer Spalte zu einer beliebigen anderen Spalte gezogen werden. Die Spalte _Backlog_ und _Closed_ sind voreingestellt.

Unbearbeitete Aufgaben sind demnach im Backlog gesammelt, abgeschlossene Aufgaben in Closed (Archiv).

> Tipp: Alsbald Ihr eine Issue angeht, sollte diese aus dem Backlog in z.B. To Do (mache ich bald) oder in Doing (ich bin gerade dabei) verschoben werden. Somit erhalten alle Projektteilnehmenden einen groben Überblick, welche Aufgaben gerade in Bearbeitung sind bzw. noch ausstehen.

Wie neue Spalten erstellt, Meilensteine definiert und Labels die visuelle Darstellung fördern sind _nicht_ Teil dieses Workshops und werden in einem separatem **GitLab Projektmanagement Workshop** angeboten

### Detailansicht

Sie gelangen auf die Issue Detailansicht indem Sie auf den Titel eines Issues klicken. Die nachfolgend verlinkte Issue kann als Spielwiese für erste Gehversuche genutzt werden – klicken Sie auf den Link um zur Issue Detailansicht zu gelangen: [git.universitaetskolleg.uni-hamburg.de/Manfred/gitlab-workshop-manfred/`issues/13`]([https://git.universitaetskolleg.uni-hamburg.de/Manfred/gitlab-workshop-manfred/issues/13)

> Tipp: Halten Sie die Command-Taste (Mac) / STRG-Taste (Win) vor dem Klicken auf den Titel eines Issues gedrückt, damit öffnet sich die Issue Detailansicht in einem neuen Tab im Browser und sie können jeder Zeit schnell zur Kanban-Board Ansicht zurückkehren.

![In diesem Bild wird Issue Detailansicht des öffentlichen Projekts gitlab-workshop-manfred angezeigt. Dieses Bild enthält sehr viele zusätzlichen Informationen, die in den folgenden Absätzen ausführlich beschrieben werden.](gitlab-issue-detail.png)

Die Issue Detailansicht ist für das wissenschaftliche Arbeiten der Dreh- und Angelpunkt. Als MitarbeiterIn eines Projekts wird Ihre Hauptaufgabe darin bestehen, in Issues Materialien zu entwickeln, bestehende Materalien oder Informationen einzupflegen und/oder Prozessschritte zu dokumentieren.

#### Dokumentieren

Über den Button _Edit_ können Sie eine Beschreibung hinzufügen oder abändern. Sie schreiben in einem einfachen Editor-Fenster, der mit Auszeichnungsbefehlen in Markdown formatiert werden kann.

> Tipp: Eine Übersicht aller Markdown Befehle finden Sie im [Markdown Cheat Sheet von adam-p](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) auf GitHub.

> Tipp: [Dillinger.io](https://dillinger.io) ist ein online Markdown Editor, mit dem Sie auf der linken Seite schreiben und auf der rechten das Geschriebene formatiert lesen können. Sieht verwirrend aus? Kein Problem! Einfach mal etwas löschen und Eigenes eintippen.

_Die wichtigsten Befehle für den Start finden Sie in dieser Tabelle:_

| Markdown Befehl | Formatierung Word | HTML Tag |
| ------ | ----------- |  ----------- |
| #   | Überschrift 1 | h1 |
| ##   | Überschrift 2 | h2 |
| ###   | Überschrift 3 | h3 |
| Text   | Absatz  | p  |
| \*Text\*   | Kursiv | em |
| \*\*Text\*\*   | Fett | strong |
| - Text   | Liste  | ul li  |
| 1. Text   | Geordnete Liste  | ol li |


#### Aufgaben übernehmen

Wie bereits im Kapitel [Issues](https://gitlab.com/flavoursofopenscience/openlab/blob/master/workshops/gitlab-introduction/script/gitlab-script-introduction.md#issues) als Leitsatz definiert, können/sollten Aufgaben von Teamangehörigen selbstorganisiert gewählt und abgearbeitet werden.

Für ein Issue kann ein _assignee_ ein sog. Kümmerer fremdgesteuert bestimmt werden. Im klassischen Scrum ist die oder der Kümmerer nicht zwingend bzw. selten zugleich der/die Bearbeiterin.

> Klingt komisch, is' aber so! Ganz nach Peter Lustig Manier folgt wieder eine kurze Exkursion in den agilen Prozess á la Scrum. In den routinierten Team Meetings sog. Sprint Planning, plant das Team ohne Externe den anstehenden Sprint (Zeitraum bis zum nächsten Sprint Planning). Vereinfacht ausgedrückt werden dort vorbereitete Themen oder Issues vom Product Owner/Scrum Master eingebracht, die während des anstehenden Sprints erledigt werden sollen. Während dieser Besprechung bei erfahrenen Teams viele Dinge – für uns ist vor allem die Aufgabenverteilung wichtig.
>
> Leitsatz: Am Ende des Sprints sollen alle Aufgaben erledigt werden, wer das explizit tut, ist nebensächlich – der Erfolg oder Misserfolg ist eine Teamleistung!
>
> Die oder der Kümmerer sorgt in einem Issue lediglich dafür, dass diese Aufgabe am Ende abgeschlossen wird.

Fühlen Sie sich für eine Aufgabe bestimmt? Dann nehmen Sie die Initiative selbst in die Hand und machen sich zum _assignee_. Sie werden dann automatisch zum _participant_ und erhalten automatisch Benachrichtigungen.

Für diesen Workshop ist es sinnvoll davon auszugehen, dass ein _assignee_ die Aufgabe eigenständig erledigt, da keine Scrum Erfahrung vorausgesetzt wird.

### Kommunikation

In diesem Workshop wird lediglich die aufgabenzentrierte Kommunikation beschrieben, also diejenige die für das Team zur Erledigung eines expliziten Issues relevant ist.

Diese Einschränkung ist für den Einsteiger Kurs wichtig, da ich sonst ein großes Fass aufmachen müsste. Nachfolgend kommunizieren wir mit dem Workshop-Team in diesem [Issue](https://git.universitaetskolleg.uni-hamburg.de/Manfred/gitlab-workshop-manfred/issues/13).

Jedes Issue besitzt zusätzlich zur Beschreibung auch eine Kommentarfunktion. Diese Kommentare sind der Schlüssel für eine gute Zusammenarbeit. Sobald ihr einen Kommentar verfasst, geltet ihr als _participant_ sprich TeilnehmerIn zu diesem Issue und erhalten Benachrichtigungen. Darüberhinaus seit Ihr im internen System des Issues verfügbar.

Alle Teilnehmenden eines Issues können über den Befehl `@username` mithilfe der Autovervollständigung direkt benannt und automatisch via Email benachrichtigt werden. Personen die nicht in einem Issue als Teilnehmende gelten, können trotzdem benannt werden, jedoch muss der Benutzername dann vollständig bekannt sein.

> Tipp: Es gibt zwei Arten von Benennungen die für das Team interessant sind:
>
>`@username = Direktnachricht`
>
>`@all = Alle Projekt- und Gruppenteilnehmende`

Erhaltet Ihr vom System eine Email Benachrichtigung, könnt ihr auf diese Email mit der Antworten-Funktion einen Kommentar hinterlassen, ohne GitLab aufzurufen. Den Workflow mit Emails wird unter dem Kapitel [Notifications](https://gitlab.com/flavoursofopenscience/openlab/blob/master/workshops/gitlab-introduction/script/gitlab-script-introduction.md#notifications) beschrieben und die nötigen Einstellungen dafür gezeigt.

### Referenzieren

Das Issue mit dem Namen **GitLab Einführung Issue für EinsteigerInnen** hat eine einzigartige Auszeichnung – wieder hilft uns die URL dabei, das System hinter GitLab zu verstehen:

[git.universitaetskolleg.uni-hamburg.de/Manfred/`gitlab-workshop-manfred`/`issues/13`](https://git.universitaetskolleg.uni-hamburg.de/Manfred/gitlab-workshop-manfred/issues/13)

#### Interne Referenzen

Im Projekt **gitlab-workshop-manfred** hat das Issue **GitLab Einführung Issue für EinsteigerInnen** die Nummer **13**.

Was das bedeutet? Ein Zitat aus der [offiziellen GitLab Dokumentation](https://about.gitlab.com/2016/03/08/gitlab-tutorial-its-all-connected/):

> It's all connected in GitLab ... Your issue is the Single Source of Truth

Ein Issue und dessen Informationen sind einzigartig und können unter dem einzigartigen bzw. einem permanenten Link aufgerufen werden. Dies gilt nicht nur für Issues, Projekte haben auch einen Permalink.

Wir können also der Issue Beschreibung und/oder einem Kommentar auf ein bereits bestehendes Issue referenzieren. Die Funktionsweise ist relativ einfach, wenn man die URL betrachtet.

[git.universitaetskolleg.uni-hamburg.de/Manfred/
gitlab-workshop-manfred/`issues/13`](https://git.universitaetskolleg.uni-hamburg.de/Manfred/gitlab-workshop-manfred/issues/13)

In der Detailansicht eines Issues steht die Zahl am Ende der URL für die Issue Nummer.

Folgendes Beispiel zeigt eine Referenz im Projekt **gitlab-workshop-manfred** in **Issue \#13** (GitLab Einführung Issue für EinsteigerInnen) auf die **Issue \#14** (Referenzieren leicht gemacht) in Form eines Kommentars.

> Liebe \@Asteroid, die benötigten Informationen findest du die in \#14 in der Beschreibung. Viele Grüße Manfred

Sobald in ihr eine Raute (#) in der Issue Beschreibung oder dem Kommentarfeld eintippt, erscheint änlich bei den Benennungen eine Autovervollständigung, die euch die Auswahl erleichtert.

Klickt ihr nachdem Speichern oder Absenden auf den ausgezeichneten Link, werdet ihr zum jeweiligen Issue bzw. Projekt weitergeleitet.

Die wichtigsten

#### Externe Referenzen

Arbeitet Ihr an unterschiedlichen Projekten und erkennt z.B. ein Problem, dass in einem anderen Projekt in einem Issue gelöst wurde, könnt ihr als Kommentar oder in der Issue Beschreibung eine Referenz dazu erstellen.

Im Prinzip gilt das gleiche Schema wie bei internen Referenzen, zur Issue Nummer benötigt ihr zusätzlich noch den Projektnamen.

> Lieber @Manfred trage dich doch bitte für dieses Issue als Kümmerer ein: **workshops-manfred#1**. Danke

Im gezeigten Beispiel wird eine Verlinkung auf die URL:
[git.universitaetskolleg.uni-hamburg.de/Manfred/workshops-manfred/issues/1](https://git.universitaetskolleg.uni-hamburg.de/Manfred/workshops-manfred/issues/1) gesetzt.

Die Syntax ist also recht einfach: `projektname#nummer`.

## Nützliche Links

* [Markdown Cheat Sheet - GitHub](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
* [Markdown Editor Online mit Befehlsübersicht - Dillinger](https://dillinger.io/)
* [Markdown Offline Editor Mac - Macdown](https://macdown.uranusjr.com/)
* [Markdown Offline Editor Win - Typora](https://typora.io/)
* [Markdown iOS App - MWeb](https://itunes.apple.com/de/app/mweb-powerful-markdown-app/id1183407767?mt=8)
* [Markdown Android App - MarkdownX](https://play.google.com/store/apps/details?id=com.ryeeeeee.markdownx&hl=de)
* [Hackable Editor - Atom (Pro)](https://atom.io/)
