# Markdown: Eine Formatvorlage für alle Textverarbeitungsprogramme

In dieser Einführung lernen Sie die Auszeichnungssprache Markdown kennen und deren grundliegende Funktionsweise im medial geprägten Arbeitsalltag. Im Fokus stehen dabei das Kennenlernen der Syntax, eine Auswahl an Webtools und offline Programme zur Unterstützung des individuellen Schreibprozesses. Abschließend werden die gesammelten Erfahrungen mit Markdown in praktischen Übungen erprobt. Die Übungen richten sich an gängigen Management Aufgaben wie z.B. Protokolle schreiben, ToDo-Listen erstellen, Dokumentationen vorbereiten und dokumentübergreifende Textverarbeitungsaufgaben aus.

Diese Veranstaltung ist für eine Zeitstunden ausgelegt. Die Teilnehmenden sollten nach der Veranstaltung zwei weitere Zeitstunde für die Nachbereitung der praktischen Übungen einplanen.

**Voraussetzungen für diesen Kurs:**

- Internetfähiges Endgerät (vorzugsweise Laptop)

**Unterlagen für diesen Workshop:**

- [Link zur Präsentation](https://openlab.blogs.uni-hamburg.de/markdown-eine-formatvorlage-fuer-alle-textverarbeitungsprogramme/)
- [Link zum Veranstaltungsskript](https://gitlab.com/flavoursofopenscience/openlab/blob/master/workshops/markdown-introduction/script/markdown-script-introduction.md)

**Dieser Workshop wird an folgenden Daten angeboten:**

- [Donnerstag, 30.11.17 von 15-16 Uhr](https://synlloer.blogs.uni-hamburg.de/veranstaltung/markdown-eine-formatvorlage-fuer-alle-textverarbeitungsprogramme)
- [Donnerstag, 7.12.17 von 15-16 Uhr](https://synlloer.blogs.uni-hamburg.de/veranstaltung/markdown-eine-formatvorlage-fuer-alle-textverarbeitungsprogramme-2)
- [Donnerstag, 14.12.17 von 15-16 Uhr](https://synlloer.blogs.uni-hamburg.de/veranstaltung/markdown-eine-formatvorlage-fuer-alle-textverarbeitungsprogramme-3)
- [Donnerstag, 11.1.18 von 15-16 Uhr](https://synlloer.blogs.uni-hamburg.de/veranstaltung/markdown-eine-formatvorlage-fuer-alle-textverarbeitungsprogramme-4)
- [Donnerstag, 18.1.18 von 15-16 Uhr](https://synlloer.blogs.uni-hamburg.de/veranstaltung/markdown-eine-formatvorlage-fuer-alle-textverarbeitungsprogramme-5)
- [Donnerstag, 25.1.18 von 15-16 Uhr](https://synlloer.blogs.uni-hamburg.de/veranstaltung/markdown-eine-formatvorlage-fuer-alle-textverarbeitungsprogramme-6)
- [Donnerstag, 1.2.18 von 15-16 Uhr](https://synlloer.blogs.uni-hamburg.de/veranstaltung/markdown-eine-formatvorlage-fuer-alle-textverarbeitungsprogramme-7)
- [Donnerstag, 8.2.18 von 15-16 Uhr](https://synlloer.blogs.uni-hamburg.de/veranstaltung/markdown-eine-formatvorlage-fuer-alle-textverarbeitungsprogramme-8)
- [Donnerstag, 22.2.18 von 15-16 Uhr](https://synlloer.blogs.uni-hamburg.de/veranstaltung/markdown-eine-formatvorlage-fuer-alle-textverarbeitungsprogramme-9)
- [Donnerstag, 1.3.18 von 15-16 Uhr](https://synlloer.blogs.uni-hamburg.de/veranstaltung/markdown-eine-formatvorlage-fuer-alle-textverarbeitungsprogramme-10)

**Mindestteilnehmendenzahl: 1 Person:**

Um [Voranmeldung über das Formular](https://openlab.blogs.uni-hamburg.de/anmeldung-synlloer-hochschul-workshops/) wird gebeten.

---

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)
](http://creativecommons.org/licenses/by/4.0/)

Die Dokumentation **Markdown: Eine Formatvorlage für alle Textverarbeitungsprogramme** von [Manfred Steger für SynLLOER](https://gitlab.com/flavoursofopenscience/openlab/blob/master/workshops/markdown-introduction/script/markdown-script-introduction.md) ist lizenziert unter einer [CC BY 4.0 International Lizenz](http://creativecommons.org/licenses/by/4.0/).

---

## Fahrplan

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [Markdown: Eine Formatvorlage für alle Textverarbeitungsprogramme](#markdown-eine-formatvorlage-für-alle-textverarbeitungsprogramme)
	- [Fahrplan](#fahrplan)
	- [Markdown – Ein Format für Vieles](#markdown-ein-format-für-vieles)
	- [Die wichtigsten Befehle](#die-wichtigsten-befehle)
	- [Online Editor](#online-editor)
	- [Offline Editor](#offline-editor)
	- [Übung: Buch Kapitel formatieren](#übung-buch-kapitel-formatieren)
	- [Übung: Markdown Austauschformate](#übung-markdown-austauschformate)
	- [Nützliche Links](#nützliche-links)

<!-- /TOC -->

## Markdown – Ein Format für Vieles

Was ist eigentlich Markdown? [Markdown](https://de.wikipedia.org/wiki/Markdown) kann kategorisch der Familie der Auszeichnungssprachen, wie z.B. auch [HTML](https://de.wikipedia.org/wiki/Hypertext_Markup_Language), zugeordnet werden. Der Unterschied dieser Sprache besteht aber darin, dass die Ausgangsform, sprich während des Schreibens, bereits ohne Vorkenntnisse *menschenlesbar* also verständlich ist.

Warum also nicht einfach schreiben? Ganz einfach – da ein Text ohne Strukturierung und Auszeichnungen für alle Menschen schwerer zu verstehen ist. Diese normative Handlungsweise ist fachunspezifisch und vom Brief bishin zur Doktorarbeit gängige Praxis. Aber nicht nur Menschen lesen, in einer zunehmend mediatisierten Welt lesen Maschinen ebenso Textdokumente und bereiten diese z.B. für blinde Personen als Sprachnachricht auf. Umso wichtiger wird es, ein Format zu nutzen, das zum einen von jedem ohne Zusatzwerkzeuge gelesen werden kann und zum anderen eine fehlerfreie Darstellung und Struktur für die Weiterverarbeitung bietet.

So kann z.B. ein Protokoll in einer Sitzung in Markdown mitgeschrieben, anschließend ohne zusätzlichem Aufwand als PDF, Word, Wikisyntax, HTML, LaTeX uvm. ausgegeben und für das jeweilige Medium mit Copy\&Paste eingefügt werden.

Der große Unterschied zu WYSIWIG (what-you-see-is-what-you-get) Textverarbeitungsprogramm wie z.B. LibreOffice ist darstellungsunabhängige Form des Inhalts. LibreOffice schreibt bei jeder Darstellungsänderung z.B. beim Wechseln einer Schriftart (z.B. von Times New Roman in Helvetica) dies unsichtbar in das Dokument ein, ebenso wenn eine Farbe benutzt oder etwas unterstrichen wird. Beim Einfügen eines Textes aus einem anderen Dokument wie z.B. einer Word-Datei werden sehr viele unsichtbare Informationen mit übernommen – der schlimmste Fall wird erzeugt, wenn Informationen aus Internetseiten mit Copy\&Paste übernommen werden (HTML-Fragmente). Es gibt Tricks in WYSIWIG-Editoren wie z.B. das *Einfügen und an Formatierung anpassen* Feature, jedoch ist dies bei Weitem keine Garantie, dass darstellungsabhängige Informationen nach und nach die Textdarstellung beeinträchtigen.

Markdown selbst hat keine Darstellungsform, diese wird beim Konvertierungsprozess (rendering) des jeweiligen Systems (z.B. WordPress Blog) an die Gestaltungsvorlage (Theme des Blogs) angepasst. Am Beispiel Textverarbeitungsprogramm (z.B. Word) wäre es analog, eine ausgezeichnete Überschrift der Ordnung 1 in Markdown wird zur Formatvorlage Überschrift 1 in Word. Das Ausgangsdokument bleibt immer darstellungsunabhängig und ermöglicht einen sauberen Austausch zwischen verschiedenen Formaten.

Damit Sie einen visuelle Vorstellung erhalten, wie in etwa das Endergebnis Ihrer Mitschrift aussieht, gibt es sog. Markdown Editoren, die eine geteilte Darstellung von Markdown und visuelle Darstellung in einem Splitscreen Modus (geteilte Darstellung) ermöglichen. In diesem Workshop werden wir mit unterschiedlichen Editoren arbeiten.

## Die wichtigsten Befehle

Markdown ist nicht gleich Markdown, vergleichsweise gibt es bei der deutschen Sprache Regeln der alten und der neuen Rechtschreibreform sowie Dialekte. Die wichtigsten Merkmale bleiben dabei gleich. Wenn zwischen dem sog. *Markdown Dialekt* unterschieden wird – ist [CommonMark](http://commonmark.org/) und [GitHub flavored Markdown (GFM)](https://github.github.com/gfm/) die weitverbreiteste Mundart.

In diesem [Online Tutorial von CommonMark.org](http://commonmark.org/help/tutorial/) können die wichtigsten Markdown Befehle als **SOL** (**S**elbst **O**rganisierte **L**erneinheit) erlernt werden.

*Die wichtigsten Befehle in tabellarischer Form:*

| Markdown Befehl  | Ergebnis  |
|---|---|
| \# Überschrift 1  | **Überschrift 1**  |
| \#\# Überschrift 2  | Überschrift 2  |
| \*Kursiv Auszeichnung\*  | *Kursivauszeichnung*  |
| \*\*Fett Auszeichnung\*\*  | **Fett Auszeichnung**  |
| \- Listenelement unsortiert  |  • Listenelement unsortiert |
| 1. Listenelement sortiert  |  1. Listenelement sortiert  |

## Online Editor

Markdown kann in jedem beliebigen Dokumenttyp geschrieben – jedoch nur von bestimmten Editoren in formatierten Text dargestellt werden. Einen sehr guten Einstieg in die Markdown Thematik bietet der online browserbasierte Editor [Dillinger.io](https://dillinger.io/). Beim ersten Start wird links ein Beispieltext mit beinahe allen Markdown Formatierungsbefehlen angezeigt. Auf der rechten Seite ist das umgesetzte (gerenderte) Ergebnis zu sehen. Sie können den Text auf der linken Seite auswählen (STRG+A oder CMD+A) und löschen, damit Sie ein leeres Dokument vor sich haben.

## Offline Editor

Datenschutzrechtlich ist ein Online Editor nur bedingt geeignet, da nicht wirklich einzusehen ist, welche Informationen gespeichert und ggf. an Dritte weitergegeben werden. Bei einfachen Tätigkeiten wie z.B. einem Sitzungsprotokoll das anschließend sowieso öffentlich gemacht wird, gibt es keine Bedenken. Bei sensiblen Daten wie z.B. Informationen über Schülerinnen und Schüler müssen die Daten mit offline Tools gesammelt bzw. verarbeitet werden.

In dieser Liste finden Sie unterschiedliche OpenSource (kostenlose) Markdown fähige Programme:

- [MacDown](https://macdown.uranusjr.com/) (Mac)
- [Typora](https://typora.io/) (Win/Linux/Mac)
- [Bear Writer](http://www.bear-writer.com/) ([IOS](https://itunes.apple.com/us/app/bear-beautiful-writing-app/id1016366447?ls=1&mt=8)/[Mac](https://itunes.apple.com/us/app/bear-beautiful-writing-app/id1091189122?ls=1&mt=12))
- [MarkdownX](https://play.google.com/store/apps/details?id=com.ryeeeeee.markdownx) (Android)
- [Atom Editor](https://atom.io/) (Profi-Tool Win/Linux/Mac)

## Übung: Buch-Kapitel formatieren

1. Formatieren Sie mindestens zwei Kapitel mit jeweils zwei Unterkapiteln in einem Markdown-Editor ihrer Wahl.
2. Fügen Sie jedem Kapitel sowie Unterkapitel mehrere Zeilen und Absätze Text hinzu.
3. Formatieren Sie den Text mit folgenden Auszeichnungen (Fett-, Kursivdruck und einer Aufzählung).

`Hinweis:` Sie können auch gerne ein bereits vorhandenes Buch/Material umsetzen. Hier ein Link zum [Open Content - ein Praxisleitfaden zur Nutzung von Creative-Commons-Lizenzen](https://irights.info/wp-content/uploads/2015/10/Open_Content_-_Ein_Praxisleitfaden_zur_Nutzung_von_Creative-Commons-Lizenzen.pdf) Buch, das zur Veränderung und Wiederverwendung ausgezeichnet ist (CC BY).

## Übung: Markdown Austauschformate

1. Exportieren Sie den Inhalt aus der vorherigen Übung als PDF Format.
2. Suchen Sie bei der Suchmaschine Ihres Vertrauens (z.B. DuckDuckGo) nach einem `Markdown to Word Converter` und konvertieren Sie das geschriebene Markdown in ein Word Dokument.
3. Und noch *reverse* – nehmen Sie ein bereits bestehendes LibreOffice oder Word Dokument und konvertieren Sie dieses mit einem online Generator nach Markdown.

## Nützliche Links

- [Markdown Cheat Sheet von Adam Pritchard](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
- [Markdown Table Generator](https://www.tablesgenerator.com/markdown_tables)
- [Markdown Table Konverter stevecat (HTML, Excel, CSV)](http://stevecat.net/table-magic/)
- [Markdown to Word Konverter](https://documentalchemy.com/demo/md2docx)
- [Word to Markdown Konverter](https://word-to-markdown.herokuapp.com/)
- [Writage: Word-Addin, speichert Word in  Markdown](http://www.writage.com/)
- [Syntax Highlighting in Markdown](https://support.codebasehq.com/articles/tips-tricks/syntax-highlighting-in-markdown)
