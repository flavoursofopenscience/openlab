# WordPress: Erste Schritte ins System

In dieser Einführung lernen Sie die Weblog-Software WordPress und deren grundliegende Funktionsweise im Hinblick auf Medieneinbindung und Einsatz im Hochschulkontext der Universität Hamburg kennen. Im Fokus stehen dabei das Kennenlernen des [WordPress Starterkit Userguides](https://synlloer.gitbooks.io/starterkit-user-guide/content/), erste redaktionelle Schritte im Front- sowie Backend und die Einbindung von interaktiven Medien mit dem Tool [H5P](https://h5p.org/).

Darüberhinaus werden Ihnen Administrationseinstellungen für die Benutzerverwaltung vertraut gemacht. Diese helfen Ihnen dabei, NutzerInnen in das System zu integrieren oder mit den benötigten Rechten auszustatten. Abschließend werden noch Systemerweiterungen sog. Plugins vorgestellt und für Ihren Handlungsbedarf im Projektalltag hin diskutiert.

Diese Veranstaltung ist für drei Zeitstunden ausgelegt. Die Teilnehmenden sollten nach der Veranstaltung zwei weitere Zeitstunde für die Nachbereitung der praktischen Übunungen einplanen.

**Voraussetzungen für diesen Kurs:**

- Internetfähiges Endgerät (vorzugsweise Laptop)

**Unterlagen für diesen Workshop:**

- [Link zur Präsentation](https://openlab.blogs.uni-hamburg.de/wordpress-erste-schritte-ins-system/)
- [Link zum Veranstaltungsskript](https://gitlab.com/flavoursofopenscience/openlab/blob/master/workshops/wordpress-introduction/script/wordpress-script-introduction.md)
- [Link zum WordPress Starterkit Userguide des Universitätskolleg der Universität Hamburg](https://synlloer.gitbooks.io/starterkit-user-guide/content/)
- [Link zum WordPress Kompendium Band 6 des Universitätskolleg – Erste Schritte zum eigenen Blog](https://uhh.de/uk-wp-kompendium)

**Dieser Workshop wird an folgenden Daten angeboten:**

- [Donnerstag, 16.11.17 von 10-13 Uhr](https://synlloer.blogs.uni-hamburg.de/veranstaltung/wordpress-erste-schritte-ins-system-20171116/)
- [Dienstag, 05.12.17 von 10-13 Uhr](https://synlloer.blogs.uni-hamburg.de/veranstaltung/wordpress-erste-schritte-ins-system-20171205/)

**Mindestteilnehmendenzahl: 2 Personen:**

Um [Voranmeldung über das Formular](https://openlab.blogs.uni-hamburg.de/anmeldung-synlloer-hochschul-workshops/) wird gebeten.

---

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)
](http://creativecommons.org/licenses/by/4.0/)

Die Dokumentation **WordPress: Erste Schritte ins System** von [Manfred Steger für SynLLOER](https://gitlab.com/flavoursofopenscience/openlab/blob/master/workshops/wordpress-introduction/script/wordpress-script-introduction.md) ist lizenziert unter einer [CC BY 4.0 International Lizenz](http://creativecommons.org/licenses/by/4.0/).

---

## Fahrplan

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

- [WordPress: Erste Schritte ins System](#wordpress-erste-schritte-ins-system)
	- [Fahrplan](#fahrplan)
	- [WordPress Blog – Vorbereitung und Orientierung](#wordpress-blog-vorbereitung-und-orientierung)
		- [Registrierung Universität Hamburg](#registrierung-universität-hamburg)
		- [WordPress Starterkit](#wordpress-starterkit)
		- [Nutzungsleitfaden für den Medieneinsatz](#nutzungsleitfaden-für-den-medieneinsatz)
		- [WordPress Kompendium](#wordpress-kompendium)
	- [WordPress kennenlernen](#wordpress-kennenlernen)
		- [Frontend](#frontend)
		- [Backend](#backend)
		- [Dashboard](#dashboard)
		- [Artikel](#artikel)
	- [Zusammenspiel Front- und Backend](#zusammenspiel-front-und-backend)
		- [Übung: Personen im Backend anlegen](#übung-personen-im-backend-anlegen)
		- [Übung: Artikel schreiben](#übung-artikel-schreiben)
	- [Plugins – der Blog als Multitalent](#plugins-der-blog-als-multitalent)
		- [Einführung H5P für interaktive Inhalte](#einführung-h5p-für-interaktive-inhalte)
		- [Übung: H5P Präsentation erstellen](#übung-h5p-präsentation-erstellen)
	- [Nützliche Links](#nützliche-links)

<!-- /TOC -->

## WordPress Blog – Vorbereitung und Orientierung

[WordPress](https://de.wikipedia.org/wiki/WordPress) ist ein sog. Weblog – also eine Vermischung aus den Begriffen Web (Internet) und Log (Logbuch). Seit der ersten Version 2004 hat sich viel verändert und das Blog-System, das meist wie ein Tagebuch gehandhabt wird, ist mittlerweile ein Multitalent für jegliche Anwendungsszenarien (Webshop, Dateiablage, Chatsystem uvm.) im Internet geworden.

Wenn von WordPress die Rede ist, gilt es zunächst die zwei Angebote von [WordPress.com](https://de.wordpress.com/) und [WordPress.org](https://wordpress.org/) differenziert zu betrachten, bzw. nachzufragen welcher Dienst gemeint ist.

![Darstellung der zwei WordPress Logos von WordPress.org auf der linken und WordPress.com auf der rechten Seite. Ähnlich wie bei einem Boxkampf werden die beiden Kontrahenten mit einem VERSUS zwischen den beiden Namen aufgeführt.](wordpress-org-vs-com.png)

**WordPress.org**

[WordPress.org](https://wordpress.org/) ist die offizielle Software und Community Seite. Im Gegensatz zu WordPress.com wird hier kein Hosting angeboten, sondern die Software als [Download](https://wordpress.org/download/) und die [Dokumentation](https://codex.wordpress.org/), wie diese genutzt werden soll – der sog. Codex.

**WordPress.com**

Dieser Dienstleister bietet Hostingpakete (Speicherplatz und Einrichtung) zur Nutzung der OpenSource (quelloffene und kostenlose) Software von WordPress.org mithilfe einer   [GUI](https://de.wikipedia.org/wiki/Grafische_Benutzeroberfl%C3%A4che) (grafischen Benutzeroberfläche) an. Hiermit kann in wenigen Minuten und ohne weitreichende Fachkenntnisse WordPress innerhalb von Minuten in Betrieb genommen werden. Dem Nutzenden stehen dabei mehrere Preismodelle zur Verfügung. In der kostenlosen Variante kann der Blog in seiner ursprünglich gedachten Variante – als Online-Tagebuch – mit einer [Subdomain](https://de.wikipedia.org/wiki/Domain_(Internet)#Subdomain) z.B. **manfred**.wordpress.com genutzt werden. Die kostenpflichtige Variante erlaubt auch eine Aufschaltung einer eigenen [Domain](https://de.wikipedia.org/wiki/Domain_(Internet)) und die Nutzung von Plugins zur Erweiterung des Systems.

Eine ausführliche Differenzierung zwischen WordPress.com und WordPress.org gibt es [hier](https://de.support.wordpress.com/com-vs-org/).

### Registrierung Universität Hamburg

Die Universität Hamburg bietet eigene [WordPress Installationen](https://www.blogs.uni-hamburg.de/) für Studierende (userblogs) und Bedienstete (blogs) an. Bei der Beantragung über [das Formular](http://antrag.blogs.uni-hamburg.de/formulare/antragsformular) wird vorab nicht zwischen den einzelnen Nutzergruppen unterschieden – dies geschieht während des Antrags automatisch aufgrund der Angaben zur Personengruppe:

- Professor/in (blog)
- Wissenschaftliche/r Mitarbeiter/in (blog)
- TVP (blog)
- Student/in (userblog)

### WordPress Starterkit

Für Interessenten der Universität Hamburg gibt es einen sog. **[WordPress Startkit Userguide](https://synlloer.gitbooks.io/starterkit-user-guide/content/)**, das Orientierung von der Antragsstellung bishin zur mediengerechten Nutzung bietet. In diesem Paket enthalten ist eine Aufrüstung des normalen WordPress-Blogs mit zusätzlichen Design-Vorlagen sowie weiteren Funktionen wie z.B. das [H5P-Plugin](https://h5p.org/).

### Nutzungsleitfaden für den Medieneinsatz

Im Kapitel [6. Leitfaden zum Vorbereiten der Inhalte](https://synlloer.gitbooks.io/starterkit-user-guide/content/6.html) wird kurz und bündig erklärt, welche Eigenschaften Medieninhalte aufweisen sollten, damit sie ansprechend dargestellt und barrierefrei eingebunden werden.

![Gegenüberstellung zweier Qualitätsstufen eines Bildes. Links mit hoher Auflösung und rechts mit geringer Auflösung und Neuberechnung durch den Browser.](picture-quality-comparison.png)

Folgende Medientypen und Self-Assessments werden erläutert:

- Bilder/Grafiken
- Texte
- Erwartungscheck
- Quiz
- Interviews (Video)
- Videos

### WordPress Kompendium

Eine umfassende Dokumentation sowie Schritt-für-Schritt Anleitungen sind im [**WordPress Kompendium**: *Erste Schritte zum eigenen Blog*](https://uhh.de/uk-wp-kompendium) des Universitätskollegs (Stand 2016) festgehalten.

![Abbildung der Titelseite des WordPress Kompendiums: Erste Schritte zum eigenen Blog. Es werden keine weiterführenden Informationen auf diesem Bild dargestellt.](kompendium-titelbild.jpg)

Die fachsystematische Aufarbeitung einzelner Bestandteile sowie die handlungsorientierten Anleitungen werden anhand des Uni Hamburg Themes dargestellt.

## WordPress kennenlernen

Wie bereits in der [Einleitung](wordpress-introduction/script/wordpress-script-introduction.md#wordpress-erste-schritte-ins-system) definiert, ist WordPress ein sog. Blog und kann kategorisch betrachtet der großen Familie der [Content Management System](https://de.wikipedia.org/wiki/Content-Management-System) kurz CMS zugeordnet werden. Einfach übersetzt bedeutet das, ein Verwaltungs- bzw. Redaktionssystem für Inhalte. Bei der exemplarischen Betrachtung einer Zeitungsredaktion auf Organisationsebene kristallisieren sich verschiedene Rollen heraus.

- (Leser)
- Autor
- Redakteur
- (Chef)

LeserInnen lesen die Zeitung und haben eigentlich mit der Redaktion nichts zu schaffen, stellt aber indirekt die wichtigste Persongruppe dar, nämlich aus Marketingsicht die Zielgruppe. AutorInnen verfassen Artikel, die von RedakteurInnen begutachtet und publiziert werden. Chefs hingegen nehmen eine, alle oder keine der oben genannten Rollen ein.

Auf diesen Grundannahmen baut WordPress sein Fundament auf. Damit das System weiß, wer lesen und wer veröffentlichen darf und wer nicht, müssen Nutzende dort mit Berechtigungen hinterlegt werden. Alle Inhalte werden dann personalisiert bei Anfrage (z.B. Aufruf der Seite www.synlloer.blogs.uni-hamburg.de) erstellt und ausgeliefert. Diese Inhalte werden in der Fachsprache auch als dynamische Inhalte oder im Falle von WordPress als dynamisch generierte Website deklariert.

Das System beinhaltet also zwei Ebenen – eine die für die Auslieferung sowie Darstellung der Inhalte zuständig ist und eine andere die für die Verwaltung sowie Rechte Verantwortung trägt.

In diesem Zusammenhang wird von der darstellenden Ebene dem **Frontend** und der administrativen Ebene dem **Backend** gesprochen.

### Frontend

![Screenshot der Frontend-Darstellung der Seite www.synlloer.blogs.uni-hamburg.de – sie erhalten dieselbe Ansicht beim Besuch der Seite selbst.](wordpress-screenshot-frontend.png)

Beim Aufrufen eines Blogs, wird automatisch das Frontend gezeigt. Das Beispielbild oben zeigt den SynLLOER-Blog mit der Adresse ([URL](https://de.wikipedia.org/wiki/Url)): `synlloer.blogs.uni-hamburg.de`. Die visuelle Darstellung ist wie im Kapitel [WordPress kennenlernen](wordpress-introduction/script/wordpress-script-introduction.md#wordpress-kennenlernen) kurz erwähnt, von den Berechtigungen des jeweiligen Nutzenden abhänig. Ohne Anmeldung sind diese im System der Nutzergruppe **Gast** bzw. als **LeserIn** zugeordnet. Damit Inhalte erstellt bzw. verändert werden können, ist eine Anmeldung/Authentifizierung im Backend nötig.

### Backend

Das Gegenstück zum Frontend, der visuellen Darstellung der Internetseite, ist der Administrationsbereich **Backend**, zu dem nur befugte Nutzende Zugriff haben.

Das Backend Ihres Blogs erreichen Sie unter dem Zusatz `/wp-admin` bzw. oft auch einfach per `/admin`. Am Beispiel des openLab-Blogs wäre das dementsprechend: [https://www.**openlab**.blogs.uni-hamburg.de`/wp-admin`](https://www.openlab.blogs.uni-hamburg.de/wp-admin).

Sie werden direkt auf eine Anmeldemaske geführt. Hier geben Sie Ihre persönlichen Zugangsdaten ein und melden sich somit am System an.

![Screenshot der Anmeldemaske von WordPress – sie gelangen zu dieser barrierefreien Darstellung unter dem oben genannten Link.](wordpress-backend-login.png)

### Dashboard

Nach der erfolgreichen Anmeldung/Authentifizierung gelangen Sie auf das sog. Dashboard. Diese Übersicht wird auch Mitteilungszentrale oder Blogübersicht genannt und zeigt auf dem ersten Blick unterschiedliche Bereiche von WordPress. Dieser Bereich kann individuell angepasst werden. Im Auslieferungszustand sind folgende Kategorien aktiviert:

- Auf einen Blick
- Aktivität
- Schneller Entwurf
- WordPress Events and News

![Screenshot der Backend-Darstellung der Seite www.synlloer.blogs.uni-hamburg.de/wp-admin/ nachdem Login – sie erhalten dieselbe Ansicht beim Besuch und nachdem Loginverfahren.](wordpress-screenshot-backend.png)

Im WordPress Backend gibt es zwei Menüs, das sog. `header-menu`, das als schmale Leiste am oberen Bildschirmrand verankert ist und ein `side-menu`, das am linken Bildschirmrand feststeht. Die wichtigsten Funktionen befinden sich im `side-menu `.

### Artikel

Für AutorInnen und RedakteurInnen ist der Menüeintrag im `side-menu` Beiträge besonders wichtig. Unter diesem Menüpunkt befinden sich die bereits verfassten Beiträge und eben die Möglichkeit neue Beiträge einzureichen.

Beiträge sind die Einträge, die in umgekehrter chronologischer Reihenfolge auf der Startseite des Blogs erscheinen. Festgepinnte Beiträge (sticky posts) erscheinen immer vor den anderen Beiträgen.

![Screenshot der Backend-Darstellung der Seite www.synlloer.blogs.uni-hamburg.de/wp-admin/edit nachdem Login – sie erhalten dieselbe Ansicht beim Besuch und nachdem Loginverfahren und dem Aufruf der Seite Posts/Beiträge im Menü.](wordpress-backend-posts.png)

Mit einem Klick auf den Erstellen-Button (oben links) wird eine Formular-Maske mit einem Editor geöffnet. Zunächst müssen Sie einen Titel für den Beitrag festlegen und den gewünschten Inhalt in den Editor eingeben. *Es ist nicht empfehlenswert, Textpassagen aus dem Internet oder einem Textverarbeitungsprogramm wie Word mit Copy\&Paste einzufügen, da unsichtbare Formatierungsauszeichnung mit übernommen werden können.*

> Tipp: Haben Sie Text mit STRG+C (Win/Linux) oder CMD+C (Mac) kopiert können Sie diesen mit dem Tastaturbefehl STRG+SHIFT+V (Win/Linux) oder CMD+ALT+SHIFT+V (Mac) ohne Formatierung in den WordPress Editor übertragen.

Eine genau Erklärung für den Erstellungsprozess liefert das nachfolgende YouTube-Video:

[![Externer Link zum YouTube Video WordPress: Ganz einfach (Blog)Beiträge erstellen. Für externe Links wird keine Haftung übernommen.](https://img.youtube.com/vi/gCmrk75rEIU/hqdefault.jpg)](https://www.youtube-nocookie.com/embed/gCmrk75rEIU?rel=0 "WordPress: Ganz einfach (Blog)Beiträge erstellen")

## Zusammenspiel Front- und Backend

Wird ein Artikel im Backend erstellt, zeigt der Editor nur eine strukturelle Darstellung des Inhalts an. Die endgültige Darstellung wird lediglich im Frontend angezeigt und hängt vom gewählten Theme ab. Ein Theme ist eine Gestaltungsvorlage des kompletten Blogs. Damit diese Abhänigkeiten von Frontend und Backend deutlich werden, folgen nun zwei Übüngen.

### Übung: Personen im Backend anlegen

Personen können sie je nach Systemeinstellungen selbst registrieren oder müssen über eine Person mit Administrationsrechten (Rolle Admin) hinzugefügt werden. Aus Sicherheitsgründen ist die eigenständige Registrierung auf Blogs der Universität Hamburg standardmäßig deaktiviert.

Damit diese Übung funktioniert muss eine Person mit Administrationsrechten eine Person mit derselben Berechtigungsstufe (Admin) erstellen.

> Merke: Die Rolle Admin sollten nur wenige Nutzende des Blogs erhalten. Für diese Übung ist es aber notwendig diese Rolle alle Beteiligten zu vergeben. Nach erfolgreicher Absolvierung der Übung werden nachträglich die Rollen für die jeweiligen Nutzenden passend zum Aufgabenbereich herabgestuft.

Eine neue Person kann im `Backend > side menu > Benutzer > Neu hinzufügen` angelegt werden.

Die Pflichtfelder werden mit `(erforderlich)` gekennzeichnet.

### Übung: Artikel schreiben

Unter Einbeziehung des Videos aus dem Kapitel [Artikel](wordpress-introduction/script/wordpress-script-introduction.md#artikel) und dem [Leitfaden zum Vorbereiten der Inhalte](https://synlloer.gitbooks.io/starterkit-user-guide/content/6.html) soll nun jede Person einen eigenständigen Artikel verfassen, der folgende Anforderungen erfüllen soll:

- Text mit mehreren Absätzen (mindestens 1000 Zeichen) – [Link Blindtextgenerator](http://www.blindtextgenerator.de/)
- 5 Bilder inkl. korrekter Lizenzangabe (mindestens 1 CC-BY)
  - Creative Commons Bildersuche ([Link Pixabay](https://pixabay.com/))
  - Wiki-Media Bibliothek ([Link Wiki-Commons](https://commons.wikimedia.org/wiki/Main_Page))
- 1 PDF inkl. korrekter Lizenzangabe / Bibliographie
- Veröffentlichung muss für jeden Nutzenden sichtbar sein.

## Plugins – der Blog als Multitalent

Plugins bieten in WordPress die Möglichkeit, das System um zusätzliche Funktionen zu erweitern. Auf der [offiziellen WordPress Community Seite]( https://wordpress.org/plugins) finden Sie eine große Anzahl an Erweiterungen.

Im Kapitel [Übung: H5P Präsentation erstellen](wordpress-introduction/script/wordpress-script-introduction.md#%C3%9Cbung-h5p-pr%C3%A4sentation-erstellen) wird z.B. eine interaktive Präsentation in WordPress erstellt und in einem Beitrag eingebunden. Diese Funktionalität wird durch das Plugin [H5P](https://wordpress.org/plugins/h5p/) ermöglicht.

Neue Plugins können direkt im Administrationsbereich installiert, aktiviert oder deaktiviert und gelöscht werden. Der Reiter dazu befindet sich im `side-menu`auf der linken Seite.

### Einführung H5P für interaktive Inhalte

H5P ist eine Zusatzsoftware für Content Management Systeme wie WordPress, mit der interaktive Inhalte wie z.B. Präsentationen oder interaktive Videos online erstellt und dargestellt/verteilt werden können.

Eine genaue Erklärung liefert unser H5P-Trainer [Oliver Tacke](https://twitter.com/otacke?lang=de):

[![Externer Link zum YouTube Video H5P Tutorials: What is H5P? Für externe Links wird keine Haftung übernommen.](https://img.youtube.com/vi/2HtxLeXGU48/hqdefault.jpg)](https://www.youtube-nocookie.com/embed/2HtxLeXGU48?rel=0 "H5P Tutorials: What is H5P?")


Das H5P-Plugin ist auf unserem [Test-Account](http://test4.blogs.uni-hamburg.de/) bereits installiert. Ganz nach unserem OER-Motto: `Erstellen ist gut, wiederverwenden ist besser!` findet ihr nachfolgend einige H5P-Repositories:

- [SynLLOER-Materialien](https://synlloer.blogs.uni-hamburg.de/materialien)
- [H5P Repo](https://h5p.org/content-types-and-applications)
-[Shannovative](http://www.shannovative.com/interactive/h5p-example-activities)

H5P's werden ähnlich wie YouTube Video in den Blog eingebettet – soll eine H5P wiederverwendet werden, muss diese zunächst über den Herunterladen-Button am unteren Rand des eingebunden Inhalts gespeichert werden.

![Dieses Bild zeigt den Download-Button einer H5P Präsentation Sie finden die gezeigte Präsentation auf der Seite synlloer.blogs.uni-hamburg.de/material ](wordpress-h5p-download.png)

Eine heruntergelade H5P-Datei (Dateiendung .h5p) kann auf dem [Test-Account](http://test4.blogs.uni-hamburg.de/) im `side-menu`unter dem Reiter `H5P-Inhalt > Neue Hinzufügen Button > Inhaltstyp auswählen > Hochladen` hinzugefügt werden. Nachdem Upload muss noch ein Titel vergeben und abschließend auf den Button **Erstellen** geklickt werden.

### Übung: H5P Präsentation erstellen

Erstellen Sie mit dem H5P-Plugin in WordPress eine neue *Course Presentation* mit mindestens 3 Folien die folgendes enthalten sollen:

- Titelseite
  - Logo der Universität Hamburg
  - Große Überschrift zur Thematik: **Bienensterben**
  - Unterüberschrift zur Spezifizierung Ihres Beitrags
  - Metadaten zu Ihrer Person
  - Datum
- Folie mit einem gemeinfreien Bild
- Folie mit einem Link zu weiterführenden Informationen

## Nützliche Links

- [WordPress Community offizielle Website](https://wordpress.org/)
- [WordPress Plugins Repository](https://de.wordpress.org/plugins/)
- [WordPress Wiki aka Codex](https://codex.wordpress.org/)
- [WordPress Webhosting](https://de.wordpress.com/)
- [H5P offizielle Website](https://h5p.org/)
- [T3n Magazin – 30 kostenlose WordPress Themes (Responsive)](http://t3n.de/news/kostenlose-wordpress-themes-responsive-webdesign-376838/)
- [Tutorial Learning Management System with WordPress](http://www.wpbeginner.com/plugins/best-wordpress-lms-plugins-compared/)
