In dieser Einführung lernen Sie das Projektmanagement Tool GitLab und deren grundliegende Funktionsweise für den Hochschulkontext kennen. Im Fokus stehen dabei Teamkommmunikation und -Kollaboration in eigenen **Projekten** und wie sie dort in sog. **Issues** Aufgaben erstellen und gemeinsam daran arbeiten können. Darüberhinaus werden Sie mit den Grundeinstellungen für das eigene Profil und den Benachrichtigungstypen vertraut gemacht. Diese helfen Ihnen, andere Projekte im Auge zu behalten und sich selbst über deren Stand Benachrichtigungen automatisch einzuholen.

Diese Veranstaltung ist für zwei Zeitstunden ausgelegt. Die Teilnehmenden sollten nach der Veranstaltung eine weitere Zeitstunde für die Nachbereitung der praktischen Übunungen einplanen.

**Voraussetzungen für diesen Kurs:**

- Internetfähiges Endgerät (vorzugsweise Laptop)

- Abgeschlossene Registrierung ([Link zur UHH-GitLab Registrierung](https://git.universitaetskolleg.uni-hamburg.de/users/sign_in))

- Ihre B-Kennung und das zugehörige GitLab Passwort (Erstellung bei der Registrierung)

**Unterlagen für diesen Workshop:**

- [Link zur Präsentation](https://synlloer.blogs.uni-hamburg.de/einfuehrung-teamkommunikation-und-kollaboration-in-gitlab/)
- [Link zum Veranstaltungsskript](https://git.universitaetskolleg.uni-hamburg.de/Manfred/workshops-manfred/blob/master/gitlab-introduction/script/gitlab-script-introduction.md)

**Dieser Workshop wird an folgenden Daten angeboten:**

- [Donnerstag, 02.11.17 von 10-12 Uhr](https://synlloer.blogs.uni-hamburg.de/veranstaltung/team-kommunikation-und-kollaboration-mit-gitlab-20171102/)
- [Donnerstag, 30.11.17 von 10-12 Uhr](https://synlloer.blogs.uni-hamburg.de/veranstaltung/team-kommunikation-und-kollaboration-mit-gitlab-20171130/)

**Mindestteilnehmendenzahl: 2 Personen:**

Um [Voranmeldung über das Formular](https://synlloer.blogs.uni-hamburg.de/anmeldung-synlloer-hochschul-workshops/) wird gebeten.