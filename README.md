## Termine
`Offene Bildungswerkstatt im OpenLab jeden Donnerstag von 14–18 Uhr in der Schlüterstraße 51, 20146 4. Stock, Raum 4019. Erwünscht sind alle Fragen und Personen, die im Bildungssektor unterwegs sind.`

`Einführung, Teamkommunikation und -Kollaboration in GitLab für Hamburg Open Science (HOS) `: 8.3.18 (13-17 Uhr) ([Link zum Skript](https://gitlab.com/flavoursofopenscience/openlab/blob/master/workshops/gitlab-introduction/script/gitlab-script-introduction.md))

<!-- Anmeldung für die Hochschul-Workshops: [Link zum SynLLOER-Blog Veranstaltungskalender](https://synlloer.blogs.uni-hamburg.de/info-workshops-hochschule/)

`Einführung, Teamkommunikation und -Kollaboration in GitLab`: 2.11.17, 30.11.17, 13.12.17 (10-12 Uhr) ([Link zum Skript](gitlab-introduction/script/gitlab-script-introduction.md))

`Projekmanagement mit GitLab`: 14.12.17 (10-12 Uhr)

`GitBook – Einführung in das inkrementelle Schreiben mit Markdown`: 7.12.17 (10-13 Uhr)

`WordPress: Erste Schritte ins System`: 16.11.17, 12.12.17 (10-13 Uhr)

`OER-Erstellung und Bereitstellung mit WordPress`: 6.12.17 (10-13 Uhr)

`Markdown: Eine Formatvorlage für alle Textverarbeitungsprogramme`: 30.11.17, 7.12.17, 14.12.17, 11.1.18, 18.1.18, 25.1.18, 1.2.18, 8.2.18, 22.2.18, 1.3.18 (15–16 Uhr)

## Workshop Zyklus
- [x] GitLab Workshops
  - [x] Info (2 Wochen Zyklus)
  - [x] Projektmanagement (4 Wochen Zyklus)
- [x] GitBook Workshop (4 Wochen Zyklus)
- [x] WordPress Workshops
  - [x] WordPress Info (6 Wochen Zyklus)
  - [x] WordPress Hands-On OER Fokus H5P/Plugins (6 Wochen Zyklus)
- [x] Markdown (1 Wochen Zyklus)

-->

## Workshop Unterlagen

Sie finden den aktuellen Entwicklungsstand der Workshop Materialien auf dem SynLLOER-Blog unter der Rubrik [Materialien](https://openlab.blogs.uni-hamburg.de/materialien/).

## Anstehende Aufgaben

- [x] [OpenLab Blog](https://openlab.blogs.uni-hamburg.de/) erstellen
- [x] [OER Toolchains auf den OpenLab Blog zur Verfügung stellen](#40)
- [x] [OER Toolchains ausarbeiten und auf dem Blog bereitstellen](https://openlab.blogs.uni-hamburg.de/)
  - [x] Interaktive Arbeitsblätter erstellen und anreichern
  - [x] Präsentationen inklusiv(er) darstellen
  - [x] Vom Tweet zum OER Arbeitsblatt in 30 Minuten
  - [x] Digitaler Zeitstrahl im OER-Format
  - [x] Kein Platz für die Posterausstellung?
  - [x] Schulhefte lebt wohl, digitales Lerntagebuch Ahoi!
  - [x] Der gemeinschaftliche Text leicht gemacht
  - [x] Get stuff done – die kollaborative ToDo-Liste
  - [x] Kein passendes Schaubild für den Unterricht?
  - [x] Freie Ausmalbilder im Handumdrehen selbst erstellen

## Lizenz

[![Creative Commons Lizenzvertrag](https://i.creativecommons.org/l/by/4.0/88x31.png)
](http://creativecommons.org/licenses/by/4.0/)

Diese Dokumentation und alle Inhalte des OpenLab Repos „<span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">Workshops OpenLab</span>“ von [Manfred Steger für SynLLOER]() ist lizenziert unter einer [CC BY 4.0 International Lizenz](http://creativecommons.org/licenses/by/4.0/).
