#Pixelchen

####*Barcamp*
<img width="200px" src="https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Barcamp_F.png" title="" alt=""></a>

Dateien zum Download: &#8594; [png-farbig](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Barcamp_F.png)  &#8594; [png-schwarz/weiß](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Barcamp.png)  &#8594; [svg-farbig](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/svg/Barcamp_F-01.svg) &#8594; [svg-schwarz/weiß](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/svg/Barcamp-01.svg)


####*Barrierefreiheit*
<img width="200px" src="https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Barrierefreiheit_F.png" title="" alt=""></a>

Dateien zum Download: &#8594; [png-farbig](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Barrierefreiheit_F.png)  &#8594; [png-schwarz/weiß](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Barrierefreiheit.png)  &#8594; [svg-farbig](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/svg/Barrierefreiheit_F-01.svg) &#8594; [svg-schwarz/weiß](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/svg/Barrierefreiheit-01.svg)


####*Beratung*
<img width="200px" src="https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Beratung_F.png" title="" alt=""></a>

Dateien zum Download: &#8594; [png-farbig](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Beratung_F.png)  &#8594; [png-schwarz/weiß](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Beratung.png)  &#8594; [svg-farbig](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/svg/Beratung_F-01.svg) &#8594; [svg-schwarz/weiß](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/svg/Beratung-01.svg)

####*Blockseminar*
<img width="200px" src="https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Blockseminar_F.png" title="" alt=""></a>

Dateien zum Download: &#8594; [png-farbig](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Blockseminar_F.png)  &#8594; [png-schwarz/weiß](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Blockseminar.png)  &#8594; [svg-farbig](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/svg/Blockseminar_F-01.svg) &#8594; [svg-schwarz/weiß](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/svg/Blockseminar-01.svg)

####*Blog*
<img width="200px" src="https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Blog_F.png" title="" alt=""></a>

Dateien zum Download: &#8594; [png-farbig](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Blog_F.png)  &#8594; [png-schwarz/weiß](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Blog.png)  &#8594; [svg-farbig](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/svg/Blog_F-01.svg) &#8594; [svg-schwarz/weiß](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/svg/Blog-01.svg)

####*Brückentag*

<img width="200px" src="https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Brueckentag_F.png" title="" alt=""></a>

Dateien zum Download: &#8594; [png-farbig](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Brueckentag_F.png)  &#8594; [png-schwarz/weiß](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Brueckentag.png)  &#8594; [svg-farbig](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/svg/Brueckentag_F-01.svg) &#8594; [svg-schwarz/weiß](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/svg/Brueckentag-01.svg)

####*Burnout*

<img width="200px" src="https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Burnout_F.png" title="" alt=""></a>

Dateien zum Download: &#8594; [png-farbig](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Burnout_F.png)  &#8594; [png-schwarz/weiß](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Burnout.png)  &#8594; [svg-farbig](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/svg/Burnout_F-01.svg) &#8594; [svg-schwarz/weiß](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/svg/Burnout-01.svg)

####*Digitale-Dateiablage*

<img width="200px" src="https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Digitale-Dateiablage_F.png" title="" alt=""></a>

Dateien zum Download: &#8594; [png-farbig](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Digitale-Dateiablage_F.png)  &#8594; [png-schwarz/weiß](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/png/Digitale-Dateiablage.png)  &#8594; [svg-farbig](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/svg/Digitale-Dateiablage_F-01.svg) &#8594; [svg-schwarz/weiß](https://gitlab.com/flavoursofopenscience/openlab/raw/master/pixelchen/svg/Digitale-Dateiablage-01.svg)
